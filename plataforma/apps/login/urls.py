from django.urls import path, include

from apps.cobros.views import *
from apps.login.views import *

# app_name = 'login'

urlpatterns = [
    path('', Login.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]
