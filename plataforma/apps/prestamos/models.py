import os

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone as tz

# Create your models here.
from apps.prestamos.choices import tipo_cuenta_dic
from apps.ventas.models import PreAprobado
from apps.cobros.choices import *


class BancoCliente(models.Model):
    nombre_banco = models.CharField(_('nombre banco'), max_length=40)
    telefono = models.CharField(_('telefono'), max_length=10, null=True, blank=True)

    def __str__(self):
        return self.nombre_banco


class Aprobado(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=True, blank=True, default=tz.now)  #
    fecha_ingreso = models.DateTimeField(auto_now_add=True)  #
    cliente = models.ForeignKey(PreAprobado, on_delete=models.CASCADE, null=False, blank=False, related_name='aprobado')  #
    monto = models.CharField(_('monto'), max_length=10, null=False, blank=False)  #
    plazo = models.CharField(_('plazo'), max_length=5, null=False, blank=False)  #
    fecha_ini = models.CharField(_('fecha inicio'), max_length=20, null=False, blank=False)  #
    f_contrato = models.DateField(_('f contrato'), null=True, blank=True)  #
    ob_contrato = models.CharField(_('ob contrato'), null=True, blank=True, max_length=20)  #
    c_cierre = models.CharField(_('c cierre'), max_length=10, null=True, blank=True)  #
    compra_saldo = models.CharField(_('compra saldo'), max_length=100, null=True, blank=True)  #
    c_promotor = models.CharField(_('c promotor'), max_length=10, null=True, blank=True)  #
    s_descuento = models.CharField(_('s descuento'), max_length=10, null=True, blank=True)  #
    estatus = models.CharField(_('estatus'), max_length=30, null=False, blank=False)  #
    aviso_cliente = models.CharField(_('aviso cliente'), max_length=10, null=True, blank=True)  # Sólo aprobaciones
    orden_descuento = models.FileField(_('orden descuento'), upload_to='Aprobados/orden_descuento/%Y/%m/%d', null=True,
                                       blank=True)  #
    contrato = models.FileField(_('contrato'), upload_to='Aprobados/contrato/%Y/%m/%d', null=True, blank=True)  #
    pagare = models.FileField(_('pagare'), upload_to='Aprobados/pagare/%Y/%m/%d', null=True, blank=True)  #
    conoce_cliente = models.FileField(_('conoce cliente'), upload_to='Aprobados/conoce_cliente/%Y/%m/%d', null=True,
                                      blank=True)  #
    f_1 = models.FileField(_('f 1'), upload_to='Aprobados/F_1/%Y/%m/%d', null=True, blank=True)  #
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)  #
    notas_user = models.CharField(_('notas'), max_length=1000, null=True, blank=True)  #
    num_contrato = models.CharField(_('num contrato'), max_length=20, null=True, blank=True)  #
    fecha_ini_OD = models.DateField(_('f inicio OD'), null=True, blank=True)  # Fecha acordada con planilla
    # Operaciones: None, pend_desembolso, reg_desembolso, desembolsado
    operaciones = models.CharField(_('operaciones'), max_length=30, null=True, blank=True)
    email_conf = models.FileField(_('email conf'), upload_to='Aprobados/email_conf/%Y/%m/%d', null=True, blank=True)  #

    def __str__(self):
        return str(self.cliente)

    def od_name(self):
        return os.path.basename(self.orden_descuento.name)

    def f1_name(self):
        return os.path.basename(self.f_1.name)

    def contrato_name(self):
        return os.path.basename(self.contrato.name)

    def pagare_name(self):
        return os.path.basename(self.pagare.name)

    def c_cliente_name(self):
        return os.path.basename(self.conoce_cliente.name)

    def email_conf_name(self):
        return os.path.basename(self.email_conf.name)

#  Estatus: 'Sin O.D.', 'O.D. recibida y verificada', 'Documentación firmada', 'Desembolsado'

class InfoBancariaCliente(models.Model):
    cliente = models.ForeignKey(Aprobado, on_delete=models.CASCADE)
    nombre_cuenta = models.CharField(_('nombre cuenta'), max_length=100)
    numero_cuenta = models.CharField(_('numero cuenta'), max_length=50)
    banco = models.ForeignKey(BancoCliente, null=True, blank=True, on_delete=models.SET_NULL)
    tipo_cuenta = models.CharField(_('tipo cuenta'), max_length=20, choices=tipo_cuenta_dic)
    notas = models.CharField(_('notas'), max_length=1500, null=True, blank=True)

    def __str__(self):
        return self.cliente
