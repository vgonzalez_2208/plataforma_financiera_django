from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView

from apps.formulario.models import Formulario


def ViewTest(request):
    data = {
        'title': 'Listado de Categorías',
    }
    return render(request, '1_solicitudes_rev.html', data)







