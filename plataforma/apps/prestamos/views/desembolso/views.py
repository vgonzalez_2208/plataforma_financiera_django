from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.views.generic import TemplateView, DetailView

from apps.cobros.forms import FormDesembolso
from apps.cobros.models import Desembolso
from apps.prestamos.forms import CuentaBancoCliente
from apps.prestamos.views.aprobaciones.functions import *
from apps.ventas.views.cotizador.views import desglose_credito
from apps.ventas.views.solicitudes.views_nuevas import check_perms


class ListaDesembolsosPendientes(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):  # active_lista_desembolsos
    template_name = 'prestamos/desembolso/lista_pendientes.html'

    def has_permission(self):
        perm = ('Gerencia', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request):
        data = {}
        try:
            post = request.POST
            action = post['action']
            print(action)
            if action == 'cargar_tabla':
                data = []
                consulta = Aprobado.objects.filter(estatus='env_desembolso').exclude(num_contrato__isnull=True)
                data = lista_desembolsos_p(consulta, data)
            else:
                data['error'] = 'No ha ingresado a ninguna opción.'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super(ListaDesembolsosPendientes, self).get_context_data(**kwargs)
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Desembolsos Pendientes'
        context['active_prestamos'] = 'active'
        context['active_lista_desembolsos'] = 'active'

        return context


class RegistrarDesembolso(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Aprobado
    template_name = 'prestamos/desembolso/registrar_desembolso.html'

    def has_permission(self):
        perm = ('Gerencia', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, *args, **kwargs):
        modelo = self.get_object()
        data = {}
        try:
            post = request.POST
            files = request.FILES
            action = post['action']
            if action == 'registrar_desembolso':
                if len(files) != 0:
                    files['comprobante'].name = 'ACH_' + str(modelo.cliente.dni) + '.pdf'
                    detalles = desglose_credito(modelo, 'si')
                    print(detalles[8])
                    # data = aplicar_desembolso(modelo, files['comprobante'], detalles[8])
            elif action == 'aplicar_desembolso':
                # data['advertencia'] = 'Hay errores en el crédito.'
                check = check_desembolso_gerencia(modelo)
                consulta = Desembolso.objects.filter(cliente=modelo).exists()
                if check[0] and not consulta:
                    # reg_desembolso
                    modelo.operaciones = 'reg_desembolso'
                    modelo.estatus = 'reg_desembolso'
                    modelo.save()
                    data['ok'] = ''
                else:
                    if consulta: data['advertencia'] = 'Ya existe un desembolso para este cliente.'
                    else: data['advertencia'] = check[1]
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super(RegistrarDesembolso, self).get_context_data(**kwargs)
        modelo = self.get_object()
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Desembolso de crédito: '
        context['monto'] = '$ ' + f'{float(modelo.monto):,.2f}'
        if modelo.compra_saldo and modelo.compra_saldo != '0':
            lista = modelo.compra_saldo.split(',')
            contador = 1
            for i in lista:
                print(i.split(':'))
                context['c_saldo_' + str(contador)] = i.split(':')[0]
                saldo = i.split(':')[1]
                context['c_saldo_m_' + str(contador)] = '$ ' + f'{float(saldo):,.2f}'
                contador += 1
        detalles = desglose_credito(modelo, 'si')
        context['itbms'] = '$ ' + f'{float(detalles[13]):,.2f}'
        context['desembolso'] = '$ ' + f'{float(detalles[14]):,.2f}'
        nombres = modelo.cliente.nombre_1
        if modelo.cliente.nombre_2:
            nombres += ' ' + modelo.cliente.nombre_2
        apellidos = modelo.cliente.apellido_1
        if modelo.cliente.apellido_2:
            apellidos += ' ' + modelo.cliente.apellido_2
        context['nombres'] = nombres
        context['apellidos'] = apellidos
        consulta = InfoBancariaCliente.objects.filter(cliente=modelo.id).first()
        if consulta:
            context['info_banco'] = consulta
        else:
            context['info_banco'] = False
        context['form_desembolso'] = FormDesembolso
        return context


class ListaOperaciones(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permisos = ('cobros.change_desembolso', 'cobros.view_desembolso', 'prestamos.change_aprobado', 'prestamos.view_aprobado')
    permission_required = permisos
    template_name = 'prestamos/operaciones/lista_operaciones.html'

    def has_permission(self):
        perm = ('Administracion', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, **kwargs):
        data = {}
        post = request.POST
        action = post['action']
        if action == 'cargar_tabla':
            data = []
            consulta = Aprobado.objects.exclude(operaciones=None).exclude(operaciones='Desembolsado')
            data = lista_desembolsos_p(consulta, data, True)
        print(post)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Operaciones en proceso'
        context['active_prestamos'] = 'active'
        context['active_lista_operaciones'] = 'active'
        return context


class Operaciones(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permisos = ('cobros.change_desembolso', 'cobros.view_desembolso', 'prestamos.change_aprobado', 'prestamos.view_aprobado')
    permission_required = permisos
    template_name = 'prestamos/operaciones/ver_operaciones.html'
    model = Aprobado

    def has_permission(self):
        perm = ('Administracion', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, **kwargs):
        data = {}
        self.object = self.get_object()
        modelo = self.object
        post = request.POST
        action = post['action']
        # print(action)
        if action == 'info_banco_cliente':
            consulta = InfoBancariaCliente.objects.filter(cliente=post['id_cliente']).first()
            if consulta:
                data['Si'] = ''
                data['nombre_cuenta'] = consulta.nombre_cuenta
                data['numero_cuenta'] = consulta.numero_cuenta
                data['banco'] = str(consulta.banco_id)
                data['tipo_cuenta'] = consulta.tipo_cuenta
                if consulta.notas:
                    data['notas'] = consulta.notas
                else:
                    data['notas'] = ''
            else:
                data['No'] = ''
            # print('Verificar info del banco')
        elif action == 'load_info_banco':
            # print(post)
            data = guardar_info_banco(post, modelo.id)
            # print('Cargar info del banco')
        elif action == 'update_info_banco':
            data = update_info_banco(post, modelo.id)
            # print('Actualizar info del banco')
        elif action == 'cargar_email_conf':
            files = request.FILES
            files['email_conf'].name = 'Email_conf_' + str(modelo.cliente.dni) + '.pdf'
            if modelo.email_conf: modelo.email_conf.delete()
            modelo.email_conf = files['email_conf']
            modelo.save()
        elif action == 'enviar_gerencia':
            if modelo.operaciones == 'env_desembolso':
                data['advertencia'] = 'Ya ha sido enviado a Gerencia.'
            else:
                check = check_desembolso_gerencia(modelo)
                if check[0]:
                    # Operaciones: None, pend_desembolso, env_desembolso, reg_desembolso, desembolsado
                    modelo.operaciones = 'env_desembolso'
                    modelo.estatus = 'env_desembolso'
                    data['ok'] = 'Cliente listo para desembolso.'
                    modelo.save()
                else:
                    data['advertencia'] = check[1]
        elif action == 'cancelar_env_gerencia':
            modelo.operaciones = 'pend_desembolso'
            modelo.estatus = 'O.D. recibida y verificada'
            modelo.save()
            data['ok'] = 'Revertido el envío a gerencia.'
        elif action == 'gen_desembolso':
            files = request.FILES
            if modelo.operaciones == modelo.estatus == 'reg_desembolso':
                if files.get('comprobante'):
                    user = request.user.username
                    # print(user)
                    files['comprobante'].name = 'ACH_' + str(modelo.cliente.dni) + '.pdf'
                    detalles = desglose_credito(modelo, 'si')
                    # print(detalles[8])
                    consulta = Desembolso.objects.filter(cliente=modelo).exists()
                    if not consulta:
                        data = aplicar_desembolso(modelo, files['comprobante'], detalles[8], post, user)
                        modelo.estatus = 'Desembolsado'
                        modelo.operaciones = 'Desembolsado'
                        modelo.save()
                        data['ok'] = ''
                    else: data['advertencia'] = 'Ya existe un desembolso para este cliente.'
                else:
                    data['error'] = 'No se ha cargado el comprobante ACH.'
            else: data['advertencia'] = 'Ha ocurrido un error. Consulte a IT.'
            # print(data)
        else:
            data['error'] = 'No ha ingresado a ninguna opción.'
        # print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.object = self.get_object()
        modelo = self.object
        context['active_prestamos'] = 'active'
        context['active_lista_operaciones'] = 'active'
        context['info_cliente'] = self.object
        context['section_name'] = 'Préstamos'
        context[
            'card_title'] = 'Pre-desembolso: ' + '<span class="text-warning">' + self.object.num_contrato + '</span>'
        context['salario'] = '$ ' + f'{self.object.cliente.salario:,.2f}'
        consulta = InfoBancariaCliente.objects.filter(cliente=self.object)
        if consulta:
            context['info_banco_flag'] = True
            context['info_banco'] = consulta[0]
            context['action_banco'] = 'update_info_banco'
        else:
            context['info_banco_flag'] = False
            context['action_banco'] = 'load_info_banco'
        context['form_cuenta'] = CuentaBancoCliente
        if modelo.compra_saldo and modelo.compra_saldo != '0':
            lista = modelo.compra_saldo.split(',')
            contador = 1
            for i in lista:
                # print(i.split(':'))
                context['c_saldo_' + str(contador)] = i.split(':')[0]
                saldo = i.split(':')[1]
                context['c_saldo_m_' + str(contador)] = '$ ' + f'{float(saldo):,.2f}'
                contador += 1
        detalles = desglose_credito(modelo, 'si')
        context['itbms'] = '$ ' + f'{float(detalles[13]):,.2f}'
        context['desembolso'] = '$ ' + f'{float(detalles[14]):,.2f}'
        context['monto'] = '$ ' + f'{float(modelo.monto):,.2f}'
        if modelo.email_conf:
            context['email_conf_flag'] = True
        else:
            context['email_conf_flag'] = False
        if str(modelo.operaciones) == 'pend_desembolso':
            context['estatus_d'] = 'Pendiente de desembolso'
            context['flag_desembolso'] = False
        elif str(modelo.operaciones) == 'env_desembolso':
            context['estatus_d'] = 'Enviado a Gerencia'
            context['flag_desembolso'] = False
        elif str(modelo.operaciones) == 'reg_desembolso':
            context['estatus_d'] = 'Desembolso pendiente de registro.'
            context['flag_desembolso'] = True
        return context


def aplicar_desembolso(cliente, file_pdf, ob_total, post, user='pass'):
    consulta_1 = Desembolso.objects.filter(cliente=cliente).exists()
    # print('pass')
    consulta_2 = Desembolso.objects.filter(cliente_ant__num_contrato=cliente.num_contrato).exists()
    if consulta_1 or consulta_2:
        return {'advertencia': 'Ya existe un desembolso para el contrato: ' + cliente.num_contrato}
    else:
        cliente.estatus = 'Desembolsado'
        cliente.ob_contrato = ob_total
        modelo = Desembolso()
        modelo.cliente = cliente
        modelo.comprobante = file_pdf
        modelo.creado_por = user
        modelo.f_desembolso = post['f_desembolso']
        modelo.save()
        cliente.save()
    return {'ok': 'Desembolsado'}


#  Estatus: 'Sin O.D.', 'Sin F_1', 'O.D./F1 recibidos', 'Documentos firmados', 'Desembolsado', 'Anulado'

def check_desembolso(modelo):
    salida = True
    info = []
    if not modelo.monto:
        salida = False
        info.append('Monto del crédito')
    if not modelo.plazo:
        salida = False
        info.append('Plazo del crédito')
    if not modelo.fecha_ini:
        salida = False
        info.append('Fecha de inicio del crédito')
    if not modelo.f_contrato:
        salida = False
        info.append('Fecha de firma de contrato')
    if not modelo.orden_descuento:
        salida = False
    if modelo.estatus != 'reg_desembolso':
        salida = False
    if not modelo.num_contrato:
        salida = False
    return [salida, info]

