import base64
from datetime import datetime, timedelta, date

from django.core.files.base import ContentFile

from apps.prestamos.models import Aprobado, InfoBancariaCliente
from apps.ventas.models import CotizacionPreAprobado
from apps.ventas.views.solicitudes.views_proceso import check_documentos


def crear_lista(aprobado, user, data):
    if user == 'general':
        i = aprobado.cliente
        documentos = check_documentos(i)
    else:
        i = aprobado
        documentos = check_documentos(i)
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        't_ingreso': str(i.tipo_ingreso),
        'ingreso': '$ ' + f'{int(i.salario):,.2f}',
    }
    if user == 'cumplimiento':
        valor['estatus'] = str(i.check_cumplimiento)
        # valor['nacionalidad'] = 'i.nacionalidad'
        valor['documentos'] = documentos
        valor['genero'] = str(i.genero)
    elif user == 'gerencia':
        valor['cumplimiento'] = str(i.check_cumplimiento)
        valor['documentos'] = documentos
        valor['genero'] = str(i.genero)
    elif user == 'general':
        valor['documentos'] = documentos
        valor['estatus'] = str(aprobado.estatus)
        valor['genero'] = str(i.genero)
    data.append(valor)
    return data


def lista_aprobados(aprobado):
    i = aprobado.cliente
    documentos = check_documentos(i)
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        't_ingreso': str(i.tipo_ingreso),
        'ingreso': '$ ' + f'{int(i.salario):,.2f}',
        'documentos': documentos,
        'estatus': str(aprobado.estatus),
        'genero': str(i.genero)
    }
    if aprobado.num_contrato: valor['num_contrato'] = aprobado.num_contrato
    else: valor['num_contrato'] = 'No tiene'
    return valor


def fecha_inicio():
    fecha = datetime.now()
    if fecha.day <= 5:
        f_pago = datetime(fecha.year, fecha.month, 15)
    elif 5 < fecha.day <= 20:
        if fecha.month != 2:
            f_pago = datetime(fecha.year, fecha.month, 30)
        else:
            f_pago = datetime(fecha.year, fecha.month + 1, 1) - timedelta(days=1)
    elif fecha.day > 20:
        if fecha.month != 12:
            f_pago = datetime(fecha.year, fecha.month + 1, 15)
        else:
            f_pago = datetime(fecha.year + 1, 1, 15)
    if f_pago.month == 12:
        f_pago = datetime(fecha.year + 1, 1, 15)
    salida = str(f_pago.year) + '/' + str(f_pago.month) + '/' + str(f_pago.day)
    return salida


def gen_aprobacion(modelo, cambio, cotizacion):
    aprobado = Aprobado()
    aprobado.id = modelo.id
    aprobado.cliente = modelo
    aprobado.monto = cotizacion.monto
    aprobado.plazo = cotizacion.plazo
    aprobado.fecha_ini = cotizacion.fecha_ini
    aprobado.s_descuento = cotizacion.s_descuento
    aprobado.c_promotor = cotizacion.c_promotor
    aprobado.compra_saldo = cotizacion.compra_saldo
    if str(modelo.tipo_ingreso) == 'Empresa privada':
        aprobado.estatus = 'Sin O.D.'
    elif str(modelo.tipo_ingreso) == 'Empresa pública':
        aprobado.estatus = 'Sin F1'
    if cambio == '0':
        aprobado.aviso_cliente = '0'
    elif cambio == '1':
        aprobado.aviso_cliente = '1'
    aprobado.save()


def cancelaciones(modelo):
    salida = 0
    if modelo:
        if modelo.isdigit():
            salida = float(modelo)
        else:
            for val in modelo.split(','):
                salida += float(val.split(':')[1])
    salida = round(salida, 2)
    return salida


def guardar_imagen(post, tag, modelo):
    img_file = post
    formato, img_str = img_file.split(';base64,')
    # print("format", formato)
    ext = formato.split('/')[-1]
    imagen = ContentFile(base64.b64decode(img_str), name=tag + modelo.cliente.dni + '.' + ext)
    return imagen


def guardar_docs(post, files, modelo, data, form, username, od_only=False):
    if modelo.num_contrato in (None, '') and not od_only:
        return {'advertencia': 'No se ha asignado un número de contrato.'}
    if modelo.notas:
        modelo.notas += '(' + str(date.today()) + '>' + str(username) + ') ' + str(
            post['comentario']) + '\n'
    else:
        modelo.notas = '(' + str(date.today()) + '>' + str(username) + ') ' + str(
            post['comentario']) + '\n'
    if post.get('image_1'):
        print('imagen')
        if str(modelo.cliente.tipo_ingreso) == 'Empresa privada':
            imagen = guardar_imagen(post['image_1'], 'OD_', modelo)
            if modelo.orden_descuento:
                modelo.orden_descuento.delete()
                modelo.orden_descuento = imagen
            else:
                modelo.orden_descuento = imagen
            modelo.fecha_ini_OD = post['fecha_ini_OD']
            modelo.save()
        elif str(modelo.cliente.tipo_ingreso) == 'Empresa pública':
            imagen = guardar_imagen(post['image_1'], 'F1_', modelo)
            if modelo.f_1:
                modelo.f_1.delete()
                modelo.f_1 = imagen
            else:
                modelo.f_1 = imagen
            modelo.fecha_ini_OD = post['fecha_ini_OD']
            modelo.save()
    if files:
        if form.is_valid():
            pre_save = form.save(commit=False)
            if files.get('orden_descuento'):
                pre_save.orden_descuento.name = 'OD_' + str(modelo.cliente.dni) + '.pdf'
            if files.get('f_1'):
                pre_save.f_1.name = 'F1_' + str(modelo.cliente.dni) + '.pdf'
            if files.get('contrato'):
                pre_save.contrato.name = 'CONTRATO_' + str(modelo.cliente.dni) + '.pdf'
            if files.get('pagare'):
                pre_save.pagare.name = 'PAGARÉ_' + str(modelo.cliente.dni) + '.pdf'
            pre_save.save()
            modelo.save()
    if post.get('fecha_ini_OD'):
        modelo.fecha_ini_OD = post['fecha_ini_OD']
        modelo.save()
    data['ok'] = ''
    return data


def generar_cotizacion(modelo_cliente):
    cotizacion = CotizacionPreAprobado()
    cotizacion.id = modelo_cliente.id
    cotizacion.cliente = modelo_cliente  # datos = procesar_modelos(self, request.POST, Cotizaciones())
    cotizacion.nombre = modelo_cliente.nombre_1 + ' ' + modelo_cliente.apellido_1
    cotizacion.monto = modelo_cliente.monto
    cotizacion.plazo = modelo_cliente.plazo_meses
    cotizacion.fecha_ini = fecha_inicio()
    cotizacion.compra_saldo = '0'
    cotizacion.c_promotor = '0'
    cotizacion.s_descuento = '0'
    cotizacion.save()


def lista_desembolsos_p(consulta, data, flag=False):
    for i in consulta:
        valor = {
            'contrato': i.num_contrato,
            'nombre': i.cliente.nombre_1,
            'apellido': i.cliente.apellido_1,
            'tipo_ingreso': str(i.cliente.tipo_ingreso),
            'monto': i.monto,
            'plazo': i.plazo + ' m.',
            'id_cliente': i.id
        }
        if flag:
            valor['estatus'] = i.operaciones
        else:
            valor['estatus'] = i.estatus
        data.append(valor)
    return data


def guardar_info_banco(post, cliente_id):
    post_2 = {}
    for i in post.items():
        if i[0] in ('action', 'csrfmiddlewaretoken'):
            pass
        elif i[1].isdigit() and i[0] in ('banco', 'cliente'):
            post_2[i[0] + '_id'] = int(i[1])
        else:
            post_2[i[0]] = i[1]
    post_2['cliente_id'] = int(cliente_id)
    modelo = InfoBancariaCliente(**post_2)
    modelo.save()
    return {'Ok': 'Se guardó la info. del cliente.'}


def update_info_banco(post, cliente_id):
    modelo = InfoBancariaCliente.objects.get(cliente=cliente_id)
    modelo.nombre_cuenta = post['nombre_cuenta']
    modelo.numero_cuenta = post['numero_cuenta']
    modelo.banco_id = int(post['banco'])
    modelo.tipo_cuenta = post['tipo_cuenta']
    if post.get('notas'):
        modelo.notas = post['notas']
    modelo.save()
    return {'Ok_2': 'Se actualizó la información bancaria.'}


def update_info_cliente(modelo, post, username):
    if post.get('comentario'):
        if modelo.notas:
            modelo.notas += '(' + str(date.today()) + '>' + str(username) + ') ' + str(
                post['comentario']) + '\n'
        else:
            modelo.notas = '(' + str(date.today()) + '>' + str(username) + ') ' + str(
                post['comentario']) + '\n'
    else: return {'advertencia': 'Debe ingresar un comentario.'}
    modelo.cliente.nombre_1 = str(post['m_nombre_1'])
    modelo.cliente.nombre_2 = str(post['m_nombre_2'])
    modelo.cliente.apellido_1 = str(post['m_ap_1'])
    modelo.cliente.apellido_2 = str(post['m_ap_2'])
    modelo.cliente.dni = str(post['m_cip'])
    modelo.cliente.empresa_1 = str(post['m_empresa'])
    if post['empresa_2'] == '':
        modelo.cliente.empresa_2_id = None
    else:
        modelo.cliente.empresa_2_id = str(post['empresa_2'])
    modelo.cliente.email_1 = str(post['m_email'])
    modelo.cliente.save()
    modelo.save()
    return {'ok': ''}


def check_operaciones(modelo):
    salida = True
    info = []
    if not modelo.num_contrato:
        salida = False
        info.append('Número de contrato')
    if not modelo.orden_descuento:
        if not modelo.f_1:
            salida = False
            info.append('Copia aprobada de O.D./F1')
    if not modelo.fecha_ini_OD:
        salida = False
        info.append('Fecha de inicio O.D.')
    if not modelo.cliente.empresa_2:
        salida = False
        info.append('Registro de empresa')
    else:
        if not modelo.cliente.empresa_2.verificacion:
            salida = False
            info.append('Verificar Empresa: ' + str(modelo.cliente.empresa_2.nombre_corto()))
    if not modelo.f_contrato:
        salida = False
        info.append('Fecha de contrato')
    return [salida, info]


def check_desembolso_gerencia(modelo):
    salida = True
    info = []
    if not modelo.email_conf:
        salida = False
        info.append('Email de confirmación')
    if not modelo.orden_descuento:
        if not modelo.f_1:
            salida = False
            info.append('Copia aprobada de O.D./F1')
    consulta = InfoBancariaCliente.objects.filter(cliente=modelo).exists()
    if not consulta:
        salida = False
        info.append('Info. bancaria del cliente')
    if not modelo.cliente.empresa_2:
        salida = False
        info.append('Registro de empresa')
    if not modelo.fecha_ini_OD:
        salida = False
        info.append('Fecha coordinada con planilla')
    return [salida, info]


def html_salida(val, clases=None):
    if clases:
        salida = '<b class="' + clases + '">' + val + '</b>'
    else: salida = '<b>' + val + '</b>'
    return salida


def info_cliente_ap(modelo):
    salida = {
        'nombre_1': modelo.cliente.nombre_1,
        'nombre_2': modelo.cliente.nombre_2,
        'ap_1': modelo.cliente.apellido_1,
        'ap_2': modelo.cliente.apellido_2,
        'cip': modelo.cliente.dni,
        'empresa': modelo.cliente.empresa_1,
        'email_1': modelo.cliente.email_1,
    }
    if modelo.cliente.empresa_2:
        salida['empresa_2'] = modelo.cliente.empresa_2_id
    else:
        salida['empresa_2'] = ''
    return salida


def asignar_ct_ap(modelo, action):
    data = {}
    if not modelo.num_contrato:
        consulta = Aprobado.objects.filter(num_contrato__isnull=False).exclude(num_contrato='').order_by('num_contrato').values_list('num_contrato').last()
        if action == 'asignar_ct_1':
            print(consulta)
            mes_act = datetime.today().strftime('%Y-%m')
            print(mes_act)
            ult_ct = consulta[0].split('-')
            mes_ult_ct = ult_ct[0] + '-' + ult_ct[1]
            print(ult_ct[0] + '-' + ult_ct[1])
            if mes_act == mes_ult_ct:
                modelo.num_contrato = data['ct_asignado'] = mes_act + '-' + consecutivo_ct(ult_ct[2])
                modelo.save()
            else:
                val_1 = html_salida(consulta[0], 'text-primary')
                val_2 = html_salida(meses[int(ult_ct[1])], 'text-danger')
                val_3 = html_salida(meses[int(ult_ct[1]) + 1], 'text-success')
                data['advertencia'] = 'El último contrato es: ' + val_1
                data['advertencia'] += ' y corresponde al mes de ' + val_2
                data['advertencia'] += '. ¿Desea cerrar este mes e iniciar ' + val_3 + '?'
        elif action == 'asignar_ct_2':
            ult_ct = consulta[0].split('-')
            mes_act = datetime.today().strftime('%Y-%m')
            modelo.num_contrato = data['ct_asignado'] = mes_act + '-' + consecutivo_ct(ult_ct[2])
            modelo.save()
        elif action == 'asignar_ct_3':
            ult_ct = consulta[0].split('-')
            modelo.num_contrato = data['ct_asignado'] = ult_ct[0] + '-' + ult_ct[1] + '-' + consecutivo_ct(ult_ct[2])
            modelo.save()
    else:
        data['stop'] = 'Este cliente ya tiene # de contrato.'
    return data


def consecutivo_ct(num):
    val = int(num) + 1
    if val < 1000: salida = '0' + str(val)
    else: salida = str(val)
    return salida


meses = {
    1: 'enero',
    2: 'febrero',
    3: 'marzo',
    4: 'abril',
    5: 'mayo',
    6: 'junio',
    7: 'julio',
    8: 'agosto',
    9: 'septiembre',
    10: 'octubre',
    11: 'noviembre',
    12: 'diciembre',
    13: 'enero',
}


def check_db_client():
    print('check_db iuiu')
    return 'prestamos.change_aprobado', 'prestamos.view_aprobado'
