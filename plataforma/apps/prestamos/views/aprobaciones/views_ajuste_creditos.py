from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import TemplateView, UpdateView

from apps.cobros.models import Desembolso, ClienteAntiguo
from apps.prestamos.forms import FormAjustes1, FormAjustes2, FormCrearDesembolso, FormRegistroEmpresa
from apps.prestamos.models import Aprobado
from apps.ventas.models import PreAprobado
from apps.ventas.views.cotizador.views import desglose_credito


class ListaAprobacionesContratos(LoginRequiredMixin, TemplateView):
    template_name = 'prestamos/ajustes_rc/lista_aprobados.html'

    @staticmethod
    def post(request):
        data = {'ok': 'ok'}
        post = request.POST
        action = post['action']
        if action == 'aprobados':
            data = []
            consulta_2 = Aprobado.objects.all()
            for obj in consulta_2:
                valor = {
                    'id': str(obj.id),
                    'nombre': str(obj.cliente),
                    'estatus': str(obj.estatus),
                    'monto': str(obj.monto),
                    'plazo': str(obj.plazo),
                    'opciones': '',
                }
                if obj.num_contrato: valor['num_contrato'] = str(obj.num_contrato)
                else: valor['num_contrato'] = '--Sin num. contrato--'
                data.append(valor)
            print('pre aprobados')
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        context['card_title_1'] = 'Ajustes RC Aprobados'
        context['active_prestamos'] = 'active'
        context['active_prestamos_aprobacion'] = 'active'
        return context


class VerAprobados(LoginRequiredMixin, UpdateView):
    template_name = 'prestamos/ajustes_rc/detalles_cliente.html'

    def get_form_class(self, **kwargs):
        if self.kwargs['tipo'] == 'pre_aprobado':
            form_class = FormAjustes1
        else:
            form_class = FormAjustes2
        return form_class

    def get_object(self, **kwargs):
        if self.kwargs['tipo'] == 'pre_aprobado':
            salida = PreAprobado.objects.get(id=self.kwargs['pk'])
        else:
            salida = Aprobado.objects.get(id=self.kwargs['pk'])
        return salida

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        if self.kwargs['tipo'] == 'pre_aprobado':
            context['card_title'] = 'Ajustes RC Pre-Aprobados'
        else:
            context['card_title'] = 'Ajustes RC Aprobados'
        context['active_prestamos'] = 'active'
        context['active_prestamos_aprobacion'] = 'active'
        return context


class CrearDesembolso(LoginRequiredMixin, UpdateView):
    model = Aprobado
    template_name = 'prestamos/ajustes_rc/crear_desembolso.html'
    form_class = FormCrearDesembolso

    def post(self, request, **kwargs):
        self.object = self.get_object()
        modelo = self.object
        # print(self.object.cliente)
        data = {}
        post = request.POST
        print(post)
        action = post['action']
        if action == 'desembolso':
            form = self.get_form()
            check = post_check(post)
            if check:
                if form.errors:
                    print(form.errors.as_data())
                elif form.is_valid():
                    form.save()
                    detalles = desglose_credito(modelo, 'si')
                    print(detalles[8])
                    crear_desembolso(modelo, detalles[8])
                    data['ok'] = ''
        elif action == 'guardar_datos':
            form = self.get_form()
            if form.errors:
                print(form.errors.as_data())
            elif form.is_valid():
                form.save()
                data['ok'] = ''
        elif action == 'registrar_empresa':
            # print(post['empresa_2'])
            modelo.cliente.empresa_2_id = post['empresa_2']
            modelo.cliente.save()
            data['ok'] = ''
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.cliente.empresa_2:
            print('empresa reg')
            context['registro_empresa'] = True
        else:
            context['registro_empresa'] = False
        context['form_empresa'] = FormRegistroEmpresa
        return context


def post_check(post):
    value = True
    if not post.get('monto'): value = False
    if not post.get('plazo'): value = False
    if not post.get('fecha_ini'): value = False
    if not post.get('f_contrato'): value = False
    if not post.get('fecha_ini_OD'): value = False
    if not post.get('ob_contrato'): value = False
    if not post.get('num_contrato'): value = False
    return value


def crear_desembolso(modelo, ob_total):
    salida = ''
    consulta_1 = Desembolso.objects.filter(cliente=modelo).exists()
    consulta_2 = Desembolso.objects.filter(cliente__num_contrato=modelo.num_contrato).exists()
    consulta_3 = ClienteAntiguo.objects.filter(num_contrato=modelo.num_contrato).exists()
    if consulta_1: salida += 'Ya existe un desembolso con este cliente. '
    if consulta_2: salida += 'Ya existe un desembolso con este número de contrato. '
    if consulta_3: salida += 'Ya existe un cliente pre-plataforma con este número de contrato. '
    if salida != '': return {'advertencia': salida}
    else:
        modelo.estatus = 'Desembolsado'
        modelo.ob_contrato = ob_total
        desembolso = Desembolso()
        desembolso.cliente = modelo
        desembolso.save()
        modelo.save()
    return {'ok': ''}
