import calendar
import locale
import os
from datetime import datetime
from math import floor

import numpy_financial as npf
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from num2words import num2words
from weasyprint import HTML, CSS

from apps.prestamos.models import Aprobado
from apps.prestamos.views.aprobaciones.cotizador_views import compra_saldo
from apps.ventas.views.cotizador.views import desglose_credito


class OrdenDescuento(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = Aprobado.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo, 'si')
        saludo = ''
        nombre = ''
        if modelo.cliente.nombre_1:
            nombre += modelo.cliente.nombre_1
        if modelo.cliente.nombre_2:
            nombre += ' ' + modelo.cliente.nombre_2
        if modelo.cliente.apellido_1:
            nombre += ' ' + modelo.cliente.apellido_1
        if modelo.cliente.apellido_2:
            nombre += ' ' + modelo.cliente.apellido_2
        monto_word = convert_to_word(detalles_p[8], 'od')
        num_quincenas = int(modelo.plazo) * 2
        quincena_r_word = convert_to_word(detalles_p[15], 'od')
        letra_u = round(detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1), 2)
        quincena_u_word = convert_to_word(letra_u, 'od')
        fecha_descuento = inicio_descuento(detalles_p[17])
        try:
            if modelo.num_contrato:
                template = get_template('prestamos/documentos/orden_descuento_blanco.html')
                print('Hay contrato')
            else:
                template = get_template('prestamos/documentos/orden_descuento.html')
                print('No hay contrato')
            context = {
                'fecha_enc': fecha_encabezado,
                'saludo': saludo,
                'nombre': nombre,
                'dni': modelo.cliente.dni,
                'monto_word': monto_word,
                'monto_p': f'{float(detalles_p[8]):,.2f}',
                'regular_q': str(num_quincenas - 1),
                'cuota_word': quincena_r_word,
                'cuota_u_word': quincena_u_word,
                'inicio_descuento': fecha_descuento,
            }
            if modelo.cliente.empresa_2:
                context['empresa'] = modelo.cliente.empresa_1
            else:
                context['empresa'] = 'No hay empresa registrada.'
            if str(modelo.cliente.tipo_doc) == 'Pasaporte':
                context['documento'] = 'pasaporte'
            elif str(modelo.cliente.tipo_doc) == 'Cédula':
                context['documento'] = 'cédula de identificación'
            if modelo.compra_saldo != '0':
                c_saldos = modelo.compra_saldo.split(',')
                val_i = len(c_saldos)
                for i in range(val_i):
                    temp = c_saldos[i].split(':')
                    context['c_saldo_' + str(i + 1)] = temp[0] + ':'
                    context['c_sub_' + str(i + 1)] = '$ ' + f'{float(temp[1]):,.2f}'
            else:
                pass
                # context['c_saldo_1'] = '--Ninguno--'
            if modelo.num_contrato:
                context['n_contrato'] = str(modelo.num_contrato)
            else:
                pass
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))


class ContratoCliente(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = Aprobado.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo, 'si')
        nombre = ''
        if modelo.cliente.nombre_1:
            nombre += modelo.cliente.nombre_1
        if modelo.cliente.nombre_2:
            nombre += ' ' + modelo.cliente.nombre_2
        if modelo.cliente.apellido_1:
            nombre += ' ' + modelo.cliente.apellido_1
        if modelo.cliente.apellido_2:
            nombre += ' ' + modelo.cliente.apellido_2
        cliente = modelo.cliente
        direccion = str(cliente.provincia) + ', ' + str(cliente.distrito) + ', ' + str(
            cliente.corregimiento) + ', ' + str(cliente.direccion)
        monto_word = convert_to_word(detalles_p[8], 'od')
        num_quincenas = int(modelo.plazo) * 2
        letra_u = round(detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1), 2)
        fecha_descuento = inicio_descuento(detalles_p[17])
        ##
        g_inicial = (-1) * (
                detalles_p[0] + detalles_p[7] + detalles_p[13] + detalles_p[11] + detalles_p[9] + detalles_p[10])
        flujos = []
        if int(modelo.plazo) < 12:
            print('Menor que 12')
            f_year = round(detalles_p[15] * int(modelo.plazo) * 2, 2)
            flujos = [
                g_inicial, f_year
            ]
        elif int(modelo.plazo) < 23:
            print('Menor que 23')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * (int(modelo.plazo) - 11) * 2, 2)
            flujos = [
                g_inicial, f_year, s_year
            ]
        elif int(modelo.plazo) < 34:
            print('Menor que 34')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * 22, 2)
            t_year = round(detalles_p[8] - f_year - s_year, 2)
            flujos = [
                g_inicial, f_year, s_year, t_year
            ]
        # print(p_val, s_val, t_val)
        #  Monto solicitado, notaría, ITBMS, timbres, c. promotor, s. descuento, otros.
        tir = round(npf.irr(flujos) * 100, 2)
        ##
        seguros = detalles_p[4] + detalles_p[5] + detalles_p[6]
        monto_ob = detalles_p[8] - seguros - detalles_p[7]
        cuota_regular = convert_to_word(detalles_p[15], 'od')
        letra_u = detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1)
        cuota_final = convert_to_word(letra_u, 'od')
        c_manejo = detalles_p[19] + '%'
        if str(modelo.cliente.tipo_doc) == 'Cédula':
            tag_doc = 'CIP:'
        else:
            tag_doc = 'PP:'
        try:
            if modelo.num_contrato:
                template = get_template('prestamos/documentos/contrato.html')
            else:
                template = get_template('prestamos/documentos/contrato.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'nombre': nombre,
                'nacionalidad': str(modelo.cliente.nacionalidad),
                'dni': modelo.cliente.dni,
                'direccion': direccion,
                'correo': modelo.cliente.email_1,
                'celular': modelo.cliente.celular,
                'monto_word': monto_word,  # Duplicado
                # 'plazo': modelo.plazo,
                'plazo': str(detalles_p[3]),
                'tir': tir,
                'intereses': f'{float(detalles_p[1]):,.2f}',
                'c_manejo': f'{float(detalles_p[2]):,.2f}',
                'seguros': f'{seguros:,.2f}',
                'monto_ob': f'{monto_ob:,.2f}',
                'timbres': f'{float(detalles_p[11]):,.2f}',
                's_descuento': f'{float(detalles_p[10]):,.2f}',
                'otros': f'{float(detalles_p[9]):,.2f}',
                'admin': f'{float(detalles_p[12]):,.2f}',
                'n_entregar': f'{float(detalles_p[0]):,.2f}',
                'itbms': f'{float(detalles_p[13]):,.2f}',
                'desembolso': f'{float(detalles_p[14]):,.2f}',
                'cuotas_regulares': str(int(modelo.plazo) * 2 - 1),
                'cuota_1': cuota_regular,
                'cuota_2': cuota_final,
                'fecha_inicio': fecha_ct(detalles_p[17]),
                'monto_pg': convert_to_word(detalles_p[8], 'od'),
                'tag_doc': tag_doc,
                'p_manejo': c_manejo,

                'monto_p': f'{float(detalles_p[8]):,.2f}',
                'regular_q': str(num_quincenas - 1),
                'inicio_descuento': fecha_descuento,
                'empresa': modelo.cliente.empresa_1,
                'n_contrato': modelo.num_contrato,
            }
            if modelo.f_contrato:
                context['fecha_firma'] = fecha_ct(modelo.f_contrato)
            else:
                context['fecha_firma'] = fecha_ct(datetime.now().date())
            context.update(compra_saldo(modelo.compra_saldo))
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))


class PagareCliente(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = Aprobado.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo, 'si')
        nombre = ''
        if modelo.cliente.nombre_1:
            nombre += modelo.cliente.nombre_1
        if modelo.cliente.nombre_2:
            nombre += ' ' + modelo.cliente.nombre_2
        if modelo.cliente.apellido_1:
            nombre += ' ' + modelo.cliente.apellido_1
        if modelo.cliente.apellido_2:
            nombre += ' ' + modelo.cliente.apellido_2
        cliente = modelo.cliente
        direccion = str(cliente.provincia) + ', ' + str(cliente.distrito) + ', ' + str(
            cliente.corregimiento) + ', ' + str(cliente.direccion)
        monto_word = convert_to_word(detalles_p[8], 'pg')
        num_quincenas = int(modelo.plazo) * 2
        letra_u = round(detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1), 2)
        fecha_descuento = inicio_descuento(detalles_p[17])
        ##
        g_inicial = (-1) * (
                detalles_p[0] + detalles_p[7] + detalles_p[13] + detalles_p[11] + detalles_p[9] + detalles_p[10])
        flujos = []
        if int(modelo.plazo) < 12:
            print('Menor que 12')
            f_year = round(detalles_p[15] * int(modelo.plazo) * 2, 2)
            flujos = [
                g_inicial, f_year
            ]
        elif int(modelo.plazo) < 23:
            print('Menor que 23')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * (int(modelo.plazo) - 11) * 2, 2)
            flujos = [
                g_inicial, f_year, s_year
            ]
        elif int(modelo.plazo) < 34:
            print('Menor que 34')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * 22, 2)
            t_year = round(detalles_p[8] - f_year - s_year, 2)
            flujos = [
                g_inicial, f_year, s_year, t_year
            ]
        # print(p_val, s_val, t_val)
        #  Monto solicitado, notaría, ITBMS, timbres, c. promotor, s. descuento, otros.
        tir = round(npf.irr(flujos), 4) * 100
        ##
        seguros = detalles_p[4] + detalles_p[5] + detalles_p[6]
        monto_ob = detalles_p[8] - seguros - detalles_p[7]
        cuota_regular = convert_to_word(detalles_p[15], 'od')
        letra_u = detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1)
        cuota_final = convert_to_word(letra_u, 'od')
        if str(modelo.cliente.tipo_doc) == 'Cédula':
            dni = 'CÉDULA: ' + str(modelo.cliente.dni)
        else:
            dni = 'PASAPORTE: ' + str(modelo.cliente.dni)

        if str(modelo.cliente.tipo_doc) == 'Cédula':
            tag_doc = 'CIP:'
        else:
            tag_doc = 'PP:'
        try:
            if modelo.num_contrato:
                template = get_template('prestamos/documentos/pagare.html')
                print('Hay contrato')
            else:
                template = get_template('prestamos/documentos/pagare.html')
                print('No hay contrato')
            context = {
                'monto_pg': monto_word,
                'plazo': modelo.plazo,
                'nombre': nombre,
                'cuotas_regulares': str(int(modelo.plazo) * 2 - 1),
                'cuota_1': cuota_regular,
                'cuota_2': cuota_final,
                'fecha_inicio': fecha_ct(detalles_p[17]),
                'dni': dni,
                'n_contrato': modelo.num_contrato,

                'direccion': direccion,
                'correo': modelo.cliente.email_1,
                'celular': modelo.cliente.celular,
                'monto_word': monto_word,

                'tir': tir,
            }
            if modelo.f_contrato:
                context['fecha_firma'] = fecha_ct(modelo.f_contrato)
            else:
                context['fecha_firma'] = fecha_ct(datetime.now().date())
            context.update(compra_saldo(modelo.compra_saldo))
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))


def convert_to_word(num, doc):
    num_1 = floor(num)
    num_2 = int(round((num - num_1) * 100, 2))
    if num_2 == 0:
        num_2 = '00'
    elif num_2 < 10:
        num_2 = '0' + str(num_2)
    else:
        num_2 = str(num_2)
    salida = ''
    if doc == 'od':
        salida = num2words(num_1, lang='es') + ' BALBOAS CON ' + num_2 + '/100 (B/. ' + f'{float(num):,.2f}' + ')'
    elif doc == 'pg':
        salida = 'B/. ' + f'{float(num):,.2f}' + ' (' + num2words(num_1, lang='es') + ' BALBOAS CON ' + str(
            num_2) + '/100)'
    return salida


def inicio_descuento(fecha):
    # locale.setlocale(locale.LC_ALL, ("es_ES", "UTF-8")) TODO Ajuste funcional en VPS
    locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
    salida = ''
    if fecha.day == 15:
        salida += 'PRIMERA quincena de '
        salida += calendar.month_name[fecha.month]
        salida += ' de ' + str(fecha.year)
    elif fecha.day > 15:
        salida += 'SEGUNDA quincena de '
        salida += calendar.month_name[fecha.month]
        salida += ' de ' + str(fecha.year)
    return salida


def fecha_ct(fecha):
    # locale.setlocale(locale.LC_ALL, ("es_ES", "UTF-8")) TODO Ajuste funcional en VPS
    locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
    salida = ''
    salida += str(fecha.day) + ' de '
    salida += calendar.month_name[fecha.month] + ' del '
    salida += str(fecha.year)
    return salida


def fecha_completa():
    months = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
              "Noviembre", "Diciembre")
    dia_semana = ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
    dia_s = dia_semana[datetime.today().weekday()]
    day = datetime.now().day
    month = months[datetime.now().month - 1]
    year = datetime.now().year
    fecha = "{} de {} de {}".format(day, month, year)
    return fecha
