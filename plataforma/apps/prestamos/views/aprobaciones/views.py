import threading

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.forms import model_to_dict
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, TemplateView

from apps.cobros.models import Desembolso
from apps.prestamos.forms import AprobacionesVer, ClientesAprobadosVer, CuentaBancoCliente, FormRegistroEmpresa
from apps.prestamos.views.aprobaciones.functions import *
from apps.ventas.models import PreAprobado, CotizacionPreAprobado
from apps.ventas.views.solicitudes.views_nuevas import check_perms


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class AprobacionClientes(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = ('prestamos.change_aprobado', 'prestamos.view_aprobado', 'prestamos.add_aprobado')
    model = PreAprobado
    template_name = 'prestamos/aprobacion/2_aprobacion.html'

    def has_permission(self):
        perm = ('Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'cumplimiento':
                consulta = PreAprobado.objects.filter(check_1=1).filter(check_3=1).filter(check_2=0).exclude(
                    check_cumplimiento='true').exclude(check_cumplimiento='Aprobado').exclude(
                    check_cumplimiento='Rechazado').exclude(check_cumplimiento='Revisado')
                data = []
                for i in consulta:
                    data = crear_lista(i, 'cumplimiento', data)
            elif action == 'gerencia':
                consulta = PreAprobado.objects.filter(check_1=1).filter(check_3=1).filter(check_2=0).exclude(
                    check_cumplimiento='Rechazado').exclude(check_gerencia='true').exclude(
                    check_gerencia='Aprobado').exclude(check_gerencia='Rechazado')
                data = []
                for i in consulta:
                    data = crear_lista(i, 'gerencia', data)
            else:
                data['error'] = 'Ha ocurrido un error'

        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        context['card_title_1'] = 'Solicitudes pendientes de comité'
        context['card_title_2'] = 'Solicitudes pendientes de comité'
        context['active_prestamos'] = 'active'
        context['active_prestamos_aprobacion'] = 'active'
        return context


class AprobacionVer(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = ('prestamos.change_aprobado', 'prestamos.view_aprobado', 'prestamos.add_aprobado')
    model = PreAprobado
    template_name = 'prestamos/aprobacion/2_aprobacion_ver.html'
    form_class = AprobacionesVer
    success_url = ''

    def has_permission(self):
        perm = ('Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            post = request.POST
            action = post['action']
            print(action)
            if action == 'aprobar':
                cotizacion = CotizacionPreAprobado.objects.get(pk=self.object.id)
                print(request.user.username)
                if request.user.username == 'guillermo.lemos':
                    # print('sdsd')
                    if cotizacion.monto == post['monto'] and cotizacion.plazo == post[
                        'plazo_meses'] and cotizacion.fecha_ini == post['inicio']:
                        cotizacion.cambio_c = '0'
                    else:
                        cotizacion.cambio_c = '1'
                        cotizacion.monto = post['monto']
                        cotizacion.plazo = post['plazo_meses']
                        cotizacion.fecha_ini = post['inicio']
                    if self.object.check_cumplimiento == 'Devuelto':
                        self.object.check_cumplimiento = 'Revisado'
                    else:
                        self.object.check_cumplimiento = 'Aprobado'
                    if post.get('comentario'):
                        if post['comentario'] not in ('', ' '):
                            if self.object.notas_cumplimiento:
                                self.object.notas_cumplimiento += ' //' + str(date.today()) + '>> ' + str(post['comentario']) + '\n'
                            else:
                                self.object.notas_cumplimiento = ' //' + str(date.today()) + '>> ' + str(post['comentario']) + '\n'
                            self.object.save()
                            cotizacion.save()
                            data['aprobado'] = '<strong class="text-success">Cumplimiento</strong>'
                elif request.user.username == 'gerencia.rc':
                    if str(self.object.check_cumplimiento) == 'false' or str(
                            self.object.check_cumplimiento) == 'Pendiente':
                        data['advertencia'] = 'Falta la aprobación de cumplimiento'
                    else:
                        if cotizacion.monto == post['monto'] and cotizacion.plazo == post[
                            'plazo_meses'] and cotizacion.fecha_ini == post['inicio']:
                            if cotizacion.cambio_c == '0':
                                cambio = '0'
                            else:
                                cambio = '1'
                        else:
                            cambio = '1'
                            cotizacion.monto = post['monto']
                            cotizacion.plazo = post['plazo_meses']
                            cotizacion.fecha_ini = post['inicio']
                        self.object.check_gerencia = 'Aprobado'
                        if post.get('comentario'):
                            if post['comentario'] not in ('', ' '):
                                self.object.notas_gerencia = request.POST['comentario']
                                x = threading.Thread(target=gen_aprobacion, args=(self.object, cambio, cotizacion,))
                                x.start()
                                data['aprobado'] = '<strong class="text-success">Gerencia</strong>'
                                self.object.save()
                                cotizacion.save()
            elif action == 'verificar':
                cotizacion = CotizacionPreAprobado.objects.get(pk=self.object.id)
                f_inicio = cotizacion.fecha_ini.split('/')
                print(f_inicio[0])
                if int(f_inicio[0]) < 100:
                    f_inicio = f_inicio[2] + '/' + f_inicio[1] + '/' + f_inicio[0]
                else:
                    f_inicio = cotizacion.fecha_ini
                print(cotizacion.fecha_ini, f_inicio)
                if cotizacion.monto == post['monto'] and cotizacion.plazo == post['plazo'] and f_inicio == post[
                    'inicio']:
                    data['ok'] = ''
                else:
                    data['cambio'] = ''
                print(data)
            elif action == 'rechazar':
                if request.user.username == 'guillermo.lemos':
                    self.object.check_cumplimiento = 'Rechazado'
                    self.object.notas_cumplimiento = post['comentario']
                    self.object.save()
                    data['rechazado'] = '<strong class="text-success">Cumplimiento</strong>'
                if request.user.username == 'gerencia.rc':
                    self.object.check_gerencia = 'Rechazado'
                    self.object.notas_gerencia = post['comentario']
                    self.object.save()
                    data['rechazado'] = '<strong class="text-danger">Gerencia</strong>'
            elif action == 'devolver_c':
                if self.object.notas_gerencia:
                    self.object.notas_gerencia += ' //' + str(date.today()) + '>> ' + str(post['comentario']) + '\n'
                else:
                    self.object.notas_gerencia = ' //' + str(date.today()) + '>> ' + str(post['comentario']) + '\n'
                print(self.object.notas_gerencia)
                self.object.check_cumplimiento = 'Devuelto'
                self.object.save()
                data['devuelto'] = '<b class="text-warning">Cumplimiento</b>'
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        try:
            if CotizacionPreAprobado.objects.get(pk=self.object.id):
                cotizacion = CotizacionPreAprobado.objects.get(pk=self.object.id)
                context['c_monto'] = cotizacion.monto
                context['monto'] = '$ ' + f'{float(cotizacion.monto):,.2f}'
                context['c_plazo'] = cotizacion.plazo
                fecha = str(cotizacion.fecha_ini).split('/')
                if int(fecha[2]) > 1000:
                    context['c_inicio'] = fecha[2] + '/' + fecha[1] + '/' + fecha[0]
                else:
                    context['c_inicio'] = str(cotizacion.fecha_ini)
                context['c_promotor'] = cotizacion.c_promotor
                context['c_s_descuento'] = cotizacion.s_descuento
                context['c_saldo'] = f'{cancelaciones(cotizacion.compra_saldo):,.2f}'
        except Exception as e:
            generar_cotizacion(self.object)
            context['error'] = str(e)
            if CotizacionPreAprobado.objects.get(pk=self.object.id):
                cotizacion = CotizacionPreAprobado.objects.get(pk=self.object.id)
                context['c_monto'] = cotizacion.monto
                context['monto'] = '$ ' + f'{float(cotizacion.monto):,.2f}'
                context['c_plazo'] = cotizacion.plazo
                fecha = str(cotizacion.fecha_ini).split('/')
                if int(fecha[2]) > 1000:
                    context['c_inicio'] = fecha[2] + '/' + fecha[1] + '/' + fecha[0]
                else:
                    context['c_inicio'] = str(cotizacion.fecha_ini)
                context['c_promotor'] = cotizacion.c_promotor
                context['c_s_descuento'] = cotizacion.s_descuento
                context['c_saldo'] = f'{cancelaciones(cotizacion.compra_saldo):,.2f}'
        if str(self.object.tipo_ingreso) == 'Empresa pública':
            context['p_manejo'] = str(4)
        elif str(self.object.tipo_ingreso) == 'Empresa privada':
            context['p_manejo'] = str(5)
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Solicitud pendiente de comité # '
        context['num_solicitud'] = str(self.get_object().id)
        context['salario'] = '$ ' + f'{int(self.get_object().salario):,.2f}'
        context['f_pago'] = fecha_inicio()
        context['active_prestamos'] = 'active'
        context['active_prestamos_aprobacion'] = 'active'
        return context


class ListaAprobados(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = ('prestamos.change_aprobado', 'prestamos.view_aprobado')
    template_name = 'prestamos/aprobacion/lista_aprobados.html'

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'lista_aprobados':
                usuario = request.user.username
                lista_user = ('guillermo.lemos', 'vicente_rc', 'gerencia.rc')
                estatus = ('Anulado', 'Desembolsado')
                if usuario in lista_user:
                    consulta_1 = Aprobado.objects.exclude(estatus__in=estatus)
                else:
                    consulta_1 = Aprobado.objects.filter(estatus__in=('Sin F1', 'Sin O.D.'))
                consulta_2 = Desembolso.objects.filter(cliente__isnull=False).values_list('cliente_id', flat=True)
                filtro = list(filter(lambda x: x.id not in consulta_2, consulta_1))
                data = list(map(lista_aprobados, filtro))
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
            print(str(e))
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Lista de solicitudes aprobadas'
        context['active_prestamos'] = 'active'
        context['active_prestamos_lista_aprobados'] = 'active'
        return context


class AprobadosVer(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = check_db_client()
    model = Aprobado
    template_name = 'prestamos/aprobacion/aprobados_ver.html'
    form_class = ClientesAprobadosVer
    success_url = ''

    def has_permission(self):
        self.object = self.get_object()
        if self.object.num_contrato in (None, ''):
            perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        else:
            perm = ('Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        modelo = self.object
        try:
            post = request.POST
            # post = json.loads(request.body)
            # print(post)
            action = post['action']
            print(action)
            if action == 'cotizacion_cliente':
                modelo.aviso_cliente = '2'
                modelo.estatus = 'Sin O.D.'
                modelo.save()
                data['ok'] = ''
            elif action == 'devolver_comité':
                print(request.POST['comentario'])
                modelo.cliente.notas += ' //' + str(date.today()) + '>> ' + post['comentario']
                modelo.cliente.check_cumplimiento = 'Pendiente'
                modelo.cliente.check_gerencia = 'Pendiente'
                modelo.cliente.save()
                modelo.delete()
                data['ok'] = ''
            elif action == 'anular':
                modelo.estatus = 'Anulado'
                modelo.notas = ' //' + str(date.today()) + '>> ' + post['comentario']
                modelo.save()
                data['ok'] = ''
            elif action == 'cargar_od':
                files = request.FILES
                form = self.get_form()
                username = request.user.username
                if modelo.cliente.empresa_2:
                    if post.get('fecha_ini_OD'):
                        data = guardar_docs(post, files, modelo, data, form, username, True)
                        modelo.estatus = 'O.D./F1 recibidos'
                        modelo.save()
                    else: data['error'] = 'Falta la fecha acordada con planilla de la orden de descuento.'
                else: data['error'] = 'El cliente no ha sido registrado en ninguna empresa.'
            elif action == 'info_cliente':
                # Respuesta a la solicitud post de info de cliente
                data = info_cliente_ap(modelo)
            elif action == 'update_info':
                # Se actualiza la información editada del cliente a nombre del usuario editor
                username = request.user.username
                data = update_info_cliente(modelo, post, username)
            elif action == 'cargar_docs':
                files = request.FILES
                form = self.get_form()
                username = request.user.username
                data = guardar_docs(post, files, modelo, data, form, username)
                if files.get('orden_descuento') or files.get('f_1') or post.get('image_1'):
                    modelo.estatus = 'O.D./F1 recibidos'
                    if files.get('contrato') and files.get('pagare'):
                        modelo.estatus = 'Documentos firmados'
                if str(modelo.estatus) == 'O.D./F1 recibidos' and files.get('contrato') and files.get('pagare'):
                    modelo.estatus = 'Documentos firmados'
                if (modelo.orden_descuento or modelo.f_1) and modelo.contrato and files.get('pagare'):
                    modelo.estatus = 'Documentos firmados'
                elif (modelo.orden_descuento or modelo.f_1) and modelo.pagare and files.get('contrato'):
                    modelo.estatus = 'Documentos firmados'
                if modelo.num_contrato not in (None, ''):
                    modelo.save()
            elif action == 'pr_cargar':
                data['pr_monto'] = str(modelo.monto)
                data['pr_plazo'] = str(modelo.plazo)
                data['pr_inicio'] = str(modelo.fecha_ini)
                if modelo.c_cierre:
                    data['pr_c_cierre'] = str(modelo.c_cierre)
                else:
                    if str(modelo.cliente.tipo_ingreso) == 'Empresa pública':
                        data['pr_c_cierre'] = '40'
                    else:
                        data['pr_c_cierre'] = '50'
                if modelo.compra_saldo:
                    data['pr_compra_saldo'] = str(modelo.compra_saldo)
                else:
                    data['pr_compra_saldo'] = '0'
                if modelo.c_promotor:
                    data['pr_c_promotor'] = str(modelo.c_promotor)
                else:
                    data['pr_c_promotor'] = '0'
                if modelo.s_descuento:
                    data['pr_s_descuento'] = str(modelo.s_descuento)
                else:
                    data['pr_s_descuento'] = '0'
                if modelo.f_contrato:
                    data['pr_f_contrato'] = str(modelo.f_contrato)
                else:
                    data['pr_f_contrato'] = ''
            elif action == 'pr_update':
                print(post)
                modelo.monto = str(post['pr_monto'])
                modelo.plazo = str(post['pr_plazo'])
                modelo.fecha_ini = str(post['pr_inicio'])
                modelo.c_cierre = str(post['pr_c_cierre'])
                modelo.compra_saldo = str(post['pr_compra_saldo'])
                modelo.c_promotor = str(post['pr_c_promotor'])
                modelo.s_descuento = str(post['pr_s_descuento'])
                if post['pr_f_contrato'] == '':
                    modelo.f_contrato = None
                else: modelo.f_contrato = str(post['pr_f_contrato'])
                if modelo.notas:
                    modelo.notas += '\n //' + str(date.today()) + '|' + request.user.username + '>> ' + post[
                        'comentario']
                else:
                    modelo.notas = ' //' + str(date.today()) + '|' + request.user.username + '>> ' + post[
                        'comentario']
                modelo.save()
                data['ok'] = ''
                print(post)
            elif action == 'info_banco_cliente':
                consulta = InfoBancariaCliente.objects.filter(cliente=post['id_cliente']).first()
                if consulta:
                    data['Si'] = ''
                    data['nombre_cuenta'] = consulta.nombre_cuenta
                    data['numero_cuenta'] = consulta.numero_cuenta
                    data['banco'] = str(consulta.banco_id)
                    data['tipo_cuenta'] = consulta.tipo_cuenta
                    if consulta.notas:
                        data['notas'] = consulta.notas
                    else:
                        data['notas'] = ''
                else:
                    data['No'] = ''
                print('Verificar info del banco')
            elif action == 'load_info_banco':
                print(post)
                data = guardar_info_banco(post, modelo.id)
                print('Cargar info del banco')
            elif action == 'update_info_banco':
                data = update_info_banco(post, modelo.id)
                print('Actualizar info del banco')
            elif action == 'check_fecha_contrato':
                if modelo.f_contrato:
                    if modelo.num_contrato:
                        data['ok'] = ''
                    else:
                        data['warning_1'] = 'Hace falta asignar número de contrato.'
                else:
                    if modelo.num_contrato:
                        data['warning_2'] = 'Hace falta asignar la fecha del contrato.'
                    else:
                        data['warning_3'] = 'Hace falta asignar número de contrato y fecha del contrato.'
            elif action == 'verificar_empresa_od':
                if modelo.cliente.empresa_2:
                    data['ok'] = ''
                else: data['advertencia'] = str(modelo.cliente.empresa_1)
            elif action == 'enviar_operaciones':
                check_op = check_operaciones(modelo)
                if check_op[0]:
                    if modelo.operaciones:
                        data['advertencia'] = 'El cliente ya fue enviado a operaciones.'
                    else:
                        # Operaciones: None, pend_desembolso, env_desembolso, reg_desembolso, desembolsado
                        modelo.operaciones = 'pend_desembolso'
                        modelo.save()
                        data['ok'] = ''
                else:
                    data['stop'] = check_op[1]
            elif action == 'cancelar_operaciones':
                modelo.operaciones = None
                modelo.save()
                data['ok'] = ''
            elif action == 'asignar_ct_1':
                # Primera consulta para asignar numero de contrato a cliente
                data = asignar_ct_ap(modelo, action)
            elif action == 'asignar_ct_2':
                # Asignar contrato a cliente usando el mes anterior
                data = asignar_ct_ap(modelo, action)
            elif action == 'asignar_ct_3':
                # Asignar contrato a cliente usando el mes en curso
                data = asignar_ct_ap(modelo, action)
            elif action == 'quitar_ct':
                # Retirar número de contrato a cliente
                modelo.num_contrato = None
                modelo.save()
                data['ct_retirado'] = 'Se retiró el número de contrato.'
            else:
                data['error'] = 'Ha ocurrido un error'
            print(data)
        except Exception as e:
            data['error'] = str(e)
            print(str(e))
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        aprobado = self.get_object()
        context['id_aprobado'] = aprobado.id
        context['c_monto'] = f'{float(aprobado.monto):,.2f}'
        context['monto'] = aprobado.monto
        context['c_plazo'] = aprobado.plazo
        context['c_inicio'] = aprobado.fecha_ini
        context['c_promotor'] = aprobado.c_promotor
        context['c_s_descuento'] = aprobado.s_descuento
        context['c_saldo'] = f'{(cancelaciones(aprobado.compra_saldo)):,.2f}'
        print(context['c_saldo'], 'is here')
        if aprobado.compra_saldo:
            context['saldo'] = cancelaciones(aprobado.compra_saldo)
        else:
            context['saldo'] = '0'
        context['notas'] = str(aprobado.cliente.notas)
        if self.object.notas:
            context['notas_aprobado'] = str(aprobado.notas)
        else:
            context['notas_aprobado'] = '--Sin anotaciones--'
        if str(self.object.cliente.tipo_ingreso) == 'Empresa pública':
            if self.object.c_cierre:
                context['p_manejo'] = str(float(self.object.c_cierre) / 10)
            else:
                context['p_manejo'] = str(4)
            context['t_empresa'] = 'gobierno'
        elif str(self.object.cliente.tipo_ingreso) == 'Empresa privada':
            if self.object.c_cierre:
                context['p_manejo'] = str(float(self.object.c_cierre) / 10)
            else:
                context['p_manejo'] = str(5)
            context['t_empresa'] = 'privada'
        if aprobado.aviso_cliente == '1':
            context['bg'] = 'alert-warning'
            context['alerta'] = 'El crédito aprobado no coincide con la pre-aprobación. ' \
                                'Envíe al cliente una cotización actualizada.'
        elif aprobado.aviso_cliente == '0':
            context['bg'] = 'alert-success'
            context['alerta'] = 'El crédito aprobado coincide con la pre-aprobación.'
        elif aprobado.aviso_cliente == '2':
            context['bg'] = 'alert-success'
            context['alerta'] = 'El cliente aceptó el crédito aprobado.'
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Solicitud aprobada # '
        context['num_solicitud'] = str(self.get_object().id)
        context['salario'] = '$ ' + f'{float(self.get_object().cliente.salario):,.2f}'
        context['f_pago'] = fecha_inicio()
        context['active_prestamos'] = 'active'
        context['active_prestamos_lista_aprobados'] = 'active'
        context['form_cuenta'] = CuentaBancoCliente
        context['aprobado_edit'] = 'aprobado_edit-' + str(self.object.id)
        context['form_empresa'] = FormRegistroEmpresa
        context['dep_ventas'] = ('ariel.reina', 'jeimy.diaz')
        context['permisos'] = self.request.user.groups.values_list('name', flat=True)
        print(self.request.user.is_superuser)
        context['superuser'] = self.request.user.is_superuser
        if aprobado.fecha_ini_OD: context['fecha_ini_OD'] = str(aprobado.fecha_ini_OD)
        else: context['fecha_ini_OD'] = None
        return context

#  Estatus: 'Sin O.D.', 'Sin F_1', 'O.D./F1 recibidos', 'Documentos firmados', 'Desembolsado', 'Anulado'
