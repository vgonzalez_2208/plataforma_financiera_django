from datetime import datetime, date, timedelta

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import View
from weasyprint import HTML, CSS
import numpy_financial as npf

import os
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template

from apps.prestamos.models import Aprobado
from apps.ventas.views.cotizador.views import fecha_completa, desglose_credito


class CotizacionAprobado(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = Aprobado.objects.get(pk=self.kwargs.get('pk'))
        tipo = self.kwargs['tipo']
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16])
        if tipo == 'hoy': fecha_encabezado = fecha_completa()
        else: fecha_encabezado = fecha_completa(modelo.f_contrato)
        detalles_p = desglose_credito(modelo, 'si')
        saludo = ''
        if datetime.now().hour < 12:
            saludo = "Buenos días"
        elif datetime.now().hour < 18:
            saludo = "Buenas tardes"
        else:
            saludo = "Buenas noches"
        try:
            template = get_template('ventas/cotizador/cotizacion_pdf.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'saludo': saludo,
                'nombre': modelo.cliente.nombre_1 + ' ' + modelo.cliente.apellido_1,
                'monto': f'{detalles_p[0]:,.2f}',
                'comision': f'{detalles_p[2]:,.2f}',
                'plazo': modelo.plazo + "/" + str(detalles_p[3]),
                'fecha_ini': modelo.fecha_ini,
                'compra_saldo': f'{detalles_p[18]:,.2f}',
                'desembolso': f'{detalles_p[14]:,.2f}',
                'letra': f'{detalles_p[15]:,.2f}',
                'itbms': f'{detalles_p[13]:,.2f}',
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))


class DesgloseAprobado(LoginRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        modelo = Aprobado.objects.get(pk=self.kwargs.get('pk'))
        tipo = self.kwargs['tipo']
        modelo_solicitud = modelo.cliente
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        if tipo == 'hoy':
            fecha_encabezado = fecha_completa()
        else:
            fecha_encabezado = fecha_completa(modelo.f_contrato)
        detalles_p = desglose_credito(modelo, 'si')
        total_meses = detalles_p[3]
        letra_u = detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1)
        g_inicial = (-1) * (
                detalles_p[0] + detalles_p[7] + detalles_p[13] + detalles_p[11] + detalles_p[9] + detalles_p[10])
        # print(g_inicial)
        flujos = []
        if int(modelo.plazo) < 12:
            print('Menor que 12')
            f_year = round(detalles_p[15] * int(modelo.plazo) * 2, 2)
            flujos = [
                g_inicial, f_year
            ]
        elif int(modelo.plazo) < 23:
            print('Menor que 23')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * (int(modelo.plazo) - 11) * 2, 2)
            flujos = [
                g_inicial, f_year, s_year
            ]
        elif int(modelo.plazo) < 34:
            print('Menor que 34')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * 22, 2)
            t_year = round(detalles_p[8] - f_year - s_year, 2)
            flujos = [
                g_inicial, f_year, s_year, t_year
            ]
        # print(p_val, s_val, t_val)
        #  Monto solicitado, notaría, ITBMS, timbres, c. promotor, s. descuento, otros.
        tir = round(npf.irr(flujos), 4) * 100
        # print(tir)
        seguros_mes = round((detalles_p[4] + detalles_p[5] + detalles_p[6]) / int(modelo.plazo), 2)
        aplica_promotor = ''
        as_descuento = ''
        c_promotor = round(float(modelo.c_promotor), 2)
        if c_promotor > 0:
            aplica_promotor = 'Sí'
        else:
            aplica_promotor = 'No'
        if int(modelo.s_descuento) > 0:
            as_descuento = 'Sí'
        else:
            as_descuento = 'No'
        user = request.user.first_name + ' ' + request.user.last_name
        # print(detalles_p[17])
        c_manejo = detalles_p[19] + '.00 %'
        try:
            template = get_template('ventas/cotizador/detalles_prestamo.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'nombre': modelo_solicitud.nombre_1 + ' ' + modelo_solicitud.apellido_1,
                'dni': str(modelo_solicitud.dni),
                'salario': '$ ' + f'{int(modelo_solicitud.salario):,.2f}',
                't_ingreso': str(modelo_solicitud.tipo_ingreso),
                'c_promotor': f'{c_promotor:,.2f}',
                'c_manejo': c_manejo,
                's_descuento': f'{int(modelo.s_descuento):,.2f}',
                's_vida': '3.50',
                's_desempleo': '2.50',
                's_dental': '1.25',
                'ls_vida': '$ ' + f'{detalles_p[4]:,.2f}',
                'ls_desempleo': '$ ' + f'{detalles_p[5]:,.2f}',
                'ls_dental': '$ ' + f'{detalles_p[6]:,.2f}',
                'total_seguros': '$ ' + f'{(detalles_p[4] + detalles_p[5] + detalles_p[6]):,.2f}',
                'letra_q': '$ ' + f'{detalles_p[15]:,.2f}',
                'letra_m': '$ ' + f'{detalles_p[15] * 2:,.2f}',
                'letra_u': '$ ' + f'{letra_u:,.2f}',
                'tir': f'{tir:,.2f}',
                'seguros_mes': '$ ' + f'{seguros_mes:,.2f}',
                'monto': '$ ' + f'{float(modelo.monto):,.2f}',
                'plazo': modelo.plazo,
                'meses_ad': str(total_meses - int(modelo.plazo)),
                'total_meses': str(total_meses),
                'quincenas': str(int(modelo.plazo) * 2),
                'a_promotor': aplica_promotor,
                'as_descuento': as_descuento,
                'fecha_ini': detalles_p[17].strftime('%d/%m/%Y'),
                'fecha_fin': detalles_p[16].strftime('%d/%m/%Y'),
                'f_desembolso': '--',
                'comision_manejo': '$ ' + f'{detalles_p[2]:,.2f}',
                'intereses': '$ ' + f'{detalles_p[1]:,.2f}',
                'promotor': '$ ' + f'{detalles_p[9]:,.2f}',
                'servicio_desc': '$ ' + f'{detalles_p[10]:,.2f}',
                'notaria': '$ ' + f'{12:,.2f}',
                'timbres': '$ ' + f'{detalles_p[11]:,.2f}',
                'otros_gastos': '$ ' + f'{0:,.2f}',
                'total_prestamo': '$ ' + f'{detalles_p[8]:,.2f}',
                'admin': '$ ' + f'{detalles_p[12]:,.2f}',
                'itbms': '$ ' + f'{detalles_p[13]:,.2f}',
                'desembolso': '$ ' + f'{detalles_p[14]:,.2f}',
                'compra_saldo': '$ ' + f'{detalles_p[18]:,.2f}',
                'user_name': user,
            }
            context.update(compra_saldo(modelo.compra_saldo))
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
        return HttpResponseRedirect(reverse_lazy('ventas:solicitudes_nuevas'))


def compra_saldo(modelo):
    salida = {}
    if modelo.isdigit():
        if float(modelo) > 0:
            salida['c_saldo_1'] = 'Sin definir:'
            salida['c_sub_1'] = '$ ' + f'{float(modelo):,.2f}'
        else:
            salida['c_saldo_1'] = '--Ninguna--'
    else:
        c_saldos = modelo.split(',')
        val_i = len(c_saldos)
        for i in range(val_i):
            temp = c_saldos[i].split(':')
            salida['c_saldo_' + str(i + 1)] = temp[0] + ':'
            salida['c_sub_' + str(i + 1)] = '$ ' + f'{float(temp[1]):,.2f}'
    return salida
