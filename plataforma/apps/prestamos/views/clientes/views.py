from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import ListView

from apps.formulario.models import Formulario


class ListaClientes(LoginRequiredMixin, ListView):
    model = Formulario
    template_name = 'prestamos/1_lista_clientes.html'

    @staticmethod
    def post(request, *args, **kwargs):
        data = {'test': 'Plataforma'}
        print(request.POST)
        # cat = Formulario.objects.get(pk=request.POST['id'])
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Lista de clientes'
        context['active_prestamos'] = 'active'
        context['active_prestamos_clientes'] = 'active'
        return context
