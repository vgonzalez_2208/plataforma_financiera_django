from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView

from apps.formulario.models import Formulario


class CartasSaldo(ListView):
    model = Formulario
    template_name = 'prestamos/3_carta_saldo.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {'test': 'Plataforma'}
        print(request.POST)
        # cat = Formulario.objects.get(pk=request.POST['id'])
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Préstamos'
        context['card_title'] = 'Solicitudes pre-aprobadas'
        context['active_prestamos'] = 'active'
        context['active_prestamos_cartasaldo'] = 'active'
        return context
