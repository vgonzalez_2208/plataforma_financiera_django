from django.urls import path

from apps.cobros.views.cobros.views_pagos import VerClienteCobros
from apps.prestamos.views.aprobaciones.cotizador_views import *
from apps.prestamos.views.aprobaciones.docs_views import *
from apps.prestamos.views.aprobaciones.views import *
from apps.prestamos.views.aprobaciones.views_ajuste_creditos import *
from apps.prestamos.views.clientes.views import ListaClientes
from apps.prestamos.views.desembolso.views import *
from apps.prestamos.views.saldos.views import CartasSaldo
from apps.ventas.views.solicitudes.views_proceso import SolicitudesProcesoEdit

app_name = 'prestamos'

urlpatterns = [
    # Aprobación
    path('aprobacion/', AprobacionClientes.as_view(), name='aprobacion_clientes'),
    path('aprobados/', ListaAprobados.as_view(), name='lista_aprobados'),
    path('aprobacion/ver/<int:pk>/', AprobacionVer.as_view(), name='aprobacion_ver'),
    path('aprobados/ver/<int:pk>/', AprobadosVer.as_view(), name='aprobados_ver'),
    # Desembolsos
    path('aprobados/desembolsos/', ListaDesembolsosPendientes.as_view(), name='lista_desembolsos_p'),
    path('aprobados/desembolsos/<int:pk>/', RegistrarDesembolso.as_view(), name='aprobados_ver'),
    # Lista de clientes
    path('clientes/', ListaClientes.as_view(), name='lista_clientes'),
    # Cartas de saldo
    path('cartasaldo/', CartasSaldo.as_view(), name='cartas_saldo'),
    # path('prestamos/lista_clientes/', FormularioView.as_view(), name='formulario_create'),
    # Documentos
    path('aprobados/cotizacion/<int:pk>/<str:tipo>/', CotizacionAprobado.as_view(), name='aprobado_cotizacion'),
    path('aprobados/detalles/<int:pk>/<str:tipo>/', DesgloseAprobado.as_view(), name='aprobado_desglose'),
    path('aprobados/orden_descuento/<int:pk>/', OrdenDescuento.as_view(), name='orden_descuento'),
    path('aprobados/contrato_cliente/<int:pk>/', ContratoCliente.as_view(), name='contrato_cliente'),
    path('aprobados/pagare_cliente/<int:pk>/', PagareCliente.as_view(), name='pagare_cliente'),
    # Editar info de Aprobado
    path('aprobados/edit/<int:pk>/<str:info>/', SolicitudesProcesoEdit.as_view(), name='edit_info_cliente'),
    # Ajustes Préstamos clientes
    path('aprobados/lista_ajuste/', ListaAprobacionesContratos.as_view(), name='ajustes_aprobaciones'),
    path('aprobados/lista_ajuste/<int:pk>/<str:tipo>/', VerAprobados.as_view(), name='info_cliente'),
    path('aprobados/crear_desembolso/<int:pk>/', CrearDesembolso.as_view(), name='crear_desembolso'),
    # Operaciones
    path('aprobados/lista_operaciones/', ListaOperaciones.as_view(), name='lista_operaciones'),
    path('aprobados/lista_operaciones/<int:pk>/', Operaciones.as_view(), name='operacion_cliente'),
    # Clientes desembolsados activos
    path('aprobados/desembolsados/<int:pk>/<str:tipo>/', VerClienteCobros.as_view(), name='info_desembolsado'),
]
