from apps.cobros.choices import *
from apps.cobros.models import Desembolso
from apps.prestamos.models import Aprobado, InfoBancariaCliente
from apps.ventas.models import PreAprobado, Empresa
from django.forms import *
from django import forms


class AprobacionesVer(ModelForm):
    tipo_doc = TextInput()

    class Meta:
        model = PreAprobado
        fields = '__all__'
        # fields = ['notas', 'check_1', 'check_2', 'check_3']

        widgets = {
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '5',
                    'cols': '150'
                }
            ),
            'check_1': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'ventas();'
                }
            ),
            'check_2': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'descartar();'
                }
            ),
            'check_3': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'cumplimiento();'
                }
            ),
            'monto': NumberInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    'class': 'form-control',
                }
            ),
        }


class ClientesAprobadosVer(ModelForm):
    class Meta:
        model = Aprobado
        fields = ['orden_descuento', 'f_1', 'contrato', 'pagare', 'fecha_ini_OD']
        # fields = ['notas', 'check_1', 'check_2', 'check_3']

        widgets = {
        }


class CuentaBancoCliente(ModelForm):
    class Meta:
        model = InfoBancariaCliente

        fields = '__all__'

        widgets = {
            'nombre_cuenta': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese el nombre de la cuenta',
                    'autocomplete': 'off'
                }
            ),
            'numero_cuenta': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingreso el número de cuenta',
                    'autocomplete': 'off'
                }
            ),
            'banco': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                }
            ),
            'tipo_cuenta': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                }
            ),
            'notas': Textarea(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Escriba sus observaciones aquí',
                    'rows': '2',
                    'cols': '150'
                }
            )
        }


class FormAjustes1(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            # form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    # c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)

    class Meta:
        model = PreAprobado
        fields = '__all__'

        # widgets = {}


class FormAjustes2(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            # form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    # c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)

    class Meta:
        model = Aprobado
        fields = '__all__'

        # widgets = {}


class FormCrearDesembolso(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            if form.name == 'cliente':
                form.field.widget.attrs['class'] = 'form-control select2'
                # form.field.widget.attrs['disabled'] = ''
            else:
                form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    c_promotor = forms.ChoiceField(choices=c_promotor_dic, required=False)
    s_descuento = forms.ChoiceField(choices=s_descuento_dic, required=False)
    c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)
    estatus = forms.ChoiceField(choices=estatus_dic, required=True)

    class Meta:
        model = Aprobado
        # exclude = ('created_date', 'cliente_ant', 'lista_pagos', 'lista_cambios', 'estatus', 'comprobante',)
        fields_1 = ('cliente', 'monto', 'plazo', 'fecha_ini', 'f_contrato', 'fecha_ini_OD', 'ob_contrato')
        fields_2 = ('c_cierre', 'compra_saldo', 'c_promotor', 's_descuento', 'estatus', 'num_contrato', 'creado_el')
        fields = fields_1 + fields_2


class FormRegistroEmpresa(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control select2'
            form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = PreAprobado
        # exclude = ('created_date', 'cliente_ant', 'lista_pagos', 'lista_cambios', 'estatus', 'comprobante',)
        fields = ('empresa_2',)


