# Generated by Django 3.2.3 on 2021-11-08 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prestamos', '0005_auto_20211108_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aprobado',
            name='c_cierre',
            field=models.CharField(blank=True, choices=[(50, '50%'), (45, '45%'), (40, '40%'), (35, '35%')], max_length=10, null=True, verbose_name='c cierre'),
        ),
    ]
