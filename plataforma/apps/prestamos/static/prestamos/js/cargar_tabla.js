var csrftoken = getCookie('csrftoken');

$(document).ready(function () {
    let table = $('#tabla_aprobacion_1').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'gerencia'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "t_ingreso"},
            {"data": "ingreso"},
            {"data": "cumplimiento"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/prestamos/aprobacion/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Completo') {
                        valor = '<span class="badge badge-pill badge-success">' + data + '</span>';
                    } else if (data === 'Incompleto') {
                        valor = '<span class="badge badge-pill badge-warning">' + data + '</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">' + data + '</span>';
                    }
                    return valor;
                },
            },
            {
                targets: [-3],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Aprobado') {
                        valor = '<span class="badge badge-pill badge-success">Aprobado</span>';
                    } else if (data === 'Pendiente' || data === 'false') {
                        valor = '<span class="badge badge-pill badge-warning">Pendiente</span>';
                    } else if (data === 'Devuelto') {
                        valor = '<span class="badge badge-pill badge-warning">Pendiente <i class="fas fa-exclamation-triangle"></i></span>';
                    } else if (data === 'Revisado') {
                        valor = '<span class="badge badge-pill badge-success">Revisado <i class="fas fa-exclamation-triangle"></i></span>';
                    }
                    return valor;
                },
            },

        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {

        }
    });
    $("#update_gerencia").on("click", function () {
        console.log('sdsd');
        // table.button('.buttons-reload').trigger();
        table.ajax.reload();
    });
});

$(document).ready(function () {
    let table = $('#tabla_aprobacion_2').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'cumplimiento'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "t_ingreso"},
            {"data": "ingreso"},
            {"data": "estatus"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/prestamos/aprobacion/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    console.log(data)
                    if (data === 'Completo') {
                        valor = '<span class="badge badge-pill badge-success">' + data + '</span>';
                    } else if (data === 'Incompleto') {
                        valor = '<span class="badge badge-pill badge-warning">' + data + '</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">' + data + '</span>';
                    }
                    return valor;
                },
            },
            {
                targets: [-3],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    console.log(data)
                    if (data === 'Pendiente' || data === 'false') {
                        valor = '<span class="badge badge-pill badge-secondary">' + 'Pendiente' + '</span>';
                    } else if (data === 'Devuelto') {
                        valor = '<span class="badge badge-pill badge-warning">' + data + ' <i class="fas fa-exclamation-triangle"></i></span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">' + data + '</span>';
                    }
                    return valor;
                },
            },
        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {

        }
    });
    $("#update_cumplimiento").on("click", function () {
        console.log('sdsd');
        // table.button('.buttons-reload').trigger();
        table.ajax.reload();
    });
});

$(document).ready(function () {
    var table = $('#lista_aprobados').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'lista_aprobados'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "t_ingreso"},
            {"data": "ingreso"},
            {"data": "documentos"},
            {"data": "estatus"},
            {"data": "num_contrato"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/prestamos/aprobados/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-3],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    valor = '<span class="badge badge-pill badge-warning text-center">' + data + '</span>';
                    return valor;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    if (data === 'No tiene'){
                        return '<i>' + data + '</i>';
                    } else {
                        return '<b class="text-primary">' + data + '</b>';
                    }
                },
            },
            {
                targets: [-4],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Completo') {
                        valor = '<span class="badge badge-pill badge-success text-center">' + data + '</span>';
                    } else if (data === 'Incompleto') {
                        valor = '<span class="badge badge-pill badge-warning text-center">' + data + '</span>';
                    } else if (data === 'Ninguno') {
                        valor = '<span class="badge badge-pill badge-danger text-center">' + data + '</span>';
                    }
                    return valor;
                },
            },
        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {

        }
    });
    $("#updateTable_2").on("click", function () {
        console.log('sdsd');
        // table.button('.buttons-reload').trigger();
        table.ajax.reload();
    });
});