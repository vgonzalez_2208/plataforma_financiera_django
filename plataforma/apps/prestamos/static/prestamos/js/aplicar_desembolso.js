// document.addEventListener('DOMContentLoaded', function (e) {
//     FormValidation.formValidation(document.getElementById('cargar_comprobante'), {
//         fields: {
//             comprobante: {
//                 validators: {
//                     notEmpty: {
//                         message: 'Por favor seleccione el comprobante.',
//                     },
//                     file: {
//                         extension: 'pdf, PDF',
//                         type: 'application/pdf',
//                         maxSize: 512000,
//                         message: 'El archivo pdf no es válido.',
//                     },
//                 },
//             },
//         },
//         plugins: {
//             trigger: new FormValidation.plugins.Trigger(),
//             bootstrap: new FormValidation.plugins.Bootstrap({
//                 rowSelector: function (field, ele) {
//                     // field is the field name
//                     // ele is the field element
//                     switch (field) {
//                         case 'comprobante':
//                             return '.col-12';
//
//                         default:
//                             return '.form-group';
//                     }
//                 }
//             }),
//             submitButton: new FormValidation.plugins.SubmitButton(),
//             icon: new FormValidation.plugins.Icon({
//                 valid: 'fa fa-check',
//                 invalid: 'fa fa-times',
//                 validating: 'fa fa-refresh',
//             }),
//         },
//     }).on('core.form.valid', function (event) {
//         Swal.fire({
//             title: '¿Desea generar el desembolso del crédito?',
//             text: "Una vez aplicados los cambios el crédito pasará a estado activo.",
//             icon: 'warning',
//             showCancelButton: true,
//             confirmButtonColor: '#3085d6',
//             cancelButtonColor: '#d33',
//             confirmButtonText: '¡Sí, continuar!'
//         }).then((result) => {
//             if (result.isConfirmed) {
//                 let parameters = new FormData(document.getElementById("cargar_comprobante"));
//                 parameters.append('action', 'registrar_desembolso');
//                 let url = window.location.pathname;
//
//                 fetch(url, {
//                     method: 'POST', // or 'PUT'
//                     body: parameters,
//                 }).then(res => res.json())
//                     .catch(error => console.error('Error:', error))
//                     .then(function (response) {
//                         if (!response.hasOwnProperty('error')) {
//                             if ('ok' in response) {
//                                 Swal.fire({
//                                     position: 'top-end',
//                                     icon: 'success',
//                                     title: '¡Se ha registrado el desembolso!',
//                                     showConfirmButton: false,
//                                     timer: 1500
//                                 })
//                                 setTimeout(function () {
//                                     location.href = '/prestamos/aprobados/desembolsos/';
//                                 }, 1500);
//                             }
//                         }
//                         console.log(response);
//                     });
//             }
//         })
//
//
//     });
// });

function aplicar_desembolso() {
    let adv = '<b class="text-primary">Recuerde enviar los comprobantes de ACH a administración.</b>';
    Swal.fire({
        title: '¿Desea continuar?',
        html: "El desembolso se enviará a operaciones para habilitar el cliente a cobros. " + adv,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, proceder!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            let parameters = new FormData(document.getElementById('token_form'));
            parameters.append('action', 'aplicar_desembolso');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(error => console.log(error))
                .then(function (response) {
                    if (!response.hasOwnProperty('error')) {
                        if ('ok' in response) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se ha aplicado el desembolso.',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function () {
                                location.href = '/prestamos/aprobados/desembolsos/';
                            }, 1200);
                            return false;
                        }else if ('advertencia' in response){
                            Swal.fire(response['advertencia']);
                        }
                    }
                })
        }
    })
}