var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('aprobacion'),
        {
            fields: {
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                inicio: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                compra_saldo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 3500,
                            message: 'Ingrese un monto entre $0 y $3,500'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto':
                            case 'plazo_meses':
                            case 'inicio':
                            case 'compra_saldo':
                                return '.col-sm-4';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        var id_modelo = document.getElementById("id_modelo").value;
        // console.log(parameters);
        let visualizar = document.getElementById("input_v").value;
        if (visualizar === '1') {
            desglose();
        } else if (visualizar === '2') {
            let monto = document.getElementById('id_monto').value;
            let plazo = document.getElementById('id_plazo_meses').value;
            let inicio = document.getElementById('id_inicio').value;
            let arreglo = {
                'action': 'verificar',
                'monto': monto,
                'plazo': plazo,
                'inicio': inicio
            };
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: arreglo,
                dataType: 'json',
            }).done(function (data) {
                // alert('guardado');
                if (!data.hasOwnProperty('error')) {
                    console.log(data);
                    if (data.hasOwnProperty('ok')) {
                        continuar();
                    } else if (data.hasOwnProperty('cambio')) {
                        Swal.fire({
                            title: 'Los parámetros de la cotización han cambiado. ¿Continuar?',
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: `Sí`,
                            denyButtonText: `No`,
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                continuar();
                            } else if (result.isDenied) {
                                return false;
                            }
                        })
                    }
                }
                //message_error(data.error)
            }).fail(function (data) {
                alert("error");
            }).always(function (data) {
                // alert("complete")
            });

        } else if (visualizar === '3') {
            Swal.fire({
                title: '¿Desea rechazar la solicitud?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, Rechazar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    (async () => {
                        const {value: text} = await Swal.fire({
                            input: 'textarea',
                            inputLabel: 'Comentario',
                            inputPlaceholder: 'Escriba sus comentarios...',
                            inputAttributes: {
                                'aria-label': 'Type your message here'
                            },
                            showCancelButton: true
                        })
                        if (!text) {
                            Swal.fire('Debe escribir sus comentarios para continuar.')
                        }
                        if (text) {
                            var parameters = $('#aprobacion').serializeArray();
                            parameters.push({name: 'comentario', value: text});
                            parameters.push({name: 'action', value: 'rechazar'});
                            console.log(parameters);
                            $.ajax({
                                url: window.location.pathname,
                                type: 'POST',
                                data: parameters,
                                dataType: 'json',
                            }).done(function (data) {
                                // alert('guardado');
                                if (!data.hasOwnProperty('error')) {
                                    // console.log(data);
                                    if (data.hasOwnProperty('rechazado')) {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: '¡Rechazado en ' + data['rechazado'] + '!',
                                            showConfirmButton: false
                                        });
                                        setTimeout(function () {
                                            location.href = '/prestamos/aprobacion/';
                                        }, 1500);
                                        return false;
                                    } else if (data.hasOwnProperty('advertencia')) {
                                        Swal.fire({
                                            icon: 'info',
                                            title: 'Falta la aprobación de cumplimiento.',
                                            text: 'Verifique e intente nuevamente.',
                                        })
                                        return false;
                                    }
                                    return false;
                                }
                                message_error(data.error)
                            }).fail(function (data) {
                                alert("error");
                            }).always(function (data) {
                                // alert("complete")
                            });
                        }
                    })()
                }
            })
        }
    });

    $('[name="inicio"]')
        .datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('inicio');
        });
});

function desglose() {
    // Etiquetas en la tabla
    let solicitado = document.getElementById('d_solicitado');
    let manejo_1 = document.getElementById('d_manejo_1');
    let manejo_2 = document.getElementById('d_manejo_2');
    let interes = document.getElementById('d_interes');
    let promotor = document.getElementById('d_promotor');
    let descuento = document.getElementById('d_descuento');
    let notaria = document.getElementById('d_notaria');
    let timbres = document.getElementById('d_timbres');
    let seguros = document.getElementById('d_seguros');
    let otros = document.getElementById('d_otros');
    let obligacion = document.getElementById('d_obligacion');
    let admin = document.getElementById('d_administracion');
    let m_solicitado = document.getElementById('m_solicitado');
    let span_cuota_q = document.getElementById('cuota_q');
    let span_cuota_u = document.getElementById('cuota_q_u');
    let span_cuota_m = document.getElementById('cuota_m');
    let compra_saldo = document.getElementById('compra_saldo_1');
    let span_itbms = document.getElementById('itbms');
    let desembolso = document.getElementById('desembolso');
    // Parámetros del crédito
    let monto = parseFloat(document.getElementById('id_monto').value);
    let plazo = parseInt(document.getElementById('id_plazo_meses').value);
    let f_ini = new Date(Date.parse(document.getElementById('id_inicio').value));
    let p_promotor = parseInt(document.getElementById('val_promotor').innerHTML);
    let p_descuento = parseInt(document.getElementById('val_descuento').innerHTML);
    let p_manejo = parseInt(document.getElementById('p_manejo').value);
    let c_saldo = parseFloat(document.getElementById('c_saldo').value);
    let temp, ajuste, mes_extra, val_interes, val_manejo, val_promotor, val_descuento, val_seguros, val_timbres,
        val_admin;
    let itbms, val_desembolso, val_ultima_q;
    let ajuste_mes = 0;
    // Cálculos
    if (f_ini.getMonth() === 11) {
        if (f_ini.getDate() <= '15') {
            ajuste_mes = 1;
        }
        temp = Date.parse((f_ini.getFullYear() + 1).toString() + "-1" + "-15");
        f_ini = new Date(temp);
    }
    if (f_ini.getDate() > 15) {
        ajuste = 0.5;
    } else {
        ajuste = 0;
    }
    mes_extra = Math.floor(((f_ini.getMonth() + plazo + ajuste) / 12 + plazo + f_ini.getMonth() + ajuste) / 12);
    let total_meses = plazo + mes_extra + ajuste_mes;
    val_manejo = (monto * p_manejo / 10);
    val_interes = (monto * total_meses * 0.02);
    val_promotor = (monto * p_promotor / 100).toFixed(2);
    val_seguros = (monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25);
    let monto_prestamo = (monto + val_manejo + val_interes + val_seguros + 12).toFixed(2);
    val_descuento = (monto_prestamo * p_descuento / 100).toFixed(2);
    val_timbres = ((Math.ceil(monto_prestamo / 100)) / 10).toFixed(2);
    let cuota_q = (monto_prestamo / (plazo * 2) + 0.005).toFixed(2);
    val_ultima_q = monto_prestamo - cuota_q * (plazo * 2 - 1);
    let cuota_m = cuota_q * 2;
    val_admin = val_manejo - val_promotor - val_descuento - val_timbres;
    itbms = val_admin * 0.07;
    val_desembolso = monto - c_saldo - itbms;
    solicitado.innerHTML = '$ ' + monto.toFixed(2).toString();
    manejo_1.innerHTML = '$ ' + val_manejo.toFixed(2).toString();
    manejo_2.innerHTML = manejo_1.innerHTML;
    interes.innerHTML = '$ ' + val_interes.toFixed(2).toString();
    promotor.innerHTML = '$ ' + val_promotor.toString();
    descuento.innerHTML = '$ ' + val_descuento.toString();
    notaria.innerHTML = '$ ' + ((12).toFixed(2)).toString();
    timbres.innerHTML = '$ ' + val_timbres.toString();
    seguros.innerHTML = '$ ' + val_seguros.toFixed(2).toString();
    otros.innerHTML = '$ ' + ((0).toFixed(2)).toString();
    obligacion.innerHTML = '$ ' + monto_prestamo;
    admin.innerHTML = '$ ' + val_admin.toFixed(2).toString();
    // Segunda tabla de desglose
    m_solicitado.innerHTML = solicitado.innerHTML;
    span_cuota_q.innerHTML = '$ ' + cuota_q.toString();
    span_cuota_u.innerHTML = '$ ' + val_ultima_q.toFixed(2).toString(); //
    span_cuota_m.innerHTML = '$ ' + cuota_m.toString();
    compra_saldo.innerHTML = '$ ' + c_saldo.toFixed(2).toString(); //
    span_itbms.innerHTML = '$ ' + itbms.toFixed(2).toString();
    desembolso.innerHTML = '$ ' + val_desembolso.toFixed(2).toString();
}


function continuar() {
    Swal.fire({
        title: '¿Desea aprobar la solicitud?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, Aprobar!'
    }).then((result) => {
        if (result.isConfirmed) {
            (async () => {
                const {value: text} = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Comentario',
                    inputPlaceholder: 'Escriba sus comentarios...',
                    inputAttributes: {
                        'aria-label': 'Type your message here'
                    },
                    showCancelButton: true
                })
                if (!text) {
                    Swal.fire('Debe escribir sus comentarios para continuar.')
                }
                if (text) {
                    var parameters = $('#aprobacion').serializeArray();
                    parameters.push({name: 'comentario', value: text});
                    parameters.push({name: 'action', value: 'aprobar'});
                    console.log(parameters);
                    $.ajax({
                        url: window.location.pathname,
                        type: 'POST',
                        data: parameters,
                        dataType: 'json',
                    }).done(function (data) {
                        // alert('guardado');
                        if (!data.hasOwnProperty('error')) {
                            console.log(data);
                            if (data.hasOwnProperty('aprobado')) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Aprobado en ' + data['aprobado'] + '!',
                                    showConfirmButton: false
                                });
                                setTimeout(function () {
                                    location.href = '/prestamos/aprobacion/';
                                }, 1500);
                                return false;
                            } else if (data.hasOwnProperty('advertencia')) {
                                Swal.fire({
                                    icon: 'info',
                                    title: 'Falta la aprobación de cumplimiento.',
                                    text: 'Verifique e intente nuevamente.',
                                })
                                return false;
                            }
                            return false;
                        }
                        message_error(data)
                    }).fail(function (data) {
                        alert("error");
                    }).always(function (data) {
                        // alert("complete")
                    });
                }
            })()
        }
    })
}


function devolver() {
    Swal.fire({
        title: '¿Desea devolver la solicitud?',
        text: "Se comunicará a cumplimiento su observación.",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, devolver!'
    }).then((result) => {
        if (result.isConfirmed) {
            (async () => {
                const {value: text} = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Observaciones',
                    inputPlaceholder: 'Escriba sus observaciones aquí...',
                    inputAttributes: {
                        'aria-label': 'Type your message here'
                    },
                    showCancelButton: true
                })

                if (!text) {
                    Swal.fire('Debe ingresar sus observaciones para continuar.')
                }
                if (text) {
                    $.ajax({
                        url: window.location.pathname,
                        type: 'POST',
                        data: {
                            'action': 'devolver_c',
                            'comentario': text,
                        },
                        dataType: 'json',
                    }).done(function (data) {
                        // alert('guardado');
                        if (!data.hasOwnProperty('error')) {
                            // console.log(data);
                            if (data.hasOwnProperty('devuelto')) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Solicitud devuelta a ' + data['devuelto'] + '!',
                                    showConfirmButton: false
                                });
                                setTimeout(function () {
                                    location.href = '/prestamos/aprobacion/';
                                }, 1500);
                                return false;
                            } else if (data.hasOwnProperty('advertencia')) {
                                Swal.fire({
                                    icon: 'info',
                                    title: 'Falta la aprobación de cumplimiento.',
                                    text: 'Verifique e intente nuevamente.',
                                })
                                return false;
                            }
                            return false;
                        }
                        message_error(data.error)
                    }).fail(function (data) {
                        alert("error");
                    }).always(function (data) {
                        // alert("complete")
                    });
                }

            })()
        }
    })
}




