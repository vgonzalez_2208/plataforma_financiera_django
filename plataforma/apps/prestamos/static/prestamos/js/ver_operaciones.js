let fv_banco;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_banco = FormValidation.formValidation(
        document.getElementById('info_bancaria'),
        {
            fields: {
                nombre_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 5 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                numero_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            max: 40,
                            message: 'Mínimo 5 letras, máximo 40.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9- ]+$/,
                            message: 'Escriba caracteres válidos.'
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_cuenta':
                            case 'numero_cuenta':
                            case 'notas':
                                return '.col-md-12';

                            case 'banco':
                            case 'tipo_cuenta':
                                return '.col-md-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        let parameters = new FormData(document.getElementById("info_bancaria"));
        let action_val = document.getElementById('action_info_banco').value;
        console.log(action_val);
        let url = window.location.pathname;
        if (action_val === 'update_info_banco') {
            let parameters_check = $('#info_bancaria').serializeArray();
            let info = document.getElementById('info_recuperada').value;
            let val = JSON.parse(info.toString());
            console.log(val);
            console.log(parameters);
            let flag = 1;
            parameters_check.some(function (i) {
                if (i.name !== 'action' && i.name !== 'Si' && i.name !== 'csrfmiddlewaretoken') {
                    if (i.value !== val[i.name]) {
                        console.log(i.value, ' ', val[i.name]);
                        flag = 0;
                        Swal.fire({
                            title: '¿Desea guardar los cambios?',
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Guardar',
                            denyButtonText: `No guardar`,
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                f_fetch_info(url, parameters);
                            } else if (result.isDenied) {
                                $('#modal_info_bancaria').modal('hide');
                            }
                        })
                    }
                    return i.value !== val[i.name];
                }
            });
            if (flag === 1) {
                $('#modal_parametros').modal('hide');
            }
        } else if (action_val === 'load_info_banco') {
            f_fetch_info(url, parameters);
        }
        // let parameters = new FormData(document.getElementById("info_bancaria"));
    });
});

function f_fetch_info(url, parameters) {
    console.log(parameters);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                $('#modal_info_bancaria').modal('hide');
                document.getElementById("info_bancaria").reset();
                fv_banco.resetForm(true);
                if ('Ok' in response) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response['Ok'],
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else if ('Ok_2' in response) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response['Ok_2'],
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
                setTimeout(function () {
                    location.reload();
                }, 1200);
                return false;
            } else {
                message_error(response);
            }
            console.log(response);
        });
}

function info_bancaria_cliente(id_cliente) {
    let parameters = new FormData(document.getElementById("token_form"));
    parameters.append('action', 'info_banco_cliente');
    parameters.append('id_cliente', id_cliente);
    // var id_form = document.getElementById('id_form').value;
    let url = window.location.pathname;
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                if ('Si' in response) {
                    document.getElementById('info_recuperada').value = JSON.stringify(response);
                    document.getElementById('action_info_banco').value = 'update_info_banco';
                    document.getElementById('id_nombre_cuenta').value = response['nombre_cuenta'];
                    document.getElementById('id_numero_cuenta').value = response['numero_cuenta'];
                    document.getElementById('id_banco').value = response['banco'];
                    document.getElementById('id_tipo_cuenta').value = response['tipo_cuenta'];
                    document.getElementById('id_notas').value = response['notas'];
                } else {
                    // alert('No existe');
                    document.getElementById('action_info_banco').value = 'load_info_banco';
                }
            } else {
                message_error(response);
            }
            console.log(response);
        });
}

let email_conf = document.getElementById('email_conf');

email_conf.onchange = function () {
    load_pdf(email_conf, 'email_span');
}

function load_pdf(pdf_file, span) {
    let archivo = getExt(pdf_file.files[0].name);
    if (!(archivo === 'pdf' || archivo === 'PDF')) {
        pdf_file.value = '';
        Swal.fire('El archivo debe ser un PDF.');
        return false;
    }
    let file_span = document.getElementById(span);
    file_span.innerHTML = pdf_file.files[0].name;
    file_span.classList.add('text-warning');
}

// email_conf.onchange = function () {
//     let archivo = getExt(email_conf.files[0].name);
//     if (!(archivo === 'pdf' || archivo === 'PDF')) {
//         email_conf.value = '';
//         Swal.fire('El archivo debe ser un PDF.')
//         return false;
//     }
//     let email_span = document.getElementById('email_span');
//     email_span.innerHTML = email_conf.files[0].name;
//     email_span.classList.add('text-warning');
// }

function cargar_email_conf() {
    let archivo = document.getElementById('email_conf');
    if (archivo.value) {
        Swal.fire({
            title: '¿Desea cargar el documento?',
            showCancelButton: true,
            confirmButtonText: '¡Si, cargar!',
            cancelButtonText: `Cancelar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                let parameters = new FormData(document.getElementById('form_email_conf'));
                parameters.append('action', 'cargar_email_conf');
                let url = window.location.pathname;
                fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                    .catch(error => console.log(error))
                    .then(function (response) {
                        if (!response.hasOwnProperty('error')) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se ha cargado el archivo.',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function () {
                                location.reload();
                            }, 1200);
                        }
                    })
            }
        })
    }
}

function enviar_gerencia(flag_1, flag_2) {
    // console.log(flag_1, flag_2);
    if (flag_1 === 'True' && flag_2 === 'True') {
        let info = '<b class="text-danger">' + 'Asegúrese de que la información bancaria es correcta.' + '</b>';
        Swal.fire({
            title: '¿Desea continuar?',
            html: "La solicitud será enviada a Gerencia para que proceda con el desembolso. " + info,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, proceder!',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let parameters = new FormData(document.getElementById('token_form'));
                parameters.append('action', 'enviar_gerencia');
                let url = window.location.pathname;
                fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                    .catch(error => console.log(error))
                    .then(function (response) {
                        if ('stop' in response) {
                            let lista = '';
                            response['stop'].forEach(function (obj) {
                                lista += '<li>' + obj + '</li>'
                            });
                            // console.log(lista);
                            Swal.fire({
                                icon: 'error',
                                title: 'Hace falta la siguiente información:',
                                html: '<ul>' + lista + '</ul>',
                            })
                        } else if ('ok' in response) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: '¡El cliente ha sido enviado a gerencia!',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function () {
                                location.reload();
                            }, 1200);
                        } else if ('advertencia' in response) {
                            Swal.fire({
                                title: 'EL cliente ya fue enviado a Gerencia.',
                                text: "¿Desea revertir el envío?",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '¡Si, revertir!',
                                cancelButtonText: 'Cancelar',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    let parameters_2 = new FormData(document.getElementById('token_form'));
                                    parameters_2.append('action', 'cancelar_env_gerencia');
                                    fetch(url, {method: 'POST', body: parameters_2}).then(res => res.json())
                                        .catch(error => console.log(error))
                                        .then(function (response) {
                                            if ('ok' in response) {
                                                Swal.fire({
                                                    position: 'top-end',
                                                    icon: 'info',
                                                    title: 'Envío revertido.',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                setTimeout(function () {
                                                    location.reload();
                                                }, 1200);
                                            }
                                        })
                                }
                            })
                        }
                    })
            }
        })
    } else {
        if (flag_1 === 'False' && flag_2 === 'False') {
            let list = '<ul>' + '<li>Información bancaria</li>';
            list += '<li>Correo de confirmación de O.D.</li>' + '</ul>';
            Swal.fire({
                icon: 'error',
                title: 'Hace falta lo siguiente:',
                html: list,
            })
        } else if (flag_1 === 'False') {
            Swal.fire('Debe ingresar la información bancaria.');
        } else if (flag_2 === 'False') {
            Swal.fire('Debe cargar el email de confirmación de O.D.');
        }
    }
}

function enviar_desembolso(contrato) {
    let comprobante = document.getElementById('comprobante');
    let f_desembolso = document.getElementById('f_desembolso');
    if (comprobante.value && f_desembolso.value) {
        let info = '<b class="text-danger">' + 'Esta acción solo será reversible a través de cumplimiento e IT.' + '</b>';
        Swal.fire({
            title: '¿Desea continuar con el desembolso?',
            html: "El cliente aparecerá disponible en cobros. " + info,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, continuar!',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {
                let parameters = new FormData(document.getElementById('form_comprobante'));
                parameters.append('action', 'gen_desembolso');
                let url = window.location.pathname;
                fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                    .catch(error => console.log(error))
                    .then(function (response) {
                        // console.log(response);
                        if (!response.hasOwnProperty('error')) {
                            if ('ok' in response) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se generó el desembolso ' + contrato,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                setTimeout(function () {
                                    location.href = '/prestamos/aprobados/lista_operaciones/';
                                }, 1200);
                            }
                        }
                    })
            }
        })
    } else {
        // console.log(comprobante.value, f_desembolso.value);
        if (!comprobante.value && !f_desembolso.value) {
            let lista = '<li>El/los comprobante(s) ACH</li><li>Fecha de desembolso</li>'
            Swal.fire({
                title: 'Hace falta lo siguiente',
                icon: 'info',
                html: '<ul>' + lista + '</ul>',
            });
        } else if (!comprobante.value) {
            Swal.fire('Debe cargar el/los comprobante(s) ACH.')
        } else if (!f_desembolso.value) {
            Swal.fire('Debe ingresar la fecha del desembolso.')
        }
    }
}