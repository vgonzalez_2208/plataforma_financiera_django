var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('modal_cliente'),
        {
            fields: {
                m_nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_ap_1: {
                    notEmpty: {
                        message: 'Campo requerido.'
                    },
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_ap_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_cip: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_empresa: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 60,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ"()&'Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                m_email: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'm_nombre_1':
                            case 'm_nombre_2':
                            case 'm_ap_1':
                            case 'm_ap_2':
                            case 'm_cip':
                            case 'm_email':
                                return '.col-md-6';

                            case 'm_empresa':
                                return '.col-md-12';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea guardar los cambios?',
            showCancelButton: true,
            confirmButtonText: 'Guardar',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                (async () => {
                    const {value: text} = await Swal.fire({
                        input: 'textarea',
                        inputLabel: 'Comentarios',
                        inputPlaceholder: 'Escriba sus comentarios aquí...',
                        inputAttributes: {
                            'aria-label': 'Type your message here'
                        },
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                    })
                    if (text) {
                        let parameters = new FormData(document.getElementById('modal_cliente'));
                        parameters.append('comentario', text);
                        let url = window.location.pathname;
                        let response = await fetch(url, {method: 'POST', body: parameters});
                        let data = await response.json();
                        if (!('error' in response)) {
                            console.log(data);
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se ha actualizado la info del cliente',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#ModalCenter').modal('hide');
                            setTimeout(function () {
                                location.reload();
                            }, 1500);
                            return false;
                        }
                        message_error(data.error)
                    } else if (!text) {
                        Swal.fire('Debe ingresar sus comentarios para continuar.')
                    }
                })()
            }
        })
    });
});

async function info_cliente() {
    let parameters = new FormData();
    parameters.append('action', 'info_cliente');
    let csrftoken = getCookie('csrftoken');
    let url = window.location.pathname;
    let response = await fetch(url, {method: 'POST', body: parameters, headers: {"X-CSRFToken": csrftoken}});
    let data = await response.json();
    if (!('error' in data)) {
        // console.log(data['nombre_1']);
        document.getElementById('modal_nombre_1').value = data['nombre_1'];
        document.getElementById('modal_nombre_2').value = data['nombre_2'];
        document.getElementById('modal_ap_1').value = data['ap_1'];
        document.getElementById('modal_ap_2').value = data['ap_2'];
        document.getElementById('modal_cip').value = data['cip'];
        document.getElementById('modal_empresa').value = data['empresa'];
        document.getElementById('modal_email_1').value = data['email_1'];
        document.getElementById('id_empresa_2').value = data['empresa_2'];
        return false;
    } else {
        alert(data['error']);
    }
}

function cancelaciones_cliente() {
    (async () => {
        const {value: formValues} = await Swal.fire({
            title: 'Ingrese cancelaciones:',
            html:
                '<input id="swal-input1" class="swal2-input">' +
                '<input id="swal-input2" class="swal2-input">' +
                '<input id="swal-input3" class="swal2-input">',
            focusConfirm: false,
            preConfirm: () => {
                let val_1 = document.getElementById('swal-input1').value;
                let val_2 = document.getElementById('swal-input2').value;
                let val_3 = document.getElementById('swal-input3').value;
                let salida = [];
                if (!(val_1 === '')) {
                    salida.push(val_1);
                }
                if (!(val_2 === '')) {
                    salida.push(val_2);
                }
                if (!(val_3 === '')) {
                    salida.push(val_3);
                }
                if (salida.length === 0) {
                    salida = 0;
                }
                return salida;
            }
        })

        if (formValues) {
            let suma = 0;
            for (let i = 0; i < (formValues.length); i++) {
                suma += parseFloat(formValues[i].split(':')[1]);
            }
            console.log(suma);
            if (isNaN(suma)) {
                Swal.fire({
                    icon: 'error',
                    title: 'Atención...',
                    text: 'Hubo un error al ingresar las cancelaciones.',
                })
            } else if (suma.toFixed(2) > 2000) {
                document.getElementById('pr_compra_saldo').value = 0;
                document.getElementById('cancelaciones_span').innerHTML = '$ 0.00';
                Swal.fire({
                    icon: 'error',
                    title: 'Atención...',
                    text: 'La suma de cancelaciones excede $ 2,000 dólares.',
                })
            } else if (suma.toFixed(2) <= 2000) {
                document.getElementById('pr_compra_saldo').value = formValues;
                document.getElementById('cancelaciones_span').innerHTML = '$ ' + suma.toFixed(2).toString();
            }
            console.log(formValues);
        }
        if (!formValues) {
            document.getElementById('pr_compra_saldo').value = 0;
            document.getElementById('cancelaciones_span').innerHTML = '$ 0.00';
        }
    })()
}

function check_fecha_contrato(tipo, id_aprobado) {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'check_fecha_contrato');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters,}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (tipo === 'cotización' || tipo === 'detalles') {
                cot_detalles(response, tipo, id_aprobado);
            } else {
                contrato_pagare(response, tipo, id_aprobado);
            }
        })
}

function contrato_pagare(response, tipo, id_aprobado) {
    if (!('ok' in response)) {
        Swal.fire(Object.values(response)[0]);
    } else if ('ok' in response) {
        if (tipo === 'contrato') {
            window.open('/prestamos/aprobados/contrato_cliente/' + id_aprobado + '/', '_blank');
        } else if (tipo === 'pagare') {
            window.open('/prestamos/aprobados/pagare_cliente/' + id_aprobado + '/', '_blank');
        }
    }
}

function cot_detalles(response, tipo, id_aprobado) {
    if (Object.keys(response)[0] === 'warning_2' || Object.keys(response)[0] === 'warning_3') {
        if (tipo === 'cotización') {
            window.open('/prestamos/aprobados/cotizacion/' + id_aprobado + '/hoy/', '_blank');
        } else if (tipo === 'detalles') {
            window.open('/prestamos/aprobados/detalles/' + id_aprobado + '/hoy/', '_blank');
        }
    } else if ('ok' in response) {
        Swal.fire({
            title: 'Opciones para generar ' + tipo + ':',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Con fecha de hoy',
            denyButtonText: 'Con fecha de contrato',
            denyButtonColor: '#4cbb17',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                if (tipo === 'cotización') {
                    window.open('/prestamos/aprobados/cotizacion/' + id_aprobado + '/hoy/', '_blank');
                } else {
                    window.open('/prestamos/aprobados/detalles/' + id_aprobado + '/hoy/', '_blank');
                }
            } else if (result.isDenied) {
                if (tipo === 'cotización') {
                    window.open('/prestamos/aprobados/cotizacion/' + id_aprobado + '/contrato/', '_blank');
                } else {
                    window.open('/prestamos/aprobados/detalles/' + id_aprobado + '/contrato/', '_blank');
                }
            }
        })
    }
}

function check_empresa_2(id_aprobado) {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'verificar_empresa_od');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters,}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                if ('advertencia' in response) {
                    Swal.fire(
                        'Falta registrar el cliente en una empresa.',
                        'Una vez realizado el registro podrá generar la Orden de Descuento.',
                        'error'
                    )
                } else if ('ok' in response) {
                    window.open('/prestamos/aprobados/orden_descuento/' + id_aprobado + '/', '_blank');
                }
            }
        })
}

function asignar_ct() {
    Swal.fire({
        title: '¿Desea asignar un # de contrato a este cliente?',
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            (async () => {
                let data = await global_fetch('asignar_ct_1');
                if (!('error' in data)) {
                    if ('ct_asignado' in data) {
                        alerta_ct(data['ct_asignado']);
                    } else if ('advertencia' in data) {
                        Swal.fire({
                            html: '<h4>' + data['advertencia'] + '</h4>',
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Sí',
                            denyButtonText: 'No',
                            cancelButtonText: 'Cancelar',
                        }).then((result) => {
                                if (result.isConfirmed) {
                                    (async () => {
                                        let data = await global_fetch('asignar_ct_2');
                                        if (!('error' in data)) {
                                            alerta_ct(data['ct_asignado']);
                                        }
                                    })()
                                } else if (result.isDenied) {
                                    (async () => {
                                        let data = await global_fetch('asignar_ct_3');
                                        if (!('error' in data)) {
                                            alerta_ct(data['ct_asignado']);
                                        }
                                    })()
                                }
                            }
                        )
                    } else if ('stop' in data) {
                        let mensaje = '<span class="h4">' + data['stop'] + '</span>';
                        mensaje += '<span class="h4 font-weight-bold text-primary">' + ' ¿Desea retirar el # de contrato a este cliente?' + '</span>';
                        Swal.fire({
                            html: mensaje,
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Sí',
                            denyButtonText: 'No',
                            cancelButtonText: 'Cancelar',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                (async () => {
                                    let data = await global_fetch('quitar_ct');
                                    if (!('error' in data)) {
                                        if ('ct_retirado' in data){
                                            Swal.fire(data['ct_retirado']);
                                            document.getElementById('num_contrato_tag').innerText = 'Sin contrato';
                                        }
                                    }
                                })()
                            }
                        })
                    }
                }
            })()
        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
    })
}

function alerta_ct(num_ct) {
    Swal.fire({
            html: '<h2>Se asignó el # de contrato ' + '<b class="text-success">' + num_ct + '</b></h2>',
            icon: 'success'
        }
    )
    document.getElementById('num_contrato_tag').innerText = num_ct;
}
