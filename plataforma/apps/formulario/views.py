import base64
import threading
from datetime import datetime, date, timedelta
from math import floor

from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.http import JsonResponse
from django.shortcuts import render
# Create your views here.
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView

from apps.formulario.forms import FormularioForm, testForm
from apps.formulario.models import Formulario, Distrito, Corregimiento
from plataforma import settings
from plataforma.settings import DEBUG


def vista_formulario(request):
    data = {
        'name': 'Vicente'
    }
    return render(request, 'email_cliente.html', data)


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class TestView(CreateView):
    model = Formulario
    form_class = testForm


class FormularioView(CreateView):
    model = Formulario
    form_class = FormularioForm
    template_name = 'campos.html'
    success_url = 'https://rapi-cashpanama.com/'

    # Sobre-escritura de método POST
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # print(action)
            if action == 'add':
                post = request.POST
                dni = post['dni']
                # form = CategoryForm(request.POST)
                form = self.get_form()
                if form.is_valid():
                    imagen_1 = None
                    imagen_2 = None
                    try:
                        if post['image_1']:
                            img_dni = post['image_1']
                            formato, img_str = img_dni.split(';base64,')
                            # print("format", formato)
                            ext = formato.split('/')[-1]
                            imagen_1 = ContentFile(base64.b64decode(img_str), name='DNI_' + dni + '.' + ext)
                    except:
                        pass
                    try:
                        if post['image_2']:
                            img_ficha = post['image_2']
                            formato, img_str = img_ficha.split(';base64,')
                            # print("format", formato)
                            ext = formato.split('/')[-1]
                            imagen_2 = ContentFile(base64.b64decode(img_str), name='ficha_' + dni + '.' + ext)
                    except:
                        pass
                    nombre = form.cleaned_data['nombre_1']
                    apellido = form.cleaned_data['apellido_1']
                    t_doc = form.cleaned_data['tipo_doc']
                    dni = form.cleaned_data['dni']
                    t_ingreso = form.cleaned_data['tipo_ingreso']
                    salario = form.cleaned_data['salario']
                    monto = form.cleaned_data['monto']
                    meses = form.cleaned_data['plazo_meses']
                    f_nac = form.cleaned_data['f_nacimiento']
                    celular = form.cleaned_data['celular']
                    correo = form.cleaned_data['email_1']
                    p_exp = form.cleaned_data['p_expuesto']
                    provincia = form.cleaned_data['provincia']
                    distrito = form.cleaned_data['distrito']
                    terms = form.cleaned_data['terms']
                    apc = form.cleaned_data['apc']
                    refer = form.cleaned_data['ref']
                    a = form.save(commit=False)
                    if imagen_1:
                        a.DNI_img = imagen_1
                        # print('se guardo cedula')
                    if imagen_2:
                        a.ficha_img = imagen_2
                        # print('se guardo ficha')
                    a.save()
                    x = threading.Thread(target=send_email,
                                         args=(nombre, apellido, t_doc, dni, t_ingreso, salario, monto, meses, f_nac,
                                               celular, correo, p_exp, provincia, distrito, terms, apc, refer))
                    x.start()
                    y = threading.Thread(target=email_cliente, args=(nombre, apellido, monto, meses, correo))
                    y.start()
                    # print('éxito')
                else:
                    data['error'] = form.errors
                    # print(data)
            elif action == 'select_p2':
                consulta = Distrito.objects.filter(provincia_id=request.POST['id'])
                data = list(map(lambda val: {'id': val.id, 'name': val.nombre}, consulta))
            elif action == 'select_d2':
                consulta = Corregimiento.objects.filter(distrito_id=request.POST['id'])
                data = list(map(lambda val: {'id': val.id, 'name': val.nombre}, consulta))
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
            # data = Category.objects.get(pk=request.POST['id']).toJSON()
        except Exception as e:
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Formulario de pre-aprobación Rapicash'
        context['inicio'] = 'https://rapi-cashpanama.com/'
        context['action'] = 'add'
        return context


def send_email(self, apellido, t_doc, dni, t_ingreso, salario, monto, meses, f_nac, celular, correo, p_exp, provincia,
               distrito, terms, apc, refer):
    if refer is None:
        refer = 'Ninguno'
    else:
        pass
    context = {
        'nombre': self,
        'apellido': apellido,
        't_doc': t_doc,
        'dni': dni,
        't_ingreso': t_ingreso,
        'salario': f'{int(salario):,.2f}',
        'monto': f'{int(monto):,.2f}',
        'meses': meses,
        'f_nac': f_nac,
        'celular': celular,
        'correo': correo,
        'p_exp': p_exp,
        'ciudad': provincia,
        'sector': distrito,
        'terms': 'De acuerdo',
        'apc': 'De acuerdo',
        'refer': refer,
    }

    template = get_template('email_3.html')
    content = template.render(context)
    if DEBUG is True:
        env_correo = EmailMultiAlternatives(
            'Nuevo ingreso de solicitud',
            'Formulario',
            settings.EMAIL_HOST_USER,
            ['vicente.gonzalez.2208@gmail.com']
        )
    else:
        env_correo = EmailMultiAlternatives(
            'Nuevo ingreso de solicitud',
            'Formulario',
            settings.EMAIL_HOST_USER,
            ['ventas@rapi-cashpanama.com'],
            cc=['admin@rapi-cashpanama.com', 'gerencia@rapi-cashpanama.com', 'cumplimiento@rapi-cashpanama.com',
                'callcenter@rapi-cashpanama.com']
        )
    env_correo.attach_alternative(content, 'text/html')
    env_correo.send()


def email_cliente(self, apellido, monto, meses, correo):
    fecha_ini = date.today()
    now = date.today()
    fecha = fecha_completa()
    if now.month == 12:
        fecha_ini = date(now.year + 1, 1, 15)
    else:
        if now.day <= 8:
            fecha_ini = date(now.year, now.month, 15)
        elif now.day <= 25:
            last_day = last_day_of_month(now).day
            if last_day == 31:
                last_day = 30
            fecha_ini = date(now.year, now.month, last_day)
        elif now.day > 25:
            if now.month == 11:
                fecha_ini = date(now.year + 1, 1, 15)
            else:
                fecha_ini = date(now.year, now.month + 1, 15)
    total_meses = int(meses) + meses_extra(meses, fecha_ini)
    comision = round(int(monto) * 0.50, 2)
    int_mensual = 0.02
    int_total = int(monto) * int_mensual * total_meses
    monto_prestamo = int(monto) + comision + int_total + int(monto) * (
            0.035 + 0.025) * total_meses / 12 + total_meses * 1.25 + 12
    letra_quincenal = round(monto_prestamo / (int(meses) * 2) + 0.005, 2)
    timbres = round(monto_prestamo / 1000 + 0.05, 1)
    itbms = round((comision - timbres) * 0.07, 2)
    desembolso = int(monto) - itbms

    if datetime.now().hour < 12:
        saludo = "Buenos días"
    elif datetime.now().hour < 18:
        saludo = "Buenas tardes"
    else:
        saludo = "Buenas noches"

    context = {
        'fecha_encabezado': fecha,
        'saludo': saludo,
        'nombre': self,
        'apellido': apellido,
        'monto': f'{int(monto):,.2f}',
        'meses': meses,
        'letra_quincenal': f'{letra_quincenal:,.2f}',
        'comision': f'{comision:,.2f}',
        'notaria': f'{12:,.2f}',
        'itbms': f'{itbms:,.2f}',
        'desembolso': f'{desembolso:,.2f}',
        'fecha_inicio': fecha_ini
    }

    template = get_template('email_cliente.html')
    content = template.render(context)
    if DEBUG is True:
        env_correo = EmailMultiAlternatives(
            '¡Cotización de tu crédito!',
            'Formulario',
            settings.EMAIL_HOST_USER,
            ['vicente.gonzalez.2208@gmail.com'],
        )
    else:
        env_correo = EmailMultiAlternatives(
            '¡Cotización de tu crédito!',
            'Formulario',
            settings.EMAIL_HOST_USER,
            [correo],
            cc=['admin@rapi-cashpanama.com']
        )
    env_correo.attach_alternative(content, 'text/html')
    env_correo.send()


def fecha_completa():
    months = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
              "Noviembre", "Diciembre")
    dia_semana = ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
    dia_s = dia_semana[datetime.today().weekday()]
    day = datetime.now().day
    month = months[datetime.now().month - 1]
    year = datetime.now().year
    fecha = "{}, {} de {} del {}".format(dia_s, day, month, year)
    return fecha


def last_day_of_month(fecha):
    if fecha.month == 12:
        return fecha.replace(day=31)
    return fecha.replace(month=fecha.month + 1, day=1) - timedelta(days=1)


def meses_extra(plazo, f_ini):
    plazo = int(plazo)
    if f_ini.day > 15:
        ajuste = 0.5
    else:
        ajuste = 0
    mes_extra = floor(((f_ini.month + plazo - 1 + ajuste) / 12 + plazo + f_ini.month - 1 + ajuste) / 12)
    return mes_extra

# now = datetime.now()
#
# print(current_date_format(now))
#
# print(now)
