var fv;

document.addEventListener('DOMContentLoaded', function () {
    const form = document.getElementById('formulario');
    /*const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        form,
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_doc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras o números.'
                        }
                    }
                },
                motivo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                f_nacimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                email_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                confirmEmail: {
                    validators: {
                        identical: {
                            compare: function () {
                                return form.querySelector('[name="email_1"]').value;
                            },
                            message: 'Los correos ingresados no son iguales.'
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                e_civil: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                genero: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                dependientes: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 7,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9.]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                empresa_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñ ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                in_extras: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                estudios: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                p_expuesto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                provincia: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                distrito: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                corregimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 caracteres, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[0-9a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba caracteres válidos.'
                        }
                    }
                },
                terms: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                apc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^[0-9.]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^[0-9.]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                DNI_i: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            maxSize: 2097152,   // 2048 * 1024
                            message: 'Archivo seleccionado no es válido.'
                        },
                    }
                },
                ficha_i: {
                    validators: {
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            maxSize: 2097152,   // 2048 * 1024
                            message: 'Archivo seleccionado no es válido.'
                        },
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap5: new FormValidation.plugins.Bootstrap5(),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                /*recaptcha: new FormValidation.plugins.Recaptcha({
                    element: 'g-captcha',
                    message: 'The captcha is not valid',
                    // Replace with the site key provided by Google
                    siteKey: '6LdB08gaAAAAAMZmG-jcL-3_QFvffJS3Wpac7VLW',
                    theme: 'light',
                }),*/
            },
        }
    ).on('core.form.valid', function (event) {
            // var parameters = $('#formulario').serializeArray();
            var parameters = new FormData(document.getElementById("formulario"));
            /*parameters.forEach(function (value, key) {
                console.log(key + ":" + value);
            });*/
            // console.log(parameters);
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: parameters,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function (data) {
                // console.log(data);
                // alert('guardado');
                if (!data.hasOwnProperty('error')) {
                    Swal.fire({
                        icon: 'success',
                        title: '¡Tus datos han sido recibidos!',
                        text: 'Pronto nos comunicaremos contigo.',
                        confirmButtonText: '<a href="https://rapi-cashpanama.com/" style="color:#ffffff;">Ir a inicio</a>',
                    })
                    return false;
                }
                message_error(data.error)
            }).fail(function (data) {
                alert("error");
            }).always(function (data) {
                // alert("complete")
            });
        });
    form.querySelector('[name="email_1"]').addEventListener('input', function () {
        fv.revalidateField('confirmEmail');
    });

    $('[name="f_nacimiento"]')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            endDate: '-22y'
        })
        .on('changeDate', function () {
            fv.revalidateField('f_nacimiento');
        });
});

$(document).ready(function() {
   $('#file-reset').on('click', function() {
      $('#id_DNI_img').val('');
      $('#id_ficha_img').val('');
   });
});






