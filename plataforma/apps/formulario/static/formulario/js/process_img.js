let fileinput_1 = document.getElementById('id_DNI_i');
let fileinput_2 = document.getElementById('id_ficha_i');
let max_width = 1920;  // Ancho
let max_height = 1080;  // Alto
// var preview = document.getElementById('preview');
let form = document.getElementById('formulario');

function processfile(file, campo) {
    if (!(/image/i).test(file.type)) {
        alert("File " + file.name + " is not an image.");
        return false;
    }
    // read the files
    var reader = new FileReader();
    reader.readAsArrayBuffer(file);

    reader.onload = function (event) {
        // blob stuff
        var blob = new Blob([event.target.result]); // create blob...
        window.URL = window.URL || window.webkitURL;
        var blobURL = window.URL.createObjectURL(blob); // and get it's URL

        // helper Image object
        var image = new Image();
        image.src = blobURL;
        //preview.appendChild(image); // preview commented out, I am using the canvas instead
        image.onload = function () {
            // have to wait till it's loaded
            var resized = resizeMe(image); // send it to canvas
            var newinput = document.createElement("input");
            if (campo === 1) {
                newinput.type = 'hidden';
                newinput.name = 'image_1';
                newinput.value = resized; // put result from canvas into new hidden input
                // console.log('imagen 1');
            } else if (campo === 2) {
                newinput.type = 'hidden';
                newinput.name = 'image_2';
                newinput.value = resized; // put result from canvas into new hidden input
                // console.log('imagen 2');
            }
            // console.log(resized);
            form.appendChild(newinput);
        }
    };
}

function readfiles(files) {
    var existinginputs;
    // remove the existing canvases and hidden inputs if user re-selects new pics
    if (fileinput_1.value) {
        existinginputs = document.getElementsByName('image_1');
        while (existinginputs.length > 0) { // it's a live list so removing the first element each time
            // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
            form.removeChild(existinginputs[0]);
            // preview.removeChild(existingcanvases[0]);
        }
        for (var i = 0; i < files.length; i++) {
            processfile(files[i], 1); // process each file at once
        }
        fileinput_1.value = ""; //remove the original files from fileinput
    }
    else if (fileinput_2.value) {
        existinginputs = document.getElementsByName('image_2');
        while (existinginputs.length > 0) { // it's a live list so removing the first element each time
            // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
            form.removeChild(existinginputs[0]);
            // preview.removeChild(existingcanvases[0]);
        }
        for (var j = 0; j < files.length; j++) {
            processfile(files[j], 2); // process each file at once
        }
        fileinput_2.value = ""; //remove the original files from fileinput
    }

    // TODO remove the previous hidden inputs if user selects other files
}

// this is where it starts. event triggered when user selects files
fileinput_1.onchange = function () {
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        alert('Archivo inválido.');
        return false;
    }
    document.getElementById('archivo_1').innerHTML = '<strong class="font-monospace text-primary">' + fileinput_1.files[0].name + '<strong>';
    document.getElementById('archivo_1').classList.remove("fst-italic");
    document.getElementById('label_arc_1').classList.remove("btn-secondary");
    document.getElementById('label_arc_1').classList.add("btn-success");
    readfiles(fileinput_1.files);
}

fileinput_2.onchange = function () {
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        alert('Archivo inválido.');
        return false;
    }
    document.getElementById('archivo_2').innerHTML = '<strong class="font-monospace text-primary">' + fileinput_2.files[0].name + '<strong>';
    document.getElementById('archivo_2').classList.remove("fst-italic");
    document.getElementById('label_arc_2').classList.remove("btn-secondary");
    document.getElementById('label_arc_2').classList.add("btn-success");
    readfiles(fileinput_2.files);
}

// === RESIZE ====

function resizeMe(img) {

    var canvas = document.createElement('canvas');

    var width = img.width;
    var height = img.height;

    // calculate the width and height, constraining the proportions
    if (width > height) {
        if (width > max_width) {
            //height *= max_width / width;
            height = Math.round(height *= max_width / width);
            width = max_width;
        }
    } else {
        if (height > max_height) {
            //width *= max_height / height;
            width = Math.round(width *= max_height / height);
            height = max_height;
        }
    }

    // resize the canvas and draw the image data into it
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    // preview.appendChild(canvas); // do the actual resized preview
    return canvas.toDataURL("image/jpeg", 0.8); // get the data from canvas as 70% JPG (can be also PNG, etc.)

}

/////////////////////////////////////////////

function message_error(obj) {
    let html = '<ul style="text-align: left;">';
    $.each(obj, function (key, value) {
        /* console.log(key);
        console.log(value); */
        html += '<li>' + value + '</li>';
    });
    html += '</ul>';
    Swal.fire({
        title: 'Atención!',
        html: html,
        icon: 'warning',
    });
}