let provincia = document.getElementById('id_provincia');
let distrito = document.getElementById('id_distrito');
let corregimiento = document.getElementById('id_corregimiento');

provincia.onchange = function () {
    select_dir('select_p2', this, distrito, true)
}

distrito.onchange = function () {
    select_dir('select_d2', this, corregimiento)
}

function select_dir(action, dir_1, dir_2, flag=false) {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', action);
    let url = window.location.pathname;
    let options = '<option value="">---------------</option>';
    if (flag){
        corregimiento.innerHTML = options;
    }
    if (dir_1.value !== '') {
        parameters.append('id', dir_1.value);
    } else {
        dir_2.innerHTML = options;
        return false;
    }
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (!('error' in response)) {
                response.forEach(function (value, key) {
                    options += '<option value="' + value.id + '">' + value.name + '</option>';
                })
                dir_2.innerHTML = options;
            }
        })
}

// $(function () {
//     $('select[name="provincia"]').on('change', function () {
//         var id = $(this).val();
//         var select_distrito = $('select[name="distrito"]');
//         var select_corregimiento = $('select[name="corregimiento"]');
//         var options = '<option value="">---------------</option>';
//         select_corregimiento.html(options);
//         if (id === '') {
//             select_distrito.html(options);
//             return false;
//         }
//         $.ajax({
//             url: window.location.pathname, //window.location.pathname
//             type: 'POST',
//             data: {
//                 'action': 'select_p',
//                 'id': id
//             },
//             dataType: 'json',
//         }).done(function (data) {
//             // console.log(data);
//             if (!data.hasOwnProperty('error')) {
//                 $.each(data, function (key, value) {
//                     options += '<option value="' + value.id + '">' + value.name + '</option>'
//                 })
//                 return false;
//             }
//             message_error(data.error);
//         }).fail(function (jqXHR, textStatus, errorThrown) {
//             alert(textStatus + ': ' + errorThrown);
//         }).always(function (data) {
//             select_distrito.html(options);
//         });
//     })
// })
//
// $(function () {
//     $('select[name="distrito"]').on('change', function () {
//         var id = $(this).val();
//         var select_corregimiento = $('select[name="corregimiento"]');
//         var options = '<option value="">---------------</option>';
//         if (id === '') {
//             select_corregimiento.html(options);
//             return false;
//         }
//         $.ajax({
//             url: window.location.pathname, //window.location.pathname
//             type: 'POST',
//             data: {
//                 'action': 'select_d',
//                 'id': id
//             },
//             dataType: 'json',
//         }).done(function (data) {
//             // console.log(data);
//             if (!data.hasOwnProperty('error')) {
//                 $.each(data, function (key, value) {
//                     options += '<option value="' + value.id + '">' + value.name + '</option>'
//                 })
//                 return false;
//             }
//             message_error(data.error);
//         }).fail(function (jqXHR, textStatus, errorThrown) {
//             alert(textStatus + ': ' + errorThrown);
//         }).always(function (data) {
//             select_corregimiento.html(options);
//         });
//     })
// })