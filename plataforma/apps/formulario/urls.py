from django.contrib import admin
from django.urls import path, include

from apps.formulario.views import vista_formulario, FormularioView

app_name = 'formulario'

urlpatterns = [
    path('form/', vista_formulario),
    path('', FormularioView.as_view(), name='formulario_create'),
]

# path('category/update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),