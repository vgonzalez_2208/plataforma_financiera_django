tipo_documento = (  # #
    (None, 'Seleccione...'),
    ('Cédula', 'Cédula'),
    ('Pasaporte', 'Pasaporte'),
    ('Licencia', 'Licencia de conducir'),
)

sexo = (  # #
    (None, 'Seleccione...'),
    ('Femenino', 'Femenino'),
    ('Masculino', 'Masculino'),
    ('Otros', 'Otros'),
)

e_civil = (  # #
    (None, 'Seleccione...'),
    ('Soltero', 'Soltero'),
    ('Casado', 'Casado'),
    ('Divorciado', 'Divorciado'),
    ('Viudo', 'Viudo'),
)

cantidad_dep = (  # #
    (None, 'Seleccione...'),
    ('Ninguno', 'Ninguno'),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5+'),
)

t_trabajo = (  # #
    (None, 'Seleccione...'),
    ('Empresa privada', 'Empresa privada'),
    ('Empresa pública', 'Empresa pública'),
    ('Independiente', 'Independiente'),
    ('Jubilado', 'Jubilado')
)

i_extras = (  # #
    (None, 'Seleccione...'),
    ('Bonos', 'Bonificaciones'),
    ('Comisiones', 'Comisiones'),
    ('Ninguno', 'Ninguno'),
)

n_estudio = (  # #
    (None, 'Seleccione...'),
    ('Primaria', 'Primaria'),
    ('Secundaria', 'Secundaria'),
    ('Universidad', 'Universidad'),
)

t_casa = (
    (None, 'Seleccione...'),
    ('Alquilada', 'Alquilada'),
    ('Propia', 'Propia'),
    ('Familiar', 'Familiar'),
)

# t_expuesto = (
#     (None, 'Seleccione...'),
#     ('No', 'Ninguna Condición Especial PEP'),
#     ('p1', 'Alto Dignatario del Poder Ejecutivo de un Estado'),
#     ('p2', 'Alto Dignatario del Poder Judicial de un Estado'),
#     ('p3', 'Alto Directivo de una Empresa Pública'),
#     ('p4', 'Asociado Cercano o Allegado de Confianza a una PEP'),
#     ('p5', 'Dirigente de Partido Político'),
#     ('p6', 'Familiar de PEP'),
#     ('p7', 'Funcionario Público con Jurisdicción y Mando'),
#     ('p8', 'Jefe de Estado o Gobierno'),
#     ('p9', 'Jefe de Organismo Supranacional'),
#     ('p10', 'Miembro de la Familia Real con Funciones de Gobierno'),
#     ('p11', 'Miembro de la Fuerza Pública de un Estado'),
#     ('p12', 'Miembro del Parlamento, Congreso o Asamblea Nacional'),
#     ('p13', 'Miembro o Directivo de un Banco Central'),
#     ('p14', 'Persona Natural Electa y Posesionado en Cargo por Votación Popular'),
#     ('p15', 'Persona Públicamente Expuesta Internacional'),
#     ('p16', 'Persona Públicamente Expuesta Nacional'),
#     ('p17', 'Persona Políticamente Expuesta Internacional'),
#     ('p18', 'Persona Políticamente Expuesta Nacional'),
#     ('p19', 'Representante Diplomático'),
# )

t_expuesto = (
    (None, 'Seleccione...'),
    ('No', 'No'),
    ('Si', 'Si'),
)
