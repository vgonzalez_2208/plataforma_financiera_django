# Generated by Django 3.2.3 on 2021-10-18 05:00

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Corregimiento',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Dependientes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dependientes', models.CharField(max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Distrito',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Escolaridad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estudios', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='EstadoCivil',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('e_civil', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Genero',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('genero', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='IngresosExtra',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('in_extras', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Motivo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('motivo', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='PEP',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pep', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Provincia',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='TipoDocumento',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_documento', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='TipoIngreso',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('t_ingreso', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Formulario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creado_el', models.DateTimeField(blank=True, default=django.utils.timezone.now, verbose_name='creado el')),
                ('fecha_ingreso', models.DateTimeField(auto_now_add=True, null=True)),
                ('monto', models.CharField(max_length=40, verbose_name='monto')),
                ('plazo_meses', models.CharField(max_length=40, verbose_name='plazo meses')),
                ('nombre_1', models.CharField(max_length=40, verbose_name='nombre 1')),
                ('nombre_2', models.CharField(blank=True, max_length=40, null=True, verbose_name='nombre 2')),
                ('apellido_1', models.CharField(max_length=40, verbose_name='apellido 1')),
                ('apellido_2', models.CharField(blank=True, max_length=40, null=True, verbose_name='apellido 2')),
                ('dni', models.CharField(max_length=20, verbose_name='dni')),
                ('f_nacimiento', models.DateField(verbose_name='f_nacimiento')),
                ('email_1', models.EmailField(max_length=254, verbose_name='email 1')),
                ('celular', models.IntegerField(verbose_name='celular')),
                ('salario', models.DecimalField(decimal_places=2, max_digits=6, verbose_name='salario')),
                ('empresa_1', models.CharField(max_length=50, verbose_name='empresa')),
                ('direccion', models.CharField(blank=True, default='', max_length=250, null=True, verbose_name='dir_cliente')),
                ('ref', models.CharField(blank=True, max_length=100, null=True, verbose_name='ref')),
                ('terms', models.BooleanField(default=False, verbose_name='terms')),
                ('apc', models.BooleanField(default=False, verbose_name='apc')),
                ('check_1', models.BooleanField(blank=True, default=False, null=True, verbose_name='check 1')),
                ('check_2', models.BooleanField(blank=True, default=False, null=True, verbose_name='check 2')),
                ('check_3', models.BooleanField(blank=True, default=False, null=True, verbose_name='check 2')),
                ('DNI_img', models.ImageField(blank=True, null=True, upload_to='DNI/%Y/%m/%d', verbose_name='dni_img')),
                ('ficha_img', models.ImageField(blank=True, null=True, upload_to='ficha_seguro/%Y/%m/%d', verbose_name='ficha_img')),
                ('check_doc', models.BooleanField(blank=True, default=False, null=True, verbose_name='check doc')),
                ('notas', models.CharField(blank=True, max_length=1000, null=True, verbose_name='notas')),
                ('user_view', models.CharField(blank=True, max_length=50, null=True, verbose_name='user view')),
                ('inicio_pago', models.CharField(blank=True, max_length=100, null=True, verbose_name='inicio pago')),
                ('corregimiento', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.corregimiento')),
                ('dependientes', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.dependientes')),
                ('distrito', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.distrito')),
                ('e_civil', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.estadocivil')),
                ('estudios', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.escolaridad')),
                ('genero', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.genero')),
                ('in_extras', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.ingresosextra')),
                ('motivo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.motivo')),
                ('p_expuesto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.pep')),
                ('provincia', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.provincia')),
                ('tipo_doc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.tipodocumento')),
                ('tipo_ingreso', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='formulario.tipoingreso')),
            ],
            options={
                'verbose_name': 'Formulario',
                'verbose_name_plural': 'Formularios',
                'ordering': ['id'],
            },
        ),
        migrations.AddField(
            model_name='distrito',
            name='provincia',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='formulario.provincia'),
        ),
        migrations.CreateModel(
            name='Cotizaciones',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank=True, max_length=100, null=True, verbose_name='nombre cliente')),
                ('monto', models.CharField(blank=True, max_length=100, null=True, verbose_name='monto')),
                ('plazo', models.CharField(blank=True, max_length=100, null=True, verbose_name='plazo')),
                ('fecha_ini', models.CharField(blank=True, max_length=100, null=True, verbose_name='fecha ini')),
                ('compra_saldo', models.CharField(blank=True, max_length=100, null=True, verbose_name='compra saldo')),
                ('c_promotor', models.CharField(blank=True, max_length=100, null=True, verbose_name='c promotor')),
                ('s_descuento', models.CharField(blank=True, max_length=100, null=True, verbose_name='s descuento')),
                ('cliente', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='formulario.formulario')),
            ],
        ),
        migrations.AddField(
            model_name='corregimiento',
            name='distrito',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='formulario.distrito'),
        ),
    ]
