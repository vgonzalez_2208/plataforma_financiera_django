from io import BytesIO

from PIL import Image
from django.core.files import File
from django.db import models
from django.forms import model_to_dict
from django.utils import timezone as tz
from django.utils.translation import gettext_lazy as _

from apps.formulario.choices import *


# Create your models here.


class Provincia(models.Model):
    nombre = models.CharField(max_length=20)

    def __str__(self):
        return self.nombre


class Distrito(models.Model):
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=25)

    def __str__(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        return item


class Corregimiento(models.Model):
    distrito = models.ForeignKey(Distrito, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        return item


class TipoDocumento(models.Model):
    tipo_documento = models.CharField(max_length=30)

    def __str__(self):
        return self.tipo_documento


class Genero(models.Model):
    genero = models.CharField(max_length=20)

    def __str__(self):
        return self.genero


class EstadoCivil(models.Model):
    e_civil = models.CharField(max_length=20)

    def __str__(self):
        return self.e_civil


class Dependientes(models.Model):
    dependientes = models.CharField(max_length=15)

    def __str__(self):
        return self.dependientes


class TipoIngreso(models.Model):
    t_ingreso = models.CharField(max_length=20)

    def __str__(self):
        return self.t_ingreso


class IngresosExtra(models.Model):
    in_extras = models.CharField(max_length=20)

    def __str__(self):
        return self.in_extras


class Escolaridad(models.Model):
    estudios = models.CharField(max_length=20)

    def __str__(self):
        return self.estudios


class PEP(models.Model):
    pep = models.CharField(max_length=20)

    def __str__(self):
        return self.pep


class Motivo(models.Model):
    motivo = models.CharField(max_length=100)

    def __str__(self):
        return self.motivo


class Formulario(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=False, blank=True, default=tz.now)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    monto = models.CharField(_('monto'), null=False, blank=False, max_length=40)
    plazo_meses = models.CharField(_('plazo meses'), null=False, blank=False, max_length=40)
    # Nombres y apellidos
    nombre_1 = models.CharField(_('nombre 1'), max_length=40, null=False, blank=False)
    nombre_2 = models.CharField(_('nombre 2'), max_length=40, null=True, blank=True)  # Puede estar en blanco
    apellido_1 = models.CharField(_('apellido 1'), max_length=40, null=False, blank=False)
    apellido_2 = models.CharField(_('apellido 2'), max_length=40, null=True, blank=True)

    motivo = models.ForeignKey(Motivo, on_delete=models.SET_NULL, null=True, blank=True)
    tipo_doc = models.ForeignKey(TipoDocumento, on_delete=models.SET_NULL, null=True)
    dni = models.CharField(_('dni'), max_length=20, null=False, blank=False)
    f_nacimiento = models.DateField(_('f_nacimiento'), null=False, blank=False)
    email_1 = models.EmailField(_('email 1'), max_length=254, null=False, blank=False)
    celular = models.IntegerField(_('celular'), null=False, blank=False)
    e_civil = models.ForeignKey(EstadoCivil, on_delete=models.SET_NULL, null=True)
    genero = models.ForeignKey(Genero, on_delete=models.SET_NULL, null=True)
    dependientes = models.ForeignKey(Dependientes, on_delete=models.SET_NULL, null=True)
    tipo_ingreso = models.ForeignKey(TipoIngreso, on_delete=models.SET_NULL, null=True)
    salario = models.DecimalField(_('salario'), max_digits=6, decimal_places=2, null=False, blank=False)
    empresa_1 = models.CharField(_('empresa'), max_length=50)
    in_extras = models.ForeignKey(IngresosExtra, on_delete=models.SET_NULL, null=True)
    estudios = models.ForeignKey(Escolaridad, on_delete=models.SET_NULL, null=True)
    p_expuesto = models.ForeignKey(PEP, on_delete=models.SET_NULL, null=True)
    # Dirección
    provincia = models.ForeignKey(Provincia, on_delete=models.SET_NULL, null=True)
    distrito = models.ForeignKey(Distrito, on_delete=models.SET_NULL, null=True)
    corregimiento = models.ForeignKey(Corregimiento, on_delete=models.SET_NULL, null=True)
    direccion = models.CharField(_('dir_cliente'), max_length=250, null=True, blank=True, default='')

    ref = models.CharField(_('ref'), max_length=100, null=True, blank=True)  # opcional
    terms = models.BooleanField(_('terms'), default=False)
    apc = models.BooleanField(_('apc'), default=False)
    check_1 = models.BooleanField(_('check 1'), default=False, null=True, blank=True)
    check_2 = models.BooleanField(_('check 2'), default=False, null=True, blank=True)  # Procesar solicitud
    check_3 = models.BooleanField(_('check 2'), default=False, null=True, blank=True)  # Descartar solicitud
    # Documentos
    DNI_img = models.ImageField(upload_to='DNI/%Y/%m/%d', verbose_name='dni_img', null=True, blank=True)
    ficha_img = models.ImageField(upload_to='ficha_seguro/%Y/%m/%d', verbose_name='ficha_img', null=True, blank=True)
    check_doc = models.BooleanField(_('check doc'), default=False, null=True, blank=True)
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)

    user_view = models.CharField(_('user view'), max_length=50, null=True, blank=True)
    inicio_pago = models.CharField(_('inicio pago'), null=True, blank=True, max_length=100)

    def __str__(self):
        return self.nombre_1

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Formulario'
        verbose_name_plural = 'Formularios'
        ordering = ['id']

    def save(self, *args, **kwargs):
        if not self.id:
            if self.DNI_img:
                self.DNI_img = self.compress_image(self.DNI_img)
            if self.ficha_img:
                self.ficha_img = self.compress_image(self.ficha_img)
        super(Formulario, self).save()

    def compress_image(self, image):
        img = Image.open(image).convert("RGB")
        im_io = BytesIO()

        if image.name.split('.')[1] == 'jpeg' or image.name.split('.')[1] == 'jpg' or image.name.split('.')[1] == 'JPG':
            img.save(im_io, format='jpeg', optimize=True, quality=70)
            new_image = File(im_io, name="%s.jpeg" % image.name.split('.')[0], )
        else:
            img.save(im_io, format='png', optimize=True, quality=70)
            new_image = File(im_io, name="%s.png" % image.name.split('.')[0], )
        return new_image


class Cotizaciones(models.Model):
    cliente = models.OneToOneField(Formulario, on_delete=models.CASCADE, null=False, blank=False)
    nombre = models.CharField(_('nombre cliente'), max_length=100, null=True, blank=True)
    monto = models.CharField(_('monto'), max_length=100, null=True, blank=True)
    plazo = models.CharField(_('plazo'), max_length=100, null=True, blank=True)
    fecha_ini = models.CharField(_('fecha ini'), max_length=100, null=True, blank=True)
    compra_saldo = models.CharField(_('compra saldo'), max_length=100, null=True, blank=True)
    c_promotor = models.CharField(_('c promotor'), max_length=100, null=True, blank=True)
    s_descuento = models.CharField(_('s descuento'), max_length=100, null=True, blank=True)

    def __str__(self):
        return self.nombre
