from django.apps import AppConfig


class FormularioConfig(AppConfig):
    name = 'apps.formulario'
