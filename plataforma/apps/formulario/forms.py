from datetime import datetime

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox, ReCaptchaV3, ReCaptchaV2Invisible
from django.forms import *

from apps.formulario.models import Formulario, Provincia, Distrito, Corregimiento

import datetime

from plataforma import settings

dia = datetime.date.today()
year = dia.year - 22

x = datetime.datetime(year, dia.month, dia.day)
x = x.strftime("%d/%m/%Y")


class testForm(ModelForm):
    captcha = ReCaptchaField(
        error_messages={'required': 'Complete el CAPTCHA'},
        widget=ReCaptchaV2Checkbox(attrs={
            # 'class': 'validate',
            'required': 'true',
            'id': 'Captcha',
            # 'data-sitekey': '6LdB08gaAAAAAMZmG-jcL-3_QFvffJS3Wpac7VLW'
        }))


class FormularioForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     for form in self.visible_fields():
    #         form.field.widget.attrs['class'] = 'form-control'

    captcha = ReCaptchaField(error_messages={'required': 'Complete el CAPTCHA'},
                             widget=ReCaptchaV2Checkbox(attrs={
                                'required': None
                             }))

    provincia = ModelChoiceField(queryset=Provincia.objects.all(), widget=Select(attrs={
        'class': 'form-select',
    }))

    distrito = ModelChoiceField(queryset=Distrito.objects.all(), widget=Select(attrs={
        'class': 'form-select',
    }))

    corregimiento = ModelChoiceField(queryset=Corregimiento.objects.all(), widget=Select(attrs={
        'class': 'form-select',
    }))

    class Meta:
        model = Formulario
        fields = '__all__'

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'motivo': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'tipo_doc': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'dni': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese ID',
                    'autocomplete': 'off'
                }
            ),
            'f_nacimiento': TextInput(attrs={
                'type': 'text',
                'class': 'form-control',
                'placeholder': x,
                # 'class': 'form-control datepicker',
                # 'id': 'datepicker',
                # 'placeholder': x,
                # 'value': x,
                # 'data-target': '#f_nacimiento'
            }
            ),
            'email_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'micorreo@mail.com',
                    'autocomplete': 'off'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '61234567',
                    'autocomplete': 'off'
                }
            ),
            'e_civil': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'genero': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'dependientes': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'tipo_ingreso': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '100',
                    'placeholder': '100.00'
                }
            ),
            'empresa_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'in_extras': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'estudios': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'p_expuesto': Select(
                attrs={
                    'class': 'form-select',
                    'autocomplete': 'off'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'placeholder': 'Barriada San José, Residencial/PH El Paraíso, etc.',
                }
            ),
            'ref': TextInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off'
                }
            ),
            'terms': CheckboxInput(
                attrs={
                    'class': 'form-check-input',
                    'required': 'True'
                }
            ),
            'apc': CheckboxInput(
                attrs={
                    'class': 'form-check-input',
                    'required': 'True'
                }
            ),
            'monto': NumberInput(
                attrs={
                    'type': 'range',
                    'class': 'form-range',
                    'required': 'True',
                    'min': '300',
                    'max': '3500',
                    'step': '10',
                    'id': 'entrada_1',
                    'oninput': 'myFunction()',
                    'value': '1000'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    'type': 'range',
                    'class': 'form-range',
                    'required': 'True',
                    'min': '6',
                    'max': '30',
                    'step': '1',
                    'id': 'entrada_2',
                    'oninput': 'myFunction()',
                    'value': '24'
                }
            ),
            'DNI_img': FileInput(
                attrs={
                    'type': 'file',
                    'class': 'form-control',
                    'lang': 'es'
                }
            ),
            'ficha_img': FileInput(
                attrs={
                    'type': 'file',
                    'class': 'form-control',
                    'lang': 'es'
                }
            ),
        }

        exclude = ['check_1', 'check_2', 'check_3']
