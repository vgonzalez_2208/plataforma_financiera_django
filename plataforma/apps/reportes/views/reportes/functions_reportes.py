from datetime import datetime
from functools import reduce

from django.db.models import Sum

from apps.cobros.models import Pagos
from apps.cobros.views.cobros.functions import desglose_cobros, to_num, meses_atraso
from apps.cobros.views.cobros.functions_reportes import parametros_cobro


def crear_tabla_reporte(desembolso, pagos):
    print(len(desembolso))
    print(len(pagos))
    for obj in desembolso:
        info = info_desembolso(obj)


def info_desembolso(desembolso):
    salida = {}
    if desembolso.cliente:
        info_cliente = desembolso.cliente
        salida['nombre'] = info_cliente.cliente
        salida['tipo_doc'] = info_cliente.cliente.tipo_doc
        salida['dni'] = info_cliente.cliente.dni
        salida['num_contrato'] = info_cliente.num_contrato
        salida['plazo'] = info_cliente.plazo
        salida['monto'] = info_cliente.monto
        salida['fecha_ini'] = info_cliente.fecha_ini
    elif desembolso.cliente_ant:
        info_cliente = desembolso.cliente_ant
        salida['nombre'] = info_cliente
        salida['tipo_doc'] = info_cliente.tipo_doc
        salida['dni'] = info_cliente.dni
        salida['num_contrato'] = info_cliente.num_contrato
        salida['plazo'] = info_cliente.plazo
        salida['monto'] = info_cliente.monto
        salida['ob_total'] = info_cliente.ob_contrato
        salida['fecha_ini'] = info_cliente.fecha_ini
    print(salida)
    return salida


def info_prepagos_ant(modelo_1, modelo_2):
    salida = ()
    total_capital = 0
    total_agregado = 0
    for obj in modelo_1:
        salida, total_capital, total_agregado = loop_prepagos_ant(obj, salida, total_capital, total_agregado)
    for obj in modelo_2:
        salida, total_capital, total_agregado = loop_prepagos_ant(obj, salida, total_capital, total_agregado)
    # print(total_capital)
    return [salida, to_num(total_capital, True), to_num(total_agregado, True)]


def loop_prepagos_ant(obj, salida, total_capital, total_agregado):
    pre_salida = []
    det_cliente = desglose_cobros(obj, info=4)
    prepago = obj.prepagoanticipado
    if obj.cliente:
        obj = obj.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    else:
        obj = obj.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    pre_salida.append(obj.num_contrato)
    pre_salida.append(nombre)
    pre_salida.append(str(obj.f_contrato))
    pre_salida.append(to_num(obj.monto, True))
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida.append(info_c[0])
    if obj.c_promotor:
        promotor = round(float(obj.c_promotor) * float(obj.monto) / 100, 2)
        pre_salida.append(promotor)
    else:
        promotor = 0
        pre_salida.append(promotor)
    if obj.s_descuento:
        s_descuento = round(float(obj.s_descuento) * float(obj.ob_contrato) / 100, 2)
        # print(s_descuento)
        pre_salida.append(s_descuento)
    else:
        s_descuento = 0
        pre_salida.append(0)
    pre_salida.append(12)
    timbres = round(float(obj.ob_contrato) / 1000 + 0.05, 1)
    pre_salida.append(timbres)
    admin = info_c[3] - promotor - s_descuento - timbres
    pre_salida.append(admin)
    pre_salida.append(0)
    # print(det_cliente)
    s_vida = round(meses_t * float(obj.monto) * 0.035 / 12, 2)
    s_desempleo = round(meses_t * float(obj.monto) * 0.025 / 12, 2)
    s_dental = round(1.25 * meses_t, 2)
    pre_salida.append(s_vida)
    pre_salida.append(s_desempleo)
    pre_salida.append(s_dental)
    pre_salida.append(0)
    total = round(float(obj.ob_contrato) - float(prepago.ret_int + prepago.ret_seg - prepago.penalidad), 2)
    pre_salida.append(to_num(total, True))
    salida = salida + (pre_salida,)
    total_capital += round(float(obj.monto), 2)
    total_agregado += total
    return salida, total_capital, total_agregado


def tabla_prepagos(clientes):
    salida = map(map_tabla_2, clientes)
    return ''.join(salida)


def map_tabla_2(lista):
    columna = map(tabla_colum, lista)
    salida = '<tr>' + ''.join(columna) + '</tr>'
    return salida


def context_prepagos():
    pass


def tabla_colum(val):
    salida = '<td>' + str(val) + '</td>'
    return salida


def info_cancelados(modelo):
    # for i in modelo:
    salida = ()
    total_capital = 0
    total_agregado = 0
    for obj in modelo:
        consulta_p = Pagos.objects.filter(desembolso=obj).aggregate(Sum('monto_1'))
        # if to_num(obj.cliente_ant.ob_contrato) == to_num(consulta_p['monto_1__sum']):
            # print('Son iguales los montos')
        # print(consulta_p)
        pre_salida = []
        det_cliente = desglose_cobros(obj, info=4)
        pre_salida.append(obj.cliente_ant.num_contrato)
        pre_salida.append(str(obj.cliente_ant))
        pre_salida.append(str(obj.cliente_ant.f_contrato))
        pre_salida.append(obj.cliente_ant.monto)
        meses_t = round(float(obj.cliente_ant.plazo) + det_cliente['meses_extra'], 0)
        info_c = parametros_cobro(obj.cliente_ant.monto, obj.cliente_ant.ob_contrato, meses_t, 0.02, 0)
        # print(info_c)
        pre_salida.append(info_c[0])
        if obj.cliente_ant.c_promotor:
            promotor = round(float(obj.cliente_ant.c_promotor)*float(obj.cliente_ant.monto)/100, 2)
            pre_salida.append(promotor)
        else:
            promotor = 0
            pre_salida.append(promotor)
        if obj.cliente_ant.s_descuento:
            s_descuento = round(float(obj.cliente_ant.s_descuento)*float(obj.cliente_ant.ob_contrato)/100, 2)
            # print(s_descuento)
            pre_salida.append(s_descuento)
        else:
            s_descuento = 0
            pre_salida.append(0)
        pre_salida.append(12)
        timbres = round(float(obj.cliente_ant.ob_contrato) / 1000 + 0.05, 1)
        pre_salida.append(timbres)
        admin = info_c[3] - promotor - s_descuento - timbres
        pre_salida.append(admin)
        pre_salida.append(0)
        # print(det_cliente)
        print(meses_t)
        s_vida = round(meses_t*float(obj.cliente_ant.monto)*0.035/12, 2)
        s_desempleo = round(meses_t*float(obj.cliente_ant.monto)*0.025/12, 2)
        s_dental = round(1.25*meses_t, 2)
        pre_salida.append(s_vida)
        pre_salida.append(s_desempleo)
        pre_salida.append(s_dental)
        pre_salida.append(0)
        total = to_num(consulta_p['monto_1__sum'])
        pre_salida.append(total)
        salida = salida + (pre_salida,)
        total_capital += round(float(obj.cliente_ant.monto), 2)
        total_agregado += total
        # print(obj.prepagoanticipado.penalidad)
    # print(total_capital)
    return [salida, total_capital, total_agregado]


def info_desembolsados(modelo_1, modelo_2):
    salida = ()
    total_capital = 0
    total_agregado = 0
    salida_1 = list(map(loop_desembolsos_2, modelo_1))
    salida_2 = list(map(loop_desembolsos_2, modelo_2))
    salida_test = salida_1 + salida_2
    # print(salida_test)
    val_total = list(map(lambda x: [to_num(x[3]), x[15]], salida_test))
    total_salida = reduce(reduce_def, val_total)
    salida_test_2 = list(map(map_to_num, salida_test))
    # print(salida_test_2)
    # for obj in modelo_1:
    #     salida, total_capital, total_agregado = loop_desembolsos(obj, salida, total_capital, total_agregado)
    # for obj in modelo_2:
    #     salida, total_capital, total_agregado = loop_desembolsos(obj, salida, total_capital, total_agregado)
    # print(total_capital)
    return [salida_test_2, to_num(total_salida[0], True), to_num(total_salida[1], True)]
    # return [salida, to_num(total_capital, True), to_num(total_agregado, True)]


def map_to_num(val):
    val[3] = to_num(val[3], True)
    val[15] = to_num(val[15], True)
    return val


def reduce_def(val_1, val_2):
    salida_1 = val_1[0] + val_2[0]
    salida_2 = val_1[1] + val_2[1]
    return [salida_1, salida_2]


def loop_desembolsos(modelo, salida, total_capital, total_agregado):
    pre_salida = []
    det_cliente = desglose_cobros(modelo, info=4)
    if modelo.cliente_ant:
        obj = modelo.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    else:
        obj = modelo.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    pre_salida.append(obj.num_contrato)
    pre_salida.append(str(nombre))
    pre_salida.append(str(obj.f_contrato))
    pre_salida.append(obj.monto)
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida.append(info_c[0])
    if obj.c_promotor:
        promotor = round(float(obj.c_promotor) * float(obj.monto) / 100, 2)
        pre_salida.append(promotor)
    else:
        promotor = 0
        pre_salida.append(promotor)
    if obj.s_descuento:
        s_descuento = round(float(obj.s_descuento) * float(obj.ob_contrato) / 100, 2)
        # print(s_descuento)
        pre_salida.append(s_descuento)
    else:
        s_descuento = 0
        pre_salida.append(0)
    pre_salida.append(12)
    timbres = round(float(obj.ob_contrato) / 1000 + 0.05, 1)
    pre_salida.append(timbres)
    admin = to_num(info_c[3] - promotor - s_descuento - timbres)
    pre_salida.append(admin)
    pre_salida.append(0)
    # print(det_cliente)
    # print(meses_t)
    s_vida = round(meses_t * float(obj.monto) * 0.035 / 12, 2)
    s_desempleo = round(meses_t * float(obj.monto) * 0.025 / 12, 2)
    s_dental = round(1.25 * meses_t, 2)
    pre_salida.append(s_vida)
    pre_salida.append(s_desempleo)
    pre_salida.append(s_dental)
    pre_salida.append(0)
    # print(obj.num_contrato)
    total = to_num(obj.ob_contrato)
    pre_salida.append(to_num(total, True))
    salida = salida + (pre_salida,)
    total_capital += to_num(obj.monto)
    total_agregado += to_num(total)
    return salida, total_capital, total_agregado


def loop_desembolsos_2(modelo):
    pre_salida = []
    det_cliente = desglose_cobros(modelo, info=4)
    if modelo.cliente_ant:
        obj = modelo.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    else:
        obj = modelo.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    pre_salida.append(obj.num_contrato)
    pre_salida.append(str(nombre))
    pre_salida.append(str(obj.f_contrato))
    pre_salida.append(obj.monto)
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida.append(info_c[0])
    if obj.c_promotor:
        promotor = round(float(obj.c_promotor) * float(obj.monto) / 100, 2)
        pre_salida.append(promotor)
    else:
        promotor = 0
        pre_salida.append(promotor)
    if obj.s_descuento:
        s_descuento = round(float(obj.s_descuento) * float(obj.ob_contrato) / 100, 2)
        # print(s_descuento)
        pre_salida.append(s_descuento)
    else:
        s_descuento = 0
        pre_salida.append(0)
    pre_salida.append(12)
    timbres = round(float(obj.ob_contrato) / 1000 + 0.05, 1)
    pre_salida.append(timbres)
    admin = to_num(info_c[3] - promotor - s_descuento - timbres)
    pre_salida.append(admin)
    pre_salida.append(0)
    # print(det_cliente)
    # print(meses_t)
    s_vida = round(meses_t * float(obj.monto) * 0.035 / 12, 2)
    s_desempleo = round(meses_t * float(obj.monto) * 0.025 / 12, 2)
    s_dental = round(1.25 * meses_t, 2)
    pre_salida.append(s_vida)
    pre_salida.append(s_desempleo)
    pre_salida.append(s_dental)
    pre_salida.append(0)
    # print(obj.num_contrato)
    total = to_num(obj.ob_contrato)
    pre_salida.append(total)
    return pre_salida


def cartera_total(mod_1, mod_2, mod_3, js=False):
    salida = ()
    total_capital = 0
    total_agregado = 0
    total_pagado = 0
    ult_pg = {}
    if js:
        for i in mod_1:
            ult_pg[i.desembolso_id] = i.fecha_pago_1
        for obj in mod_2:
            salida, total_capital, total_agregado, total_pagado = loop_clientes_total_js(obj, salida, total_capital, total_agregado, total_pagado, mod_3)
        return salida
    else:
        for i in mod_1:
            ult_pg[i.desembolso_id] = i.fecha_pago_1
        for obj in mod_2:
            salida, total_capital, total_agregado, total_pagado = loop_clientes_total(obj, salida, total_capital, total_agregado, total_pagado, mod_3)
        return salida, to_num(total_capital, True), to_num(total_agregado, True), to_num(total_pagado, True)


def clientes_al_dia(mod_1, mod_2, mod_3):
    salida = ()
    total_capital = 0
    total_agregado = 0
    ult_pg = {}
    for i in mod_1:
        # print(i.fecha_pago_1, i.desembolso_id)
        ult_pg[i.desembolso_id] = i.fecha_pago_1
    for obj in mod_2:
        if ult_pg.get(obj.id):
            atraso = meses_atraso(ult_pg[obj.id], datetime.now().date())
            if atraso < 2:
                salida, total_capital, total_agregado = loop_clientes(obj, salida, total_capital, total_agregado, mod_3)
        else:
            if obj.cliente:
                contrato = obj.cliente.num_contrato.split('-')
                if contrato[0] == '2021' and int(contrato[1]) >= 10:
                    salida, total_capital, total_agregado = loop_clientes(obj, salida, total_capital, total_agregado, mod_3)
    # print(salida)
    return salida, to_num(total_capital, True), to_num(total_agregado, True)


def clientes_menor_90(mod_1, mod_2, mod_3):
    # print(mod_3)
    salida = ()
    total_capital = 0
    total_agregado = 0
    ult_pg = {}
    for i in mod_1:
        # print(i.fecha_pago_1, i.desembolso_id)
        ult_pg[i.desembolso_id] = i.fecha_pago_1
    for obj in mod_2:
        if ult_pg.get(obj.id):
            atraso = meses_atraso(ult_pg[obj.id], datetime.now().date())
            if atraso in (2, 3):
                salida, total_capital, total_agregado = loop_clientes(obj, salida, total_capital, total_agregado, mod_3)
        else:
            # print(obj.id, ' este es')
            if obj.cliente_ant:
                contrato = obj.cliente_ant.num_contrato.split('-')
            else: contrato = obj.cliente.num_contrato.split('-')
            contrato_2 = contrato[0] + '-' + contrato[1]
            if contrato_2 not in ('2021-10', '2021-11', '2021-12', '2022-01'):
                salida, total_capital, total_agregado = loop_clientes(obj, salida, total_capital, total_agregado, mod_3)
        # else:
        #     if obj.cliente:
        #         contrato = obj.cliente.num_contrato.split('-')
        #         if contrato[0] == '2021' and int(contrato[1]) >= 10:
        #             salida, total_capital, total_agregado = loop_al_dia(obj, salida, total_capital, total_agregado, mod_3)
    # print(salida)
    return salida, to_num(total_capital, True), to_num(total_agregado, True)


def clientes_mayor_90(mod_1, mod_2, mod_3):
    # print(mod_3)
    salida = ()
    total_capital = 0
    total_agregado = 0
    ult_pg = {}
    for i in mod_1:
        # print(i.fecha_pago_1, i.desembolso_id)
        ult_pg[i.desembolso_id] = i.fecha_pago_1
    for obj in mod_2:
        if ult_pg.get(obj.id):
            atraso = meses_atraso(ult_pg[obj.id], datetime.now().date())
            if atraso > 3:
                salida, total_capital, total_agregado = loop_clientes(obj, salida, total_capital, total_agregado, mod_3)
        # else:
        #     if obj.cliente:
        #         contrato = obj.cliente.num_contrato.split('-')
        #         if contrato[0] == '2021' and int(contrato[1]) >= 10:
        #             salida, total_capital, total_agregado = loop_al_dia(obj, salida, total_capital, total_agregado, mod_3)
    # print(salida)
    return salida, to_num(total_capital, True), to_num(total_agregado, True)


def loop_clientes(modelo, salida, total_capital, total_agregado, mod_3):
    pre_salida = []
    det_cliente = desglose_cobros(modelo, info=4)
    if modelo.cliente_ant:
        obj = modelo.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    else:
        obj = modelo.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    pre_salida.append(obj.num_contrato)
    pre_salida.append(str(nombre))
    pre_salida.append(str(obj.f_contrato))
    pre_salida.append(obj.monto)
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida.append(info_c[0])
    if obj.c_promotor:
        promotor = round(float(obj.c_promotor) * float(obj.monto) / 100, 2)
        pre_salida.append(promotor)
    else:
        promotor = 0
        pre_salida.append(promotor)
    if obj.s_descuento:
        s_descuento = round(float(obj.s_descuento) * float(obj.ob_contrato) / 100, 2)
        # print(s_descuento)
        pre_salida.append(s_descuento)
    else:
        s_descuento = 0
        pre_salida.append(0)
    pre_salida.append(12)
    timbres = round(float(obj.ob_contrato) / 1000 + 0.05, 1)
    pre_salida.append(timbres)
    admin = to_num(info_c[3] - promotor - s_descuento - timbres)
    pre_salida.append(admin)
    pre_salida.append(0)
    # print(det_cliente)
    # print(meses_t)
    s_vida = to_num(meses_t * float(obj.monto) * 0.035 / 12)
    s_desempleo = to_num(meses_t * float(obj.monto) * 0.025 / 12)
    s_dental = to_num(1.25 * meses_t)
    seg_total = to_num(s_vida + s_desempleo + s_dental)
    pre_salida.append(seg_total)
    pre_salida.append(to_num(obj.ob_contrato, True))
    pagado = list(filter(lambda val: val['desembolso'] == modelo.id, mod_3))
    if pagado:
        # print(pagado[0]['total'])
        pre_salida.append(to_num(pagado[0]['total'], True))
        pagado_val = pagado[0]['total']
    else:
        pre_salida.append(to_num(0, True))
        pagado_val = 0
    total = to_num(obj.ob_contrato) - to_num(pagado_val)
    pre_salida.append(to_num(total, True))
    salida = salida + (pre_salida,)
    total_capital += to_num(obj.monto)
    total_agregado += to_num(total)
    return salida, total_capital, total_agregado


def loop_clientes_total(modelo, salida, total_capital, total_agregado, total_pagado, mod_3):
    pre_salida = []
    det_cliente = desglose_cobros(modelo, info=4)
    if modelo.cliente_ant:
        obj = modelo.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    else:
        obj = modelo.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    pre_salida.append(obj.num_contrato)
    pre_salida.append(str(nombre))
    pre_salida.append(str(obj.f_contrato))
    pre_salida.append(obj.monto)
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida.append(info_c[0])
    if obj.c_promotor:
        promotor = round(float(obj.c_promotor) * float(obj.monto) / 100, 2)
        pre_salida.append(promotor)
    else:
        promotor = 0
        pre_salida.append(promotor)
    if obj.s_descuento:
        s_descuento = round(float(obj.s_descuento) * float(obj.ob_contrato) / 100, 2)
        # print(s_descuento)
        pre_salida.append(s_descuento)
    else:
        s_descuento = 0
        pre_salida.append(0)
    pre_salida.append(12)
    timbres = round(float(obj.ob_contrato) / 1000 + 0.05, 1)
    pre_salida.append(timbres)
    admin = to_num(info_c[3] - promotor - s_descuento - timbres)
    pre_salida.append(admin)
    pre_salida.append(0)
    # print(det_cliente)
    # print(meses_t)
    s_vida = to_num(meses_t * float(obj.monto) * 0.035 / 12)
    s_desempleo = to_num(meses_t * float(obj.monto) * 0.025 / 12)
    s_dental = to_num(1.25 * meses_t)
    seg_total = to_num(s_vida + s_desempleo + s_dental)
    pre_salida.append(seg_total)
    pre_salida.append(to_num(obj.ob_contrato, True))
    pagado = list(filter(lambda val: val['desembolso'] == modelo.id, mod_3))
    if pagado:
        # print(pagado[0]['total'])
        pre_salida.append(to_num(pagado[0]['total'], True))
        pagado_val = pagado[0]['total']
    else:
        pre_salida.append(to_num(0, True))
        pagado_val = 0
    total = to_num(obj.ob_contrato) - to_num(pagado_val)
    pre_salida.append(to_num(total, True))
    salida = salida + (pre_salida,)
    total_capital += to_num(obj.monto)
    total_agregado += to_num(total)
    total_pagado += to_num(pagado_val)
    return salida, total_capital, total_agregado, total_pagado


def loop_clientes_total_js(modelo, salida, total_capital, total_agregado, total_pagado, mod_3):
    pre_salida = {}
    det_cliente = desglose_cobros(modelo, info=4)
    if modelo.cliente_ant:
        obj = modelo.cliente_ant
        nombre = obj.nombre_1 + ' ' + obj.apellido_1
    else:
        obj = modelo.cliente
        nombre = obj.cliente.nombre_1 + ' ' + obj.cliente.apellido_1
    pre_salida['# Contrato'] = obj.num_contrato
    pre_salida['Nombre Cliente'] = str(nombre)
    pre_salida['F. contrato'] = str(obj.f_contrato)
    pre_salida['Capital'] = obj.monto
    meses_t = round(float(obj.plazo) + det_cliente['meses_extra'], 0)
    info_c = parametros_cobro(obj.monto, obj.ob_contrato, meses_t, 0.02, 0)
    # print(info_c)
    pre_salida['Interés'] = (info_c[0])
    if obj.c_promotor:
        promotor = to_num(float(obj.c_promotor) * float(obj.monto) / 100)
        pre_salida['Promotor'] = promotor
    else:
        promotor = 0
        pre_salida['Promotor'] = promotor
    if obj.s_descuento:
        s_descuento = to_num(float(obj.s_descuento) * float(obj.ob_contrato) / 100)
        # print(s_descuento)
        pre_salida['S. descuento'] = s_descuento
    else:
        s_descuento = 0
        pre_salida['S. descuento'] = 0
    pre_salida['Notaría'] = 12
    timbres = to_num(round(float(obj.ob_contrato) / 1000 + 0.05, 1))
    pre_salida['Timbres'] = timbres
    admin = to_num(info_c[3] - promotor - s_descuento - timbres)
    pre_salida['Administración'] = admin
    pre_salida['Feci'] = 0
    # print(det_cliente)
    # print(meses_t)
    s_vida = to_num(meses_t * float(obj.monto) * 0.035 / 12)
    s_desempleo = to_num(meses_t * float(obj.monto) * 0.025 / 12)
    s_dental = to_num(1.25 * meses_t)
    seg_total = to_num(s_vida + s_desempleo + s_dental)
    pre_salida['Total seguros'] = seg_total
    pre_salida['Ob. Total'] = to_num(obj.ob_contrato, True)
    pagado = list(filter(lambda val: val['desembolso'] == modelo.id, mod_3))
    if pagado:
        # print(pagado[0]['total'])
        pre_salida['Pagado'] = to_num(pagado[0]['total'], True)
        pagado_val = pagado[0]['total']
    else:
        pre_salida['Pagado'] = to_num(0, True)
        pagado_val = 0
    total = to_num(obj.ob_contrato) - to_num(pagado_val)
    pre_salida['Saldo'] = to_num(total, True)
    salida = salida + (pre_salida,)
    total_capital += to_num(obj.monto)
    total_agregado += to_num(total)
    total_pagado += to_num(pagado_val)
    return salida, total_capital, total_agregado, total_pagado
