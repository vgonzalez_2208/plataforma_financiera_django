import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView
from weasyprint import HTML, CSS

from apps.cobros.models import Desembolso
from apps.reportes.views.reportes.functions_reportes import *

img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbwAAAD8CAYAAAAMnxEHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAMAxJREFUeNrsfT12G0mydYqHvvDsZwhcgUD7M1hYAcEVCFgBCeOzSdhjAFwBoRUIWgFBY2xCKxDaePZDr2BeBRmlLlEAUZUZkX917zkYdfd0A1WZGXEjIuPngwEAJfT++2JQ/vGl/Az4H30vP8vd/zztsDoAAPjGKZYAUCK7ovzj8c0/pn92WX6GWCEAAHzjg5JV/6389BNcD/I8NuVnW35+lJ916Y1scEyszsHPd87AWbmu2w55uTHLwzrl816u77j8Y05/KfB10/L9F5BeEF7Tw0dC/Sx0+GIBKYNV+bnvipIWUkIP7/wrk3Itlx1YhxTlgc74V/Maet5Gvr77ogiuOIeRmy+kQ5rjzMjOsGV+Q59SwMganpUCscbROaiEemxxv4cLUqgdWI4U5YHO+y19yr1c8nmPlfiuFb6TPHIQXol//ftFlgf1f/b//1/auk+a8C4yPwNkURZMfBN4fHtx00DJj2j9OrAWqcsDEfa4PO8LJr7Yko0GSh5uVwitz2tIn09s7Azek9/yv6n/7cv1T0mCV10lvF5HzgoR33OpCGaI+f/m3VXewdF/le62OhA6ykUebpj4riKLbvRBeK0JrmBDrBBYPyLHWZc9vIHpDl5Cd6USuGBvD6n2zciu7uXlTniDzM77Ixt5dxEYVyprm1vUhkmOZO2LwnmclN7dqpOEx9Z9F0GHqV++/7DLpMcJBOMW/8lF5uuRqzzQ3R6Fv6aBz7uG95yNAVYSHcniJesnDSxLslumti6SHl5XCa+y5B87Tnq3Lf99ugvtZbxeOcsDKdNB4PNeaDh4iZNcj/fmWvn8rUqymzg855g9zq++SfNEWOl3GRXp9br24lyGYKOAiszPQ+7n/VvA3/+o8J1PCZPdXfkH1b7OlcmOvGAXshvwM5LsP5R//7P8jHytkyThdU7RHyI9eHeNkXNYswvyQF76Q0YGRXIeHpEFkQbLoPaZo/UZll7ZzvJZe2wk1Z+TyPlb+f89lh91A1iS8D6D714FsVQC8668bPmudw4W5SjjpbnoyBGg7M2bAL+r4cUkc4dHyShEEsZfFx8nsmM8vvOsRHZEenMmRnh4CeGGkzhyJzvac5fi336Hk51ywlwra9Iz4W0TITsyMJ6N3yuBaUl2G4dnfmjoldO7/dTy9iQJr4Dc/4aHDtznSYRRcj03XZMHb6HNrpYkkOfDXp1U79CmmLkklzBBj1s6Ty/eXpSE18VEjYYW6E2uL8eemcT7XWbq+XYNAw5ve1nirnl3nOzxM4AhReUHdw7PPTLHWw0ewo9YPbyuZ2gewnXGITspiz5HT2iA856c9xwt4XEaf4gm5BTCnDqStI2eqO4Ll9IvJEV4fQMcskRvs3up1/tJKaXTC3D/48O7x3nXg0ZJQpQJKxzWC5EJq5GR2eZ31xovBcLTxyjDEJd0bD23bM0uy8PYg5enYSD9HSHZEdGFuBaRILtHCzmoflfN+JAiPJQkvG/1jjPy7sYKCie3e7yuy8O18vdrEOo6pgVksgulN6aOpDO30BGNyI4Td6wNZCnCQ9LK+/iSCdk1mXVnZbFn5gV3XR60FbUG4UVTdB6Y7BaOGZl3Fs9OJHfekGQpZP6N7zVbQ2Tieams/qNhZRi9uDopJCoMHhl/4aez1Duxcxae1h0NjZ5ZmQygJA+uHsjAMxGrTLXn+95ncbb7n6cPMZwdvrMLld29Lkln6PDsREJt7xs3pmH4lGvz6p2sJm3J+VTgAGoJ0VK5MS0p1ymH6G49EB+Ra7Kz8/heRjNUdcl7koMXLI1NKQtDgWcbcLRh7IH8aD+XGkus8J1RGKJMGKHIjojnyuHZbTIy25Bdb8/3Uy/OTZvwq0RIU6sI1EuIga3Qc6Mfw0+91ZR2r77C5IFoezzSwN3yM/V03kcJnZPghMfeS6i+pDv2lmyTVGx6CDcmu5r+2eeUPPLMP2+Ep+EZrb3u9iu5Xhnd1ORkFTp7BmPln8mlzZjGOzwJn/cte4xL5XOTSvZt0JKEWgp/KExsk1Qsyw9akR0bAzfvePyN1y5WwvNv4ryS3tDoXV73Elbovpph51Ce0E/ozNOYF80w8kUi3xm6JMGmXk0KM9up5ZblB23JrtfA8x00bUMmQXgaKdhPgRQAbcI9lOEfVrov7zSHCQMa8rDWtO4VjTyN8G4vsfU9ptDvTLjoz8qlbZhpX37wEklrGTptml9x06Th9EmkBzBkivAiMQWQi3dnTB73eEnJAxt504T2M5s5eHz3FaoT09a4DXJtWzpR1dltW/wGGdttkngejo0WOon0UAeLqbMC0ArzJFWfxXPOfHqlvQzGKhUKZ3KjfOaXRilxQzKMr3UloL2+kRiTrp5WnYjGlmS3afEbTUKZb9E/ZkCcOB5ALQW+NWHx3XQcvLchrM8i8TVLVRa0jLx+pN8VVNcwaYQ661OHJJVxSyKybRdme695w56zioeX61wqLYsvpTsq7TKEQ0i5zZiGPPiSha9adgDWd6/3Esq7s+6kYlFrZ0V2PD/PxRiYaxGeSpFtaK2VekcUAU+FLOlQBbAptxnrpyoPimE9SZLKRd/cBDImqUjb6r7WstbOhuwGAsZAcajfZoweXiw97aIcF+IJ88C/n2p5ggbh+UyZX0e+vsmXJLB3dx1g7apaY5tn7jPZtSHpiQXZSdYjzjUI75PCxjxFIlw700Fw0khowkm1PEFDHtYG0PTwfK9vKO/uqk2G5B4Sakt2S0uSkjIa+/saTLsSnoZFmzPRPCXwjDEMrC3g4XXb8DqApCNKAb27mc1A1VpheZt1tyI7i8zPJvgiTXgaB3ADuQ7m3Y0jIZt+olPQxZ85YMp8bGdTw5jwvb6jAN7d2qG4/KHlmV5Ykp3Evd1ew/ltxmaMSSvbSGRsYDqEgGUIOXl5vUxlAd6zDHzLl8u93YNpd7WxtEmIqdXbaRkC1yKEp1UgHFGGZJYZqO/Ad5H5MSR1j6ckD1uceVUD1Js8sqfhW76sisstuqgQ2dl2bZkbXediVO++cpKpcMSovEJYlG28u+vIHiu1TM0coh0x35Glvr5fPO+l7b3dXUuy29iSndK93b5zM5IgvJxLErQsjlg9vLmJsO1ZYm3GNM7MXx7XWuvMS8n0Rcrr69mAs7q3YwJqE3Z9mXzg4PH6mv93KUF4OZckaAjXztdQWwtFJ2ll7TQOagLQkAefBpKKcSGYFJJsRMlzOPNlmKsl2bUhIPKOh5YhU9/z/36FNV0Ir6+0WaEJoKdkjcXs3Unu3yx2JayE1OVBI+S2jdyD9rW+Pr27Sdt6O8v+mNbNp/m3+p7ls3AlvFxLErRaakVXg8chQ0lSoVmCa0kll1CbMY2ShLWnc9CPWZ4VSxLWns6GrwSsRdthrtyCy0cz6Or37kyY+/kLV8LLriRBOXljbeKDZAydBGHBISxJyzmV5BVpefDp3Wmly/+A9+wtUkFy1yq6YnmP5jJpoTDhSp/sPbyMSxLU2v54tCab7uFYWJHc1+4oJT316MsTlORh4+nZ6QyMIzfy+qmub5Mp3EKYtAkx1ppB+2gZVvXj/BZQTAcuHl52JQmcvKFlfaxMRGBPVvrublH7e8nwbQoeXsrRDq1G4TtBI6+f8Pr6aGAxazlc1Ybslg5kZ9OP89jeLdp66WR8nES0icESVpgANFNkYxsoK+3J3r/JQJU0XnoJtBlLsiShXNeRokEhGdH4nOL6Kj77b45CmxIEB7KbODyjVHH5jsn9jLu63LeV01PLH86tJOHB6Fpi0Xh4HMKS9GTfenfSyo5QmLi71CRXksDnIBUjL+WIUl/xu1u1DrMkO1qnqe0D8jDXsdB+vZ340Pa649NphJsYwrPTDJstI6u/kw7bvvXu6L5yV67tVvCcXO4h1ZigIQ875XMvGWLSNvKKlNbXg/dfYda0BMGS7Oi7h7blB3x/KREyX7zt08l3gm3PxSCmkObaeASHyR6N/h3RvYkEnFwxFlYaCw/7WZi4kUyJDpPdo9FVxGJGnlZZisckMi2jgrqpNDICLcnOqdZOKEnlpYj+QFNqKz0WU9KKF4uLBKj83JV/+Wz0L5TXkY13UffuavghvG8xJ6+Iy4NGVICNvJ8ezv3XyI0JL7qGlb5Rev5Jw2ewITtj3GrtJCIIVb3f8sD325SPFaeWQqNhcWnfWdBzf2HLwFcx88xEAoVZd+95dxoeCsXro8p2rXnNUUc72Eu6MX5qoDbC3pMGaaR+f3ffJJTpQHYTW7JjuCapbMz7oVTrpDubO7yekuK4UzocF7z43gcvRlZ759O7ewkZlXsq+XuF6Q6kwoEhjDzpEL4GaWwTPhuNsjIdyG5hW37Av3tn3K5N3iU716nxNoSnpXhuTV6YxPIgbExIKo5j3l3dU5E6L9RmrB/RvERNeaB3fXT9jgBGHnl3S+HvTLkkQQNHMyY5WcQmpGg1xLX2uyNHPd6k/MFpsosN4X00wDHMYlHMSu3S7hveMW2ECYG+axnZXmvIQ9+kmQk91TjCGsScqF5ZHptxZ9EIur4mLmTnOu7nKNkxkbt4j1ZJKwMDHLNy7yJ6nlthpdHUuyNI11bG2GYM8vCKlVIIX8OD3iW4vrtjhORAdlWCiG1GpmuSStPCducSBxvC60O23z04V7E8DBcXS09/uG+RQShtSceYqQl5sJzB1jA6oWKUJrjG9+8REt+deSc7xqODHCyakB0XsDsblyA8WVxFdsck3UmjjXdXNQOXtKZjbDMGeSjJTqm5glZGeGoe3va9RJWSDEjObe/Opi4Zmfzbtvs0aXJnyOUdIjkere7wEuhpGFro1xF5d4WRDwfdWyiLtbBnNorFQoc8vICKzLXKRTSMiXWCazw7QARVKNFWzmeOGZkubcPaTF4gUhXx9tt6eD3I90GyW0b2TNJd8Ft5dzX8EH6OmO7xui4PTokOgQjPp3cn8Vu7A8XXVdmBLdkt2zSd3vP7Iwcd05js+F5SzHBvS3iFAaInOy4yl/Y+7i1DQdIWdRHRFPQuy8PL3Y9yeFCjJOGHrwVyLN7+RUwHyMalPVzIjMw2ZNcXNtw3bcsSUJLwu8BPIyQ76Vl3Lt5dJVwaRBND15WPHT77Qw93YSnPGZTC1xoB0HrQXdaN495dOWZkPlruTdsBsg/CZ2DX1sPDncU/QjOMMIxpjM7UdlvvrkoQ0GgzFgO6KA8V2fm4Ry2UZNcn1hLPy6G9Z+OedT1sOmEhJNnx/aD0/m/aEl4fXPcSYjiPrCl05d3R/kgXmbt4d1peXizlCV2TB59kpwXfz+5KsI+l8v/J3o7reXPtkWmbkTltSXZ9o9N56y8QXnuvbhJxWrN0kbmTd1eDdAF6n8kdhOeXKM58kZ1SU+4QJQmubcwGQuds6ZiRaTs3dNl0jFENWrMam3t4HU7Bfhkrz17dOtaH5P0ZR+jdaVnVRQTr3aWoxtAzWWgovBDyG4PO2DTsZHKI7MaWumXZ9ne5gF5LtlolrXQtBZuEmzq/LxIpVJ0rfKeEd/cy+kl4cgLh0oTtq9nriAxMFOvsjnk2Gu8TwjMOvYdDB7Ijr84mI9OG7MiI1RoiQKTfKmmlMN3AhoX8v6gnZgpkx8NRCwVBWQh+n7SlG/o85i4PZEycBSI7wieF7/zh+yU4GzIk6bn0yLQtP7Ahu56R7wz1h+HRhvByT8FesYCfR5p9maR3V4P0PV7oNmO5ygMZJjHcVfcVvnMbcE1DYOIwtZzW3yYjc2kZPn0wunfi3+l/2oQ0NZTLxiHMID7vK8JZa028uxuFgyLt3RklKzdkm7Hc7vBIKc8iuqfWWN9Q8k3K9sbzb1onqThMP7AqaOc7wpGH821C3+FNbLO+SkVPFsFYUnlS0XZKjWW5yFwj5n2vsA4axET3eHcZeSC+seXIxn2Exl42c/Bohl2p1HfG372vdZJKrdZuYLG2rcOnHDadK6/HqnquNiFNcYvLMcX5SWFhYhw/8x40yhA0vLvKe5ZWqoOAbcZSJbwN7y+F7imEP42N7DIqSfhN6Xr6HdcRZXOPZFfd22nL8PfqL04bHkAN4d4KHCDpS87QmX9tlEJfKUxyr6gYNgpEURjPbcYiqQE8pvQ2tTWnWrBNzGU1Hry70O9OLcLGHn7nyqGTik3UzGWeng25OhkbTUOa0REeKeVS8WyEFyylsKZGGEDFu3vjlUt70ZfGf19NDXlYl+duaABj8ilJ+AUOa26VIwM07seK2C1H/ViTnUNtX1ss6893EvAASsTTvys8V5GAd1cYnfDrvTLZ51KAPkhkbVJFFiUJ++RL04uxHffD5NPWgK7IbmPxez7u7eqetWnr4WmEGP4WclWlkzZCeAxtoVWceVuS6a1JCy9txjzfQ8UqD7lAwwuKIWqzNDr37nT2bZNUbAvLJ5Zk5+ve7mVd3nq8TT08je70a+cT/Jr0In2Qo05c4Vl30XuhnuF7z6KUh4yQpQfNoTUNY/rKMqxoW1hOZGf7Hg/GX0nPH5PimxJezG2UpA9QjzuXxEh2WmUIqcP3uCANedhhG1XXdxvJu82Ev2/qEFa0KSyfOtT33Xg0Trf7njPYHZ5gxphGecJlpIpAo8g8B/j2eGMr0cmH6fRKEqIgPM6gXAp93cpiEoHLXDubyQd1gp17XOq9hsVJgwOooWAlrVmNEMEoQkVAh/PaAIe88sLTPmjIwxZbqOrdxWZMTAV0IJ2Z1vd2jmTnWszuC9tDXmgTD68f8wFUmqgdY1hzbro3sSJGLw+El5j3bCILF/N9m2vG5pVlobdtF5Wpw7Nqzbc7hIPEfBLoAEoLuEZ5wkUsAqI06y43+ApDoyRBFxolCU+xvSSXENjqwdb3dr5bhtV+d278Xjms3qtFbEJ4Gsz8l/RLKjxjTB7e3ABHichTmzGUJKTnQceaEGQTIlxb3qPZdDV5mYfoQHZkpPtsmr07tqZNCE/D0xG1aJXKE/oxTLXmu6nCALEYKShJgAct5eXRvrchL6s+mY4tw2zHC/lOUjFNyDmUh7dLRGl8iUAuHgwQkoxSlYdUkXNJwj7Sm7Yg5InFvZ3tVJmpA9nZjhdywapJbWCQOzylJrYa93hBw5pcZN6HDmwMH54wShL0zrtKRCWBOZeTBkbPom2xtwPZTWxr7RjfPOutrWkYHj45cgA1HlrLmtUg0WBhTb6Pwt1dRPuFkoQkvbvojQn2pKZH3qFVwboD2S1dyC5AkspLmLep53vMw+uncgDZitP47lBhzRuDMoTYvDwQXnp7l0S4mElm9o7H1fg9HMhuZVtrx787Nv4nu7fq6XmM8AaJCfg6ESFs4kmghZgdNO/xUJKgi48K3/mUystzqcJb72rWRqE7jN2h33Ahu1BJKq3CvMcIL4WShDo07vEGAQZ+guzsoXnvipIEXWRfdN6A9CY10tu0GfnDZPdguUZXDuUHtt1bXGAVej1GeJ9Tsmg5GUbjgHtLXuEyhLEBXNcwFe9xjR37BQ3DMjkPmkmvlcflSHZD2ynpjBBkZ+WNHpuHl2IK9lqBoOgeb+FpM+HdueMyISJBSYIu4W1TXIhSoZ97IDuCdfkB/7bPcT9OZNeE8MQtZaWShDq+KxDewMeQUcVZd7TmMd9lXAsbV1oenoY84A7PdLokwQk8csf27mzmmJFJvz32+Lqu5RKHCU+pTZMPa1aLUEcevDwt724Ss+CXZ+1CmExe2oxxY3GpZ0TBufIxUPjOrI0Jh2zMylO6c/jtkfGXpLIzDnP46njvDi/JjLRUyxNKhUqHr6/w1YsErFwN71Pcy4dCVoWGV56tQeFIdk7TDxwmpduAdNdQguyOEV5f6eF9QMPLU2tOrDjrjgR+loD8auyXdIJJyvKQAjRKErI0KBzJrkpSccnI9NU2jEoOzl3uGEMT3l+ezsR3pe/VytbUmnV3LxnWU8Qmgb1KWR5SgIYHnWvJh+25cSI7xqPRbxtWhTCvHJ+1FeElVZLw22rplSeIz1zjGr+x0qFZpCC9ikN8JZVosvKQCDSU6DrHheK7t3OL85NCRuaavToV3fUe4aV+Sa9x2EcKYU2tWHgq3p3mfhWSBJq4PHSR8LJdXyIuLl2YNnzPhWNG5p3Rzcjcmtfid9eaQGvCK8RPn35JQh3RhzUVZ90l493V8CNyjzx1eYgWiiUJ2XvQ7Amdmde7+kPEt+IxRLZkR0SnlUH+kmdQPt9Z2zZhYoSXSQq2ljKRVKJaab3TxLy7qD08lCToc56Sx9AJ0D0Xhzn3EZ9Ej8wHpf2hZz1zKY+Q8vCST8FWLE8QCWtykblKc+7y3ZepCS3vlzgJlOss4ZGjJEEXhYYcdG0R3xDflI3IiUNGJp37RwXDdsIe3Z10UsoxHCo872dyANdKyoqUqDWpKM+6myUss2sjn11J5QmuoZJc5CFWoCRBmPjM65WG9bUGlx88CHnfW5bBe837udgIL0QKNt3jacxnunAhPKM36y5J766GHwqEJ+E95CIPsQIlCRGhNv3AZV+I4KihxCo0yTUhvCxSsCkpoPSmdgrkQkrZKi7OZQjXSq88TVzWyMOTvhyX6IOKkgRd9JTOEmCHeQuy2/JnwwbrRrJQ3Bfh5XRJv1bwGqjGa1QqUZtQ2a2WgFs+TzRgA0XjqwtHjxxJK+l5eFhfN8P563v/QklqSRoUp+8oCHFlFugdn4xOh5RL0/JuSHnW3SwTYVsrnD/XEHRO8hCXa6c0XBlTKOzBd4BZns+TPQcwN2tWy+uxIVGtWpZ1Rgo0qjZjKElQhwbhbbGsQCPCM5mlYPPdjYYA9NqkvPO/Wyi95iyjM/mktFe25xolCbpQKc3BsgJNCS/HItBVBMIK7y4sGdh6efBAdIE5eEB2Hl7oFOwYpn1rNV2d5HQgFT1yW/QzlIeYcIH1BUISnsb9wjKwEl0pvVdoxTxJYLirDb5GdAalz80utDxEhq3C+q6wrEBTwlsKhgR2ESnlobBwrVsWeU+FlcQw8SLz9wyUOyPX/Nr1DOYqD7FgJmhUvMxRw/oCh/Dh0P/BKfSu2MTWxFjovXY2ac+cgt13JbuuCDRnSA5iOIO5ykNG+4z1BQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALTwAUsAAIAW/vXvi7FpX3u6TnXeGhA3TrEEAAAo4ouxmxICwgPEcYIlAAAAAEB4AAAAAADCAwAAAAAQHgAAAACA8AAAAAAAhAcAAAAAIDwAAAAAsEOydXj/+vevgZw0OLJX+78+1/6ehkH+ePOfrul/UNga1V5We/h2Lz+Zf4qWD+5liU25nxj8CaR27h9dv6M890Oswwum5VocHcp9msBi9FkR0udij1I8htGbv7/l762U6IY/pEypw8MWoqi2l7RvRW0v+6ZdF469e1nbz3VtLzdNBAAAAqLAEoitQyNOiJLwSuU1YoU4Mu3bErVdpKK+4OVvb1lxPpWfFTwHsb2siE5bcOp7ueO9/O7LmKlFHtoAHioAeEA0hMeK8ZJJrhfwUYhgx/x5KJ9rxQoT5JfeXvb4GUb8XOTxfS0/S8W9tAnPDE3irbQ4EtNng+NjzbixMQC+lN93wd7637w2W0RfgKQJj4XkmsmlF+kaVQpzzuQ3g+Ad3Evaxy/KXrkLqtA47eWSyE/yLpfvIruy35U3bXPN0MTo7NfI8vaNx/7EHjtC1kD8hMfC8oUVZCroVZ5f+fxrJr511w8QE91tYntpanu55b1cCp2RXPe58pYvmYh6gWSw7rHT3q3YcAH5AXERHhPdrUn/svbFuu0y8SVMdPu8CQpd3woQ3yDDfY7ZOKW9u6EPh6vvDa4egNCEl5FyPER8ZGVOuxDqZEv/lhVNTqiI75r30saI6WW0zySr1wmROD3ng3kNVxPxLUB8wFuceBCcu/KP5wzJrg4KsTzzu+ZMdkRyPzMku7eK87F81wcm9za4yMGjKz8/mTxS9Fgrg+wnkzYA6Ht4fIGfqtBYC1r53nTHMcnpToE99AfTrbohUpaj8t1pL1cd8Nxz2+Mee+xfTMOiZAAenosn8NwhstvnIWRhXXKJwbPpZpEsKc1vLby9ItE9Hme8xwXL440BQHjCgtMrP9/Kv5x3fF0r69ImLBaTIiSL/5vJOPuwhbf3mGPZAe/xQ+Z7TO8253cFQHgigvPi2Zg/2z9BUSZGemy45H7vauu5jw6sWYE9jl8e6Z1TNkKBCAivRnYDLOleRfmcinfAz/mMvTzoKXxLPVzNCr+r8jpI0QgFIiG8WvwfB+gw+iaBkFjNcOljy97Fw547oSQ8vI6T3W+kh2MMwrMhO8TFm3sH0ZJejexguDTD2zuhj6mQdQCy25rXlmDVZ1l+Zua1S0r9n3slPdzpdQ/WZQkRkR0Jyr5ZaW9RzVaT7vtnQ3rDmNKkIyO7SvE9Hfn3Ptb2MpQRQXdCP8q9XKTgMXGd6MjD/tHe0fnetj3n7IEOzD99OgtP+weA8A4eylEgstuyVfgiULadTTwL1T7So3ug8xg6QQQmu2o/nWcR8nsMavvZ9+jp7WL3jHl9bhW+esd7+F2iXpFl4jePj/XNFyWyptrZFRrCg/DeExyfZEcHseqRJ3Io3wrVm8a4PrJM+zVPLxjp1e5zfCrrakSPqJJhT4I+y9o5rZSkNvmlEBp7UJLLpfYZZiJd1SZyXAue2R6vTfKTw4Hj+GChIH96UpB0yO99N2bmd7wxfsbckLKYBCY7X6G4Je/nJsC7FqwkYy2ZGWqec+Hrhx3v411gQ21uZEsqrnx31Cnf4z8CxsAHkzgk1qGpDLX18Hx4A6QYg82cY2uVhPmOFcWtIvGFvEOYeyC7HXsBQRv5siCsM25ifgxSoUxax0no8B+fpUm5n9+NXNH8nI1sIGM0ztIsD5e2gtwwS09iiafTqJjyc2ZeM8q0FPbcd+YmE7m20ifDhe4p72LpWk/nij3qc5P4hPEWey0V0iWjZRjTXRd7ZOesO1zRR7NpEF4lNIXR65BPypCau57HOleOwzeaStLbHRB7OZqt36IzXPbsJyU80Z3NRNGQiQVfBL6D9nIa6V7SGRsKkd4XA3Sb8Dhe/k1ZOUafFszeAQnWVEFJDjyOFtLsm7gwyvdR0h58+cdZrt5eLRnLyVMXmgavuY87Jj1XA6tggxDosIc3V1KQS1aOSY3tYHKWEK63uNUWNu4OUih56XTpP01t6CY9Lxsyswzl23WvN6GSqixJ70rgq9ALuKuEx6HMscLvzjjklWQ4iUla6u7grfelRXZEplp1WMPUZ8Zx2HqSmXy7DqSdJiiXrobLJWihux6ehgKehExpFrYope4Oflnkil33bxU8dXr3s1yGa3Lo7tzkc6/nkgy1TiU0vcdw2Qb0ioEUCY8zlvoKZLfMZfGUSE/cC1Py1Kv716ySPpi8h5mQnovy/prwe88E5AXoCuHVCjtFD2FOZKdIehpenjSJbnMkuzekd5XyOwiMv0k5RL1yNFhAeB3z8Ci5QTL8tcwhjNmA9LaxERSTp6QAVwkqWafzczgv5Ts9l3DmLuX9rbUOtMUnUEO3CO9a8DeSyfQSELIrIxMKk/TypL27q1zu7BrsKUUkuthJP4f9/e7w3/ZBDR0hPL67k/LupFKFU1GQElliFZyLYDkzU9K7m6WYyOC4p9NMCKBr2Dr8t5gJ2SEPT9IjmHVt7AbX6Uncf4wF7mEk93KTc1j6mFdr8u/IkpscuhhmA6xgBwiPw2hS7vy6w4MVpVpWjR28O4kuG2+VfleVJxlt9x165cIAQAc8PMlecpOuLirf50mENl32g8hOKjQz6/qATIH6rqTgu6E5APjA6R4lKYEFFOTTolQa144eM/XY7FuupVTHCPrtBUTllxH3mMj5o3FIrsbWJnEZ/IAjC+z18HiMiIRHIOXd5ACJdWhthAiHM2e5lyC0IRHTkbFCRuYOGQCi9fCkPIIlFOQvBbkslcaloyFhUxMkRXbbHJsFCBgxRSLPunZ41qr5xARbDuRIeFJK8h7L+hvphUj2kDJevmIH//TySiNma9Ko1do4kjN5eU8weoBc8BLS5AtqifDFqut3d5FAygPB3V3aRt2TwHc8YBI4kBXhCSpIeASBIWi8IDT9ztok4o269pSsk94c2w7kQngXAt+1S30mGry73/AdS3mQSIhEUjnrUs95U5LeMyYJAPDwZAULcMNnGC9ekIpBIBl+pejBY0l6D6jTA5IkPO63KBECe8JyRgEJRbTGMuZh4HF/V+n9HJcf8vYecb8HpObh9bukAEB4WXkvIYmEwpqpFGZrlRYU5vV+73/Z6xvhZAAx49TIhDO3SHAID8EwE6YDNPeEow/tUeZ0eTaofvBW6Sd67PWNubsLGb8U8Vl3ZZQUkA7hfRQSfCA8RDrlQEk1xo+EPNI7boLgg6BH/CEjrBrGCgIEoiA8CQH4C0sZBSS8dSik5tgm9rzD8vPT+J331ntDgJWBvGES3KB21x7lev4Hq9CO8CQADy8fgPCae02uDZp9P++ufF4ivUcTdshpwZ+bmhf4iwBBgoAm4Ul4Bbi/iwOfBL7jbyxj67OfTJNlCilGQnpvvcCirou4fRuRH4WN144DXQFAzsNDXD4a9OHhBfGIi5QemEnvjElvEPFZpg+FQm/Zk65KLKr7QBjagH/CA7LzWIDMwWRxzi3DbhJ57AF/qlAoESCV0KxgdANNcIIlAIBOE9/UvCazpEgYRH5UakFF8D+JvNEBBtAmPFhWQJexzYD0KDx4bl4L1FN9nz57fhX53XEXKQAQJTyEwIAuI5uSHJp7V37OEie+ivzI8/vJHWAKHFNAivAAoMv4lNsL1YiPhhen3jJwbF4bXj+C+AAJwsMhArqMfq4vRhMzyg+RHpEf3fWlfH1RMPF9Q6gThAcAAHCI+KhX7oLv+SryS9XzozIHuue7wc52DyhLAN6ihyUA3iO/8o8FfwyHCelzYdKJ9tAZn3Nv0SvU88HDawWkAkcDCcHFXmK92hAgZXjelZ9h+flgXkscZuwBbiN//IK9PZz5Dnl4GwGhhVcQB6gNk+tMso9YRpx9FwI0td66JZn0WL8QuXzmv+5H9Mj0LHS3N0TxejcIT8IrKAwaSMNj6RiQ9deIAKvxQDGTYA+k1x3Ckwg7fMJSRgFSKq5DPkF47bwDQI4ECz5/F/yn7zFGRHrniU1qyMHR8GY4EuFJFM5CSeaDHqVtYzxLI3zGEoiS4MrUsj/5bq1gAhz5OPvl51v5OU9o3Yap773PmX4nQhbCgC00IOzhl7L2Cqwm1imC87zhcogrToihmsCl0e3uRLrsDqufJ4jwpCz5EZYzCkjcQVxgGY9apdU9FOCPAKkQflJ+/qtGfhq4RXF6poTHoSsJiwlKMg5IGDAwXrBGSZCfeS2Enyl4fbdY5Tw9PCmvAAogDjwJfAfd42E/YeClQHzUBebOvN67SXp8Y3h5+RIelGQ+WAt9zyWWcj84nDnGSkRHfOTxXQl6e9dY2TwJT0pJfsGSBhf8jZDAj5CIdHhtsATRnv8Ve3sSUSsYNZnhlA/JulRupCRdFdwIKe1/eAOFgBC3NUjWAkq5x9+xxC7C8k/N26Mi8vIvn41brSRFrQrB7GcgBsJjrIQsGlIGUyztL7J7dPyajWlfF/RdyAu5BeHt3VNkZ8ZPertyr66Y9FxQGHSRygb15tFPQt85RijsN8JwhY2wSY1u6Zd7OcY2iu8p4If0yFicOX4NkpMyJTxSkhJ3Pz0ohd/Gprjiq411K0h6UPDyewr4w8JRr8Gbz5HwhJXkDVJ6zVzgO7YOzWy/C3p5dxAVsT0F/Hp5pNeWjgY8kAlO93gTY6HvfjCvs7G66AncCFmGXx3+2xUraAmBvS7fadnlZCTBPdV+TpJf22xpauWV4/07XdfcOKzpAFMUMvPw2BpaG7lWYwUria4pRvJspcKA1papsMfeYwOmq2QnuafaoGctLD+5hu/WAucfyI3wGDPB7+9iT7oHIQFZCXhUknvZSQNGeE9jV+5ZEh4bfwCwl/Ckklcqy+hbhzyBOyOX1HAvIOhbI5tSPeeRLQZ7miXgyQDdIjy2hu4Ff4PGbWQfDuO2alJhr41gsetM+FUfu1J2IrynqbxzYQCgQx4ewTWV9y3GOYfD2OuRJHUxg4OJU9LL63WB9BT21BdckysGGe4lPFfgMOEpeHmEeY5FzKwYH41cOIi8u6XwY0p7eYOcSU9hT72BZdfFWM2xH64riW9BFXl7eIZHbkhv9ENOpKekGMXTwtnLkybRLEkvZbIT8vIGGSaaXTrKDwgvd8JjTBR+8yGHQmYlxbhWbFSrMSQzK9Kr9T5N/X1c2wTmdm/p0lcW9XddITxWviuF36VyhYdUFSV7qRqKcaL1zGylzhS+mkjvZ+rZm4p7mpqHR8hm+CkbMS7vAu+uQx5epYQ16ljG7B30ExMg6l6iUZc10w6dlN+/ULJYaS2eU/TcyejiLOKcMoklogS5tFFzfY8nA3SH8PgSXMvzGLCijD6DkzyY8kOjRjSedcN3pj6gZcBUnnsyRgxb/88ms0GfQl12Rqnftwu1g1sboFMeXjVFeKn0DOQdzFlRDiIUmh57Ls9GJ2Vb06DYt5fk4Wn2SyQSoRDnXawhayLk8kMNESiE2c9UtiWahyfbaICf29W726KHZgcJj0FKUnPzC/b2HmLxENjC/Wl0L/GnvoWKyx6Wyj9zy8R3Ewvx1YwX2tNRzoLNe+zqyVc1l0mRXi2hzBVfDdBNwuMwyZXRC4dVGLOifAghaKwUSUmTUtTuobhUqLmLxYD55b3XPL4ghgx7dLSX/2u61TnlXmgPkyE94ezppQGywgfLA/Xs8Rk3bGmtNJM6uI3UpfF3n0P3duehPR72dnx6YBQe/877uVN8tz57cVRIHbuyHmqUoyjs75QTn2Ilu7GRG4lFxuhE+Xn/I+DJfzCJQ2IdmsrQB4eDFSKrbcPKcs2EsXNYZFKCRfm54D97nt9jGEMX98CF1rSPlAXnXH/Iyr2+nz5Jbmrc7ouGWvWXHMKV9GpXTHxbEwl47x+MbJj6TPsdQXiJEF5g0qtjy58qdXhn/gzT9c0/iQmf+a9DWvw73pxNRAculu4i1X7S2vx9YD+N+Wd6wUfey0HAZ1/Q0FRHodUkvB5HZPrCZ/ie330X8NzSu1Em5rXw/qt7dyC8MIR36rDQy/JBTWDSq8isSGRvoyM73stNuZfDCEgvtf1c1iaEb02EGZ9ESOXeToxMEkeFHnuN1+V33/M6ePP4FImuktEcp74Dpl2W5l7SMx5T6hNHlGRXJz16PqOflJQLFm+8gG2sD8qWr8bdW0V8P7msSK1DCyeTjbmcpEo+0jDOJhgYmy9OBYSJPL2dSWsqtG8QmVzF3oQ2Ik8vdkz2ZNfGriRnRvdus6g88/IM0Tknkv3BZ7/1fXutJdhn4+9OdsE1xwAI711FueJDDkW5n+yGqViNTHpnvJcDbN8fpDY9UEpCyn0U8b7u2JjxkZVLRDV+Q2CVLByTg1D3setaeBrIFCeCAkWH+cygFU8ddLdxnlqIhJ+XlOMSW/gLWzZckl2T2r6GOo+Dmid46BOC7F4iMDjiILzWAlV+SKBmHV/Xl3ZhPjK9NJUjP79m781UQGGu8yP3r+tE9hV3tX+S3RD3diA8F6G6Y6HadnBN16wcs/CO+D2GpptzwSrD5Sonhcikd24w620FsgPhSQnVmoWqK95edb8zzG1CMilI7goz7ZBnsGppuGwS29MtGzJdTdKY5WbIAAEJj4Vqx95e7nd7S1aOi5wPC7/fucn7bm/LVn+rrNoUFSfL51XHDJmqPOgO6h+Ep2ZN8t3eMDPiW7PwTHLz6o7s5STDvazCl2cOXU+2ie5pZcisMz++9J5nWl1tgPhx6lmw6KCtucaGCkeLhIlu1mXByWgviaQo7C7RzJq+q5+qIUNGDDdRn5u85gR2Xl6BAIS3R1lSmjK1ByIhi71+r5oifY+hkHv3ss/El8JeVkrway7JRYL7SWd8xb1ybxMnPhAdEJ7wasJFxDEphWvKivLSxFe862WcTQaKcpvAXtIz0qgprd6PTwl7um/3kwyBJXvw1yadgbkwTIE4Ca8mXHRIKwHr1RRmEcBb2LFlCJKT28uLgJ5ftZdrDwrQ1pPYRryflQffi9iQSdUwnUFjiK1DIxmKfrREbW4d9dSrRsFIYsOfH56UYmfBezlgAuwreEPbN3u5xqqr7GPI2YN1uV2bf+YpwjAF0ie8AwLXN//Mtau8hs/veBA7VoLVX7/09AO5RbWX1efYXhrzz/zDX14VyC34PlbE94n/rO+nqxFTfV6aUWOvgU4RHgAASZEhza5rOxF+CGIDpHGCJQAAQBmIpAAgPAAAAAAA4QEAAAAACA8AAAAAQHgAAAAAAMIDAAAAQHgAAAAAAMIDAAAAABAeAAAAAIDwAAAAAACEBwAAAABecIolAABAGdXIrbb/DQCI4v8EGABorsyDtPIo8gAAAABJRU5ErkJggg=='


class ReportesView(LoginRequiredMixin, TemplateView):
    template_name = 'reportes/reporte_cartera_view.html'

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        post = request.POST
        action = post['action']
        print(action)
        if action == 'get_tabla':
            # Enviar diccionario con data y headers
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only(
                'fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            # print(consulta_3)
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = cartera_total(consulta_1, consulta_2, consulta_3, True)
            # print(lista_clientes[0].keys())
            # print(lista_clientes)
            data['data'] = lista_clientes
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fecha_hoy = datetime.now().date()
        context['fecha_hoy'] = fecha_hoy
        context['section_name'] = 'Reportes'
        context['card_title'] = 'Reportes de créditos'
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        return context


class GenerarPdf(LoginRequiredMixin, TemplateView):
    template_name = 'reportes_js/base_reportes.html'

    # def get_template_names(self, **kwargs):
    #     tipo = self.kwargs['tipo']
    #     if tipo == 'cartera_hoy':
    #         template = 'reportes_js/base_reportes.html'
    #     elif tipo == 'prepagos':
    #         template = 'reportes_js/reporte_prepago.html'
    #     return template

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tipo = self.kwargs['tipo']
        if tipo == 'cartera_hoy':
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            lista_clientes = cartera_total(consulta_1, consulta_2, consulta_3)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(lista_clientes[0])
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            total_pagado = lista_clientes[3]
            context['nombre_reporte'] = 'CARTERA CLIENTES ACTIVOS'
            context['tab_title'] = 'Clientes activos'
        elif tipo == 'prepagos':
            consulta_1 = Desembolso.objects.filter(estatus='Anticipado').exclude(cliente_ant=None).order_by('cliente_ant__num_contrato')
            consulta_2 = Desembolso.objects.filter(estatus='Anticipado').exclude(cliente=None).order_by('cliente__num_contrato')
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = info_prepagos_ant(consulta_1, consulta_2)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'CARTERA DE PREPAGOS'
            context['tab_title'] = 'Prepagos'
        elif tipo == 'cancelados':
            consulta_1 = Desembolso.objects.filter(estatus='Cancelado').exclude(cliente_ant=None).order_by('cliente_ant__num_contrato')
            consulta_2 = Desembolso.objects.filter(estatus='Cancelado').exclude(cliente=None).order_by('cliente__num_contrato')
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = info_cancelados(consulta_1)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'CARTERA DE CANCELADOS'
            context['tab_title'] = 'Cancelados'
        elif tipo == 'desembolsos':
            consulta_1 = Desembolso.objects.exclude(cliente_ant=None).order_by('cliente_ant__num_contrato')
            consulta_2 = Desembolso.objects.exclude(cliente=None).order_by('cliente__num_contrato')
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = info_desembolsados(consulta_1, consulta_2)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'HISTÓRICO DE DESEMBOLSOS'
            context['tab_title'] = 'Desembolsos'
        elif tipo == 'al_dia':
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            # print(consulta_3)
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = clientes_al_dia(consulta_1, consulta_2, consulta_3)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(lista_clientes[0])
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'CARTERA CLIENTES AL DÍA'
            context['tab_title'] = 'Clientes al día'
        elif tipo == 'menor_90':
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            lista_clientes = clientes_menor_90(consulta_1, consulta_2, consulta_3)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'CARTERA CLIENTES ATRASO < 90 DÍAS'
            context['tab_title'] = 'Mora menor a 90'
        elif tipo == 'mayor_90':
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            lista_clientes = clientes_mayor_90(consulta_1, consulta_2, consulta_3, )
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            context['nombre_reporte'] = 'CARTERA CLIENTES ATRASO > 90 DÍAS'
            context['tab_title'] = 'Mora mayor a 90'
        fecha_hoy = datetime.now().strftime('%Y-%m-%d')
        # print(fecha_hoy)
        context['tipo'] = tipo
        context['tipo_1'] = ('prepagos', 'cancelados', 'desembolsos')
        context['tipo_2'] = ('cartera_hoy', 'al_dia', 'menor_90', 'mayor_90')
        context['tabla'] = tabla
        context['total_clientes'] = total_clientes
        context['total_capital'] = total_capital
        context['total_agregado'] = total_agregado
        context['fecha_ini'] = '2019-10-01'
        context['fecha_fin'] = fecha_hoy
        context['img'] = img
        return context


class GenerarReportes(LoginRequiredMixin, View):
    # /<str:tipo>/<str:opciones>/
    def get(self, request, *args, **kwargs):
        # Lógica para generar reportes
        # time = datetime.now()
        info_get = self.kwargs
        print(info_get)
        total_pagado = False
        if info_get['tipo'] == 'cartera_hoy':
            opciones = info_get['opciones'].split(';')
            if opciones[0] == 'ninguna':  # Estatus anticipado
                template = get_template('reportes/reporte_cartera_total.html')
                consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
                consulta_2 = Desembolso.objects.filter(estatus='Activo')
                consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
                # print(consulta_3)
                # print(len(consulta_1), len(consulta_2))
                lista_clientes = cartera_total(consulta_1, consulta_2, consulta_3)
                tabla = tabla_prepagos(lista_clientes[0])
                total_clientes = len(lista_clientes[0])
                total_capital = lista_clientes[1]
                total_agregado = lista_clientes[2]
                total_pagado = lista_clientes[3]
            # consulta_1 = Desembolso.objects.filter(estatus='Activo')
            # print('pass')
            # consulta_2 = Pagos.objects.filter(clase_pago_id=1)
            # # crear_tabla_reporte(consulta_1, consulta_2)
            # tabla = ''
        elif info_get['tipo'] == 'prepagos':
            opciones = info_get['opciones'].split(';')
            if opciones[0] == 'prepagos':  # Estatus anticipado
                template = get_template('reportes/reporte_prepago.html')
                consulta_1 = Desembolso.objects.filter(estatus='Anticipado').exclude(cliente_ant=None).order_by('cliente_ant__num_contrato')
                consulta_2 = Desembolso.objects.filter(estatus='Anticipado').exclude(cliente=None).order_by('cliente__num_contrato')
                # tabla_prepagos(consulta_1, consulta_2)
                context_prepagos()
                print(len(consulta_1), len(consulta_2))
                lista_clientes = info_prepagos_ant(consulta_1, consulta_2)
                tabla = tabla_prepagos(lista_clientes[0])
                total_clientes = len(consulta_1) + len(consulta_2)
                total_capital = lista_clientes[1]
                total_agregado = lista_clientes[2]
            else:
                tabla = ''
            # consulta_1 = Desembolso.objects.filter(estatus='Activo')
            # print('pass')
            consulta_2 = Pagos.objects.filter(clase_pago_id=1)
            # crear_tabla_reporte(consulta_1, consulta_2)
        elif info_get['tipo'] == 'cancelados':
            template = get_template('reportes/reporte_cancelados.html')
            consulta_1 = Desembolso.objects.filter(estatus='Cancelado').exclude(cliente_ant=None).order_by(
                'cliente_ant__num_contrato')
            consulta_2 = Desembolso.objects.filter(estatus='Cancelado').exclude(cliente=None).order_by(
                'cliente__num_contrato')
            # tabla_prepagos(consulta_1, consulta_2)
            context_prepagos()
            print(len(consulta_1), len(consulta_2))
            lista_clientes = info_cancelados(consulta_1)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
        elif info_get['tipo'] == 'desembolsos':
            # time = datetime.now()
            # print(time)
            template = get_template('reportes/reporte_desembolsos.html')
            consulta_1 = Desembolso.objects.exclude(cliente_ant=None).order_by('cliente_ant__num_contrato')
            consulta_2 = Desembolso.objects.exclude(cliente=None).order_by('cliente__num_contrato')
            # tabla_prepagos(consulta_1, consulta_2)
            context_prepagos()
            print(len(consulta_1), len(consulta_2))
            lista_clientes = info_desembolsados(consulta_1, consulta_2)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
            # print(datetime.now() - time)
        elif info_get['tipo'] == 'al_dia':
            template = get_template('reportes/reporte_al_dia.html')
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only(
                'fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            # print(consulta_3)
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = clientes_al_dia(consulta_1, consulta_2, consulta_3)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(lista_clientes[0])
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
        elif info_get['tipo'] == 'menor_90':
            template = get_template('reportes/reporte_menor_90.html')
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only(
                'fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            # print(consulta_3)
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = clientes_menor_90(consulta_1, consulta_2, consulta_3)
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
        elif info_get['tipo'] == 'mayor_90':
            template = get_template('reportes/reporte_mayor_90.html')
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only(
                'fecha_pago_1', 'desembolso')
            consulta_2 = Desembolso.objects.filter(estatus='Activo')
            consulta_3 = Pagos.objects.all().values('desembolso').annotate(total=Sum('monto_1'))  # Query!!
            # print(consulta_3)
            # print(len(consulta_1), len(consulta_2))
            lista_clientes = clientes_mayor_90(consulta_1, consulta_2, consulta_3, )
            tabla = tabla_prepagos(lista_clientes[0])
            total_clientes = len(consulta_1) + len(consulta_2)
            total_capital = lista_clientes[1]
            total_agregado = lista_clientes[2]
        else:
            template = get_template('reportes/reporte_pdf_horizontal.html')
            tabla = ''
        # print(self.kwargs)
        try:
            # template = get_template('cobros/desglose_prepago.html')
            context = {
                'info': 'test',
                'tabla': tabla,
                'total_clientes': total_clientes,
                'total_capital': total_capital,
                'total_agregado': total_agregado,
                'fecha_ini': '2019-10-01',
                'fecha_fin': '2021-11-30'
            }
            if total_pagado: context['total_pagado'] = total_pagado
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            # print(datetime.now() - time)
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))
