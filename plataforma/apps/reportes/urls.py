from django.urls import path

from apps.reportes.views.reportes.views import *

app_name = 'reportes'

urlpatterns = [
    path('opciones/', ReportesView.as_view(), name='reportes'),
    # Views para generación de reportes
    path('opciones/<str:tipo>/<str:opciones>/', GenerarReportes.as_view(), name='generar_reportes'),
    path('opciones/pdf/<str:tipo>/<str:opciones>/', GenerarPdf.as_view(), name='generar_pdf'),
    # path('reporte_timbres/', Morosidad.as_view(), name='morosidad_clientes'),
    # path('reporte_seguros/<int:pk>/', CobroCliente.as_view(), name='cobro_cliente'),
    # path('reporte_personalizado/', AgregarClienteAntiguo.as_view(), name='agregar_antiguo'),
]

# r'^solicitudes/agregar/(?P<class>\d+)/$' | r'^editar_antiguo/<int:pk>/(?P<class>\d+)/$'
