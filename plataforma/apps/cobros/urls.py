from django.urls import path

from apps.cobros.views.cobros.views_nod.views_nod import AplicarNuevaOD
from apps.cobros.views.cobros.views_pagos import *
from apps.cobros.views.cobros.views_reportes import *
from apps.cobros.views.cobros.views_rtt.views_rtt import *
from apps.ventas.views.solicitudes.views_proceso import SolicitudesProcesoEdit

app_name = 'cobros'

urlpatterns = [
    path('lista_clientes/', ListaClientesCobros.as_view(), name='lista_clientes'),
    path('morosidad/', Morosidad.as_view(), name='morosidad_clientes'),
    path('lista_clientes/<int:pk>/', CobroCliente.as_view(), name='cobro_cliente'),
    path('agregar_ant/', AgregarClienteAntiguo.as_view(), name='agregar_antiguo'),
    path('editar_cliente/<int:pk>/<str:tipo>/', EditClienteAntiguo.as_view(), name='editar_antiguo'),
    path('ver_cliente/<int:pk>/<str:tipo>/', VerClienteCobros.as_view(), name='ver_cliente_cobros'),
    # Editar Info de cliente 'nuevo' Aprobado
    path('editar_cliente/aprobado/<int:pk>/<str:info>/', SolicitudesProcesoEdit.as_view(), name='edit_info_cliente_nuevo'),
    # Transacciones
    path('info_tr/<int:pk>/<str:tipo>/', InfoTransacciones.as_view(), name='info_transacciones'),
    path('lista_tr/', ListaTransacciones.as_view(), name='info_transacciones'),
    # Reportes Cobros
    path('estado_cuenta/<int:pk>/<str:tipo>/', EstadoCuenta.as_view(), name='estado_cuenta'),
    path('desglose_prepago/<int:pk>/<str:tipo>/', DesglosePrepago.as_view(), name='desglose_prepago'),
    # Clientes cancelación anticipada
    path('prepago_cliente/<int:pk>/<str:tipo>/', AplicarPrepago.as_view(), name='aplicar_prepago'),
    # Proceso reestructuración
    path('reestructurar/<int:pk>/<str:tipo>/', ReestructurarCredito.as_view(), name='reestructurar_2'),
    # Clientes reestructuración
    path('lista_clientes_rtt/', ListaClientesRtt.as_view(), name='clientes_rtt'),
    path('lista_clientes_rtt/<int:pk>/', CobroClienteRtt.as_view(), name='cobro_cliente_rtt'),
    # Clientes Nueva OD
    # path('lista_clientes_rtt/', ListaClientesRtt.as_view(), name='clientes_rtt'),
    path('cliente_nueva_OD/<int:pk>/', AplicarNuevaOD.as_view(), name='nueva_OD'),
]

# r'^solicitudes/agregar/(?P<class>\d+)/$' | r'^editar_antiguo/<int:pk>/(?P<class>\d+)/$'
