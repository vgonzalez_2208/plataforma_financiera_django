import os

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from weasyprint import HTML, CSS

from apps.cobros.models import Desembolso, Pagos
from apps.cobros.views.cobros.functions_reportes import *


class EstadoCuenta(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        modelo = Desembolso.objects.get(pk=self.kwargs.get('pk'))
        if self.kwargs['tipo'] == 'new':
            info_cliente = modelo.cliente
        else:
            info_cliente = modelo.cliente_ant
        consulta_1 = Pagos.objects.filter(desembolso_id=self.kwargs['pk']).order_by('fecha_pago_1')
        if consulta_1:
            info_tabla = crear_lista_2(modelo, consulta_1, info_cliente.ob_contrato)
            tabla = crear_tabla(info_tabla)
        else:
            info_tabla = '<tr><td class="text-center" colspan="8">No se han realizado pagos.</td></tr>'
            tabla = info_tabla
        # tabla = crear_tabla(info_tabla)tabla = crear_tabla(info_tabla)
        try:
            template = get_template('cobros/estado_cuenta.html')
            context = {
                'nombre_cliente': str(info_cliente),
                'num_prestamo': str(info_cliente.num_contrato),
                'fecha_inicial': str(info_cliente.f_contrato),
                'saldo_inicial': '$ ' + f'{float(info_cliente.ob_contrato):,.2f}',
                'tabla': tabla
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))


class DesglosePrepago(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        modelo = Desembolso.objects.get(pk=self.kwargs.get('pk'))
        if self.kwargs['tipo'] == 'new':
            info_cliente = modelo.cliente
            tipo_ingreso = str(modelo.cliente.cliente.tipo_ingreso)
        else:
            info_cliente = modelo.cliente_ant
            tipo_ingreso = str(modelo.cliente_ant.tipo_ingreso)
        consulta_1 = Pagos.objects.filter(desembolso_id=self.kwargs['pk']).order_by('fecha_pago_1')
        if info_cliente.c_cierre:
            comision = int(info_cliente.c_cierre)
        else:
            if tipo_ingreso == 'Empresa privada': comision = 50
            else: comision = 40
        if consulta_1:
            info_tabla = lista_prepago(modelo, info_cliente.monto, consulta_1, info_cliente.ob_contrato, info_cliente.plazo, comision)
        else:
            pagos = {None: None}
            info_tabla = lista_prepago(modelo, info_cliente.monto, pagos, info_cliente.ob_contrato, info_cliente.plazo, comision, False)
        tabla = crear_tabla(info_tabla)
        try:
            template = get_template('cobros/desglose_prepago.html')
            context = {
                'nombre_cliente': str(info_cliente),
                'num_prestamo': str(info_cliente.num_contrato),
                'fecha_inicial': str(info_cliente.f_contrato),
                'saldo_inicial': '$ ' + f'{float(info_cliente.ob_contrato):,.2f}',
                'tabla': tabla
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('prestamos:aprobados_ver'))

