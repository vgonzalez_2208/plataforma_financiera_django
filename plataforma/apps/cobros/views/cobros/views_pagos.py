from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, TemplateView, DetailView, UpdateView, CreateView

from apps.cobros.forms import FormClienteAntiguo, FormTransacciones, FormPagosClientes, FormClienteAntiguo2, \
    FormClienteNuevo, FormPrepago, FormReestructurado2
from apps.cobros.views.cobros.functions import *
from apps.formulario.models import Formulario, Distrito, Corregimiento
from apps.ventas.views.solicitudes.views_nuevas import check_perms


class ListaClientesCobros(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = ('cobros.change_desembolso', 'cobros.view_desembolso', 'cobros.add_desembolso')
    template_name = 'cobros/1_lista_clientes.html'

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request):
        # tiempo = time.time()
        data = {}
        # try:
        # post = json.loads(request.body)
        modelo = Desembolso.objects
        post = request.POST
        action = post['action']
        print(action)
        if action == 'CargarTabla':
            data = []
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = modelo.filter(estatus='Activo')
            data = lista_cobros_rev(consulta_1, consulta_2, data)
        elif action == 'add_cliente':
            val = verificar_prestamo(post)
            if val == 0:
                data = add_cliente_antiguo(post, request.user.username, data)
            else:
                data['advertencia'] = str(val)
        elif action == 'force_add':
            data = add_cliente_antiguo(post, request.user.username, data)
        elif action == 'resumen_saldos':
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = modelo.filter(estatus='Activo')
            data['ok'] = saldos_tiempo_mora(consulta_1, consulta_2, data)
            # print(data)
        else:
            data['error'] = 'No se ha seleccionado ninguna opción.'
        # except Exception as e:
        #     print(e, 'error')
        # print(time.time() - tiempo)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Lista de clientes'
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['form'] = FormClienteAntiguo
        return context


class AgregarClienteAntiguo(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = ClienteAntiguo
    form_class = FormClienteAntiguo2
    template_name = 'cobros/agregar_antiguo.html'

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        post = request.POST
        action = post['action']
        # print(action)
        # print(post.keys())
        if action == 'consulta':
            consulta_1 = ClienteAntiguo.objects.filter(num_contrato=post['num_contrato'])
            consulta_2 = Aprobado.objects.filter(num_contrato=post['num_contrato'])
            if consulta_1 or consulta_2:
                data['stop'] = 'Ya existe ese ID de contrato'
            else:
                val = verificar_prestamo(post)
                if val == 0:
                    data['ok'] = ''
                else:
                    data['advertencia'] = str(val)
        elif action == 'agregar_antiguo':
            user = str(request.user.username)
            files = request.FILES
            form = self.get_form()
            if form.is_valid():
                # print('form válido')
                pre_save = form.save(commit=False)
                pre_save.creado_por = user
                pre_save.save()
                registrar_desembolso(pre_save.id, 2)
                files_len = len(files)
                if files_len != 0:
                    for f in files.keys():
                        files[f].name = change_pdf_name(f, pre_save)
                # print(post['tipo_ingreso'])
                if post['tipo_ingreso'] == '1':
                    save_pdf_files(files, pre_save, 'ant', 'orden_descuento')
                    save_img_files(post, pre_save, 'ant', 'orden_descuento')
                else:
                    save_pdf_files(files, pre_save, 'ant', 'f_1')
                    save_img_files(post, pre_save, 'ant', 'f_1')
                data['ok'] = ''
            else:
                data['error'] = form.errors.as_data()
        elif action == 'force_add':
            user = str(request.user.username)
            files = request.FILES
            form = self.get_form()
            if form.is_valid():
                # print('form válido')
                pre_save = form.save(commit=False)
                pre_save.creado_por = user
                pre_save.save()
                # print(pre_save.id)
                registrar_desembolso(pre_save.id, 2, 1)
                files_len = len(files)
                if files_len != 0:
                    for f in files.keys():
                        files[f].name = change_pdf_name(f, pre_save)
                if post['tipo_ingreso'] == '1':
                    save_pdf_files(files, pre_save, 'ant', 'orden_descuento')
                    save_img_files(post, pre_save, 'ant', 'orden_descuento')
                else:
                    save_pdf_files(files, pre_save, 'ant', 'f_1')
                    save_img_files(post, pre_save, 'ant', 'f_1')
                data['ok'] = ''
            else:
                data['error'] = form.errors.as_data()
        elif action == 'select_p':
            data = []
            for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                data.append({'id': i.id, 'name': i.nombre})
        elif action == 'select_d':
            data = []
            for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                data.append({'id': i.id, 'name': i.nombre})
        else:
            data['error'] = 'No se ha seleccionado ninguna opción.'
        # print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['permisos_cobros'] = ('vicente_rc', 'sum.ling', 'guillermo.lemos', 'stephanie.hernandez')
        context['permisos_documentos'] = ('vicente_rc', 'guillermo.lemos', 'stephanie.hernandez')
        context['card_title'] = 'Agregar cliente existente'
        context['section_name'] = 'Cobros'
        context['action'] = 'agregar_antiguo'
        return context


class EditClienteAntiguo(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def get_form_class(self, **kwargs):
        if self.kwargs['tipo'] == 'ant':
            form_class = FormClienteAntiguo2
        else:
            form_class = FormClienteNuevo
        return form_class

    def get_template_names(self, **kwargs):
        if self.kwargs['tipo'] == 'ant':
            template = 'cobros/edit_antiguo.html'
        else:
            template = 'cobros/edit_nuevo.html'
        return template

    def get_object(self, **kwargs):
        if self.kwargs['tipo'] == 'ant':
            salida = ClienteAntiguo.objects.get(id=self.kwargs['pk'])
        else:
            salida = Aprobado.objects.get(id=self.kwargs['pk'])
        return salida

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        data = {}
        print(self.kwargs['tipo'])
        post = request.POST
        action = post['action']
        print(post.keys())
        if action == 'editar_antiguo':
            modelo = self.object
            files = request.FILES
            print(files.keys())
            form = self.get_form()
            if form.is_valid():
                files_len = len(files)
                if files_len != 0:
                    for f in files.keys():
                        files[f].name = change_pdf_name(f, self.object)
                print(files)
                form.save()
                save_pdf_files(files, self.object)
                save_img_files(post, modelo)
            else:
                data['error'] = form.errors
                user = request.user.username
                lista_per = ('dennis.caballero', 'orbin.moreno', 'jeison.gonzalez', 'stephanie.hernandez')
                if user in lista_per:
                    files_len = len(files)
                    if files_len != 0:
                        for f in files.keys():
                            files[f].name = change_pdf_name(f, self.object)
                    # print(files)
                    # form.save()
                    save_pdf_files(files, self.object)
                    save_img_files(post, modelo)
                print(form.errors)
        elif action == 'editar_nuevo':
            modelo = self.object
            # print(post.keys())
            files = request.FILES
            # print(files.keys())
            user = request.user.username
            lista_per = ('dennis.caballero', 'orbin.moreno', 'jeison.gonzalez', 'stephanie.hernandez')
            if user in lista_per:
                files_len = len(files)
                if files_len != 0:
                    for f in files.keys():
                        files[f].name = change_pdf_name(f, modelo, 'new')
                # print(files)
                # form.save()
                save_pdf_files(files, modelo, 'new')
                save_img_files(post, modelo, 'new')
            else:
                form = self.get_form()
                if form.is_valid():
                    print('form válido')
                    files_len = len(files)
                    if files_len != 0:
                        for f in files.keys():
                            files[f].name = change_pdf_name(f, self.object, 'new')
                    print(files)
                    # form.save()
                    save_pdf_files(files, self.object, 'new')
                    save_img_files(post, modelo, 'new')
                else:
                    print(form.errors)
        elif action == 'select_p':
            data = []
            for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                data.append({'id': i.id, 'name': i.nombre})
        elif action == 'select_d':
            data = []
            for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                data.append({'id': i.id, 'name': i.nombre})
        else:
            data['error'] = 'No se ha seleccionado ninguna opción.'
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # self.object = self.get_object()
        context['permisos_cobros'] = ('vicente_rc', 'sum.ling', 'guillermo.lemos', 'stephanie.hernandez')
        docs = ('vicente_rc', 'guillermo.lemos', 'stephanie.hernandez', 'dennis.caballero', 'jeison.gonzalez', 'orbin.moreno')
        context['permisos_documentos'] = docs
        context['card_title'] = 'Editar cliente. Contrato # '
        context['section_name'] = 'Cobros'
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        if self.kwargs['tipo'] == 'ant':
            context['action'] = 'editar_antiguo'
            # context['action'] = 'editar_<i class="fas fa-cash-register"></i>antiguo'
        else:
            context['action'] = 'editar_nuevo'
            context['edit_nuevo'] = 'cobros_new_edit-' + str(self.object.id)
        return context


class VerClienteCobros(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'cobros/ver_cliente.html'

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def get_object(self, **kwargs):
        if self.kwargs['tipo'] == 'ant':
            salida = ClienteAntiguo.objects.get(id=self.kwargs['pk'])
        else:
            salida = Aprobado.objects.get(id=self.kwargs['pk'])
        return salida

    def post(self, request, **kwargs):
        self.object = self.get_object()
        modelo = self.object
        data = {}
        post = request.POST
        action = post['action']
        if action == 'notas':
            modelo.notas_user = post['notas_user']
            modelo.save()
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.object = self.get_object()
        modelo = self.object
        context['card_title'] = 'Información del cliente. Contrato # '
        context['section_name'] = 'Cobros'
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        if self.kwargs['tipo'] == 'ant':
            context['cliente'] = modelo
        else:
            context['cliente'] = modelo.cliente
        context['f_contrato'] = modelo.f_contrato
        context['aprobado'] = modelo
        return context


class CobroCliente(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = ('cobros.change_desembolso', 'cobros.view_desembolso', 'cobros.add_desembolso', 'cobros.add_pagos')
    model = Desembolso
    template_name = 'cobros/cobro_cliente.html'

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, *args, **kwargs):
        modelo = self.get_object()
        data = {}
        # try:
        post = request.POST
        # print(post)
        action = post['action']
        print(action)
        if action == 'cargar_info':  # [pagos, pagos_m, total_pagos, detalles]
            # fecha_ult_pago(modelo)
            data = desglose_cobros(modelo)
        elif action == 'agregar_tr':
            data = agregar_tr(modelo, post, data)
        elif action == 'credito_disponible':
            data = credito_disponible(modelo)
            # print(data)
        elif action == 'lista_pagos':  # Listado de pagos registrados
            data = []
            ob_total = desglose_cobros(modelo, 1)
            data = tabla_pagos(modelo, ob_total, data)
        elif action == 'cargar_notas':
            data['ok'] = notas_pagos(post['id_pago'])
        elif action == 'cargar_pago':
            data = cargar_info_pago(post['id_pago'], modelo)
        elif action == 'editar_pago':
            data = editar_pago(post, modelo)
        elif action == 'lista_cuotas':
            data = []
            cuotas_cliente = desglose_cobros(modelo, 3)
            lista_cuotas(cuotas_cliente, modelo, data)
            # print(data)
        elif action == 'pago_cuota':
            # print(post)
            user = request.user.username
            # fecha_ult = fecha_ult_pago(modelo)
            plazo = val_cuotas(modelo)
            if int(post['num_cuota']) == plazo:
                data = adv_ultimo_pago(modelo, post, data, user)
            else:
                data = pagar_cuota_2(modelo, post, data, user)
            # print(data)
        elif action == 'aplicar_ult_pago':
            user = request.user.username
            data = pagar_cuota_2(modelo, post, data, user)
            modelo.estatus = 'Cancelado'
            modelo.save()
        elif action == 'info_cuota':
            # print(post)
            data = detalles_cuota(post, modelo)
            # print(action)
        elif action == 'borrar_pago':
            # print(post)
            consulta = Pagos.objects.get(id=post['id_pago'])
            user = str(request.user.username)
            fecha = str(datetime.now().date())
            val = '// ' + str(consulta.monto_1) + ' / ' + str(consulta.fecha_pago_3)
            if modelo.lista_cambios:
                modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + val + '\n'
            else:
                modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + val + '\n'
            modelo.save()
            consulta.delete()
        elif action == 'notas_cobros':
            # print(post)
            modelo.notas = post['notas_textarea']
            modelo.save()
            data['ok'] = ''
        elif action == 'ajustar_letra':
            # print(post)
            # if post['ajuste_letra'] == '': modelo.ajuste_letra = None
            # else: modelo.ajuste_letra = post['ajuste_letra']
            user = str(request.user.username)
            data = ajuste_letra_v2(modelo, post, user)
        elif action == 'consulta_nc':
            if modelo.ajuste_NC:
                ajuste = modelo.ajuste_NC.split('/')
                data['monto_nc'] = ajuste[2]
                data['motivo_nc'] = ajuste[3]
            else:
                data['monto_nc'] = ''
                data['motivo_nc'] = ''
        elif action == 'agregar_NC':  # usuario / fecha / monto / motivo
            print(post)
            user = str(request.user.username)
            fecha = str(datetime.now().date())
            nota_c = ''
            if user in ('guillermo.lemos', 'vicente_rc', 'sum.ling'):
                nota_c += user + '/' + fecha + '/' + post['monto_nc'] + '/' + post['motivo_nc']
                if modelo.lista_cambios:
                    modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post[
                        'motivo_nc'] + '| Se agregó nota de crédito.' + '\n'
                else:
                    modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post[
                        'motivo_nc'] + '| Se agregó nota de crédito.' + '\n'
                modelo.ajuste_NC = nota_c
                modelo.save()
                if modelo.ajuste_letra in (None, ''): eliminar_pagos(modelo)
                data['ok'] = 'cumplimiento'
            else: data['advertencia'] = 'No está autorizado'
            # ajuste = modelo.ajuste_NC.split('/')
            # print(ajuste)
        elif action == 'borrar_pagos':
            consulta = Pagos.objects.filter(desembolso=modelo)
            consulta.delete()
            data['ok'] = ''
            print(post)
        elif action == 'eliminar_nc':
            if modelo.ajuste_NC:
                modelo.ajuste_NC = None
                modelo.save()
                consulta = Pagos.objects.filter(desembolso=modelo)
                if consulta:
                    consulta.delete()
                data['ok'] = ''
            else: data['advertencia'] = 'El cliente no tiene N.C. aplicada.'
        elif action == 'gen_tr_id':
            print(post)
            data = gen_tr_id(modelo, post)
        else:
            data['error'] = 'No se ha ingresado a ninguna opción.'
        # except Exception as e:
        #     print(e, ' error')
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        modelo = self.get_object()
        if modelo.cliente:
            context['tipo'] = 'new'
            context['info_cliente'] = modelo.cliente.cliente
            context['info_credito'] = modelo.cliente
            f_adj = modelo.cliente.fecha_ini.split('-')
            if len(f_adj) > 1: f_rev = f_adj[0] + '/' + f_adj[1] + '/' + f_adj[2]
            else: f_rev = modelo.cliente.fecha_ini
            context['fecha_inicio'] = datetime.strptime(f_rev, '%Y/%m/%d').date()
        else:
            context['tipo'] = 'ant'
            context['info_cliente'] = modelo.cliente_ant
            context['info_credito'] = modelo.cliente_ant
            context['fecha_inicio'] = modelo.cliente_ant.fecha_ini
            # info = desglose_cobros(modelo)
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Contrato #: '
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['cumplimiento'] = ('guillermo.lemos', 'vicente_rc', 'sum.ling')
        context['form'] = FormTransacciones
        context['form_pagos'] = FormPagosClientes
        return context


class InfoTransacciones(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'cobros/info_transacciones.html'
    model = Desembolso

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, **kwargs):
        self.object = self.get_object()
        modelo = self.object
        data = {}
        post = request.POST
        action = post['action']
        print(action)
        if action == 'CargarTabla_1':
            consulta = Pagos.objects.filter(desembolso=modelo).order_by('tr_id').distinct('tr_id')
            data = list(map(cargar_trs_2, consulta))
        elif action == 'cargar_info_tr':
            data = cargar_info_trs(post)
        elif action == 'borrar_tr':
            print(post)
            check_1 = Pagos.objects.filter(desembolso=modelo).values('id', 'tr__id_tr').last()
            check_2 = Pagos.objects.filter(tr_id=post['id_tr']).filter(desembolso=modelo).values_list('id', flat=True).last()
            print(check_2, check_1)
            if check_1['id'] > check_2:
                data['advertencia'] = check_1['tr__id_tr']
            else:
                consulta_1 = Pagos.objects.filter(tr_id=post['id_tr'])
                consulta_1.delete()
                consulta_2 = Transacciones.objects.get(id=post['id_tr'])
                data['ok'] = 'Transacción ' + consulta_2.id_tr + ' eliminada.'
                consulta_2.delete()
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.object = self.get_object()
        modelo = self.object
        context['card_title'] = 'Info. de transacciones para contrato # '
        context['section_name'] = 'Cobros'
        if self.kwargs['tipo'] == 'ant':
            context['cliente'] = modelo.cliente_ant
        else:
            context['cliente'] = modelo.cliente
        context['aprobado'] = modelo
        return context


class ListaTransacciones(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'cobros/lista_transacciones.html'

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request):
        data = {}
        post = request.POST
        action = post['action']
        print(post)
        if action == 'cargar_tr':
            consulta_1 = Pagos.objects.all()
            consulta_2 = Transacciones.objects.all()
            lista_tr = consulta_1.values_list('tr_id', flat=True).distinct('tr')
            sin_pagos = list(filter(lambda x: x.id not in lista_tr, consulta_2))
            # Total disponible en TR
            l_con_pagos = list(map(lambda x: disp_tr_list(consulta_1, x), lista_tr))
            l_sin_pagos = list(map(gen_tr_list, sin_pagos))
            # print(len(sin_pagos))
            data = l_sin_pagos + l_con_pagos
        elif action == 'cargar_info_tr':
            data = cargar_info_trs(post)
            print(data)
        elif action == 'clientes_tr':
            consulta = Pagos.objects.filter(tr_id=post['id_tr']).distinct('desembolso')
            salida = list(map(clientes_pagos_tr, consulta))
            data = {'ok': ''.join(salida)}
        else:
            data['info'] = 'No ha ingresado a ninguna opción.'
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Lista de Transacciones'
        context['active_cobros'] = 'active'
        context['active_cobros_morosidad'] = 'active'
        return context


class Morosidad(LoginRequiredMixin, ListView):
    model = Formulario
    template_name = 'cobros/2_morosidad.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {'test': 'Plataforma'}
        print(request.POST)
        # cat = Formulario.objects.get(pk=request.POST['id'])
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Clientes morosos'
        context['card_title'] = 'Lista de clientes'
        context['active_cobros'] = 'active'
        context['active_cobros_morosidad'] = 'active'
        return context


class AplicarPrepago(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'cobros/aplicar_prepago.html'
    model = Desembolso

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, **kwargs):
        data = {}
        self.object = self.get_object()
        modelo = self.object
        post = request.POST
        action = post['action']
        if action == 'saldo_cliente':
            # disponible = saldo_tr_cliente(modelo)
            # data['ok'] = disponible
            data = credito_disponible(modelo, 0, 1)
            data['total_pagado'] = cuotas_pagadas(modelo)
            data['saldo_cliente'] = saldo_sin_ajuste(modelo, data['total_pagado'])
            # print(info)
        elif action == 'aplicar_anticipado':
            # print(post)
            user = request.user.username
            data = generar_prepago(modelo, post, user)
        elif action == 'agregar_tr':
            data = agregar_tr(modelo, post, data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        # modelo = Desembolso.objects.get(pk=self.kwargs['pk'])
        self.object = self.get_object()
        modelo = self.object
        if modelo.cliente:
            context['tipo'] = 'new'
            context['info_cliente'] = modelo.cliente.cliente
            context['info_credito'] = modelo.cliente
            context['fecha_inicio'] = datetime.strptime(modelo.cliente.fecha_ini, '%Y/%m/%d').date()
            info_cr = modelo.cliente
        else:
            context['tipo'] = 'ant'
            context['info_cliente'] = modelo.cliente_ant
            context['info_credito'] = modelo.cliente_ant
            context['fecha_inicio'] = modelo.cliente_ant.fecha_ini
            info = desglose_cobros(modelo)
            info_cr = modelo.cliente_ant
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Contrato #: '
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['cumplimiento'] = ('guillermo.lemos', 'vicente_rc', 'sum.ling')
        context['form'] = FormTransacciones
        context['form_pagos'] = FormPagosClientes
        # 'letra_q', 'letra_qu', 'letra_m', 'letra_mu', 'ob_salida', 'meses_extra'
        detalles = desglose_cobros(modelo, 4)  # info=4 corresponde sólo a detalles
        monto = round(float(info_cr.monto), 2)
        plazo = round(float(info_cr.plazo), 0)
        meses_total = round(plazo + detalles['meses_extra'], 2)
        ob_total = round(float(info_cr.ob_contrato), 2)
        # [int_total, seg_vida, seg_desempleo, seg_dental, notaria, comision]
        detalles_cr = detalles_cliente_global(monto, plazo, meses_total, ob_total)
        print(detalles_cr)
        total_seguros = sum(detalles_cr[1:4])
        print(total_seguros)
        context['meses_extra'] = str(detalles['meses_extra'])
        context['cuota_regular'] = str(detalles['letra_q'])
        context['cuota_final'] = detalles['letra_qu']
        context['total_cuotas'] = plazo*2
        context['total_intereses'] = detalles_cr[0]
        context['total_seguros'] = total_seguros
        context['comision'] = detalles_cr[5]
        context['notaria'] = 12
        monto_pagado, cuotas_pagas = cuotas_pagadas(modelo, detalles['letra_q'])
        context['cuotas_pagas'] = cuotas_pagas
        context['cuotas_faltantes'] = round(plazo*2 - cuotas_pagas, 1)
        context['form_pr'] = FormPrepago
        return context


class ReestructurarCredito(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'cobros/reestructurar_directo.html'
    model = Reestructurado
    form_class = FormReestructurado2

    def has_permission(self):
        perm = ('Cobros', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def post(self, request, *args, **kwargs):
        data = {}
        post = request.POST
        action = post['action']
        modelo = Desembolso.objects.get(id=self.kwargs['pk'])
        if action == 'aplicar_rtt':
            user = request.user.username
            form = self.get_form()
            if form.is_valid():
                calcular_rtt(modelo, post)
                rtt = form.save(commit=False)
                rtt.usuario = user
                rtt.desembolso = modelo
                rtt.estatus = 'Activo'
                rtt.save()
                modelo.estatus = 'Reestructurado'
                modelo.save()
            else: print(form.errors.as_data())
            data['ok'] = ''
            print(post)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        # modelo = Desembolso.objects.get(pk=self.kwargs['pk'])
        print(self.kwargs)
        modelo = Desembolso.objects.get(id=self.kwargs['pk'])
        if modelo.cliente:
            context['tipo'] = 'new'
            context['info_cliente'] = modelo.cliente.cliente
            context['info_credito'] = modelo.cliente
            context['fecha_inicio'] = datetime.strptime(modelo.cliente.fecha_ini, '%Y/%m/%d').date()
            info_cr = modelo.cliente
        else:
            context['tipo'] = 'ant'
            context['info_cliente'] = modelo.cliente_ant
            context['info_credito'] = modelo.cliente_ant
            context['fecha_inicio'] = modelo.cliente_ant.fecha_ini
            info = desglose_cobros(modelo)
            info_cr = modelo.cliente_ant
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Contrato #: '
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['cumplimiento'] = ('guillermo.lemos', 'vicente_rc', 'sum.ling')
        # context['form'] = FormTransacciones
        # context['form_pagos'] = FormPagosClientes
        # 'letra_q', 'letra_qu', 'letra_m', 'letra_mu', 'ob_salida', 'meses_extra'
        detalles = desglose_cobros(modelo, 4)  # info=4 corresponde sólo a detalles
        monto = round(float(info_cr.monto), 2)
        plazo = round(float(info_cr.plazo), 0)
        meses_total = round(plazo + detalles['meses_extra'], 2)
        ob_total = round(float(info_cr.ob_contrato), 2)
        # [int_total, seg_vida, seg_desempleo, seg_dental, notaria, comision]
        detalles_cr = detalles_cliente_global(monto, plazo, meses_total, ob_total)
        print(detalles_cr)
        total_seguros = sum(detalles_cr[1:4])
        print(total_seguros)
        context['meses_extra'] = str(detalles['meses_extra'])
        context['cuota_regular'] = to_num(detalles['letra_q'], True)
        context['cuota_final'] = detalles['letra_qu']
        context['total_cuotas'] = plazo*2
        context['total_intereses'] = detalles_cr[0]
        context['total_seguros'] = total_seguros
        context['comision'] = to_num(detalles_cr[5], True)
        context['notaria'] = 12
        monto_pagado, cuotas_pagas = cuotas_pagadas(modelo, detalles['letra_q'])
        context['cuotas_pagas'] = cuotas_pagas
        context['cuotas_faltantes'] = round(plazo*2 - cuotas_pagas, 1)
        context['form_rtt'] = FormPrepago
        context['pagado_cliente'] = cuotas_pagadas(modelo)
        context['saldo_agregado'] = to_num(float(ob_total) - float(monto_pagado))
        # Fecha último pago
        consulta_1 = Pagos.objects.filter(desembolso=modelo).last()
        if consulta_1:
            context['fecha_ult_pago'] = consulta_1.fecha_pago_1
            m_atraso = meses_atraso(consulta_1.fecha_pago_1, datetime.now().date())
            context['tiempo_mora'] = atraso_lista_desembolso(m_atraso)
        else:
            context['fecha_ult_pago'] = '**Sin pagos**'
            m_atraso = meses_atraso(context['info_credito'].fecha_ini_OD, datetime.now().date())
            context['tiempo_mora'] = atraso_lista_desembolso(m_atraso)
        # context['fecha_ult_pago']
        # context['tiempo_mora']
        return context
