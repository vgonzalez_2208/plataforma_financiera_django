from datetime import datetime, timedelta

from apps.cobros.models import Transacciones, Pagos
from apps.cobros.views.cobros.functions import to_num, meses_atraso
from apps.ventas.views.cotizador.views import meses_extra


def desglose_cobros_rtt(cliente_rtt, info=0):
    if cliente_rtt.ajuste_letra:
        ajuste_letra = cliente_rtt.ajuste_letra
    else:
        ajuste_letra = 0
    if cliente_rtt.cliente:
        modelo = cliente_rtt.cliente
        if type(modelo.fecha_ini_OD) == str:
            fecha_ini = datetime.strptime(modelo.fecha_ini_OD, '%Y/%m/%d').date()  # Transformar a str_date
        else:
            fecha_ini = modelo.fecha_ini_OD
    else:
        modelo = cliente_rtt.cliente_ant
        fecha_ini = modelo.fecha_ini_OD
    if cliente_rtt.ajuste_NC:
        info_nc = cliente_rtt.ajuste_NC.split('/')
        ajuste_nc = round(float(info_nc[2]), 2)
    else:
        ajuste_nc = 0
    monto = to_num(modelo.monto)
    plazo = to_num(modelo.plazo)
    ob_contrato = round(float(modelo.ob_contrato) - ajuste_nc, 2)
    # print(ob_contrato)
    f_contrato = modelo.f_contrato
    if fecha_ini.day == 15 and fecha_ini.month == 1 and cliente_rtt.diciembres == 'No':
        if f_contrato <= datetime(fecha_ini.year - 1, 12, 15).date():
            dic_extra = 1
        else:
            dic_extra = 0
    else:
        dic_extra = 0
    # Se ajusta el plazo en caso de que el cliente no pague los diciembres.
    if cliente_rtt.diciembres == 'No':
        total_meses = dic_extra + plazo + meses_extra(modelo.plazo, fecha_ini)
    else:
        total_meses = plazo
    if modelo.c_cierre:
        c_cierre = float(modelo.c_cierre)
    else:
        c_cierre = 0
    if c_cierre != 0:
        comision = round(monto * c_cierre / 100, 2)
    else:
        if cliente_rtt.cliente:
            if int(modelo.cliente.tipo_ingreso_id) == 1:
                comision = round(monto * 0.5, 2)
            elif int(modelo.cliente.tipo_ingreso_id) == 2:
                comision = round(monto * 0.4, 2)
            else:
                comision = round(monto * 0.5, 2)
        else:
            if int(modelo.tipo_ingreso_id) == 1:
                comision = round(monto * 0.5, 2)
            elif int(modelo.tipo_ingreso_id) == 2:
                comision = round(monto * 0.4, 2)
            else:
                comision = round(monto * 0.5, 2)
    monto_prestamo = calcular_ob_rtt(modelo, comision)
    t_intereses = round(monto * 0.02 * total_meses, 2)
    t_seguros = round(monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25, 2)
    t_notaria = 12
    if monto_prestamo != ob_contrato:
        t_comision = ob_contrato - t_intereses - t_seguros - t_notaria - monto
        letra_q, letra_qu, letra_m, letra_mu, ob_salida = detalles_salida(ob_contrato, plazo, ajuste_letra)
        if info == 1:
            return ob_contrato
    else:
        t_comision = comision
        letra_q, letra_qu, letra_m, letra_mu, ob_salida = detalles_salida(monto_prestamo, plazo, ajuste_letra)
        if info == 1:
            return monto_prestamo
    count = 1
    fecha_temp = fecha_ini
    pagos = {}
    while count <= plazo * 2:
        if fecha_temp.month == 2:
            if count == (plazo * 2):
                pagos[str(fecha_temp)] = letra_qu
            else:
                pagos[str(fecha_temp)] = letra_q
            if fecha_temp.day == 15:
                fecha_temp = (datetime(fecha_temp.year, fecha_temp.month + 1, 1) - timedelta(days=1)).date()
            else:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month + 1, 15).date()
            count += 1
        elif fecha_temp.month == 12:
            pagos[str(fecha_temp)] = 0
            if fecha_temp.day == 15:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month, 30).date()
            else:
                fecha_temp = datetime(fecha_temp.year + 1, 1, 15).date()
        else:
            if count == (plazo * 2):
                pagos[str(fecha_temp)] = letra_qu
            else:
                pagos[str(fecha_temp)] = letra_q
            if fecha_temp.day == 15:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month, 30).date()
            else:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month + 1, 15).date()
            count += 1
    if info == 3:
        return pagos
    temp_val = 0
    pagos_m = {}
    contador = 0
    for i in pagos.items():
        # print(i[0].split('-')[1])
        contador += 1
        if float(i[0].split('-')[2]) > 15:
            pagos_m[i[0]] = float(i[1]) + temp_val
            temp_val = 0
        else:
            # pagos_m[i[0]] = i[1]
            temp_val = float(i[1])
        if contador == float(len(pagos)) and (float(i[0].split('-')[2]) == 15):
            pagos_m[i[0]] = float(i[1])
    # print(pagos_m)
    pagos_check, total_pagos = saldo_cliente(cliente_rtt, pagos)
    if info == 2:
        return pagos_check
    # print(pagos_check)
    detalles = {
        'letra_q': letra_q,
        'letra_qu': letra_qu,
        'letra_m': letra_m,
        'letra_mu': letra_mu,
        'ob_salida': ob_salida,
        'meses_extra': (total_meses - plazo),
    }
    if info == 4: return detalles
    salida = [pagos, pagos_m, total_pagos, detalles]
    return salida


def calcular_ob_rtt(modelo, c_cierre):
    int_global = to_num(modelo.int_global)
    s_colectivo = to_num(modelo.s_colectivo)
    s_desempleo = to_num(modelo.s_desempleo)
    s_dental = to_num(modelo.s_dental)
    monto_r = to_num(modelo.monto_r)
    plazo_r = to_num(modelo.plazo_r)
    ob_contrato = to_num(modelo.ob_contrato)
    fecha_ini = modelo.fecha_ini
    fecha_ini_od = modelo.fecha_ini_OD
    cargos_extra = to_num(modelo.cargos_extra)
    notaria = 12
    if modelo.diciembres == 'Si':
        int_rtt = to_num(monto_r * plazo_r * int_global / 100)
        seguros_rtt = to_num((monto_r * (s_colectivo + s_desempleo + s_dental) / 100) * plazo_r / 12)
        comision_rtt = to_num(monto_r * c_cierre)
        total_rtt = to_num(monto_r + int_rtt + seguros_rtt + comision_rtt + notaria)
        if total_rtt == ob_contrato:
            print('Bien calculado')
            print(int_rtt, 'interés')
            print(seguros_rtt, 'seguros')
            print(comision_rtt, 'comisión')
            print(total_rtt, 'total')
            return total_rtt
        else:
            return ob_contrato


def lista_cobros_rtt(modelo_1, modelo_2, salida, rtt=False):
    ult_pg = {}
    for i in modelo_1:
        # print(i.fecha_pago_1, i.desembolso_id)
        ult_pg[i.desembolso_id] = i.fecha_pago_1
    # print(ult_pg)
    if not rtt:
        for i in modelo_2:
            if i.cliente:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.cliente.num_contrato,
                    'nombre': i.cliente.cliente.nombre_1,
                    'apellido': i.cliente.cliente.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
                    'plazo': i.cliente.plazo + ' m.',
                    'celular': i.cliente.cliente.celular,
                    'id': str(i.id) + '/new/' + str(i.cliente.id),
                }
                # valor['retraso'] =
                salida.append(valor)
            elif i.cliente_ant:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.cliente_ant.num_contrato,
                    'nombre': i.cliente_ant.nombre_1,
                    'apellido': i.cliente_ant.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.cliente_ant.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.cliente_ant.monto):,.2f}',
                    'plazo': i.cliente_ant.plazo + ' m.',
                    'celular': i.cliente_ant.celular,
                    'id': str(i.id) + '/ant/' + str(i.cliente_ant.id),
                }
                salida.append(valor)
    else:
        for i in modelo_2:
            if i.desembolso.cliente:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.cliente.num_contrato,
                    'nombre': i.cliente.cliente.nombre_1,
                    'apellido': i.cliente.cliente.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
                    'plazo': i.cliente.plazo + ' m.',
                    'celular': i.cliente.cliente.celular,
                    'id': str(i.id) + '/new/' + str(i.cliente.id),
                }
                # valor['retraso'] =
                salida.append(valor)
            elif i.desembolso.cliente_ant:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.num_contrato,
                    'nombre': i.desembolso.cliente_ant.nombre_1,
                    'apellido': i.desembolso.cliente_ant.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.desembolso.cliente_ant.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.monto_r):,.2f}',
                    'plazo': str(i.plazo_r) + ' m.',
                    'celular': i.desembolso.cliente_ant.celular,
                    'id': str(i.id) + '/ant/' + str(i.desembolso.cliente_ant.id),
                }
                salida.append(valor)
    # print(salida)
    return salida


def atraso_lista_desembolso(atraso):
    salida = 0
    if atraso == 0:
        salida = '0 días'
    elif atraso == 1:
        salida = '30 días'
    elif atraso == 2:
        salida = '60 días'
    elif atraso == 3:
        salida = '90 días'
    elif atraso > 3:
        salida = '+90 días'
    return str(salida)


def fecha_ult_pago_rtt(modelo):
    if modelo.cliente:
        fecha_od = modelo.cliente.fecha_ini_OD
        plazo = int(modelo.cliente.plazo)
    else:
        fecha_od = modelo.cliente_ant.fecha_ini_OD
        plazo = int(modelo.cliente_ant.plazo)
    info = loop_ult_pago_rtt(fecha_od, plazo)
    if fecha_od.day == 15:
        if info[0] == 2:
            salida = (datetime(info[1], info[0] + 1, 1) - timedelta(days=1))
        elif info[0] == 12:
            salida = datetime(info[1] + 1, 1, 30)
        else:
            salida = datetime(info[1], info[0], 30)
    else:
        if info[0] == 11:
            salida = datetime(info[1] + 1, 1, 15)
        elif info[0] == 12:
            salida = datetime(info[1] + 1, 2, 15)
        else:
            salida = datetime(info[1], info[0] + 1, 15)
    # print(salida.date())
    # print(info, 'fecha ult pago')
    return salida.date()


def loop_ult_pago_rtt(fecha_od, plazo):
    info = [fecha_od.month, fecha_od.year]
    contador = 0
    while contador <= plazo:
        if info[0] < 12:
            contador += 1
            info[0] += 1
        else:
            info[0] = 0
            info[1] += 1
    # if info[0] == 12:
    #     info[0] = 1
    #     info[1] += 1
    return info


def agregar_tr(modelo_c, post, data, flag='cliente'):
    print(post)
    modelo = Transacciones()
    consulta_1 = Transacciones.objects.filter(id_tr=post['id_tr']).exists()
    if consulta_1:
        data['advertencia'] = 'Ya está registrada una transacción con el ID= ' + '<span class="text-warning">' + post[
            'id_tr'] + '</span>'
        return data
    if int(post['registro_tr']) == 1:
        if modelo_c.cliente:
            modelo.empresa_id = modelo_c.cliente.cliente.empresa_2_id
        else:
            modelo.empresa_id = modelo_c.cliente_ant.empresa_2_id
    else:
        modelo.cliente_id = modelo_c.id
    modelo.monto_tr = post['monto_tr']
    modelo.tipo_pago = post['tipo_pago']
    modelo.fecha_tr = post['fecha_tr']
    modelo.id_tr = post['id_tr']
    modelo.banco_id = post['banco']
    if post.get('info_tr'):
        modelo.info_tr = post['info_tr']
    if post.get('notas'):
        modelo.notas = post['notas']
    modelo.save()
    data['ok'] = ''
    return data


def credito_disponible(modelo, flag_1=0, flag_2=0):
    balance_empresa = 0
    balance_cliente = 0
    if flag_2 == 0:
        if modelo.cliente:
            empresa = modelo.cliente.cliente.empresa_2_id
            consulta_1 = Transacciones.objects.filter(empresa=empresa).filter(estatus_tr='Regular')
        else:
            empresa = modelo.cliente_ant.empresa_2_id
            consulta_1 = Transacciones.objects.filter(empresa=empresa).filter(estatus_tr='Regular')
        if consulta_1:
            consulta_3 = Pagos.objects.filter(tr__empresa=empresa)
            balance_empresa, tr_empresa = disponible_tr(consulta_1, consulta_3, 'empresa', flag_1)
        else:
            tr_empresa = []
        consulta_2 = Transacciones.objects.filter(cliente=modelo.id).filter(estatus_tr='Regular')
        if consulta_2:
            consulta_4 = Pagos.objects.filter(tr__cliente=modelo.id)
            balance_cliente, tr_cliente = disponible_tr(consulta_2, consulta_4, 'cliente', flag_1)
        else:
            tr_cliente = []
        return {'balance_empresa': balance_empresa, 'balance_cliente': balance_cliente, 'tr_empresa': tr_empresa,
                'tr_cliente': tr_cliente}
    else:
        consulta_1 = Transacciones.objects.filter(cliente=modelo).filter(estatus_tr='Regular')
        if consulta_1:
            consulta_2 = Pagos.objects.filter(tr__cliente=modelo)
            balance_cliente, tr_cliente = disponible_tr(consulta_1, consulta_2, 'cliente', flag_1)
        else:
            tr_cliente = []
        return {'balance_cliente': balance_cliente, 'tr_cliente': tr_cliente}


def tabla_pagos(cliente, ob_total, data):
    consulta = Pagos.objects.filter(desembolso=cliente).order_by('fecha_pago_1')
    if not consulta:
        # print('No hay pagos')
        return []
    saldo = ob_total
    contador = 0
    for i in consulta:
        saldo -= float(i.monto_1)
        contador += 1
        valor = {
            'contador': contador,
            'fecha': str(i.fecha_pago_1),
            'monto': '$ ' + f'{float(i.monto_1):,.2f}',
            'saldo': '$ ' + f'{float(saldo):,.2f}',
            'opciones': i.id
        }
        data.append(valor)
    # print(data)
    return data


def notas_pagos(id_pago):
    consulta = Pagos.objects.get(id=id_pago)
    if not consulta.notas:
        return '---Sin comentarios---'
    return str(consulta.notas)


def cargar_info_pago(id_pago, modelo):
    consulta = Pagos.objects.get(id=id_pago)
    if consulta:
        salida = {
            'monto_1': str(consulta.monto_1),
            'fecha_pago_2': consulta.fecha_pago_2,
            'tr_id': str(consulta.tr_id),
            'notas': consulta.notas
        }
    else:
        return {}
    # {'balance_empresa': balance_empresa, 'balance_cliente': balance_cliente, 'tr_empresa': tr_empresa, 'tr_cliente': tr_cliente}
    # {'model_id': tr.id, 'tr_id': tr.id_tr, 'empresa': str(tr.empresa), 'disponible': disponible}
    transacciones = credito_disponible(modelo, consulta.tr_id)
    # print(transacciones)
    salida['transacciones'] = transacciones
    return salida


def editar_pago(post, modelo_2):
    # if strpdate(post['fecha_tr']) > strpdate(post['fecha_pago_2']):
    #     return {'advertencia': 'La fecha de transacción no puede ser anterior a la fecha de registro.'}
    data = []
    modelo = Pagos.objects.filter(id=post['id_pago']).get()
    desembolso, fecha = modelo.desembolso, modelo.fecha_pago_1
    cuotas_cliente = desglose_cobros(modelo_2, 3)
    ult_fecha = list(cuotas_cliente)[-1]
    ajuste = 0
    if strpdate(post['fecha_pago_2']) != modelo.fecha_pago_2:
        atraso_act = meses_atraso(modelo.fecha_pago_1, modelo.fecha_pago_2)
        atraso_temp = meses_atraso(modelo.fecha_pago_1, strpdate(post['fecha_pago_2']))
        ajuste = float(modelo.monto_2) * 0.02 * (atraso_act - atraso_temp)
        # print(atraso_act, atraso_temp, ajuste, 'ajustes de mora')
    cuotas_cliente_2 = {
        str(fecha): modelo.monto_2
    }
    datos_cuota = lista_cuotas(cuotas_cliente_2, modelo_2, data, fecha, ult_fecha)
    # print(datos_cuota)
    if modelo:
        consulta = Pagos.objects.filter(desembolso=desembolso).filter(fecha_pago_1=fecha).aggregate(Sum('monto_1'))
        # print(consulta)
        max_pago = round(float(modelo.monto_2) - (float(consulta['monto_1__sum']) - float(modelo.monto_1)) + float(
            datos_cuota[0]['morosidad']) - ajuste, 2)
        # print(max_pago, 'max pago')
        if float(post['monto_1']) <= max_pago:
            data = {}
            if modelo.tr_id == int(post['tr_rel']) and modelo.monto_1 >= dec(post['monto_1']):
                registrar_pago(post, modelo, data, '', tipo=1)
            elif modelo.tr_id == int(post['tr_rel']) and modelo.monto_1 < dec(post['monto_1']):
                consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
                consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum'] + modelo.monto_1
                if disponible >= dec(post['monto_1']):
                    registrar_pago(post, modelo, data, '', tipo=1)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            else:
                consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
                consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    registrar_pago(post, modelo, data, '', tipo=1)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            return data
        else:
            return {'advertencia': 'El monto editado supera el monto pendiente: $' + str(max_pago)}


def lista_cuotas(cuotas_cliente, cliente, data, flag_1=0, flag_2=''):
    # print(cuotas_cliente)
    if flag_1 == 0:
        consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(clase_pago=1).only('fecha_pago_1', 'fecha_pago_2',
                                                                                        'monto_1')
    else:
        # print(flag_1)
        consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(clase_pago=1).filter(fecha_pago_1=flag_1).only(
            'fecha_pago_1', 'fecha_pago_2',
            'monto_1')
    # print('pass ')
    lista_1 = []
    contador = 0
    if flag_2 == '':
        ult_fecha = strpdate(list(cuotas_cliente)[-1])
    else:
        ult_fecha = strpdate(flag_2)
    # print(ult_fecha)
    if consulta_1:
        for i in consulta_1:
            lista_2 = [i.fecha_pago_1, i.fecha_pago_2, i.monto_1]
            lista_1.append(lista_2)
        ultima_pendiente = 0
        for i in cuotas_cliente.items():
            # print(cuotas_cliente.items())
            if float(i[1]) != 0:
                contador += 1
                pre_salida = [contador, i[0], i[1], i[1], 0, 0]  # Contador / fecha / cuota / pendiente / mora / estatus
                saldo = float(i[1])
                saldo_cuota = saldo
                fecha_pago = strpdate(i[0])
                f_ult_pago = 0
                morosidad = 0
                fecha_actual = datetime.now().date()
                delta_fechas = fecha_actual - fecha_pago
                if delta_fechas.days > 30:
                    pre_salida[5] = 2
                for j in lista_1:
                    if fecha_pago == j[0]:
                        # print(fecha_pago, j[1], 'aqui')
                        # if fecha_pago <= j[1]:
                        if f_ult_pago == 0:
                            if fecha_pago < ult_fecha and j[1] < ult_fecha:
                                atraso = meses_atraso(fecha_pago, j[1])
                            else:
                                atraso = meses_atraso(fecha_pago, ult_fecha)
                            f_ult_pago = fecha_pago + relativedelta(months=+atraso)
                        else:
                            if f_ult_pago < ult_fecha and j[1] < ult_fecha:
                                atraso = meses_atraso(f_ult_pago, j[1])
                            elif f_ult_pago < ult_fecha < j[1]:
                                atraso = meses_atraso(f_ult_pago, ult_fecha)
                            else:
                                atraso = 0
                            f_ult_pago = f_ult_pago + relativedelta(months=+atraso)
                        if atraso > 0:
                            morosidad_temp = round(saldo * atraso * 0.02, 2)
                            morosidad += morosidad_temp
                            if (saldo + morosidad) > float(j[2]):
                                saldo = round(saldo - float(j[2]) + morosidad_temp, 2)
                            elif (saldo + morosidad) == float(j[2]):
                                saldo = 0
                            else:
                                saldo = 'error'
                        else:
                            if saldo > float(j[2]):
                                saldo = round(saldo - float(j[2]), 2)
                            elif saldo == float(j[2]):
                                saldo = 0
                            else:
                                saldo = 'error'
                            saldo_cuota = saldo
                    else:
                        pass
                # print(saldo)
                if saldo > 0:
                    # print(f_ult_pago, datetime.now().date())
                    if f_ult_pago not in ('0', 0):
                        fecha_hoy = datetime.now().date()
                        if fecha_hoy < ult_fecha:
                            atraso = meses_atraso(f_ult_pago, fecha_hoy)
                        else:
                            atraso = meses_atraso(f_ult_pago, ult_fecha)
                        morosidad_temp = round(saldo * atraso * 0.02, 2)
                        morosidad += morosidad_temp
                        saldo += morosidad_temp
                    else:
                        fecha_hoy = datetime.now().date()
                        if fecha_hoy < ult_fecha:
                            atraso = meses_atraso(strpdate(i[0]), datetime.now().date())
                        else:
                            atraso = meses_atraso(strpdate(i[0]), ult_fecha)
                        morosidad_temp = round(saldo * atraso * 0.02, 2)
                        morosidad += morosidad_temp
                        saldo += morosidad_temp
                pre_salida[3] = saldo
                pre_salida[4] = morosidad
                if pre_salida[3] == 0:
                    pre_salida[5] = 1
                elif pre_salida[2] < pre_salida[3]:
                    if ultima_pendiente == 0:
                        pre_salida[5] = 3
                        ultima_pendiente = 1
                elif 0 < pre_salida[3] < pre_salida[2]:
                    if ultima_pendiente == 0:
                        pre_salida[5] = 3
                        ultima_pendiente = 1
                elif pre_salida[2] == pre_salida[3]:

                    if ultima_pendiente == 0:
                        ultima_pendiente = 1
                        pre_salida[5] = 3
                valor = {
                    'contador': pre_salida[0],
                    'fecha': pre_salida[1],
                    'cuota': f'{float(pre_salida[2]):,.2f}',  # '$ ' + f'{float(i.monto_1):,.2f}',
                    'pendiente': f'{float(pre_salida[3]):,.2f}',
                    'morosidad': f'{float(pre_salida[4]):,.2f}',
                    'opciones': pre_salida[5],
                    'saldo_cuota': saldo_cuota,
                }
                data.append(valor)
    else:
        ultima_pendiente = 0
        for i in cuotas_cliente.items():
            if float(i[1]) != 0:
                # print(cuotas_cliente.items())
                contador += 1
                pre_salida = [contador, i[0], i[1], i[1], 0, 0]  # Contador / fecha / cuota / pendiente / mora / estatus
                saldo_cuota = pre_salida[2]
                fecha_pago = datetime.strptime(i[0], '%Y-%m-%d').date()
                fecha_actual = datetime.now().date()
                delta_fechas = fecha_actual - fecha_pago
                if delta_fechas.days > 30:
                    pre_salida[5] = 2
                if ultima_pendiente == 0:
                    pre_salida[5] = 3
                    ultima_pendiente = 1
                pre_salida[4] = calculo_mora(strpdate(i[0]), ult_fecha, i[1])
                pre_salida[3] = round(float(pre_salida[3]) + float(pre_salida[4]), 2)
                valor = {
                    'contador': pre_salida[0],
                    'fecha': pre_salida[1],
                    'cuota': pre_salida[2],
                    'pendiente': pre_salida[3],
                    'morosidad': pre_salida[4],
                    'opciones': pre_salida[5],
                    'saldo_cuota': saldo_cuota,
                }
                data.append(valor)
    return data


def adv_ultimo_pago(modelo, post, data, user, fecha_ult):
    consulta = Pagos.objects.filter(desembolso=modelo).filter(fecha_pago_1=fecha_ult).aggregate(Sum('monto_1'))
    if consulta['monto_1__sum']:
        total_pago = to_num(post['monto_1']) + to_num(consulta['monto_1__sum'])
    else:
        total_pago = to_num(post['monto_1'])
    a_pagar = to_num(post['val_cuota']) + to_num(post['val_mora'])
    if to_num(total_pago) == to_num(a_pagar):
        salida = {'advertencia_2': 'ult_completa'}
        print('cancelado')
    else:
        salida = pagar_cuota_2(modelo, post, data, user)
        print('pago incompleto')
    # else: salida = {'stop'}
    return salida


def pagar_cuota_2(cliente, post, data, user):
    atraso = meses_atraso(strpdate(post['fecha_pago']), strpdate(post['fecha_pago_2']))
    pendiente = round(float(post['val_cuota']) * (1 + 0.02 * atraso), 2)
    if pendiente < float(post['monto_1']):
        data['advertencia'] = 'Con la fecha de registro: ' + post['fecha_pago_2'] + ' el total pendiente es: $' + str(
            pendiente)
        return data
    consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(fecha_pago_1=post['fecha_pago']).only('monto_1')
    suma = 0
    if consulta_1:
        for i in consulta_1:
            suma += i.monto_1
        total_pago = round(suma + dec(post['monto_1']), 2)
        saldo_cuota = round(float(post['val_cuota']) + float(post['val_mora']), 2)
        # print(pendiente - float(suma))
        check_pendiente = round(pendiente - float(suma), 2)
        if float(post['monto_1']) > check_pendiente:
            data['advertencia'] = 'Con la fecha de registro: ' + post[
                'fecha_pago_2'] + ' el total pendiente es: $' + str(check_pendiente)
            return data
        if float(total_pago) <= saldo_cuota:
            consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
            consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
            # print(consulta_2.monto_tr, dec(post['monto_1']))
            if consulta_2 and not consulta_3['monto_1__sum']:
                if consulta_2.monto_tr >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            elif consulta_2 and consulta_3['monto_1__sum']:
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
                # print(consulta_3)
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
    else:
        # print('pass')
        consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
        consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
        if post['fecha_pago'] == post['fecha_pago_2']:
            mora = 0
        else:
            mora = round(float(post['val_mora']), 2)
        saldo_cuota = round(float(post['val_cuota']) + mora, 2)
        if float(post['monto_1']) <= saldo_cuota:
            if consulta_2 and not consulta_3['monto_1__sum']:
                # print(consulta_2.monto_tr, dec(post['monto_1']), saldo_cuota)
                if float(consulta_2.monto_tr) >= saldo_cuota:
                    data = registrar_pago(post, cliente, data, user)
                else:
                    if round(float(post['monto_1']), 2) <= round(float(consulta_2.monto_tr), 2):
                        data = registrar_pago(post, cliente, data, user)
                    else:
                        data['advertencia'] = 'Crédito insuficiente en la transacción.'
            elif consulta_2 and consulta_3['monto_1__sum']:
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
                # print(consulta_3)
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
    return data


def detalles_cuota(post, cliente):
    salida = {}
    id_cuota = post['id_pago']
    cuota = float(post['cuota'])
    consulta_1 = Pagos.objects.filter(desembolso=cliente.id).filter(fecha_pago_1=id_cuota)
    if consulta_1:
        contador = 0
        morosidad = 0
        pendiente = cuota
        n_pagos = len(consulta_1)
        for i in consulta_1:
            contador += 1
            fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
            fecha_actual = i.fecha_pago_2
            atraso = meses_atraso(fecha_pago, fecha_actual)
            if atraso > 0:
                morosidad += round(pendiente * 0.02 * atraso, 2)
            pendiente -= float(i.monto_1)
            if contador == n_pagos:
                fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
                fecha_actual = datetime.now().date()
                atraso = meses_atraso(fecha_pago, fecha_actual)
                if atraso > 0:
                    morosidad += round(pendiente * 0.02 * atraso, 2)
            # print(morosidad)
        salida['pagos'] = 1
        salida['morosidad'] = 0
    else:
        fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
        fecha_actual = datetime.now().date()
        atraso = meses_atraso(fecha_pago, fecha_actual)
        if atraso > 0:
            morosidad = round(cuota * 0.02 * atraso, 2)
            salida['pagos'] = 0
            salida['morosidad'] = str(morosidad)
        else:
            salida['pagos'] = 0
            salida['morosidad'] = 0
    # print(salida)
    return salida


def eliminar_pagos(modelo):
    consulta = Pagos.objects.filter(desembolso=modelo)
    if consulta:
        consulta.delete()
