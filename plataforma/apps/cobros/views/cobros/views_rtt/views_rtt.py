from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
# Create your views here.
from django.views.generic import TemplateView, DetailView

from apps.cobros.forms import FormClienteAntiguo
# from apps.cobros.views.cobros.functions import *
from apps.cobros.models import Reestructurado, Pagos
from apps.cobros.views.cobros.views_rtt.functions_rtt import *


class ListaClientesRtt(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = ('cobros.change_desembolso', 'cobros.view_desembolso', 'cobros.add_desembolso')
    template_name = 'cobros/clientes_rtt/lista_rtt.html'

    @staticmethod
    def post(request):
        data = {}
        # try:
        # post = json.loads(request.body)
        modelo = Reestructurado.objects
        post = request.POST
        action = post['action']
        print(action)
        if action == 'CargarTabla':
            data = []
            consulta_1 = Pagos.objects.order_by('desembolso', '-created_date').distinct('desembolso').only('fecha_pago_1', 'desembolso')
            consulta_2 = modelo.filter(estatus='Activo')
            data = lista_cobros_rtt(consulta_1, consulta_2, data, True)
        else:
            data['error'] = 'No se ha seleccionado ninguna opción.'
        # except Exception as e:
        #     print(e, 'error')
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Lista de clientes'
        context['active_cobros'] = 'active'
        context['active_cobros_rtt'] = 'active'
        context['form'] = FormClienteAntiguo
        return context


class CobroClienteRtt(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = ('cobros.change_desembolso', 'cobros.view_desembolso', 'cobros.add_desembolso', 'cobros.add_pagos')
    model = Reestructurado
    template_name = 'cobros/clientes_rtt/cobro_rtt.html'

    def post(self, request, *args, **kwargs):
        modelo = self.get_object()
        data = {}
        # try:
        post = request.POST
        # print(post)
        action = post['action']
        print(action)
        if action == 'cargar_info':  # [pagos, pagos_m, total_pagos, detalles]
            fecha_ult_pago_rtt(modelo)
            data = desglose_cobros_rtt(modelo)
        elif action == 'agregar_tr':
            data = agregar_tr(modelo, post, data)
        elif action == 'credito_disponible':
            data = credito_disponible(modelo)
            # print(data)
        elif action == 'lista_pagos':  # Listado de pagos registrados
            data = []
            ob_total = desglose_cobros_rtt(modelo, 1)
            data = tabla_pagos(modelo, ob_total, data)
        elif action == 'cargar_notas':
            data['ok'] = notas_pagos(post['id_pago'])
        elif action == 'cargar_pago':
            data = cargar_info_pago(post['id_pago'], modelo)
        elif action == 'editar_pago':
            data = editar_pago(post, modelo)
        elif action == 'lista_cuotas':
            data = []
            cuotas_cliente = desglose_cobros_rtt(modelo, 3)
            print(cuotas_cliente)
            lista_cuotas(cuotas_cliente, modelo, data)
            # print(data)
        elif action == 'pago_cuota':
            print(post)
            user = request.user.username
            fecha_ult = fecha_ult_pago_rtt(modelo)
            if post['fecha_pago'] == str(fecha_ult):
                print('Ult pago')
                data = adv_ultimo_pago(modelo, post, data, user, fecha_ult)
            else:
                data = pagar_cuota_2(modelo, post, data, user)
            # print(data)
        elif action == 'aplicar_ult_pago':
            user = request.user.username
            data = pagar_cuota_2(modelo, post, data, user)
            modelo.estatus = 'Cancelado'
            modelo.save()
        elif action == 'info_cuota':
            # print(post)
            data = detalles_cuota(post, modelo)
            # print(action)
        elif action == 'borrar_pago':
            # print(post)
            consulta = Pagos.objects.get(id=post['id_pago'])
            user = str(request.user.username)
            fecha = str(datetime.now().date())
            val = '// ' + str(consulta.monto_1) + ' / ' + str(consulta.fecha_pago_3)
            if modelo.lista_cambios:
                modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + val + '\n'
            else:
                modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + val + '\n'
            modelo.save()
            consulta.delete()
        elif action == 'notas_cobros':
            print(post)
            modelo.notas = post['notas_textarea']
            modelo.save()
            data['ok'] = ''
        elif action == 'ajustar_letra':
            print(post)
            if post['ajuste_letra'] == '': modelo.ajuste_letra = None
            else: modelo.ajuste_letra = post['ajuste_letra']
            user = str(request.user.username)
            fecha = str(datetime.now().date())
            if modelo.lista_cambios:
                modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + '| Se cambió cuota regular del crédito.' + '\n'
            else:
                modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post['mensaje'] + '| Se cambió cuota regular del crédito.' + '\n'
            modelo.save()
            eliminar_pagos(modelo)
            data['ok'] = 'Letra ajustada'
        elif action == 'consulta_nc':
            if modelo.ajuste_NC:
                ajuste = modelo.ajuste_NC.split('/')
                data['monto_nc'] = ajuste[2]
                data['motivo_nc'] = ajuste[3]
            else:
                data['monto_nc'] = ''
                data['motivo_nc'] = ''
        elif action == 'agregar_NC':  # usuario / fecha / monto / motivo
            print(post)
            user = str(request.user.username)
            fecha = str(datetime.now().date())
            nota_c = ''
            if user in ('guillermo.lemos', 'vicente_rc', 'sum.ling'):
                nota_c += user + '/' + fecha + '/' + post['monto_nc'] + '/' + post['motivo_nc']
                if modelo.lista_cambios:
                    modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post[
                        'motivo_nc'] + '| Se agregó nota de crédito.' + '\n'
                else:
                    modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post[
                        'motivo_nc'] + '| Se agregó nota de crédito.' + '\n'
                modelo.ajuste_NC = nota_c
                modelo.save()
                if modelo.ajuste_letra in (None, ''): eliminar_pagos(modelo)
                data['ok'] = 'cumplimiento'
            else: data['advertencia'] = 'No está autorizado'
            # ajuste = modelo.ajuste_NC.split('/')
            # print(ajuste)
        elif action == 'borrar_pagos':
            consulta = Pagos.objects.filter(desembolso=modelo)
            consulta.delete()
            data['ok'] = ''
            print(post)
        else:
            data['error'] = 'No se ha ingresado a ninguna opción.'
        # except Exception as e:
        #     print(e, ' error')
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        modelo = self.get_object()
        if modelo.desembolso.cliente:
            context['tipo'] = 'new'
            context['info_cliente'] = modelo.desembolso.cliente.cliente
            context['info_credito'] = modelo.desembolso.cliente
            f_adj = modelo.desembolso.cliente.fecha_ini.split('-')
            if len(f_adj) > 1: f_rev = f_adj[0] + '/' + f_adj[1] + '/' + f_adj[2]
            else: f_rev = modelo.desembolso.cliente.fecha_ini
            context['fecha_inicio'] = datetime.strptime(f_rev, '%Y/%m/%d').date()
        else:
            context['tipo'] = 'ant'
            context['info_cliente'] = modelo.desembolso.cliente_ant
            context['info_credito'] = modelo.desembolso.cliente_ant
            context['fecha_inicio'] = modelo.desembolso.cliente_ant.fecha_ini
            # info = desglose_cobros(modelo)
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Contrato #: '
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['cumplimiento'] = ('guillermo.lemos', 'vicente_rc', 'sum.ling')
        # context['form'] = FormTransacciones
        # context['form_pagos'] = FormPagosClientes
        return context
