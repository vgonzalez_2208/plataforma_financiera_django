from datetime import datetime, timedelta
from django.core.files.base import ContentFile
import base64

from dateutil.relativedelta import relativedelta
from django.db.models import Sum

from apps.cobros.models import ClienteAntiguo, Desembolso, Transacciones, Pagos, PrepagoAnticipado, Reestructurado
from apps.prestamos.models import Aprobado
from apps.ventas.views.cotizador.views import meses_extra
from decimal import Decimal as dec, getcontext


def verificar_prestamo(post):
    salida = 0
    dic_extra = 0
    monto = float(post['monto'])
    ob_total = float(post['ob_contrato'])
    plazo = int(post['plazo'])
    if post.get('c_cierre'):
        c_cierre = float(post['c_cierre'])
    else:
        c_cierre = 0
    fecha_inicio = datetime.strptime(post['fecha_ini'], '%Y-%m-%d')
    total_meses = dic_extra + plazo + meses_extra(post['plazo'], fecha_inicio)
    if c_cierre != 0:
        comision = monto * c_cierre / 100
    else:
        if int(post['tipo_ingreso']) == 1:
            comision = monto * 0.5
        elif int(post['tipo_ingreso']) == 2:
            comision = monto * 0.4
        else:
            comision = monto * 0.5
    monto_prestamo = calcular_ob(monto, total_meses, comision)
    if monto_prestamo == ob_total:
        salida = 0
    elif monto_prestamo < ob_total:
        monto_prueba = monto_prestamo
        count = 1
        while monto_prueba < ob_total:
            monto_prueba = calcular_ob(monto, (total_meses + count), comision)
            count += 1
            if count > 4:
                break
        if monto_prueba == ob_total:
            salida = 0
        else:
            salida = monto_prestamo
    else:
        salida = monto_prestamo
    return salida


def calcular_ob(monto, total_meses, comision):
    intereses = monto * total_meses * 0.02
    s_colectivo = monto * 0.025 * total_meses / 12
    s_desempleo = monto * 0.035 * total_meses / 12
    s_dental = 1.25 * total_meses
    total_seguros = s_colectivo + s_desempleo + s_dental
    notaria = 12
    monto_prestamo = round(monto + comision + intereses + total_seguros + notaria, 2)
    return monto_prestamo


def add_cliente_antiguo(post, usuario, data):
    modelo = ClienteAntiguo()
    consulta_1 = ClienteAntiguo.objects.filter(num_contrato=post['num_contrato'])
    consulta_2 = Aprobado.objects.filter(num_contrato=post['num_contrato'])
    if consulta_1 or consulta_2:
        data['ok'] = 'Ya existe ese ID de contrato'
        return data
    modelo.nombre_1 = post['nombre_1']
    if post.get('nombre_2'):
        modelo.nombre_2 = post['nombre_2']
    modelo.apellido_1 = post['apellido_1']
    if post.get('apellido_2'):
        modelo.apellido_2 = post['apellido_2']
    modelo.tipo_doc_id = post['tipo_doc']
    modelo.dni = post['dni']
    modelo.f_nacimiento = post['f_nacimiento']
    if post.get('nacionalidad'):
        modelo.nacionalidad = post['nacionalidad']
    modelo.celular = post['celular']
    if post.get('telefono'):
        modelo.telefono = post['telefono']
    if post.get('email_1'):
        modelo.email_1 = post['email_1']
    modelo.monto = post['monto']
    modelo.plazo = post['plazo']
    modelo.fecha_ini = post['fecha_ini']
    if post.get('fecha_ini_OD'):
        modelo.fecha_ini_OD = post['fecha_ini_OD']
    modelo.f_contrato = post['f_contrato']
    if post.get('cancelaciones'):
        modelo.cancelaciones = post['cancelaciones']
    if post.get('promotor'):
        modelo.promotor = post['promotor']
    modelo.ob_contrato = post['ob_contrato']
    if post.get('c_cierre'):
        modelo.c_cierre = post['c_cierre']
    modelo.empresa_2_id = post['empresa_2']
    if post.get('salario'):
        modelo.salario = post['salario']
    modelo.tipo_ingreso_id = post['tipo_ingreso']
    if post.get('notas'):
        modelo.notas = post['notas']
    modelo.num_contrato = post['num_contrato']
    modelo.creado_por = usuario
    data['ok'] = ''
    modelo.save()
    # print(modelo.id)
    if post['action'] == 'force_add':
        registrar_desembolso(modelo.id, 2, 1)
    else:
        registrar_desembolso(modelo.id, 2)
    return data


def registrar_desembolso(id_cliente, tipo, ajuste=0):
    modelo = Desembolso()
    if tipo == 1:
        modelo.cliente_id = id_cliente
    elif tipo == 2:
        modelo.cliente_ant_id = id_cliente
    if ajuste == 1:
        modelo.ajuste = 'Si'
    modelo.save()


def desglose_cobros(desembolso, info=0):
    if desembolso.ajuste_letra:
        # ajuste_letra = 0
        ajuste_letra = desembolso.ajuste_letra
    else:
        ajuste_letra = 0
    if desembolso.cliente:
        modelo = desembolso.cliente
        if type(modelo.fecha_ini_OD) == str:
            fecha_ini = datetime.strptime(modelo.fecha_ini_OD, '%Y/%m/%d').date()  # Transformar a str_date
        else:
            fecha_ini = modelo.fecha_ini_OD
    else:
        modelo = desembolso.cliente_ant
        fecha_ini = modelo.fecha_ini_OD
    if desembolso.ajuste_NC:
        info_nc = desembolso.ajuste_NC.split('/')
        ajuste_nc = round(float(info_nc[2]), 2)
    else:
        ajuste_nc = 0
    monto = float(modelo.monto)
    plazo = float(modelo.plazo)
    ob_contrato = round(float(modelo.ob_contrato) - ajuste_nc, 2)
    # print(ob_contrato)
    f_contrato = modelo.f_contrato
    if fecha_ini.day == 15 and fecha_ini.month == 1:
        if f_contrato <= datetime(fecha_ini.year - 1, 12, 15).date():
            dic_extra = 1
        else:
            dic_extra = 0
    else:
        dic_extra = 0
    total_meses = dic_extra + plazo + meses_extra(modelo.plazo, fecha_ini)
    if modelo.c_cierre:
        c_cierre = float(modelo.c_cierre)
    else:
        c_cierre = 0
    if c_cierre != 0:
        comision = round(monto * c_cierre / 100, 2)
    else:
        if desembolso.cliente:
            if int(modelo.cliente.tipo_ingreso_id) == 1:
                comision = round(monto * 0.5, 2)
            elif int(modelo.cliente.tipo_ingreso_id) == 2:
                comision = round(monto * 0.4, 2)
            else:
                comision = round(monto * 0.5, 2)
        else:
            if int(modelo.tipo_ingreso_id) == 1:
                comision = round(monto * 0.5, 2)
            elif int(modelo.tipo_ingreso_id) == 2:
                comision = round(monto * 0.4, 2)
            else:
                comision = round(monto * 0.5, 2)
    monto_prestamo = calcular_ob(monto, total_meses, comision)
    t_intereses = round(monto * 0.02 * total_meses, 2)
    t_seguros = round(monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25, 2)
    t_notaria = 12
    if monto_prestamo != ob_contrato:
        t_comision = ob_contrato - t_intereses - t_seguros - t_notaria - monto
        letra_q, letra_qu, letra_m, letra_mu, ob_salida = detalles_salida(ob_contrato, plazo, ajuste_letra)
        if info == 1:
            return ob_contrato
    else:
        t_comision = comision
        letra_q, letra_qu, letra_m, letra_mu, ob_salida = detalles_salida(monto_prestamo, plazo, ajuste_letra)
        if info == 1:
            return monto_prestamo
    count = 1
    fecha_temp = fecha_ini
    pagos = {}
    while count <= plazo * 2:
        if fecha_temp.month == 2:
            if count == (plazo * 2):
                pagos[str(fecha_temp)] = letra_qu
            else:
                pagos[str(fecha_temp)] = letra_q
            if fecha_temp.day == 15:
                fecha_temp = (datetime(fecha_temp.year, fecha_temp.month + 1, 1) - timedelta(days=1)).date()
            else:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month + 1, 15).date()
            count += 1
        elif fecha_temp.month == 12:
            pagos[str(fecha_temp)] = 0
            if fecha_temp.day == 15:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month, 30).date()
            else:
                fecha_temp = datetime(fecha_temp.year + 1, 1, 15).date()
        else:
            if count == (plazo * 2):
                pagos[str(fecha_temp)] = letra_qu
            else:
                pagos[str(fecha_temp)] = letra_q
            if fecha_temp.day == 15:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month, 30).date()
            else:
                fecha_temp = datetime(fecha_temp.year, fecha_temp.month + 1, 15).date()
            count += 1
    if info == 3:
        if desembolso.ajuste_letra_2:
            pagos = ajuste_cuotas_cliente(desembolso, pagos)
        return pagos
    temp_val = 0
    pagos_m = {}
    contador = 0
    for i in pagos.items():
        # print(i[0].split('-')[1])
        contador += 1
        if float(i[0].split('-')[2]) > 15:
            pagos_m[i[0]] = float(i[1]) + temp_val
            temp_val = 0
        else:
            # pagos_m[i[0]] = i[1]
            temp_val = float(i[1])
        if contador == float(len(pagos)) and (float(i[0].split('-')[2]) == 15):
            pagos_m[i[0]] = float(i[1])
    # print(pagos_m)
    pagos_check, total_pagos = saldo_cliente(desembolso, pagos)
    if info == 2:
        return pagos_check
    # print(pagos_check)
    detalles = {
        'letra_q': letra_q,
        'letra_qu': letra_qu,
        'letra_m': letra_m,
        'letra_mu': letra_mu,
        'ob_salida': ob_salida,
        'meses_extra': (total_meses - plazo),
    }
    if info == 4: return detalles
    salida = [pagos, pagos_m, total_pagos, detalles]
    return salida


def detalles_salida(monto_prestamo, plazo, ajuste_letra):
    if ajuste_letra == 0:
        letra_q = round(monto_prestamo / (plazo * 2) + 0.005, 2)
    else:
        letra_q = round(float(ajuste_letra), 2)
    # print(type(letra_q))
    letra_qu = round(monto_prestamo - letra_q * (plazo * 2 - 1), 2)
    letra_m = letra_q * 2
    letra_mu = round(letra_q + letra_qu, 2)
    ob_salida = monto_prestamo
    return letra_q, letra_qu, letra_m, letra_mu, ob_salida


def agregar_tr(modelo_c, post, data, flag='cliente'):
    # print(post)
    modelo = Transacciones()
    consulta_1 = Transacciones.objects.filter(id_tr=post['id_tr']).exists()
    if consulta_1:
        data['advertencia'] = 'Ya está registrada una transacción con el ID= ' + '<span class="text-warning">' + post[
            'id_tr'] + '</span>'
        return data
    if int(post['registro_tr']) == 1:
        if modelo_c.cliente:
            modelo.empresa_id = modelo_c.cliente.cliente.empresa_2_id
        else:
            modelo.empresa_id = modelo_c.cliente_ant.empresa_2_id
    else:
        modelo.cliente_id = modelo_c.id
    modelo.monto_tr = post['monto_tr']
    modelo.tipo_pago = post['tipo_pago']
    modelo.fecha_tr = post['fecha_tr']
    modelo.id_tr = post['id_tr']
    modelo.banco_id = post['banco']
    if post.get('info_tr'):
        modelo.info_tr = post['info_tr']
    if post.get('notas'):
        modelo.notas = post['notas']
    modelo.save()
    data['ok'] = ''
    return data


def credito_empresa(empresa):
    consulta_1 = Transacciones.objects.filter(empresa=empresa).filter(estatus_tr='Regular')
    in_empresa = 0
    out_empresa = 0
    for i in consulta_1:
        in_empresa += float(i.monto_tr)
    consulta_2 = Pagos.objects.filter(tr__empresa_id=empresa)
    if consulta_2:
        for i in consulta_2:
            out_empresa += float(i.monto_2)
    else:
        return in_empresa
    return in_empresa - out_empresa


def saldo_cliente(cliente, pagos_q):  # desembolso y pagos
    consulta_1 = Pagos.objects.filter(desembolso=cliente)
    pagos = []
    total_pagos = 0
    if consulta_1:
        t_pagos = 0
        fecha = 0
        for i in consulta_1:
            t_pagos += float(i.monto_1)
            fecha = i.fecha_pago_1
            if i.fecha_pago_1 > fecha:
                fecha = i.fecha_pago_1
        info_pago = [fecha, t_pagos]
        temp_pago = float(info_pago[1])
        for i in pagos_q.items():
            # suma = 0
            if float(i[1]) < temp_pago:
                salida_1 = float(i[1])
                temp_pago = round(temp_pago - float(i[1]), 2)
                dias_atraso = timedelta(days=0)
            elif float(i[1]) == temp_pago:
                salida_1 = temp_pago
                temp_pago = 0
                dias_atraso = timedelta(days=0)
            else:
                salida_1 = temp_pago
                temp_pago = 0
                dias_atraso = datetime.now().date() - datetime.strptime(i[0], '%Y-%m-%d').date()
            if dias_atraso.days > 0:
                pagos.append([i[0], i[1], salida_1, dias_atraso.days])
            else:
                pagos.append([i[0], i[1], salida_1, 0])
    else:
        for i in pagos_q.items():
            dias_atraso = datetime.now().date() - datetime.strptime(i[0], '%Y-%m-%d').date()
            if dias_atraso.days > 0:
                pagos.append([i[0], i[1], 0, dias_atraso.days])
            else:
                pagos.append([i[0], i[1], 0, 0])
        return [pagos, {'total_pagos': 0}]
    # print(info_pago[1])
    return [pagos, {'total_pagos': info_pago[1]}]


def total_pagos(cliente):
    consulta = Pagos.objects.filter(desembolso=cliente).aggregate(Sum('monto_1'))
    if consulta['monto_1__sum']:
        salida = consulta['monto_1__sum']
    else:
        salida = 0
    return salida


def tabla_pagos(cliente, ob_total, data):
    consulta = Pagos.objects.filter(desembolso=cliente).order_by('fecha_pago_1')
    if not consulta:
        # print('No hay pagos')
        return []
    saldo = ob_total
    contador = 0
    for i in consulta:
        saldo -= float(i.monto_1)
        contador += 1
        valor = {
            'contador': contador,
            'fecha': str(i.fecha_pago_1),
            'monto': '$ ' + f'{float(i.monto_1):,.2f}',
            'saldo': '$ ' + f'{float(saldo):,.2f}',
            'opciones': i.id
        }
        data.append(valor)
    # print(data)
    return data


def notas_pagos(id_pago):
    consulta = Pagos.objects.get(id=id_pago)
    if not consulta.notas:
        return '---Sin comentarios---'
    return str(consulta.notas)


def cargar_info_pago(id_pago, modelo):
    consulta = Pagos.objects.get(id=id_pago)
    if consulta:
        salida = {
            'monto_1': str(consulta.monto_1),
            'fecha_pago_2': consulta.fecha_pago_2,
            'tr_id': str(consulta.tr_id),
            'notas': consulta.notas
        }
    else:
        return {}
    # {'balance_empresa': balance_empresa, 'balance_cliente': balance_cliente, 'tr_empresa': tr_empresa, 'tr_cliente': tr_cliente}
    # {'model_id': tr.id, 'tr_id': tr.id_tr, 'empresa': str(tr.empresa), 'disponible': disponible}
    transacciones = credito_disponible(modelo, consulta.tr_id)
    # print(transacciones)
    salida['transacciones'] = transacciones
    return salida


def lista_cuotas(cuotas_cliente, cliente, data, flag_1=0, flag_2=''):
    # print(cuotas_cliente)
    if flag_1 == 0:
        consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(clase_pago=1).only('fecha_pago_1', 'fecha_pago_2',
                                                                                        'monto_1')
    else:
        # print(flag_1)
        consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(clase_pago=1).filter(fecha_pago_1=flag_1).only(
            'fecha_pago_1', 'fecha_pago_2',
            'monto_1')
    # print('pass ')
    lista_1 = []
    contador = 0
    if flag_2 == '':
        ult_fecha = strpdate(list(cuotas_cliente)[-1])
    else:
        ult_fecha = strpdate(flag_2)
    # print(ult_fecha)
    if consulta_1:
        for i in consulta_1:
            lista_2 = [i.fecha_pago_1, i.fecha_pago_2, i.monto_1]
            lista_1.append(lista_2)
        ultima_pendiente = 0
        for i in cuotas_cliente.items():
            # print(cuotas_cliente.items())
            if float(i[1]) != 0:
                contador += 1
                pre_salida = [contador, i[0], to_num(i[1]), to_num(i[1]), 0,
                              0]  # Contador / fecha / cuota / pendiente / mora / estatus
                saldo = float(i[1])
                saldo_cuota = saldo
                fecha_pago = strpdate(i[0])
                f_ult_pago = 0
                morosidad = 0
                fecha_actual = datetime.now().date()
                delta_fechas = fecha_actual - fecha_pago
                if delta_fechas.days > 30:
                    pre_salida[5] = 2
                for j in lista_1:
                    if fecha_pago == j[0]:
                        # print(fecha_pago, j[1], 'aqui')
                        # if fecha_pago <= j[1]:
                        if f_ult_pago == 0:
                            if fecha_pago < ult_fecha and j[1] < ult_fecha:
                                atraso = meses_atraso(fecha_pago, j[1])
                            else:
                                atraso = meses_atraso(fecha_pago, ult_fecha)
                            f_ult_pago = fecha_pago + relativedelta(months=+atraso)
                        else:
                            if f_ult_pago < ult_fecha and j[1] < ult_fecha:
                                atraso = meses_atraso(f_ult_pago, j[1])
                            elif f_ult_pago < ult_fecha < j[1]:
                                atraso = meses_atraso(f_ult_pago, ult_fecha)
                            else:
                                atraso = 0
                            f_ult_pago = f_ult_pago + relativedelta(months=+atraso)
                        if atraso > 0:
                            morosidad_temp = round(saldo * atraso * 0.02, 2)
                            morosidad += morosidad_temp
                            if (saldo + morosidad) > float(j[2]):
                                saldo = round(saldo - float(j[2]) + morosidad_temp, 2)
                            elif (saldo + morosidad) == float(j[2]):
                                saldo = 0
                            else:
                                saldo = 'error'
                        else:
                            if saldo > float(j[2]):
                                saldo = round(saldo - float(j[2]), 2)
                            elif saldo == float(j[2]):
                                saldo = 0
                            else:
                                saldo = 'error'
                            saldo_cuota = saldo
                    else:
                        pass
                # print(saldo)
                if saldo > 0:
                    # print(f_ult_pago, datetime.now().date())
                    if f_ult_pago not in ('0', 0):
                        fecha_hoy = datetime.now().date()
                        if fecha_hoy < ult_fecha:
                            atraso = meses_atraso(f_ult_pago, fecha_hoy)
                        else:
                            atraso = meses_atraso(f_ult_pago, ult_fecha)
                        morosidad_temp = round(saldo * atraso * 0.02, 2)
                        morosidad += morosidad_temp
                        saldo += morosidad_temp
                    else:
                        fecha_hoy = datetime.now().date()
                        if fecha_hoy < ult_fecha:
                            atraso = meses_atraso(strpdate(i[0]), datetime.now().date())
                        else:
                            atraso = meses_atraso(strpdate(i[0]), ult_fecha)
                        morosidad_temp = round(saldo * atraso * 0.02, 2)
                        morosidad += morosidad_temp
                        saldo += morosidad_temp
                pre_salida[3] = saldo
                pre_salida[4] = morosidad
                # print(type(pre_salida[2]), type(pre_salida[3]), 'son los tipos')
                if pre_salida[3] == 0:
                    pre_salida[5] = 1
                elif pre_salida[2] < pre_salida[3]:
                    if ultima_pendiente == 0:
                        pre_salida[5] = 3
                        ultima_pendiente = 1
                elif 0 < pre_salida[3] < pre_salida[2]:
                    if ultima_pendiente == 0:
                        pre_salida[5] = 3
                        ultima_pendiente = 1
                elif pre_salida[2] == pre_salida[3]:

                    if ultima_pendiente == 0:
                        ultima_pendiente = 1
                        pre_salida[5] = 3
                valor = {
                    'contador': pre_salida[0],
                    'fecha': pre_salida[1],
                    'cuota': f'{float(pre_salida[2]):,.2f}',  # '$ ' + f'{float(i.monto_1):,.2f}',
                    'pendiente': f'{float(pre_salida[3]):,.2f}',
                    'morosidad': f'{float(pre_salida[4]):,.2f}',
                    'opciones': pre_salida[5],
                    'saldo_cuota': saldo_cuota,
                }
                data.append(valor)
    else:
        ultima_pendiente = 0
        for i in cuotas_cliente.items():
            if float(i[1]) != 0:
                # print(cuotas_cliente.items())
                contador += 1
                pre_salida = [contador, i[0], i[1], i[1], 0, 0]  # Contador / fecha / cuota / pendiente / mora / estatus
                saldo_cuota = pre_salida[2]
                fecha_pago = datetime.strptime(i[0], '%Y-%m-%d').date()
                fecha_actual = datetime.now().date()
                delta_fechas = fecha_actual - fecha_pago
                if delta_fechas.days > 30:
                    pre_salida[5] = 2
                if ultima_pendiente == 0:
                    pre_salida[5] = 3
                    ultima_pendiente = 1
                pre_salida[4] = calculo_mora(strpdate(i[0]), ult_fecha, i[1])
                pre_salida[3] = round(float(pre_salida[3]) + float(pre_salida[4]), 2)
                valor = {
                    'contador': pre_salida[0],
                    'fecha': pre_salida[1],
                    'cuota': f'{float(pre_salida[2]):,.2f}',
                    'pendiente': f'{float(pre_salida[3]):,.2f}',
                    'morosidad': f'{float(pre_salida[4]):,.2f}',
                    'opciones': pre_salida[5],
                    'saldo_cuota': saldo_cuota,
                }
                data.append(valor)
    return data


def pagar_cuota(cliente, post, data, user):
    consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(fecha_pago_1=post['fecha_pago']).only('monto_1')
    suma = 0
    if consulta_1:
        for i in consulta_1:
            suma += float(i.monto_1)
        total_pago = round(suma + float(post['monto_1']), 2)
        saldo_cuota = round(float(post['val_cuota']) + float(post['val_mora']), 2)
        if total_pago <= saldo_cuota:
            data = registrar_pago(post, cliente, data, user)
            return data
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
            return data
    else:
        # print('pass')
        saldo_cuota = round(float(post['val_cuota']) + float(post['val_mora']), 2)
        if float(post['monto_1']) <= saldo_cuota:
            data = registrar_pago(post, cliente, data, user)
            return data
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
            return data


def pagar_cuota_2(cliente, post, data, user):
    atraso = meses_atraso(strpdate(post['fecha_pago']), strpdate(post['fecha_pago_2']))
    pendiente = round(float(post['val_cuota']) * (1 + 0.02 * atraso), 2)
    if pendiente < float(post['monto_1']):
        data['advertencia'] = 'Con la fecha de registro: ' + post['fecha_pago_2'] + ' el total pendiente es: $' + str(
            pendiente)
        return data
    consulta_1 = Pagos.objects.filter(desembolso=cliente).filter(fecha_pago_1=post['fecha_pago']).only('monto_1')
    suma = 0
    if consulta_1:
        for i in consulta_1:
            suma += i.monto_1
        total_pago = round(suma + dec(post['monto_1']), 2)
        saldo_cuota = round(float(post['val_cuota']) + float(post['val_mora']), 2)
        # print(pendiente - float(suma))
        check_pendiente = round(pendiente - float(suma), 2)
        if float(post['monto_1']) > check_pendiente:
            data['advertencia'] = 'Con la fecha de registro: ' + post[
                'fecha_pago_2'] + ' el total pendiente es: $' + str(check_pendiente)
            return data
        if float(total_pago) <= saldo_cuota:
            consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
            consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
            # print(consulta_2.monto_tr, dec(post['monto_1']))
            if consulta_2 and not consulta_3['monto_1__sum']:
                if consulta_2.monto_tr >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            elif consulta_2 and consulta_3['monto_1__sum']:
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
                # print(consulta_3)
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
    else:
        # print('pass')
        consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
        consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
        if post['fecha_pago'] == post['fecha_pago_2']:
            mora = 0
        else:
            mora = round(float(post['val_mora']), 2)
        saldo_cuota = round(float(post['val_cuota']) + mora, 2)
        if float(post['monto_1']) <= saldo_cuota:
            if consulta_2 and not consulta_3['monto_1__sum']:
                # print(consulta_2.monto_tr, dec(post['monto_1']), saldo_cuota)
                if float(consulta_2.monto_tr) >= saldo_cuota:
                    data = registrar_pago(post, cliente, data, user)
                else:
                    if round(float(post['monto_1']), 2) <= round(float(consulta_2.monto_tr), 2):
                        data = registrar_pago(post, cliente, data, user)
                    else:
                        data['advertencia'] = 'Crédito insuficiente en la transacción.'
            elif consulta_2 and consulta_3['monto_1__sum']:
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    data = registrar_pago(post, cliente, data, user)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
                # print(consulta_3)
        else:
            data['advertencia'] = 'El monto excede el valor pendiente: $' + post['val_pendiente']
    return data


def registrar_pago(post, cliente, data, user, tipo=0):
    if tipo == 0:
        modelo = Pagos()
        modelo.desembolso = cliente
        modelo.monto_2 = post['val_cuota']
        modelo.fecha_pago_1 = post['fecha_pago']
        modelo.fecha_pago_3 = datetime.now().date()
        modelo.usuario = user
    else:
        modelo = cliente
    modelo.monto_1 = post['monto_1']
    # modelo.tipo_pago = post['tipo_pago']
    modelo.fecha_pago_2 = post['fecha_pago_2']
    # modelo.id_tr = post['id_tr']
    # modelo.banco_id = post['banco']
    # modelo.fecha_tr = post['fecha_tr']
    # if post.get('info_tr'):
    #     modelo.info_tr = post['info_tr']
    if post.get('notas'):
        modelo.notas = post['notas']
    modelo.clase_pago_id = 1
    modelo.tr_id = post['tr_rel']
    modelo.save()
    data['ok'] = ''
    return data


def editar_pago(post, modelo_2):
    # if strpdate(post['fecha_tr']) > strpdate(post['fecha_pago_2']):
    #     return {'advertencia': 'La fecha de transacción no puede ser anterior a la fecha de registro.'}
    data = []
    modelo = Pagos.objects.filter(id=post['id_pago']).get()
    desembolso, fecha = modelo.desembolso, modelo.fecha_pago_1
    cuotas_cliente = desglose_cobros(modelo_2, 3)
    ult_fecha = list(cuotas_cliente)[-1]
    ajuste = 0
    if strpdate(post['fecha_pago_2']) != modelo.fecha_pago_2:
        atraso_act = meses_atraso(modelo.fecha_pago_1, modelo.fecha_pago_2)
        atraso_temp = meses_atraso(modelo.fecha_pago_1, strpdate(post['fecha_pago_2']))
        ajuste = float(modelo.monto_2) * 0.02 * (atraso_act - atraso_temp)
        # print(atraso_act, atraso_temp, ajuste, 'ajustes de mora')
    cuotas_cliente_2 = {
        str(fecha): modelo.monto_2
    }
    datos_cuota = lista_cuotas(cuotas_cliente_2, modelo_2, data, fecha, ult_fecha)
    # print(datos_cuota)
    if modelo:
        consulta = Pagos.objects.filter(desembolso=desembolso).filter(fecha_pago_1=fecha).aggregate(Sum('monto_1'))
        # print(consulta)
        max_pago = round(float(modelo.monto_2) - (float(consulta['monto_1__sum']) - float(modelo.monto_1)) + float(
            datos_cuota[0]['morosidad']) - ajuste, 2)
        # print(max_pago, 'max pago')
        if float(post['monto_1']) <= max_pago:
            data = {}
            if modelo.tr_id == int(post['tr_rel']) and modelo.monto_1 >= dec(post['monto_1']):
                registrar_pago(post, modelo, data, '', tipo=1)
            elif modelo.tr_id == int(post['tr_rel']) and modelo.monto_1 < dec(post['monto_1']):
                consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
                consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum'] + modelo.monto_1
                if disponible >= dec(post['monto_1']):
                    registrar_pago(post, modelo, data, '', tipo=1)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            else:
                consulta_2 = Transacciones.objects.filter(id=post['tr_rel']).get()
                consulta_3 = Pagos.objects.filter(tr_id=post['tr_rel']).aggregate(Sum('monto_1'))
                disponible = consulta_2.monto_tr - consulta_3['monto_1__sum']
                if disponible >= dec(post['monto_1']):
                    registrar_pago(post, modelo, data, '', tipo=1)
                else:
                    data['advertencia'] = 'Crédito insuficiente en la transacción.'
            return data
        else:
            return {'advertencia': 'El monto editado supera el monto pendiente: $' + str(max_pago)}


def detalles_cuota(post, cliente):
    salida = {}
    id_cuota = post['id_pago']
    cuota = float(post['cuota'])
    consulta_1 = Pagos.objects.filter(desembolso=cliente.id).filter(fecha_pago_1=id_cuota)
    if consulta_1:
        contador = 0
        morosidad = 0
        pendiente = cuota
        n_pagos = len(consulta_1)
        for i in consulta_1:
            contador += 1
            fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
            fecha_actual = i.fecha_pago_2
            atraso = meses_atraso(fecha_pago, fecha_actual)
            if atraso > 0:
                morosidad += round(pendiente * 0.02 * atraso, 2)
            pendiente -= float(i.monto_1)
            if contador == n_pagos:
                fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
                fecha_actual = datetime.now().date()
                atraso = meses_atraso(fecha_pago, fecha_actual)
                if atraso > 0:
                    morosidad += round(pendiente * 0.02 * atraso, 2)
            # print(morosidad)
        salida['pagos'] = 1
        salida['morosidad'] = 0
    else:
        fecha_pago = datetime.strptime(id_cuota, '%Y-%m-%d').date()
        fecha_actual = datetime.now().date()
        atraso = meses_atraso(fecha_pago, fecha_actual)
        if atraso > 0:
            morosidad = round(cuota * 0.02 * atraso, 2)
            salida['pagos'] = 0
            salida['morosidad'] = str(morosidad)
        else:
            salida['pagos'] = 0
            salida['morosidad'] = 0
    # print(salida)
    return salida


def meses_atraso(fecha_ini, fecha_fin):
    salida = 0
    if fecha_ini < fecha_fin:
        fecha_test = fecha_ini
        contador = 0
        while fecha_test < fecha_fin:
            if fecha_test.day > 15:
                if fecha_test.month == 1:
                    fecha_test = (datetime(fecha_test.year, 3, 1) - timedelta(days=1)).date()
                elif fecha_test.month == 2:
                    fecha_test = datetime(fecha_test.year, 3, 30).date()
                elif fecha_test.month == 12:
                    fecha_test = datetime(fecha_test.year + 1, 1, fecha_test.day).date()
                else:
                    fecha_test = datetime(fecha_test.year, fecha_test.month + 1, fecha_test.day).date()
            else:
                if fecha_test.month == 1:
                    fecha_test = datetime(fecha_test.year, 2, fecha_test.day).date()
                elif fecha_test.month == 12:
                    fecha_test = datetime(fecha_test.year + 1, 1, fecha_test.day).date()
                else:
                    fecha_test = datetime(fecha_test.year, fecha_test.month + 1, fecha_test.day).date()
            if fecha_test <= fecha_fin:
                contador += 1
    else:
        contador = 0
    salida = contador
    return salida


def strpdate(fecha):
    salida = datetime.strptime(fecha, '%Y-%m-%d').date()
    return salida


def calculo_mora(fecha_pago, fecha_ultima, saldo_cuota):
    # saldo_cuota = saldo_cuota
    fecha_hoy = datetime.now().date()
    # print(fecha_pago, fecha_ultima, fecha_hoy)
    if fecha_pago < fecha_hoy < fecha_ultima:
        atraso = meses_atraso(fecha_pago, fecha_hoy)
    elif fecha_pago > fecha_hoy:
        atraso = 0
    else:
        atraso = meses_atraso(fecha_pago, fecha_ultima)
    # print(atraso)
    if atraso > 0:
        # print(type(saldo_cuota), type(atraso))
        morosidad = round(saldo_cuota * atraso * 0.02, 2)
    else:
        morosidad = 0
    return f'{float(morosidad):,.2f}'


# def credito_disponible(modelo):
#     balance_empresa = 0
#     balance_cliente = 0
#     if modelo.cliente:
#         empresa = modelo.cliente.cliente.empresa_2_id
#         consulta_1 = Transacciones.objects.filter(empresa=empresa).exists()
#     else:
#         empresa = modelo.cliente_ant.empresa_2_id
#         consulta_1 = Transacciones.objects.filter(empresa=empresa).exists()
#     if consulta_1:
#         entrada_empresa = Transacciones.objects.filter(empresa=empresa).aggregate(Sum('monto_tr'))
#         consulta_3 = Pagos.objects.filter(tr__empresa=empresa).exists()
#         if consulta_3:
#             salida_empresa = Pagos.objects.filter(tr__empresa=empresa).aggregate(Sum('monto_tr'))
#         else:
#             salida_empresa = 0
#         balance_empresa = entrada_empresa['monto_tr__sum'] - salida_empresa
#     consulta_2 = Transacciones.objects.filter(cliente=modelo.id).exists()
#     if consulta_2:
#         entrada_cliente = Transacciones.objects.filter(cliente=modelo.id).aggregate(Sum('monto_tr'))
#         consulta_4 = Pagos.objects.filter(tr__cliente=modelo.id).exists()
#         if consulta_4:
#             salida_cliente = Pagos.objects.filter(tr__cliente=modelo.id).aggregate(Sum('monto_tr'))
#         else:
#             salida_cliente = 0
#         balance_cliente = entrada_cliente['monto_tr__sum'] - salida_cliente
#     return {'balance_empresa': balance_empresa, 'balance_cliente': balance_cliente}


def credito_disponible(modelo, flag_1=0, flag_2=0):
    balance_empresa = 0
    balance_cliente = 0
    if flag_2 == 0:
        if modelo.cliente:
            empresa = modelo.cliente.cliente.empresa_2_id
            consulta_1 = Transacciones.objects.filter(empresa=empresa).filter(estatus_tr='Regular')
        else:
            empresa = modelo.cliente_ant.empresa_2_id
            consulta_1 = Transacciones.objects.filter(empresa=empresa).filter(estatus_tr='Regular')
        if consulta_1:
            consulta_3 = Pagos.objects.filter(tr__empresa=empresa)
            balance_empresa, tr_empresa = disponible_tr(consulta_1, consulta_3, 'empresa', flag_1)
        else:
            tr_empresa = []
        consulta_2 = Transacciones.objects.filter(cliente=modelo.id).filter(estatus_tr='Regular')
        if consulta_2:
            consulta_4 = Pagos.objects.filter(tr__cliente=modelo.id)
            balance_cliente, tr_cliente = disponible_tr(consulta_2, consulta_4, 'cliente', flag_1)
        else:
            tr_cliente = []
        return {'balance_empresa': balance_empresa, 'balance_cliente': balance_cliente, 'tr_empresa': tr_empresa,
                'tr_cliente': tr_cliente}
    else:
        consulta_1 = Transacciones.objects.filter(cliente=modelo).filter(estatus_tr='Regular')
        if consulta_1:
            consulta_2 = Pagos.objects.filter(tr__cliente=modelo)
            balance_cliente, tr_cliente = disponible_tr(consulta_1, consulta_2, 'cliente', flag_1)
        else:
            tr_cliente = []
        return {'balance_cliente': balance_cliente, 'tr_cliente': tr_cliente}


def disponible_tr(consulta_1, consulta_2, tipo, flag_1):
    entrada = 0
    salida = 0
    if consulta_2:
        pagos = {}
        tr_salida = []
        for obj in consulta_2:
            if pagos.get(str(obj.tr_id)):
                pagos[str(obj.tr_id)] += obj.monto_1
            else:
                pagos[str(obj.tr_id)] = obj.monto_1
            salida += obj.monto_1
        for tr in consulta_1:
            disponible = tr.monto_tr
            if pagos.get(str(tr.id)):
                disponible -= pagos[str(tr.id)]
            if disponible > 0 or tr.id == flag_1:
                if tipo == 'empresa':
                    tr_salida.append(
                        {'model_id': tr.id, 'tr_id': tr.id_tr, 'empresa': str(tr.empresa), 'disponible': disponible})
                else:
                    tr_salida.append(
                        {'model_id': tr.id, 'tr_id': tr.id_tr, 'cliente': str(tr.cliente), 'disponible': disponible})
            entrada += tr.monto_tr
    else:
        tr_salida = []
        for tr in consulta_1:
            disponible = tr.monto_tr
            if tipo == 'empresa':
                tr_salida.append(
                    {'model_id': tr.id, 'tr_id': tr.id_tr, 'empresa': str(tr.empresa), 'disponible': disponible})
            else:
                tr_salida.append(
                    {'model_id': tr.id, 'tr_id': tr.id_tr, 'cliente': str(tr.cliente), 'disponible': disponible})
            entrada += tr.monto_tr
    balance = entrada - salida
    return balance, tr_salida


def change_pdf_name(clave, modelo, tipo='ant'):
    file_name = None
    if tipo == 'new':
        dni = str(modelo.cliente.dni)
    else:
        dni = str(modelo.dni)
    if clave == 'DNI_img':
        file_name = verificar_pdf(modelo.DNI_img, dni, 'DNI_')
    elif clave == 'carta_trabajo_img':
        file_name = verificar_pdf(modelo.carta_trabajo_img, dni, 'c_trabajo_')
    elif clave == 'talonario_1_img':
        file_name = verificar_pdf(modelo.talonario_1_img, dni, 'talonario_1_')
    elif clave == 'talonario_2_img':
        file_name = verificar_pdf(modelo.talonario_2_img, dni, 'talonario_2_')
    elif clave == 'ficha_img':
        file_name = verificar_pdf(modelo.ficha_img, dni, 'ficha_')
    elif clave == 'recibo_img':
        file_name = verificar_pdf(modelo.recibo_img, dni, 'recibo_')
    elif clave == 'carta_saldo_img':
        file_name = verificar_pdf(modelo.carta_saldo_img, dni, 'c_saldo_')
    elif clave == 'file_apc':
        if modelo.file_apc: modelo.file_apc.delete()
        file_name = 'APC_' + dni + '.pdf'
    # Archivos de aprobación
    elif clave == 'orden_descuento':
        file_name = verificar_pdf(modelo.orden_descuento, dni, 'OD_')
    elif clave == 'contrato':
        file_name = verificar_pdf(modelo.contrato, dni, 'contrato_')
    elif clave == 'pagare':
        file_name = verificar_pdf(modelo.pagare, dni, 'pagare_')
    elif clave == 'f_1':
        file_name = verificar_pdf(modelo.f_1, dni, 'F1_')
    elif clave == 'conoce_cliente':
        file_name = verificar_pdf(modelo.conoce_cliente, dni, 'c_cliente_')
    # print(file_name)
    return file_name


def verificar_pdf(modelo, dni, tag):
    if modelo:
        modelo.delete()
        file_name = tag + dni + '.pdf'
    else:
        file_name = tag + dni + '.pdf'
    return file_name


def save_pdf_files(files, modelo, tipo='ant', flag='1'):
    if tipo == 'new':
        modelo_pre = modelo.cliente
        modelo_apr = modelo
    else:
        modelo_pre = modelo
        modelo_apr = modelo
    # Docs Preaprobado
    if files.get('DNI_img'): modelo_pre.DNI_img = files['DNI_img']
    if files.get('carta_trabajo_img'): modelo_pre.carta_trabajo_img = files['carta_trabajo_img']
    if files.get('talonario_1_img'): modelo_pre.talonario_1_img = files['talonario_1_img']
    if files.get('talonario_2_img'): modelo_pre.talonario_2_img = files['talonario_2_img']
    if files.get('ficha_img'): modelo_pre.ficha_img = files['ficha_img']
    if files.get('recibo_img'): modelo_pre.recibo_img = files['recibo_img']
    if files.get('carta_saldo_img'): modelo_pre.carta_saldo_img = files['carta_saldo_img']
    if files.get('file_apc'): modelo_pre.file_apc = files['file_apc']
    # Docs Aprobado
    if flag == '1':
        if files.get('orden_descuento'): modelo_apr.orden_descuento = files['orden_descuento']
        if files.get('f_1'): modelo_apr.f_1 = files['f_1']
    elif flag == 'orden_descuento':
        if files.get('orden_descuento'): modelo_apr.orden_descuento = files['orden_descuento']
    elif flag == 'f_1':
        if files.get('orden_descuento'): modelo_apr.f_1 = files['orden_descuento']
    if files.get('contrato'): modelo_apr.contrato = files['contrato']
    if files.get('pagare'): modelo_apr.pagare = files['pagare']
    if files.get('conoce_cliente'): modelo_apr.conoce_cliente = files['conoce_cliente']
    modelo.save()


def process_img_2(post, dni, nombre, tag):
    imagen = None
    if post.get(nombre):
        # print('xxx')
        img_file = post[nombre]
        formato, img_str = img_file.split(';base64,')
        # print("format", formato)
        ext = formato.split('/')[-1]
        imagen = ContentFile(base64.b64decode(img_str), name=tag + dni + '.' + ext)

    return imagen


def guardar_img_b64(modelo, imagen):
    if modelo:
        modelo.delete()
        modelo = imagen
    else:
        modelo = imagen
    modelo.save()
    return modelo


def save_img_files(post, modelo, tipo='ant', flag='1'):
    if tipo == 'new':
        tipo_ingreso = str(modelo.cliente.tipo_ingreso);
        dni = str(modelo.cliente.dni)
    else:
        tipo_ingreso = str(modelo.tipo_ingreso);
        dni = str(modelo.dni)
    imagen_1 = process_img_2(post, dni, 'image_1', 'DNI_')
    if imagen_1:
        if modelo.DNI_img: modelo.DNI_img.delete()
        modelo.DNI_img = imagen_1
    imagen_2 = process_img_2(post, dni, 'image_2', 'c_trabajo_')
    if imagen_2:
        if modelo.carta_trabajo_img: modelo.carta_trabajo_img.delete()
        modelo.carta_trabajo_img = imagen_2
    imagen_3 = process_img_2(post, dni, 'image_3', 'talonario_1_')
    if imagen_3:
        if modelo.talonario_1_img: modelo.talonario_1_img.delete()
        modelo.talonario_1_img = imagen_3
    imagen_4 = process_img_2(post, dni, 'image_4', 'talonario_2_')
    if imagen_4:
        if modelo.talonario_2_img: modelo.talonario_2_img.delete()
        modelo.talonario_2_img = imagen_4
    imagen_5 = process_img_2(post, dni, 'image_5', 'ficha_')
    if imagen_5:
        if modelo.ficha_img: modelo.ficha_img.delete()
        modelo.ficha_img = imagen_5
    imagen_6 = process_img_2(post, dni, 'image_6', 'recibo_')
    if imagen_6:
        if modelo.recibo_img: modelo.recibo_img.delete()
        modelo.recibo_img = imagen_6
    imagen_7 = process_img_2(post, dni, 'image_7', 'c_saldo_')
    if imagen_7:
        if modelo.carta_saldo_img: modelo.carta_saldo_img.delete()
        modelo.carta_saldo_img = imagen_7
    if tipo_ingreso == 'Empresa privada':
        imagen_9 = process_img_2(post, dni, 'image_9', 'OD_')
        if imagen_9:
            if modelo.orden_descuento: modelo.orden_descuento.delete()
            modelo.orden_descuento = imagen_9
    elif tipo_ingreso == 'Empresa pública':
        imagen_9 = process_img_2(post, dni, 'image_9', 'F1_')
        if imagen_9:
            if modelo.f_1: modelo.f_1.delete()
            modelo.f_1 = imagen_9
    imagen_11 = process_img_2(post, dni, 'image_11', 'pagare_')
    if imagen_11:
        if modelo.pagare: modelo.pagare.delete()
        modelo.pagare = imagen_11
    imagen_12 = process_img_2(post, dni, 'image_12', 'c_cliente_')
    if imagen_12:
        if modelo.conoce_cliente: modelo.conoce_cliente.delete()
        modelo.conoce_cliente = imagen_12
    modelo.save()


def extraer_mora(info_cuotas):
    morosidad = 0
    saldo = 0
    for i in info_cuotas:
        pendiente_temp = round(dec(i['pendiente']), 2)
        morosidad_temp = round(dec(i['morosidad']), 2)
        cuota_temp = round(dec(i['cuota']), 2)
        suma = round(cuota_temp + morosidad_temp, 2)
        if pendiente_temp == suma:
            morosidad = round(morosidad + morosidad_temp, 2)
            saldo = round(saldo + cuota_temp, 2)
        elif 0 < pendiente_temp < suma:
            pago = round(suma - pendiente_temp, 2)
            if pago >= morosidad_temp:
                saldo = round(saldo + cuota_temp - (pago - morosidad_temp), 2)
            else:
                saldo = round(saldo + cuota_temp)
                morosidad = round(morosidad + morosidad_temp - pago)
    return saldo, morosidad


def eliminar_pagos(modelo):
    consulta = Pagos.objects.filter(desembolso=modelo)
    if consulta:
        consulta.delete()


def detalles_cliente_global(monto, plazo, meses_total, ob_total):
    # salida = []
    int_total = round(monto * 0.02 * meses_total, 2)
    seg_vida = round(monto * 0.035 * meses_total / 12, 2)
    seg_desempleo = round(monto * 0.025 * meses_total / 12, 2)
    seg_dental = round(meses_total * 1.25, 2)
    notaria = 12
    comision = ob_total - int_total - seg_vida - seg_desempleo - seg_dental - notaria - monto
    # salida = [int_total, seg_vida, seg_desempleo, seg_dental, notaria, comision]
    return [int_total, seg_vida, seg_desempleo, seg_dental, notaria, comision]


def cuotas_pagadas(modelo, cuota=0, flag=0):
    # print(cuota)
    if cuota == 0:
        consulta = Pagos.objects.filter(desembolso=modelo).aggregate(Sum('monto_1'))
        if consulta['monto_1__sum']:
            salida = consulta['monto_1__sum']
        else:
            salida = 0
        return salida
    if flag == 0:
        consulta = Pagos.objects.filter(desembolso=modelo).aggregate(Sum('monto_1'))
        if consulta['monto_1__sum']:
            cuotas_pagas = round(float(consulta['monto_1__sum']) / cuota, 1)
            # print(consulta['monto_1__sum'], cuotas_pagas)
            return consulta['monto_1__sum'], cuotas_pagas
        else:
            return 0, 0
    else:
        return 'Actualizar función'


def saldo_tr_cliente(modelo):
    consulta_1 = Transacciones.objects.filter(cliente=modelo).filter(estatus_tr='Regular').aggregate(Sum('monto_tr'))
    consulta_2 = Pagos.objects.filter(tr__cliente=modelo).aggregate(Sum('monto_1'))
    if consulta_1['monto_tr__sum'] and consulta_2['monto_1__sum']:
        disponible = consulta_1['monto_tr__sum'] - consulta_2['monto_1__sum']
    elif consulta_1['monto_tr__sum']:
        disponible = consulta_1['monto_tr__sum']
    else:
        disponible = 0
    return disponible


def generar_prepago(modelo, post, user, modo='Manual'):
    prepago = PrepagoAnticipado()
    prepago.desembolso_id = modelo.id
    prepago.fecha_pago = post['fecha_pago']
    prepago.fecha_ret_int = post['fecha_ret_int']
    prepago.fecha_ret_seg = post['fecha_ret_seg']
    prepago.ret_int = post['ret_int']
    prepago.ret_seg = post['ret_seg']
    prepago.penalidad = post['penalidad']
    prepago.calc_mode = modo
    prepago.tr_pagado_id = post['tr_pagado']
    if post.get('tr_retornado'):
        prepago.tr_retornado_id = post['tr_retornado']
    if post.get('notas'):
        prepago.notas = post['notas']
    prepago.usuario = user
    modelo.estatus = 'Anticipado'  # Activo, Anticipado, Cancelado, Vencido
    modelo.save()
    prepago.save()
    get_tr = Transacciones.objects.get(pk=post['tr_pagado'])
    get_tr.estatus_tr = 'Prepago'
    get_tr.save()
    return {'ok': ''}


def saldo_sin_ajuste(modelo, total_pago):
    if modelo.cliente:
        ob_total = round(float(modelo.cliente.ob_contrato), 2)
    else:
        ob_total = round(float(modelo.cliente_ant.ob_contrato), 2)
    salida = round(ob_total - float(total_pago), 2)
    return salida


def lista_cobros_rev(modelo_1, modelo_2, salida, rtt=False):
    # ult_pg = {}
    ult_pg = dict(map(lambda x: (x.desembolso_id, x.fecha_pago_1), modelo_1))
    # for i in modelo_1:
    #     # print(i.fecha_pago_1, i.desembolso_id)
    #     ult_pg[i.desembolso_id] = i.fecha_pago_1
    # print(ult_pg)
    if not rtt:
        salida = list(map(lambda x: test_map_2(x, ult_pg), modelo_2))
        # print(salida_2)
        #
        # for i in modelo_2:
        #     if i.cliente:
        #         if ult_pg.get(i.id):
        #             atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
        #             info_atraso = atraso_lista_desembolso(atraso)
        #         else: info_atraso = 'Sin pagos'
        #         valor = {
        #             'contrato': i.cliente.num_contrato,
        #             'nombre': i.cliente.cliente.nombre_1,
        #             'apellido': i.cliente.cliente.apellido_1,
        #             'atraso': info_atraso,
        #             'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
        #             'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
        #             'plazo': i.cliente.plazo + ' m.',
        #             'celular': i.cliente.cliente.celular,
        #             'id': str(i.id) + '/new/' + str(i.cliente.id),
        #         }
        #         # valor['retraso'] =
        #         salida.append(valor)
        #     elif i.cliente_ant:
        #         if ult_pg.get(i.id):
        #             atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
        #             info_atraso = atraso_lista_desembolso(atraso)
        #         else: info_atraso = 'Sin pagos'
        #         valor = {
        #             'contrato': i.cliente_ant.num_contrato,
        #             'nombre': i.cliente_ant.nombre_1,
        #             'apellido': i.cliente_ant.apellido_1,
        #             'atraso': info_atraso,
        #             'tipo_ingreso': str(i.cliente_ant.tipo_ingreso),
        #             'monto': '$ ' + f'{float(i.cliente_ant.monto):,.2f}',
        #             'plazo': i.cliente_ant.plazo + ' m.',
        #             'celular': i.cliente_ant.celular,
        #             'id': str(i.id) + '/ant/' + str(i.cliente_ant.id),
        #         }
        #         salida.append(valor)
    else:
        for i in modelo_2:
            if i.desembolso.cliente:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.cliente.num_contrato,
                    'nombre': i.cliente.cliente.nombre_1,
                    'apellido': i.cliente.cliente.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
                    'plazo': i.cliente.plazo + ' m.',
                    'celular': i.cliente.cliente.celular,
                    'id': str(i.id) + '/new/' + str(i.cliente.id),
                }
                # valor['retraso'] =
                salida.append(valor)
            elif i.desembolso.cliente_ant:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.num_contrato,
                    'nombre': i.desembolso.cliente_ant.nombre_1,
                    'apellido': i.desembolso.cliente_ant.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.desembolso.cliente_ant.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.monto_r):,.2f}',
                    'plazo': str(i.plazo_r) + ' m.',
                    'celular': i.desembolso.cliente_ant.celular,
                    'id': str(i.id) + '/ant/' + str(i.desembolso.cliente_ant.id),
                }
                salida.append(valor)
    # print(salida)
    return salida


def test_map_2(i, ult_pg):
    valor = {}
    if i.cliente:
        if ult_pg.get(i.id):
            atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
            info_atraso = atraso_lista_desembolso(atraso)
        else:
            info_atraso = 'Sin pagos'
        valor = {
            'contrato': i.cliente.num_contrato,
            'nombre': i.cliente.cliente.nombre_1,
            'apellido': i.cliente.cliente.apellido_1,
            'atraso': info_atraso,
            'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
            'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
            'plazo': i.cliente.plazo + ' m.',
            'celular': i.cliente.cliente.celular,
            'id': str(i.id) + '/new/' + str(i.cliente.id),
        }
        # valor['retraso'] =
    elif i.cliente_ant:
        if ult_pg.get(i.id):
            atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
            info_atraso = atraso_lista_desembolso(atraso)
        else:
            info_atraso = 'Sin pagos'
        valor = {
            'contrato': i.cliente_ant.num_contrato,
            'nombre': i.cliente_ant.nombre_1,
            'apellido': i.cliente_ant.apellido_1,
            'atraso': info_atraso,
            'tipo_ingreso': str(i.cliente_ant.tipo_ingreso),
            'monto': '$ ' + f'{float(i.cliente_ant.monto):,.2f}',
            'plazo': i.cliente_ant.plazo + ' m.',
            'celular': i.cliente_ant.celular,
            'id': str(i.id) + '/ant/' + str(i.cliente_ant.id),
        }
    return valor


def atraso_lista_desembolso(atraso):
    salida = 0
    if atraso == 0:
        salida = '0 días'
    elif atraso == 1:
        salida = '30 días'
    elif atraso == 2:
        salida = '60 días'
    elif atraso == 3:
        salida = '90 días'
    elif atraso > 3:
        salida = '+90 días'
    return str(salida)


def fecha_ult_pago(modelo):
    if modelo.cliente:
        fecha_od = modelo.cliente.fecha_ini_OD
        plazo = int(modelo.cliente.plazo)
    else:
        fecha_od = modelo.cliente_ant.fecha_ini_OD
        plazo = int(modelo.cliente_ant.plazo)
        print(fecha_od)
    info = loop_ult_pago(fecha_od, plazo)
    if fecha_od.day == 15:
        if info[0] == 2:
            salida = (datetime(info[1], info[0] + 1, 1) - timedelta(days=1))
        elif info[0] == 12:
            salida = datetime(info[1] + 1, 1, 30)
        else:
            salida = datetime(info[1], info[0], 30)
    else:
        if info[0] == 11:
            salida = datetime(info[1] + 1, 1, 15)
        elif info[0] in (12, 13):
            salida = datetime(info[1] + 1, 2, 15)
        else:
            # print(info)
            salida = datetime(info[1], info[0] + 1, 15)
    print(salida.date())
    # print(info, 'fecha ult pago')
    return salida.date()


def loop_ult_pago(fecha_od, plazo):
    info = [fecha_od.month, fecha_od.year]
    contador = 0
    while contador < plazo:
        if info[0] <= 12:
            contador += 1
            info[0] += 1
        else:
            info[0] = 0
            info[1] += 1
        # print(info, contador)
    # if info[0] == 12:
    #     info[0] = 1
    #     info[1] += 1
    # print(info, contador)
    return info


def adv_ultimo_pago(modelo, post, data, user):
    consulta = Pagos.objects.filter(desembolso=modelo).filter(fecha_pago_1=post['fecha_pago']).aggregate(Sum('monto_1'))
    if consulta['monto_1__sum']:
        total_pago = to_num(post['monto_1']) + to_num(consulta['monto_1__sum'])
    else:
        total_pago = to_num(post['monto_1'])
    a_pagar = to_num(post['val_cuota']) + to_num(post['val_mora'])
    if to_num(total_pago) == to_num(a_pagar):
        salida = {'advertencia_2': 'ult_completa'}
        print('cancelado')
    else:
        salida = pagar_cuota_2(modelo, post, data, user)
        print('pago incompleto')
    # else: salida = {'stop'}
    return salida


def to_num(val, formato=False):
    if formato:
        return '$ ' + f'{float(val):,.2f}'
    else:
        return round(float(val), 2)


def calcular_rtt(modelo, post):
    int_global = to_num(post['int_global'])
    s_colectivo = to_num(post['s_colectivo'])
    s_desempleo = to_num(post['s_desempleo'])
    s_dental = to_num(post['s_dental'])
    if post['c_cierre'] != '':
        c_cierre = to_num(post['c_cierre'])
    else:
        c_cierre = 0
    monto_r = to_num(post['monto_r'])
    plazo_r = to_num(post['plazo_r'])
    ob_contrato = to_num(post['ob_contrato'])
    fecha_ini = strpdate(post['fecha_ini'])
    fecha_ini_od = strpdate(post['fecha_ini_OD'])
    cargos_extra = to_num(post['cargos_extra'])
    notaria = 12
    if post['diciembres'] == 'No':
        int_rtt = to_num(monto_r * plazo_r * int_global / 100)
        seguros_rtt = to_num((monto_r * (s_colectivo + s_desempleo + s_dental) / 100) * plazo_r / 12)
        comision_rtt = to_num(monto_r * c_cierre)
        total_rtt = to_num(monto_r + int_rtt + seguros_rtt + comision_rtt + notaria)
        print(int_rtt, 'interés')
        print(seguros_rtt, 'seguros')
        print(comision_rtt, 'comisión')
        print(total_rtt, 'total')


def val_cuotas(modelo):
    if modelo.cliente:
        cuotas = int(modelo.cliente.plazo) * 2
    else:
        cuotas = int(modelo.cliente_ant.plazo) * 2
    return cuotas


def saldos_tiempo_mora(modelo_1, modelo_2, salida, rtt=False):
    # ult_pg = {}
    ult_pg = dict(map(lambda x: (x.desembolso_id, x.fecha_pago_1), modelo_1))
    # print(ult_pg)
    # 0-30 | 60-90 | +90
    # saldos = ((0, 0, 0, 0), (0, 0, 0, 0), (0, 0, 0, 0))
    saldos = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    contratos_1 = ()
    contratos_2 = ()
    contratos_3 = ()
    atraso = 0
    if not rtt:
        for i in modelo_2:
            # print(atraso)
            if i.cliente:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                else:
                    atraso = -1
                # print(i.cliente.num_contrato)
                if atraso in (0, 1):
                    # print(i.cliente.num_contrato)
                    saldos[0][0] = to_num(to_num(saldos[0][0]) + to_num(i.cliente.monto))
                    saldos[0][1] = to_num(to_num(saldos[0][1]) + to_num(i.cliente.ob_contrato))
                    contratos_1 = contratos_1 + (i.id,)
                elif atraso in (2, 3):
                    saldos[1][0] = to_num(to_num(saldos[1][0]) + to_num(i.cliente.monto))
                    saldos[1][1] = to_num(to_num(saldos[1][1]) + to_num(i.cliente.ob_contrato))
                    contratos_2 = contratos_2 + (i.id,)
                elif atraso > 3:
                    saldos[2][0] = to_num(to_num(saldos[2][0]) + to_num(i.cliente.monto))
                    saldos[2][1] = to_num(to_num(saldos[2][1]) + to_num(i.cliente.ob_contrato))
                    contratos_3 = contratos_3 + (i.id,)
                else:
                    contrato = i.cliente.num_contrato.split('-')
                    if contrato[0] == '2021' and int(contrato[1]) >= 10:
                        saldos[0][0] = to_num(to_num(saldos[0][0]) + to_num(i.cliente.monto))
                        saldos[0][1] = to_num(to_num(saldos[0][1]) + to_num(i.cliente.ob_contrato))
                        contratos_1 = contratos_1 + (i.id,)
                    else:
                        saldos[1][0] = to_num(to_num(saldos[1][0]) + to_num(i.cliente.monto))
                        saldos[1][1] = to_num(to_num(saldos[1][1]) + to_num(i.cliente.ob_contrato))
                        contratos_2 = contratos_2 + (i.id,)
                # print(saldos)
                # print(contratos_3)
            elif i.cliente_ant:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                else:
                    atraso = -1
                if atraso in (0, 1):
                    # print(i.cliente_ant.monto)
                    saldos[0][0] = to_num(to_num(saldos[0][0]) + to_num(i.cliente_ant.monto))
                    saldos[0][1] = to_num(to_num(saldos[0][1]) + to_num(i.cliente_ant.ob_contrato))
                    contratos_1 = contratos_1 + (i.id,)
                elif atraso in (2, 3):
                    saldos[1][0] = to_num(to_num(saldos[1][0]) + to_num(i.cliente_ant.monto))
                    saldos[1][1] = to_num(to_num(saldos[1][1]) + to_num(i.cliente_ant.ob_contrato))
                    contratos_2 = contratos_2 + (i.id,)
                elif atraso > 3:
                    saldos[2][0] = to_num(to_num(saldos[2][0]) + to_num(i.cliente_ant.monto))
                    saldos[2][1] = to_num(to_num(saldos[2][1]) + to_num(i.cliente_ant.ob_contrato))
                    contratos_3 = contratos_3 + (i.id,)
                else:
                    saldos[1][0] = to_num(to_num(saldos[1][0]) + to_num(i.cliente_ant.monto))
                    saldos[1][1] = to_num(to_num(saldos[1][1]) + to_num(i.cliente_ant.ob_contrato))
                    contratos_2 = contratos_2 + (i.id,)
    else:
        for i in modelo_2:
            if i.desembolso.cliente:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.cliente.num_contrato,
                    'nombre': i.cliente.cliente.nombre_1,
                    'apellido': i.cliente.cliente.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.cliente.cliente.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.cliente.monto):,.2f}',
                    'plazo': i.cliente.plazo + ' m.',
                    'celular': i.cliente.cliente.celular,
                    'id': str(i.id) + '/new/' + str(i.cliente.id),
                }
                # valor['retraso'] =
                salida.append(valor)
            elif i.desembolso.cliente_ant:
                if ult_pg.get(i.id):
                    atraso = meses_atraso(ult_pg[i.id], datetime.now().date())
                    info_atraso = atraso_lista_desembolso(atraso)
                else:
                    info_atraso = 'Sin pagos'
                valor = {
                    'contrato': i.num_contrato,
                    'nombre': i.desembolso.cliente_ant.nombre_1,
                    'apellido': i.desembolso.cliente_ant.apellido_1,
                    'atraso': info_atraso,
                    'tipo_ingreso': str(i.desembolso.cliente_ant.tipo_ingreso),
                    'monto': '$ ' + f'{float(i.monto_r):,.2f}',
                    'plazo': str(i.plazo_r) + ' m.',
                    'celular': i.desembolso.cliente_ant.celular,
                    'id': str(i.id) + '/ant/' + str(i.desembolso.cliente_ant.id),
                }
                salida.append(valor)
    # print(saldos)
    consulta_1 = Pagos.objects.filter(desembolso__in=contratos_1).aggregate(Sum('monto_1'))
    consulta_2 = Pagos.objects.filter(desembolso__in=contratos_2).aggregate(Sum('monto_1'))
    consulta_3 = Pagos.objects.filter(desembolso__in=contratos_3).aggregate(Sum('monto_1'))
    # print(consulta_1, consulta_2, consulta_3)
    saldos[0][2] = to_num(consulta_1['monto_1__sum'])
    saldos[1][2] = to_num(consulta_2['monto_1__sum'])
    saldos[2][2] = to_num(consulta_3['monto_1__sum'])
    saldos[0][3] = to_num(saldos[0][1] - saldos[0][2])
    saldos[1][3] = to_num(saldos[1][1] - saldos[1][2])
    saldos[2][3] = to_num(saldos[2][1] - saldos[2][2])
    return saldos


def ajuste_cuotas_cliente(modelo, cuotas_cliente):
    cuotas = modelo.ajuste_letra_2.split(';')
    num_len = len(cuotas_cliente)
    contador = 0
    acumulado = 0
    if modelo.ajuste_NC:
        info_nc = modelo.ajuste_NC.split('/')
        ajuste_nc = to_num(info_nc[2])
    else:
        ajuste_nc = 0
    if modelo.cliente:
        ob_total = to_num(modelo.cliente.ob_contrato)
    else:
        ob_total = to_num(modelo.cliente_ant.ob_contrato)
    if len(cuotas) > 1:
        for obj in cuotas_cliente.items():
            contador += 1
            val = obj[0].split('-')
            if int(val[2]) == 15 and int(val[1]) != 12:
                if contador < num_len:
                    # print(obj, 'objeto')
                    cuotas_cliente[obj[0]] = to_num(cuotas[0])
                    # cuotas_cliente[obj[0]] = f'{float(cuotas[0]):,.2f}'
                    acumulado = to_num(acumulado + to_num(cuotas[0]))
                else:
                    ult_cuota = to_num(ob_total - acumulado - ajuste_nc)
                    cuotas_cliente[obj[0]] = f'{ult_cuota:,.2f}'
            elif int(val[2]) > 15 and int(val[1]) != 12:
                if contador < num_len:
                    cuotas_cliente[obj[0]] = to_num(cuotas[1])
                    acumulado = to_num(acumulado + to_num(cuotas[1]))
                else:
                    ult_cuota = to_num(ob_total - acumulado - ajuste_nc)
                    cuotas_cliente[obj[0]] = f'{ult_cuota:,.2f}'
    else:
        for obj in cuotas_cliente.items():
            contador += 1
            if contador < num_len:
                cuotas_cliente[obj[0]] = to_num(cuotas[0])
                acumulado = to_num(acumulado + to_num(cuotas[0]))
            else:
                ult_cuota = to_num(ob_total - acumulado - ajuste_nc)
                cuotas_cliente[obj[0]] = f'{ult_cuota:,.2f}'
    return cuotas_cliente


def ajuste_letra_v2(modelo, post, user):
    fecha = str(datetime.now().date())
    if post['ajuste_letra_1'] == post['ajuste_letra_2'] == '':
        modelo.ajuste_letra_2 = None
    else:
        if post['ajuste_letra_1'] != '' and post['ajuste_letra_2'] != '':
            modelo.ajuste_letra_2 = post['ajuste_letra_1'] + ';' + post['ajuste_letra_2']
        else:
            return {'advertencia': 'Ingrese el monto de ambas cuotas para continuar.'}
    if modelo.ajuste_letra:
        modelo.ajuste_letra = None
    if modelo.lista_cambios:
        modelo.lista_cambios += '>' + fecha + ': ' + user + '=> ' + post[
            'mensaje'] + '| Se cambió cuota regular del crédito.' + '\n'
    else:
        modelo.lista_cambios = '>' + fecha + ': ' + user + '=> ' + post[
            'mensaje'] + '| Se cambió cuota regular del crédito.' + '\n'
    modelo.save()
    eliminar_pagos(modelo)
    return {'ok': 'Letra ajustada'}


def gen_tr_id(modelo, post):
    if modelo.cliente:
        empresa_id = modelo.cliente.empresa_2_id
    else:
        empresa_id = modelo.cliente_ant.empresa_2_id
    print(empresa_id)
    if post['tipo_registro'] == '1':
        consulta = Transacciones.objects.filter(empresa_id=empresa_id).last()
        salida = check_gen_tr(modelo, post, consulta, empresa_id, 'empresa')
    elif post['tipo_registro'] == '2':
        consulta = Transacciones.objects.filter(cliente_id=modelo.id).last()
        salida = check_gen_tr(modelo, post, consulta, empresa_id, 'cliente')
    else:
        return {'error': 'Consulte a dept. IT.'}
    return {'new_id': salida}


def check_gen_tr(modelo, post, consulta, empresa_id, tipo):
    fecha = ''.join(post['fecha_tr'].split('-'))
    if consulta:
        tr_info = consulta.id_tr.split('-')
        print(''.join(post['fecha_tr'].split('-')), len(tr_info), consulta.id_tr)
        if len(tr_info) == 4:
            if tr_info[2] == fecha:
                contador = str(int(tr_info[3]) + 1)
                if tipo == 'cliente':
                    salida = 'C-' + str(modelo.id) + '-' + fecha + '-' + contador
                else:
                    salida = 'E-' + str(empresa_id) + '-' + fecha + '-' + contador
            else:
                if tipo == 'cliente':
                    salida = 'C-' + str(modelo.id) + '-' + fecha + '-1'
                else:
                    salida = 'E-' + str(empresa_id) + '-' + fecha + '-1'
        else:
            if tipo == 'cliente':
                salida = 'C-' + str(modelo.id) + '-' + fecha + '-1'
            else:
                salida = 'E-' + str(empresa_id) + '-' + fecha + '-1'
    else:
        if tipo == 'cliente':
            salida = 'C-' + str(modelo.id) + '-' + fecha + '-1'
        else:
            salida = 'E-' + str(empresa_id) + '-' + fecha + '-1'
    return salida


def cargar_trs(val, contador):
    valor = {
        'num_pago': contador,
        'id_tr': str(val.tr.id_tr),
        'banco': str(val.tr.banco),
        'fecha_pago': str(val.fecha_pago_1),
        'fecha_aplicada': str(val.fecha_pago_2),
        'monto_cuota': str(val.monto_2),
        'monto_pagado': str(val.monto_1),
        'id_tr_db': str(val.tr.id)
    }
    if val.desembolso.cliente:
        emp_cliente = val.desembolso.cliente.cliente.empresa_2_id
        if val.tr.empresa: emp_tr_pago = val.tr.empresa_id
        else: emp_tr_pago = False
        if emp_tr_pago:
            if emp_tr_pago == emp_cliente: tr_source = 'Empresa actual'
            else: tr_source = 'Empresa anterior'
        else: tr_source = 'Cliente'
    else:
        emp_cliente = val.desembolso.cliente_ant.empresa_2_id
        if val.tr.empresa: emp_tr_pago = val.tr.empresa_id
        else: emp_tr_pago = False
        if emp_tr_pago:
            if emp_tr_pago == emp_cliente: tr_source = 'Empresa actual'
            else: tr_source = 'Empresa anterior'
        else: tr_source = 'Cliente'
    valor['tr_source'] = tr_source
    # print(tr_source)
    return valor


def cargar_trs_2(val):
    consulta = Pagos.objects.filter(tr_id=val.tr_id).aggregate(Sum('monto_1'))
    monto_disp = val.tr.monto_tr - consulta['monto_1__sum']
    valor = {
        'id_tr': str(val.tr.id_tr),
        'banco': str(val.tr.banco),
        'fecha_tr': str(val.tr.fecha_tr),
        'monto_tr': str(val.tr.monto_tr),
        'monto_disp': str(monto_disp),
        'id_tr_db': str(val.tr.id)
    }
    # print(consulta)
    if val.desembolso.cliente:
        emp_cliente = val.desembolso.cliente.cliente.empresa_2_id
        if val.tr.empresa:
            emp_tr_pago = val.tr.empresa_id
        else:
            emp_tr_pago = False
        if emp_tr_pago:
            if emp_tr_pago == emp_cliente:
                tr_source = 'Empresa actual'
            else:
                tr_source = 'Empresa anterior'
        else:
            tr_source = 'Cliente'
    else:
        emp_cliente = val.desembolso.cliente_ant.empresa_2_id
        if val.tr.empresa:
            emp_tr_pago = val.tr.empresa_id
        else:
            emp_tr_pago = False
        if emp_tr_pago:
            if emp_tr_pago == emp_cliente:
                tr_source = 'Empresa actual'
            else:
                tr_source = 'Empresa anterior'
        else:
            tr_source = 'Cliente'
    valor['tr_source'] = tr_source
    # print(tr_source)
    return valor


def cargar_info_trs(post):
    consulta_1 = Pagos.objects.filter(tr=post['id_tr']).order_by('created_date')
    data = []
    pagos = []
    if consulta_1:
        info = 0
        info_tr = None
        saldo_disponible = 0
        for i in consulta_1:
            if info == 0:
                tr = i.tr
                info_tr = {
                    'id_tr': tr.id_tr,
                    'banco': str(tr.banco),
                    'tipo': tr.tipo_pago,
                    'monto': str(tr.monto_tr),
                    'fecha': tr.fecha_tr,
                    'info': tr.info_tr,
                }
                if tr.cliente:
                    info_tr['registro'] = str(tr.cliente)
                else:
                    info_tr['registro'] = str(tr.empresa)
                if tr.notas:
                    info_tr['notas'] = tr.notas
                else:
                    info_tr['notas'] = None
                saldo_disponible = tr.monto_tr
            pg_info = {
                'fecha_1': str(i.fecha_pago_1),
                'fecha_2': str(i.fecha_pago_2),
                'num_cuota': i.num_cuota,
                'monto_pagado': str(i.monto_1)
            }
            if i.desembolso.cliente:
                pg_info['cliente_new'] = str(i.desembolso.cliente.cliente.nombre_corto())
            elif i.desembolso.cliente_ant:
                pg_info['cliente_ant'] = str(i.desembolso.cliente_ant)
            pagos.append(pg_info)
            saldo_disponible -= i.monto_1
        data.append(info_tr)
        data.append(pagos)
        data.append(saldo_disponible)
    return data


def disp_tr_list(consulta, tr_id):
    pagos_tr = consulta.filter(tr_id=tr_id).aggregate(sum=Sum('monto_1'))
    tr = consulta.filter(tr_id=tr_id)[0].tr
    disponible = tr.monto_tr - pagos_tr['sum']
    # print(disponible)
    salida = gen_tr_list(tr, disponible)
    # print(salida)
    return salida


def gen_tr_list(val, monto_disp='total'):
    valor = {
        'id_tr': str(val.id_tr),
        'banco': str(val.banco),
        'fecha_tr': str(val.fecha_tr),
        'monto_tr': str(val.monto_tr),
        'id_tr_db': str(val.id)
    }
    if monto_disp == 'total': valor['monto_disp'] = str(val.monto_tr)
    else: valor['monto_disp'] = str(monto_disp)
    # print(consulta)
    if val.cliente:
        tr_source = 'Cliente'
    else: tr_source = 'Empresa'
    valor['tr_source'] = tr_source
    # print(tr_source)
    return valor


def clientes_pagos_tr(modelo):
    modelo = modelo.desembolso
    if modelo.cliente:
        nombre = str(modelo.cliente)
        contrato = modelo.cliente.num_contrato
    else:
        nombre = str(modelo.cliente_ant)
        contrato = modelo.cliente_ant.num_contrato
    salida = '<li>' + contrato + ': '
    salida += nombre + '</li>'
    return salida
