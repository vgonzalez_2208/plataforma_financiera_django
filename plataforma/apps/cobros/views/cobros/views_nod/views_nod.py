from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView

from apps.cobros.forms import FormEmpresaNOD
from apps.cobros.models import Desembolso, Pagos
from apps.cobros.views.cobros.functions import desglose_cobros, to_num, detalles_cliente_global, meses_atraso, \
    atraso_lista_desembolso, cuotas_pagadas


class AplicarNuevaOD(LoginRequiredMixin, DetailView):
    model = Desembolso
    template_name = 'cobros/nueva_OD/nueva_OD_ver.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        self.object = self.get_object()
        modelo = self.object
        if modelo.cliente:
            context['tipo'] = 'new'
            context['info_cliente'] = modelo.cliente.cliente
            context['info_credito'] = modelo.cliente
            f_adj = modelo.cliente.fecha_ini.split('-')
            if len(f_adj) > 1: f_rev = f_adj[0] + '/' + f_adj[1] + '/' + f_adj[2]
            else: f_rev = modelo.cliente.fecha_ini
            context['fecha_inicio'] = datetime.strptime(f_rev, '%Y/%m/%d').date()
            info_cr = modelo.cliente
        else:
            context['tipo'] = 'ant'
            context['info_cliente'] = modelo.cliente_ant
            context['info_credito'] = modelo.cliente_ant
            context['fecha_inicio'] = modelo.cliente_ant.fecha_ini
            info_cr = modelo.cliente_ant
            # info = desglose_cobros(modelo)
        context['section_name'] = 'Cobros'
        context['card_title'] = 'Contrato #: '
        context['active_cobros'] = 'active'
        context['active_cobros_pagos'] = 'active'
        context['cumplimiento'] = ('guillermo.lemos', 'vicente_rc', 'sum.ling')
        context['form'] = FormEmpresaNOD
        detalles = desglose_cobros(modelo, 4)
        context['meses_extra'] = str(detalles['meses_extra'])
        context['cuota_regular'] = str(detalles['letra_q'])
        context['cuota_final'] = detalles['letra_qu']
        monto = to_num(info_cr.monto)
        plazo = to_num(info_cr.plazo)
        meses_total = to_num(plazo + detalles['meses_extra'])
        ob_total = to_num(info_cr.ob_contrato)
        detalles_cr = detalles_cliente_global(monto, plazo, meses_total, ob_total)
        context['total_intereses'] = detalles_cr[0]
        monto_pagado, cuotas_pagas = cuotas_pagadas(modelo, detalles['letra_q'])
        context['saldo_agregado'] = to_num(float(ob_total) - float(monto_pagado))
        context['pagado_cliente'] = cuotas_pagadas(modelo)
        # Fecha último pago
        consulta_1 = Pagos.objects.filter(desembolso=modelo).last()
        if consulta_1:
            context['fecha_ult_pago'] = consulta_1.fecha_pago_1
            m_atraso = meses_atraso(consulta_1.fecha_pago_1, datetime.now().date())
            context['tiempo_mora'] = atraso_lista_desembolso(m_atraso)
        else:
            context['fecha_ult_pago'] = '**Sin pagos**'
            m_atraso = meses_atraso(context['info_credito'].fecha_ini_OD, datetime.now().date())
            context['tiempo_mora'] = atraso_lista_desembolso(m_atraso)
        # Para ver archivo de OD se debe enviar info de od_file y od_file_dir en el contexto
        return context
