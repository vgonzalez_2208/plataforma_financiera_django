from datetime import datetime
from decimal import Decimal as dec
from math import ceil

from apps.cobros.views.cobros.functions import desglose_cobros, strpdate


def crear_tabla(info_row):
    temp = []
    for i in info_row:
        columnas = []
        for y in i:
            columnas.append(crear_td(y))
        columnas = ''.join(columnas)
        temp.append(crear_tr(columnas))
    return ''.join(temp)


def crear_td(td, align_r=False):
    if align_r:
        return '<td class="td_mod text-end">' + str(td) + '</td>'
    if type(td) == float: td = '$' + f'{round(td, 2):,.2f}'  # <class 'float'>
    elif type(td) == int: td = str(td)
    return '<td class="td_mod">' + td + '</td>'


def crear_tr(tr):
    return '<tr>' + tr + '</tr>'


def crear_lista(modelo, pagos, saldo_inicial):
    print(pagos[0].monto_1)
    data = desglose_cobros(modelo)
    # print(data[0])
    pre_salida = {}
    salida = []
    cuota_anterior = [0, 0]
    suma_cuotas = [0, 0]
    for i in data[0].items():
        fecha = strpdate(i[0])
        if fecha.month != 12 and fecha <= datetime.now().date():
            if strpdate(i[0]).day == 15:
                if cuota_anterior[0] != 0:
                    suma_cuotas[0] += cuota_anterior[0]
                    cuota_anterior[0] = round(float(i[1]), 2)
                else:
                    cuota_anterior[0] = round(float(i[1]), 2)
            elif strpdate(i[0]).day > 15:
                if cuota_anterior[1] != 0:
                    suma_cuotas[1] += cuota_anterior[1]
                    cuota_anterior[1] = round(float(i[1]), 2)
                else:
                    cuota_anterior[1] = round(float(i[1]), 2)
            pre_salida[i[0]] = [i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0']
            salida.append([i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0'])

    saldo = round(dec(saldo_inicial), 2)
    print(suma_cuotas)
    return salida


def crear_lista_2(modelo, pagos, saldo_inicial):
    # print(pagos[0].monto_1)
    data = desglose_cobros(modelo)
    # print(data[0])
    pre_salida = {}
    salida = []
    cuota_anterior = [0, 0]
    suma_cuotas = [[], []]
    for i in data[0].items():
        fecha = strpdate(i[0])
        if fecha.month != 12 and fecha <= datetime.now().date():
            # total_cuotas = [round(sum(suma_cuotas[0]), 2), round(sum(suma_cuotas[1]), 2)]
            if strpdate(i[0]).day == 15:
                # pre_salida[i[0]] = [i[0], desc_cuota(i[0]), str(i[1]), '0', str(cuota_anterior[0]), str(total_cuotas[0]), '0', '0']
                pre_salida[i[0]] = [i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0']
                salida.append([i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0'])
                if cuota_anterior[0] != 0:
                    suma_cuotas[0].append(cuota_anterior[0])
                    cuota_anterior[0] = round(float(i[1]), 2)
                else:
                    cuota_anterior[0] = round(float(i[1]), 2)
                    suma_cuotas[0].append(cuota_anterior[0])
            elif strpdate(i[0]).day > 15:
                # pre_salida[i[0]] = [i[0], desc_cuota(i[0]), str(i[1]), '0', str(cuota_anterior[1]), str(total_cuotas[1]), '0', '0']
                pre_salida[i[0]] = [i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0']
                salida.append([i[0], desc_cuota(i[0]), str(i[1]), '0', '0', '0', '0', '0'])
                if cuota_anterior[1] != 0:
                    suma_cuotas[1].append(cuota_anterior[1])
                    cuota_anterior[1] = round(float(i[1]), 2)
                else:
                    cuota_anterior[1] = round(float(i[1]), 2)
                    suma_cuotas[1].append(cuota_anterior[1])
    dic_pagos = dict_pagos(pagos, 2)
    saldo_temp = round(float(saldo_inicial), 2)
    salida_2 = ()
    contador = 0
    pago_anterior = [0, 0]
    suma_pagos = [[], []]
    for item in pre_salida.items():
        dic_item = dic_pagos.get(item[0])
        temp = item[1]
        if dic_item:
            t_pagos = 0
            for i in dic_item:
                t_pagos += float(i[1])
                temp[1] = desc_cuota(item[0], 'pago')
                temp[3] = str(i[1])
                saldo_temp -= float(i[1])
                temp[7] = '$ ' + f'{round(saldo_temp, 2):,.2f}'
                salida_2 = (*salida_2, tuple(temp))
            if strpdate(item[0]).day == 15:
                if pago_anterior[0] != 0:
                    suma_pagos[0].append(pago_anterior[1])
                    pago_anterior[0] = round(t_pagos, 2)
                else:
                    pago_anterior[0] = round(t_pagos, 2)
                    suma_pagos[0].append(pago_anterior[0])
            elif strpdate(item[0]).day > 15:
                if pago_anterior[1] != 0:
                    suma_pagos[1].append(pago_anterior[1])
                    pago_anterior[1] = round(t_pagos, 2)
                else:
                    pago_anterior[1] = round(t_pagos, 2)
                    suma_pagos[1].append(pago_anterior[1])
        else:
            contador += 1
            salida_2 = (*salida_2, tuple(item[1]))
    print(suma_cuotas)
    print(suma_pagos)
    return list(salida_2)


def desc_cuota(fecha, tipo='letra'):
    fecha = strpdate(fecha)
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio']
    meses += ['Julio', 'Agosto', 'Sept.', 'Oct.', 'Nov.', 'Dic.']
    if fecha.day == 15:
        salida = 'I de ' + meses[fecha.month - 1]
    else:
        salida = 'II de ' + meses[fecha.month - 1]
    if tipo == 'pago':
        salida = 'Pago ' + salida
    return salida


# Funciones Desglose Prepago

def lista_prepago(modelo, monto, pagos, saldo_inicial, plazo, valor_comision, flag=True):
    salida = []
    if flag:
        pagos = dict_pagos(pagos)
    cuotas_cliente = desglose_cobros(modelo, 3)
    num_pagos = len(cuotas_cliente)
    diciembres = num_pagos - int(plazo)*2
    # print(diciembres)
    contador_1, contador_2 = 0, 0
    # Interés | seguro | notaría | comisión
    detalles_cobro = parametros_cobro(monto, saldo_inicial, num_pagos / 2, 0.02, valor_comision)
    # Cuota | pago | abono_mora | interés | seguro | capital | notaría | comisión
    tabla_detalles = [[], [], [], [], [], [], [], []]
    # Pago de interés + capital quincenal
    print(detalles_cobro)
    int_capital = round((float(monto) + detalles_cobro[0])/num_pagos, 2)  # Suma de interés y capital cuota
    for i in cuotas_cliente.items():
        temp = []
        contador_1 += 1
        if i[1] != 0: contador_2 += 1
        temp.append(i[0])  # Fecha de pago
        temp.append(contador_2)  # Num. de pago
        fecha = strpdate(i[0])
        temp.append(fecha.strftime('%b') + '-' + str(fecha.year))  # Mes
        tabla_detalles[0].append(i[1])
        temp.append(i[1])  # Cuota
        temp.append('')
        temp.append('')
        temp.append('')
        tabla_detalles[1].append(i[1])
        temp.append(i[1])  # Pago
        pago = pagos.get(i[0])
        if pago:
            if float(pago) > float(i[1]):
                mora = round(float(pago) - float(i[1]), 2)
            else: mora = 0
        else:
            mora = 0
        tabla_detalles[2].append(mora)
        temp.append(mora)  # Abono morosidad
        abono_int = formula_f(detalles_cobro[0], num_pagos, contador_1, sum(tabla_detalles[3]))
        tabla_detalles[3].append(abono_int)
        temp.append(abono_int)  # Abono interés
        abono_seguro = formula_f(detalles_cobro[1], num_pagos, contador_1, sum(tabla_detalles[4]), 2)
        tabla_detalles[4].append(abono_seguro)
        temp.append(abono_seguro)  # Abono seguro
        abono_capital = round(int_capital - abono_int, 2)
        tabla_detalles[5].append(abono_capital)
        temp.append(abono_capital)  # Abono capital
        if i[1] != 0: abono_notaria = round(float((ceil(detalles_cobro[2]*100/(float(plazo)*2)))/100), 2)
        else: abono_notaria = 0
        tabla_detalles[6].append(abono_notaria)
        temp.append(abono_notaria)  # Abono notaría
        if i[1] != 0: abono_comision = round(float((ceil(detalles_cobro[3] * 100 / (float(plazo) * 2))) / 100), 2)
        else: abono_comision = 0
        tabla_detalles[7].append(abono_comision)
        temp.append(abono_comision)  # Abono comision
        saldo_capital = round(float(monto) - sum(tabla_detalles[5]), 2)
        temp.append(saldo_capital)  # Saldo capital
        saldo_agregado = round(float(saldo_inicial) - sum(tabla_detalles[1]), 2)
        temp.append(saldo_agregado)

        salida.append(tuple(temp))
    # for i in salida: print(i)
    print(sum(tabla_detalles[3]), sum(tabla_detalles[5]))
    return salida


def formula_f(int_neto, quincenas_total, contador, acumulado, imp=1):
    if contador == 0.5:
        temp_1 = ((quincenas_total - contador)**2 + (quincenas_total - contador)) * int_neto / (quincenas_total**2 + quincenas_total)
        salida = int_neto - temp_1
    else:
        meses_f = quincenas_total - contador
        temp_1 = (meses_f ** 2 + meses_f) * int_neto / (quincenas_total ** 2 + quincenas_total)
        salida = int_neto - acumulado - temp_1
    ######
        # meses_f = quincenas_total - contador + 0.5
        # temp_1 = (meses_f**2 + meses_f) * int_neto / (quincenas_total**2 + quincenas_total)
        # meses_f = quincenas_total - contador
        # temp_2 = (meses_f ** 2 + meses_f) * int_neto / (quincenas_total ** 2 + quincenas_total)
        # salida = temp_1 - temp_2
    # if imp == 1: print('int neto', int_neto, 'total quincenas', quincenas_total, 'contador', contador, 'salida', salida)
    return round(salida, 2)


def test_func(pago, info_lista):
    salida = []
    if pago.get(info_lista[0]):
        salida.append([info_lista[0], info_lista[1], pago[info_lista[0]]])
    else:
        salida.append([info_lista[0], info_lista[1], 0])
    return salida


def dict_pagos(pagos, modo=1):
    salida = {}
    for i in pagos:
        pago = salida.get(str(i.fecha_pago_1))
        if modo == 1:
            if pago:
                pago_temp = round(pago + float(i.monto_1), 2)
                salida[str(i.fecha_pago_1)] = pago_temp
            else:
                pago_temp = round(float(i.monto_1), 2)
                salida[str(i.fecha_pago_1)] = pago_temp
        else:
            if pago:
                pago_temp = [str(i.tr.fecha_tr), i.monto_1]
                salida[str(i.fecha_pago_1)].append(pago_temp)
            else:
                pago_temp = [str(i.tr.fecha_tr), i.monto_1]
                lista_pagos = [pago_temp]
                salida[str(i.fecha_pago_1)] = lista_pagos
    return salida


def parametros_cobro(monto, ob_total, plazo, int_mes, valor_comision):
    monto = float(monto)
    plazo = float(plazo)
    int_cobro = round(monto*plazo*int_mes, 2)
    seguro_cobro = round(monto*plazo*(0.035 + 0.025)/12 + plazo*1.25, 2)
    notaria = 12
    # if ajuste_cobro == 1:
    comision = round(float(ob_total) - int_cobro - seguro_cobro - notaria - monto, 2)
    return [int_cobro, seguro_cobro, notaria, comision]

