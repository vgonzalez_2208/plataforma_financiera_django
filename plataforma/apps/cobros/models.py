import os

from django.db import models
from django.forms import model_to_dict
from django.utils import timezone as tz
from django.utils.translation import gettext_lazy as _

# Create your models here.
from apps.cobros.choices import *
from apps.formulario.models import TipoIngreso, TipoDocumento, Genero, Provincia, Distrito, Corregimiento, Motivo, \
    EstadoCivil, Dependientes, IngresosExtra, Escolaridad, PEP
from apps.prestamos.models import Aprobado
from apps.ventas.models import Empresa


class ClasePago(models.Model):
    clase_pago = models.CharField(_('clase pago'), max_length=30)

    def __str__(self):
        salida = self.clase_pago
        return salida


class Bancos(models.Model):
    nombre = models.CharField(_('nombre'), max_length=40)
    num_cuenta = models.CharField(_('num cuenta'), max_length=60, null=True, blank=True)
    telefono = models.CharField(_('telefono'), max_length=15, null=True, blank=True)
    nombre_cuenta = models.CharField(_('num cuenta'), max_length=60, null=True, blank=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item

    def __str__(self):
        salida = self.nombre
        return salida


class ClienteAntiguo(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=False, blank=True, default=tz.now)  #
    fecha_ingreso = models.DateTimeField(auto_now_add=True)  #
    monto = models.CharField(_('monto'), null=False, blank=False, max_length=20)  #
    plazo = models.CharField(_('plazo meses'), null=False, blank=False, max_length=40)  #
    compra_saldo = models.CharField(_('cancelaciones'), null=True, blank=True, max_length=200)  #
    promotor = models.CharField(_('promotor'), max_length=100, null=True, blank=True)  #
    c_promotor = models.CharField(_('c promotor'), max_length=40, null=True, blank=True)  #
    s_descuento = models.CharField(_('s descuento'), max_length=40, null=True, blank=True)  #
    fecha_ini = models.DateField(_('f inicio'), null=False, blank=False)  #
    f_contrato = models.DateField(_('f contrato'), null=True, blank=True)  #
    ob_contrato = models.CharField(_('ob contrato'), null=True, blank=True, max_length=20)  #
    c_cierre = models.CharField(_('c cierre'), max_length=40, null=True, blank=True)  #
    tipo_ingreso = models.ForeignKey(TipoIngreso, on_delete=models.SET_NULL, null=True)  #
    salario = models.DecimalField(_('salario'), max_digits=6, decimal_places=2, null=False, blank=False)  #
    empresa_1 = models.CharField(_('empresa'), max_length=50, null=True, blank=True)  #
    empresa_2 = models.ForeignKey(Empresa, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)  #
    nombre_1 = models.CharField(_('nombre 1'), max_length=40, null=False, blank=False)  #
    nombre_2 = models.CharField(_('nombre 2'), max_length=40, null=True, blank=True)  # Puede estar en blanco  #
    apellido_1 = models.CharField(_('apellido 1'), max_length=40, null=False, blank=False)  #
    apellido_2 = models.CharField(_('apellido 2'), max_length=40, null=True, blank=True)  #
    genero = models.ForeignKey(Genero, null=True, blank=True, on_delete=models.SET_NULL)  #
    f_nacimiento = models.DateField(_('f nacimiento'))  #
    tipo_doc = models.ForeignKey(TipoDocumento, on_delete=models.SET_NULL, null=True)  #
    nacionalidad = models.CharField(_('nacionalidad'), max_length=50, null=True, blank=True)  #
    dni = models.CharField(_('dni'), max_length=20, null=False, blank=False)  #
    email_1 = models.EmailField(_('email 1'), max_length=254, null=True, blank=True)  #
    celular = models.IntegerField(_('celular'), null=False, blank=False)  #
    telefono = models.IntegerField(_('telefono'), null=True, blank=True)  # Disponible sólo en antiguo
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)  #
    notas_user = models.CharField(_('notas'), max_length=1000, null=True, blank=True)  #
    check_cumplimiento = models.CharField(_('check cumplimiento'), null=True, blank=True, max_length=40,
                                          default='No verificado')  # S. antiguo
    id_cliente = models.CharField(_('id cliente'), max_length=15, null=True, blank=True)  # Temporal
    estatus = models.CharField(_('estatus'), max_length=50, default='Activo')  #
    provincia = models.ForeignKey(Provincia, on_delete=models.SET_NULL, null=True, blank=True)  #
    distrito = models.ForeignKey(Distrito, on_delete=models.SET_NULL, null=True, blank=True)  #
    corregimiento = models.ForeignKey(Corregimiento, on_delete=models.SET_NULL, null=True, blank=True)  #
    direccion = models.CharField(_('dir_cliente'), max_length=250, null=True, blank=True)  #
    creado_por = models.CharField(_('creado por'), max_length=40, null=True, blank=True)  # Eq. visto por
    num_contrato = models.CharField(_('id contrato'), max_length=30, null=True, blank=True)  #
    fecha_ini_OD = models.DateField(_('f inicio OD'), null=True, blank=True)  #

    # Complementos Aprobación
    orden_descuento = models.FileField(_('orden descuento'), upload_to='Aprobados/orden_descuento/%Y/%m/%d', null=True,
                                       blank=True)
    contrato = models.FileField(_('contrato'), upload_to='Aprobados/contrato/%Y/%m/%d', null=True, blank=True)
    pagare = models.FileField(_('pagare'), upload_to='Aprobados/pagare/%Y/%m/%d', null=True, blank=True)
    conoce_cliente = models.FileField(_('conoce cliente'), upload_to='Aprobados/conoce_cliente/%Y/%m/%d', null=True,
                                      blank=True)
    f_1 = models.FileField(_('f 1'), upload_to='Aprobados/F_1/%Y/%m/%d', null=True, blank=True)

    # Complementos PreAprobado
    dependientes = models.ForeignKey(Dependientes, on_delete=models.SET_NULL, null=True)
    motivo = models.ForeignKey(Motivo, on_delete=models.SET_NULL, null=True, blank=True)
    e_civil = models.ForeignKey(EstadoCivil, on_delete=models.SET_NULL, null=True)
    in_extras = models.ForeignKey(IngresosExtra, on_delete=models.SET_NULL, null=True)
    estudios = models.ForeignKey(Escolaridad, on_delete=models.SET_NULL, null=True)
    p_expuesto = models.ForeignKey(PEP, on_delete=models.SET_NULL, null=True)
    terms = models.BooleanField(_('terms'), default=False)
    apc = models.BooleanField(_('apc'), default=False)

    # Documentos PreAprobado
    DNI_img = models.FileField(_('dni img'), upload_to='DNI/%Y/%m/%d', null=True, blank=True)
    carta_trabajo_img = models.FileField(_('carta trabajo'), upload_to='carta_trabajo/%Y/%m/%d', null=True, blank=True)
    talonario_1_img = models.FileField(_('talonario 1'), upload_to='talonario_1/%Y/%m/%d', null=True, blank=True)
    talonario_2_img = models.FileField(_('talonario 2'), upload_to='talonario_2/%Y/%m/%d', null=True, blank=True)
    ficha_img = models.FileField(_('ficha'), upload_to='ficha_seguro/%Y/%m/%d', null=True, blank=True)
    recibo_img = models.FileField(_('recibo'), upload_to='recibo_publico/%Y/%m/%d', null=True, blank=True)
    carta_saldo_img = models.FileField(_('carta_saldo'), upload_to='carta_saldo/%Y/%m/%d', null=True, blank=True)
    file_apc = models.FileField(_('file apc'), upload_to='APC/%Y/%m/%d', null=True, blank=True)
    notas_cumplimiento = models.CharField(_('notas cumplimiento'), max_length=1000, null=True, blank=True)
    notas_gerencia = models.CharField(_('notas gerencia'), max_length=1000, null=True, blank=True)
    notas_cobros = models.CharField(_('notas gerencia'), max_length=1000, null=True, blank=True)

    def __str__(self):
        salida = self.nombre_1 + ' ' + self.apellido_1
        return salida

    # Docs PreAprobado

    def dni_name(self):
        return os.path.basename(self.DNI_img.name)

    def carta_trabajo_name(self):
        return os.path.basename(self.carta_trabajo_img.name)

    def talonario_1_name(self):
        return os.path.basename(self.talonario_1_img.name)

    def talonario_2_name(self):
        return os.path.basename(self.talonario_2_img.name)

    def ficha_name(self):
        return os.path.basename(self.ficha_img.name)

    def recibo_name(self):
        return os.path.basename(self.recibo_img.name)

    def carta_saldo_name(self):
        return os.path.basename(self.carta_saldo_img.name)

    def file_apc_name(self):
        return os.path.basename(self.file_apc.name)

    # Docs Aprobado

    def od_name(self):
        return os.path.basename(self.orden_descuento.name)

    def f1_name(self):
        return os.path.basename(self.f_1.name)

    def contrato_name(self):
        return os.path.basename(self.contrato.name)

    def pagare_name(self):
        return os.path.basename(self.pagare.name)

    def c_cliente_name(self):
        return os.path.basename(self.conoce_cliente.name)


class Desembolso(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    cliente = models.ForeignKey(Aprobado, null=True, blank=True, on_delete=models.CASCADE)
    cliente_ant = models.ForeignKey(ClienteAntiguo, null=True, blank=True, on_delete=models.CASCADE)
    lista_pagos = models.CharField(max_length=1000, null=True, blank=True)
    lista_cambios = models.CharField(max_length=1500, null=True, blank=True)
    ajuste = models.CharField(_('ajuste'), max_length=100, default='No requerido')
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)
    notas_user = models.CharField(_('notas'), max_length=1000, null=True, blank=True)  #
    # Cancelado: cuotas terminadas, Reestructurado: estatus con nuevos parámetros de cálculo
    estatus = models.CharField(_('estatus'), max_length=50,
                               default='Activo')  # Activo, cancelado, vencido, pagado, anticipado**
    # Comprobante ACH de la transferencia
    comprobante = models.FileField(_('comprobante'), upload_to='Aprobados/comprobante/%Y/%m/%d', null=True, blank=True)
    ajuste_letra = models.DecimalField(_('ajuste letra'), max_digits=5, decimal_places=2, null=True, blank=True)
    ajuste_letra_2 = models.CharField(_('ajuste letra 2'), max_length=40, null=True, blank=True)
    ajuste_NC = models.CharField(_('ajuste NC'), max_length=100, null=True, blank=True)
    creado_por = models.CharField(_('creado por'), max_length=50, null=True, blank=True)
    f_desembolso = models.DateField(_('f desembolso'), null=True, blank=True)  #
    od_activa = models.CharField(_('od activa'), max_length=50, default='OD_1', null=True, blank=True)
    info_od_activa = models.CharField(_('info od activa'), max_length=2000, null=True, blank=True)

    # Órdenes de descuento nuevas
    orden_descuento_2 = models.FileField(_('orden descuento 2'), upload_to='Aprobados/orden_descuento_2/%Y/%m/%d',
                                         null=True, blank=True)
    orden_descuento_3 = models.FileField(_('orden descuento 3'), upload_to='Aprobados/orden_descuento_3/%Y/%m/%d',
                                         null=True, blank=True)
    orden_descuento_4 = models.FileField(_('orden descuento 4'), upload_to='Aprobados/orden_descuento_4/%Y/%m/%d',
                                         null=True, blank=True)

    def __str__(self):
        if self.cliente:
            return str(self.cliente)
        else:
            return str(self.cliente_ant)

    def empresa_cliente(self):
        if self.cliente:
            return self.cliente
        else:
            return self.cliente_ant


class Transacciones(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    fecha_tr = models.CharField(_('fecha tr'), max_length=40, null=False, blank=False)
    tipo_pago = models.CharField(_('tipo pago'), max_length=50, choices=tipo_pago_dic, null=True, blank=True)
    empresa = models.ForeignKey(Empresa, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)
    cliente = models.ForeignKey(Desembolso, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)
    banco = models.ForeignKey(Bancos, max_length=50, null=True, blank=True, on_delete=models.SET_NULL)
    monto_tr = models.DecimalField(_('monto tr'), max_digits=7, decimal_places=2, null=False, blank=False)
    info_tr = models.CharField(_('info tr'), max_length=100, null=False, blank=False)
    id_tr = models.CharField(_('id tr'), max_length=100, null=True, blank=True)
    notas = models.CharField(_('notas'), max_length=500, null=True, blank=True)
    estatus_tr = models.CharField(_('estatus tr'), max_length=40, default='Regular')  # Cambia a Prepago si lo hay

    def __str__(self):
        salida = self.info_tr
        return salida


class TransaccionesSalientes(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    fecha_tr = models.CharField(_('fecha tr'), max_length=40)
    modo_pago = models.CharField(_('modo pago'), max_length=50, choices=modo_pago_dic_dev, null=True, blank=True)
    tipo_pago = models.CharField(_('tipo pago'), max_length=50, choices=tipo_pago_dic_dev, null=True, blank=True)
    empresa = models.ForeignKey(Empresa, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)
    cliente = models.ForeignKey(Desembolso, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)
    banco = models.ForeignKey(Bancos, max_length=50, null=True, blank=True, on_delete=models.SET_NULL)
    monto_tr = models.DecimalField(_('monto tr'), max_digits=7, decimal_places=2)
    info_tr = models.CharField(_('info tr'), max_length=100)
    id_tr = models.CharField(_('id tr'), max_length=100, null=True, blank=True)
    notas = models.CharField(_('notas'), max_length=500, null=True, blank=True)
    comprobante = models.FileField(_('comprobante'), upload_to='Tr_salientes/comprobante/%Y/%m/%d', null=True,
                                   blank=True)
    estatus_tr = models.CharField(_('estatus tr'), max_length=40, default='Regular')  # Cambia a Prepago si lo hay

    def __str__(self):
        salida = self.info_tr
        return salida


class Pagos(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    desembolso = models.ForeignKey(Desembolso, null=False, blank=False, on_delete=models.CASCADE)
    num_cuota = models.CharField(_('num cuota'), max_length=20, null=True, blank=True)
    fecha_pago_1 = models.DateField(_('fecha pago 1'), max_length=20, null=False,
                                    blank=False)  # Fecha de pago del crédito
    fecha_pago_2 = models.DateField(_('fecha pago 2'), max_length=20, null=False,
                                    blank=False)  # Fecha en que se registró el pago
    fecha_pago_3 = models.DateField(_('fecha pago 3'), max_length=20, null=True, blank=True)  # Fecha actual
    # tipo_pago = models.CharField(_('tipo pago'), max_length=50, choices=tipo_pago_dic, null=True, blank=True)
    # id_tr = models.CharField(_('id tr'), max_length=30, null=True, blank=True)
    # banco = models.ForeignKey(Bancos, null=True, blank=True, on_delete=models.SET_NULL)
    # fecha_tr = models.CharField(_('fecha tr'), max_length=40, null=True, blank=True)
    # info_tr = models.CharField(_('info tr'), max_length=100, null=True, blank=True)
    tr = models.ForeignKey(Transacciones, max_length=50, null=True, blank=True,
                           on_delete=models.CASCADE)  # Transacción bancaria registrada
    monto_1 = models.DecimalField(_('monto 1'), max_digits=6, decimal_places=2)  # Monto pagado por el cliente
    monto_2 = models.DecimalField(_('monto 2'), max_digits=6, decimal_places=2, null=True,
                                  blank=True)  # Pago aplicado al crédito, correspondiente
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)
    clase_pago = models.ForeignKey(ClasePago, on_delete=models.SET_NULL, null=True,
                                   blank=True)  # 1_ Pago regular, 2_ cancelación, 3_ morosidad
    usuario = models.CharField(_('usuario'), max_length=40)

    def __str__(self):
        salida = self.monto_2
        return salida


class PrepagoAnticipado(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    created_ref = models.DateTimeField(auto_now_add=True)
    desembolso = models.OneToOneField(Desembolso, on_delete=models.CASCADE)
    fecha_pago = models.DateField(_('fecha pago'))
    fecha_ret_int = models.DateField(_('fecha ret int'))  # Fecha retorno intereses
    fecha_ret_seg = models.DateField(_('fecha ret seg'))  # Fecha retorno seguros
    ret_int = models.DecimalField(_('ret int'), max_digits=6, decimal_places=2, null=True,
                                  blank=True)  # Monto int. retornado a cliente
    ret_seg = models.DecimalField(_('ret seg'), max_digits=6, decimal_places=2, null=True,
                                  blank=True)  # Monto seg. retornado a cliente
    penalidad = models.DecimalField(_('penalidad'), max_digits=6, decimal_places=2)
    calc_mode = models.CharField(_('calc mode'), max_length=20, default='Automático')  # Cálculo manual o automático
    tr_pagado = models.ForeignKey(Transacciones, null=True, blank=True,
                                  on_delete=models.SET_NULL)  # Transacción de pago
    tr_retornado = models.ForeignKey(TransaccionesSalientes, null=True, blank=True,
                                     on_delete=models.SET_NULL)  # Transacción de retorno a cliente
    comprobante = models.FileField(_('comprobante'), upload_to='Prepagos/comprobante/%Y/%m/%d', null=True, blank=True)
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)
    usuario = models.CharField(_('usuario'), max_length=40, default='sum.ling')

    def __str__(self):
        return str(self.desembolso)


class Reestructurado(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    created_ref = models.DateTimeField(auto_now_add=True)
    desembolso = models.OneToOneField(Desembolso, on_delete=models.CASCADE)
    int_global = models.CharField(_('Interés global'), max_length=10)
    s_colectivo = models.CharField(_('S. Colectivo'), max_length=10)
    s_desempleo = models.CharField(_('S. Desempleo'), max_length=10)
    s_dental = models.CharField(_('S. Dental'), max_length=10)  # Múltiples parámetros
    c_cierre = models.CharField(_('C. Cierre'), max_length=40)  # 0 si no la hay
    monto_r = models.DecimalField(_('Monto'), max_digits=6, decimal_places=2)
    plazo_r = models.IntegerField(_('Plazo'))  #
    cargos_extra = models.DecimalField(_('Cargos extra'), max_digits=6, decimal_places=2, null=True, blank=True)
    ob_contrato = models.DecimalField(_('Ob. Contrato'), max_digits=6, decimal_places=2, null=True, blank=True)  #
    fecha_ini = models.DateField(_('Fecha inicio'), null=False, blank=False)  #
    f_contrato = models.DateField(_('Fecha contrato'), null=True, blank=True)  #
    fecha_ini_OD = models.DateField(_('Fecha inicio O.D.'), null=True, blank=True)  #
    diciembres = models.CharField(_('Diciembres'), max_length=10)  # Si o No
    notas = models.CharField(_('Notas'), max_length=1000, null=True, blank=True)
    usuario = models.CharField(_('usuario'), max_length=40, default='sum.ling')
    num_contrato = models.CharField(_('Num. contrato'), max_length=30, null=True, blank=True)  #
    # Pendiente, Activo, Anticipado, Cancelado
    estatus = models.CharField(_('estatus'), max_length=50, default='Pendiente')  #
    # Pendiente, Aprobado, Anulado
    check_cumplimiento = models.CharField(_('check cumplimiento'), max_length=30, default='Pendiente')
    # Pendiente, Aprobado, Anulado
    check_gerencia = models.CharField(_('check gerencia'), max_length=30, default='Pendiente')
    lista_cambios = models.CharField(max_length=1500, null=True, blank=True)
    ajuste_letra = models.DecimalField(_('ajuste letra'), max_digits=5, decimal_places=2, null=True, blank=True)
    ajuste_NC = models.CharField(_('ajuste NC'), max_length=100, null=True, blank=True)

    # Complementos Aprobación
    orden_descuento = models.FileField(_('orden descuento'), upload_to='Reestructurados/orden_descuento/%Y/%m/%d',
                                       null=True, blank=True)
    contrato = models.FileField(_('contrato'), upload_to='Reestructurados/contrato/%Y/%m/%d', null=True, blank=True)
    pagare = models.FileField(_('pagare'), upload_to='Reestructurados/pagare/%Y/%m/%d', null=True, blank=True)
    conoce_cliente = models.FileField(_('conoce cliente'), upload_to='Reestructurados/conoce_cliente/%Y/%m/%d',
                                      null=True, blank=True)
    f_1 = models.FileField(_('f 1'), upload_to='Reestructurados/F_1/%Y/%m/%d', null=True, blank=True)

    def __str__(self):
        return str(self.desembolso)

    def od_name(self):
        return os.path.basename(self.orden_descuento.name)

    def f1_name(self):
        return os.path.basename(self.f_1.name)

    def contrato_name(self):
        return os.path.basename(self.contrato.name)

    def pagare_name(self):
        return os.path.basename(self.pagare.name)

    def c_cliente_name(self):
        return os.path.basename(self.conoce_cliente.name)


class NuevaOrdenDescuento(models.Model):
    cliente_regular = models.ForeignKey(Desembolso, null=True, blank=True, on_delete=models.CASCADE)
    cliente_rtt = models.ForeignKey(Reestructurado, null=True, blank=True, on_delete=models.CASCADE)
    num_OD = models.CharField(_('Número de OD'), max_length=20)
    # Espacio para cliente Refinanciado
    fecha_ini = models.DateField(_('Fecha inicio'))
    saldo_ini = models.DecimalField(_('Saldo inicial'), max_digits=7, decimal_places=2)
    recargos = models.DecimalField(_('Recargos'), max_digits=6, decimal_places=2, null=True, blank=True)
    motivo_r = models.CharField(_('Motivo recargo'), max_length=1000, null=True, blank=True)
    ajuste_cuota = models.DecimalField(_('Cuota'), max_digits=5, decimal_places=2, null=True, blank=True)
    ajuste_plazo = models.DecimalField(_('Ajuste de plazo'), max_digits=4, decimal_places=1, null=True, blank=True)
    parametros_ex = models.CharField(max_length=1000, null=True, blank=True)
    notas = models.CharField(_('Notas'), max_length=1000, null=True, blank=True)
    lista_cambios = models.CharField(max_length=2000, null=True, blank=True)
    estatus = models.CharField(_('estatus'), max_length=50, default='Pendiente')  #
    # Documentos
    orden_descuento = models.FileField(_('orden descuento'), upload_to='Aprobados/orden_descuento_nueva/%Y/%m/%d', null=True, blank=True)

    def __str__(self):
        if self.cliente_regular: return str(self.cliente_regular)
        else: return str(self.cliente_rtt)

    def od_name(self):
        return os.path.basename(self.orden_descuento.name)
