function info_cobro() {
    let url = window.location.pathname;
    // let data = new FormData();
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'cargar_info');
    console.log(data)
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                load_info(response);
                // celda_1.innerHTML =
                // console.log(response);
                return false;
            }
            console.log(response);
        });
}

var csrftoken = getCookie('csrftoken');

var tabla_pagos = $('#pagos_cliente').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    "searching": false,
    "lengthChange": false,
    // "bPaginate": false,
    "bInfo": false,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'lista_pagos'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "contador"},
        {"data": "fecha"},
        {"data": "monto"},
        {"data": "saldo"},
        {"data": "opciones"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let buttons = '<button type="button" onclick="cargar_notas_pago(' + data + ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#notas_pago"><i class="fas fa-info-circle"></i></button>';
                buttons += ' <button onclick="cargar_info_pago(' + data + ')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal_editar_pago"><i class="far fa-edit"></i></button>';
                buttons += ' <button onclick="borrar_pago(' + data + ')" class="btn btn-danger btn-xs"><i class="far fa-trash-alt"></i></button>';
                document.getElementById('numero_pago_span').innerHTML = row.contador.toString();
                return buttons;
            },
        },
    ],
    initComplete: function (settings, json) {
    }
});

// let page_index = 0;

var lista_cuotas = $('#lista_cuotas').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    "searching": false,
    "lengthChange": false,
    "bInfo": false,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'lista_cuotas'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    "createdRow": function (row, data, dataIndex) {
        if (data['opciones'] === 2) {
            $(row).addClass('table-warning');
        } else if (data['opciones'] === 3) {
            $(row).addClass('table-secondary');
            // page_index = 20;
        } else if (data['opciones'] === 1) {
            $(row).addClass('table-success');
        }
    },
    // "displayStart": page_index,
    columns: [
        {"data": "contador"},
        {"data": "fecha"},
        {"data": "cuota"},
        {"data": "morosidad"},
        {"data": "pendiente"},
        {"data": "opciones"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let action = "'editar_pago'";
                let buttons = '--';
                if (data === 1) {
                    buttons = '<button type="button" onclick="cargar_info_cuota(' + data + ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#notas_pago"><i class="fas fa-info-circle"></i></button>';
                    document.getElementById('numero_pago_span').innerHTML = row.contador.toString();
                } else if (data === 3) {
                    let fecha = "'" + row.fecha + "'";
                    let args = row.contador + ',' + fecha + ',' + row.cuota + ',' + row.pendiente + ',' + row.morosidad;
                    buttons = '<button type="button" onclick="cargar_info_cuota(' + fecha + ',' + row.cuota + ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#notas_pago"><i class="fas fa-info-circle"></i></button>';
                    buttons += ' <button onclick="pagar_cuota(' + args + ')" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#pagos_cuotas_q"><i class="fas fa-coins"></i></button>';
                    document.getElementById('numero_pago_span').innerHTML = row.contador.toString();
                    // console.log(args);
                }
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                return '$ ' + data;
            },
        },
        {
            targets: [-3],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                return '$ ' + data;
            },
        },
        {
            targets: [-4],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                return '$ ' + data;
            },
        },
    ],
    "fnInitComplete": function (data, json) {
        // console.log(json);
        cargar_saldos_span(json);
        cargar_page(json);
    }
});

function cargar_page(json) {
    // console.log(json, 'es el JSON');
    let count = 0;
    json.some(function (value) {
            count += 1;
            return value['opciones'] === 3;
        }
    );
    let page = Math.ceil(count / 10);
    // console.log(page);
    lista_cuotas.page(page - 1).draw(false);
}

function roundToTwo(num) {
    if (num < 0.005) {
        return 0;
    }
    //return +(Math.round(num + Math.exp(2))  + Math.exp(-2))
    return +(Math.round(num + "e+2") + "e-2");
}

function cargar_saldos_span(json) {
    let morosidad = 0, suma = 0, pago = 0;
    let saldo = 0, pendiente_temp = 0, morosidad_temp = 0, cuota_temp = 0, pendiente_cuotas = 0;
    json.forEach(function (datos) {
        // console.log(datos, 'es el json');
        pendiente_temp = parseFloat(datos['pendiente']);
        pendiente_cuotas = roundToTwo(pendiente_cuotas + parseFloat(datos['saldo_cuota']));
        morosidad_temp = parseFloat(datos['morosidad']);
        cuota_temp = parseFloat(datos['cuota']);
        suma = roundToTwo(cuota_temp + morosidad_temp);
        if (pendiente_temp === suma) {
            morosidad = roundToTwo(morosidad + morosidad_temp);
            saldo = roundToTwo(saldo + cuota_temp);
        } else if (0 !== pendiente_temp && pendiente_temp < suma) {
            pago = roundToTwo(suma - pendiente_temp);
            if (pago >= morosidad_temp) {
                saldo = roundToTwo(saldo + cuota_temp - (pago - morosidad_temp));
            } else {
                saldo = roundToTwo(saldo + cuota_temp);
                morosidad = roundToTwo(morosidad + morosidad_temp - pago);
            }
        }
    })
    // console.log(pendiente_cuotas);
    let saldo_total = pendiente_cuotas + morosidad;
    let ob_contrato = document.getElementById('ob_contrato_db').value;
    ob_contrato = roundToTwo(ob_contrato);
    let total_pagado = ob_contrato - pendiente_cuotas;
    // console.log(total_pagado, 'es lo total pagado');
    // console.log(saldo, morosidad, suma, pendiente_cuotas);
    document.getElementById('saldo_span').innerHTML = f_dollar.format(pendiente_cuotas); // Suma de pendientes
    document.getElementById('saldo_morosidad').innerHTML = f_dollar.format(morosidad);
    document.getElementById('saldo_total').innerHTML = f_dollar.format(saldo_total);
    document.getElementById('total_pagado').innerHTML = f_dollar.format(total_pagado);
    cargar_page(json);
}

function load_info(response) {
    console.log(response);
    let meses_ex = response[3]['meses_extra'];
    document.getElementById('meses_extra').innerHTML = meses_ex.toString();
    // document.getElementById('span_cr_empresa').innerHTML = formatter.format(response[4][1]);
}

function cargar_info_cuota(id_pago, cuota) {
    let url = window.location.pathname;
    // let data = new FormData();
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'info_cuota');
    data.append('id_pago', id_pago);
    data.append('cuota', cuota);
    console.log(data);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                document.getElementById('span_modal_notas').innerHTML = response['ok'];
                // celda_1.innerHTML =
                console.log(response);
                return false;
            }
            console.log(response);
        });
}

function cargar_info_pago(id_pago) {
    document.getElementById('var_id_pago').value = id_pago;
    let url = window.location.pathname;
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'cargar_pago');
    data.append('id_pago', id_pago);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                console.log(response);
                if ('tr_empresa' in response['transacciones']) {
                    let select_tr = document.getElementById('ed_tr_rel');
                    let select_list = document.querySelectorAll('#ed_tr_rel option');
                    select_list.forEach(option => option.remove())
                    select_transacciones(response['transacciones']['tr_empresa'], select_tr, 'empresa');
                    if ('tr_cliente' in response['transacciones']) {
                        select_transacciones(response['transacciones']['tr_cliente'], select_tr, 'cliente');
                    }
                } else if ('tr_cliente' in response['transacciones']) {
                    let select_tr = document.getElementById('ed_tr_rel');
                    let select_list = document.querySelectorAll('#ed_tr_rel option');
                    select_list.forEach(option => option.remove())
                    select_transacciones(response['tr_cliente'], select_tr, 'cliente');
                } else {
                    let select_list = document.querySelectorAll('#pg_tr_rel option');
                    select_list.forEach(option => option.remove())
                }
                document.getElementById('ed_monto_1').value = response['monto_1'];
                document.getElementById('ed_fecha_pago_2').value = response['fecha_pago_2'];
                document.getElementById('ed_tr_rel').value = response['tr_id'];
                if (response['notas']) {
                    document.getElementById('ed_notas').value = response['notas'];
                } else {
                    document.getElementById('ed_notas').value = '';
                }
                return false;
            }
            console.log(response);
        });
}

function pagar_cuota(contador, fecha, cuota, pendiente, mora) {  // Agrega los datos de datatable a elementos del template
    document.getElementById('val_1').value = contador;
    document.getElementById('val_2').value = fecha;
    document.getElementById('val_3').value = cuota;
    document.getElementById('val_4').value = pendiente;
    document.getElementById('val_5').value = mora;
    document.getElementById('pg_monto_1').value = cuota;
    document.getElementById('pg_monto_1').value = cuota;
    document.getElementById('pago_cuota_2').checked = true;
    select_cuota('pendiente');
    //document.getElementById('pago_cuota_1').disabled = cuota > pendiente;
    ///////////
    let url = window.location.pathname;
    let data = new FormData(document.getElementById("token_form"));
    data.append('action', 'credito_disponible');
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                // console.log(response);
                document.getElementById('credito_empresa').innerHTML = '$ ' + response['balance_empresa'];
                document.getElementById('credito_cliente').innerHTML = '$ ' + response['balance_cliente'];
                if ('tr_empresa' in response) {
                    let select_tr = document.getElementById('pg_tr_rel');
                    let select_list = document.querySelectorAll('#pg_tr_rel option');
                    select_list.forEach(option => option.remove())
                    select_transacciones(response['tr_empresa'], select_tr, 'empresa');
                    if ('tr_cliente' in response) {
                        select_transacciones(response['tr_cliente'], select_tr, 'cliente');
                    }
                    // console.log(response['tr_cliente'][1]['cliente']);
                } else if ('tr_cliente' in response) {
                    let select_tr = document.getElementById('pg_tr_rel');
                    let select_list = document.querySelectorAll('#pg_tr_rel option');
                    select_list.forEach(option => option.remove());
                    select_transacciones(response['tr_cliente'], select_tr, 'cliente');
                } else {
                    let select_list = document.querySelectorAll('#pg_tr_rel option');
                    select_list.forEach(option => option.remove())
                }
                return false;
            } else {
                message_error(response);
            }
        });
}

function text_change() {
    document.getElementById('flag_notas').value = '1';
}

function guardar_notas() {
    if (document.getElementById('flag_notas').value === '1') {
        let url = window.location.pathname;
        let data = new FormData(document.getElementById('notas_desembolso'));
        data.append('action', 'notas_cobros');
        fetch(url, {
            method: 'POST', // or 'PUT'
            body: data, // data can be `string` or {object}!
        }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(function (response) {
                if (!response.hasOwnProperty('error')) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Notas guardadas!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    console.log(response);
                    // alert(response);
                    return false;
                } else {
                    message_error(response);
                }
            });
    }
}

var fv; //Editar pagos

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('editar_pago_form'),
        {
            fields: {
                monto_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                tr_rel: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                fecha_pago_2: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto_1':
                            case 'fecha_pago_2':
                                return '.col-sm-4';

                            case 'notas':
                                return '.col-sm-12';

                            case 'tr_rel':
                                return '.col-sm-8';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea editar el pago?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Sí',
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let id_pago = document.getElementById('var_id_pago').value;
                let parameters = new FormData(document.getElementById('editar_pago_form'));
                parameters.append('action', 'editar_pago');
                parameters.append('id_pago', id_pago);
                // var id_form = document.getElementById('id_form').value;
                // parameters.forEach(function (value, key) {
                //     if (!value) {
                //         parameters.delete(key);
                //     }
                // });
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se editó el pago.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#modal_editar_pago').modal('hide');
                                fv.resetForm(true);
                                document.getElementById("editar_pago_form").reset();
                                tabla_pagos.ajax.reload();
                                lista_cuotas.ajax.reload(val => cargar_saldos_span(val));
                                return false;
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire(response['advertencia']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="ed_fecha_pago_2"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_pago_2');
        });
});

var fv_2; //Pagos cuotas

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_2 = FormValidation.formValidation(
        document.getElementById('form_pagos_cuotas'),
        {
            fields: {
                monto_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                tr_rel: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                fecha_pago_2: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto_1':
                            case 'fecha_pago_2':
                                return '.col-sm-4';

                            case 'notas':
                                return '.col-sm-12';

                            case 'tr_rel':
                                return '.col-sm-8';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea agregar el pago?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Agregar',
            denyButtonText: `No agregar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let parameters = new FormData(document.getElementById('form_pagos_cuotas'));
                parameters.append('action', 'pago_cuota');
                // var id_form = document.getElementById('id_form').value;
                // parameters.forEach(function (value, key) {
                //     if (!value) {
                //         parameters.delete(key);
                //     }
                // });
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó el pago.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#pagos_cuotas_q').modal('hide');
                                fv_2.resetForm(true);
                                document.getElementById("form_pagos_cuotas").reset();
                                lista_cuotas.ajax.reload(val => cargar_saldos_span(val));
                                tabla_pagos.ajax.reload();
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire(response['advertencia']);
                        } else if ('advertencia_2') {
                            let msg = '<b class="text-danger">¿Desea continuar y cancelar la obligación al cliente?</b>'
                            Swal.fire({
                                title: '¡El pago de esta cuota cancelará el total de la obligación!',
                                html: msg,
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '¡Sí, continuar!',
                                cancelButtonText: 'Cancelar',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    let num_contrato = document.getElementById('num_contrato_cl').value;
                                    let parameters_2 = new FormData(document.getElementById('form_pagos_cuotas'));
                                    parameters_2.append('action', 'aplicar_ult_pago');
                                    let url = window.location.pathname;
                                    fetch(url, {method: 'POST', body: parameters_2}).then(res => res.json())
                                        .catch(error => console.log(error))
                                        .then(function (response) {
                                            if (!('error' in response)) {
                                                if ('ok' in response) {
                                                    Swal.fire({
                                                        position: 'top-end',
                                                        icon: 'success',
                                                        title: 'Se ha aplicado el último pago al ' + num_contrato,
                                                        showConfirmButton: false,
                                                        timer: 1500
                                                    })
                                                }
                                                setTimeout(function () {
                                                    location.href = '/cobros/lista_clientes/';
                                                }, 1500);
                                                return false;
                                            } else if ('advertencia' in response) {
                                                Swal.fire(response['advertencia']);
                                            }
                                        })
                                }
                            })
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="pg_fecha_pago_2"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('fecha_pago_2');
        });
});

var fv_3; //Agregar transacción

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_3 = FormValidation.formValidation(
        document.getElementById('form_agregar_tr'),
        {
            fields: {
                monto_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                tipo_pago: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                registro_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                fecha_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                id_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                info_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ0-9 ]+$/,
                            message: 'Escriba caracteres validos.'
                        }
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto_tr':
                            case 'tipo_pago':
                            case 'registro_tr':
                            case 'id_tr':
                            case 'banco':
                            case 'fecha_tr':
                                return '.col-sm-4';

                            case 'notas':
                                return '.col-sm-12';

                            case 'info_tr':
                                return '.col-sm-8';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea agregar la transferencia?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Agregar',
            denyButtonText: `No agregar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let parameters = new FormData(document.getElementById('form_agregar_tr'));
                parameters.append('action', 'agregar_tr');
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó la transacción.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#modal_tr').modal('hide');
                                fv_3.resetForm(true);
                                document.getElementById("form_agregar_tr").reset();
                                lista_cuotas.ajax.reload();
                                tabla_pagos.ajax.reload();
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire(response['advertencia']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="id_fecha_tr"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv_3.revalidateField('fecha_tr');
        });
});

function borrar_pago(info_pago) {
    Swal.fire({
        title: '¿Desea eliminar el pago registrado?',
        text: "¡Esta acción es irreversible!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            (async () => {
                const {value: text} = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Comentarios',
                    inputPlaceholder: 'Escriba sus comentarios aquí...',
                    inputAttributes: {
                        'aria-label': 'Escriba sus comentarios aquí.'
                    },
                    showCancelButton: true
                })
                if (text) {
                    let url = window.location.pathname;
                    let data = new FormData(document.getElementById('token_form'));
                    data.append('id_pago', info_pago);
                    data.append('action', 'borrar_pago');
                    data.append('mensaje', text);
                    fetch(url, {
                        method: 'POST', // or 'PUT'
                        body: data, // data can be `string` or {object}!
                    }).then(res => res.json())
                        .catch(error => console.error('Error:', error))
                        .then(function (response) {
                            if (!response.hasOwnProperty('error')) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'warning',
                                    title: '¡Pago eliminado!',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                // console.log(response);
                                tabla_pagos.ajax.reload();
                                lista_cuotas.ajax.reload();
                                // alert(response);
                                return false;
                            } else {
                                message_error(response);
                            }
                        });
                } else if (!text) {
                    Swal.fire('Debe ingresar sus comentarios para continuar.')
                }
            })()
        }
    })
}

function select_cuota(val, monto = 0) {
    if (val === 'regular') {
        // Valor de cuota //
        let cuota = document.getElementById('val_3').value;
        let fecha_cuota = document.getElementById('val_2').value;
        let mora = document.getElementById('val_5').value;
        let pendiente = document.getElementById('val_4').value;
        let pagado = roundToTwo(parseFloat(cuota) + parseFloat(mora) - parseFloat(pendiente));
        console.log(pagado);
        if (pagado !== 0) {
            cuota = roundToTwo(parseFloat(cuota) - pagado);
        }
        document.getElementById('pg_monto_1').readOnly = true;
        document.getElementById('pg_monto_1').value = cuota;
        document.getElementById('pg_fecha_pago_2').readOnly = true;
        document.getElementById('pg_fecha_pago_2').value = fecha_cuota;
        document.getElementById('monto_a_pagar').innerHTML = f_dollar.format(cuota);
    } else if (val === 'pendiente') {
        let pendiente = document.getElementById('val_4').value;
        document.getElementById('pg_monto_1').readOnly = true;
        document.getElementById('pg_monto_1').value = pendiente;
        document.getElementById('monto_a_pagar').innerHTML = f_dollar.format(pendiente);
    } else if (val === 'otra') {
        document.getElementById('pg_monto_1').readOnly = false;
        document.getElementById('pg_fecha_pago_2').readOnly = false;
        document.getElementById('pg_monto_1').value = 0;
        document.getElementById('monto_a_pagar').innerHTML = '$ --';
    } else if (val === 'cambio_monto') {
        document.getElementById('monto_a_pagar').innerHTML = f_dollar.format(monto);
    }
}

function select_transacciones(info_tr, select_tr, tipo) {
    let texto_op;
    info_tr.forEach(function (tr) {
        texto_op = tr[tipo] + ' /ID trans.: ' + tr['tr_id'] + ' / Disponible: ' + f_dollar.format(tr['disponible']);
        const newOption = document.createElement('option');
        const optionText = document.createTextNode(texto_op);
        newOption.appendChild(optionText);
        newOption.setAttribute('value', tr['model_id']);
        select_tr.appendChild(newOption);
    })
}

function aplicar_ajuste_cuota() {
    Swal.fire({
        title: '¿Desea continuar?',
        html: '¡El ajuste de la cuota cambiará la amortización del crédito!. <b class="text-danger">Los pagos aplicados se eliminarán.</b>',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, continuar!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $('#modal_ajuste_letra').modal('show');
        }
    })
}

let fv_cuota;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_cuota = FormValidation.formValidation(
        document.getElementById('form_ajuste_letra'),
        {
            fields: {
                ajuste_letra: {
                    validators: {
                        between: {
                            min: 0,
                            max: 500,
                            message: 'Monto no válido.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'ajuste_letra':
                                return '.col-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea guardar los cambios?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Guardar',
            denyButtonText: `No guardar`,
            cancelButtonText: `Cancelar`
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                (async () => {
                    const {value: text} = await Swal.fire({
                        input: 'textarea',
                        inputLabel: 'Comentarios',
                        inputPlaceholder: 'Escriba sus comentarios aquí...',
                        inputAttributes: {
                            'aria-label': 'Escriba sus comentarios aquí'
                        },
                        showCancelButton: true
                    })
                    if (text) {
                        let parameters = new FormData(document.getElementById('form_ajuste_letra'));
                        parameters.append('action', 'ajustar_letra');
                        parameters.append('mensaje', text);
                        let url = window.location.pathname;
                        fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                            .catch(error => console.log(error))
                            .then(function (response) {
                                console.log(response);
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'info',
                                    title: 'Se ajustó la cuota.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#modal_ajuste_letra').modal('hide');
                                fv_cuota.resetForm(true);
                                document.getElementById("form_ajuste_letra").reset();
                                lista_cuotas.ajax.reload();
                                tabla_pagos.ajax.reload();
                            })
                    } else if (!text) {
                        Swal.fire('Debe ingresar sus comentarios para continuar.')
                    }
                })()
                // Swal.fire('Saved!', '', 'success');
            } else if (result.isDenied) {
                $('#modal_ajuste_letra').modal('hide');
            }
        })
    });
});

function aplicar_nc(n_contrato) {
    Swal.fire({
        title: '¿Desea aplicar nota de crédito al ' + n_contrato + '?',
        html: "Una vez aplicado el ajuste cambiará la amortización del crédito. <b class='text-danger'>¡Es posible que los pagos aplicados se eliminen!</b>",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, aplicar N.C.'
    }).then((result) => {
        if (result.isConfirmed) {
            $('#modal_nota_c').modal('show');
            let parameters = new FormData(document.getElementById('token_form'));
            parameters.append('action', 'consulta_nc');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(response => console.log(response))
                .then(function (response) {
                    if (!response.hasOwnProperty('error')) {
                        document.getElementById('monto_nc').value = response['monto_nc'];
                        document.getElementById('motivo_nc').value = response['motivo_nc'];
                    }
                })
        }
    })
}

let fv_nota_c;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_nota_c = FormValidation.formValidation(
        document.getElementById('form_nota_c'),
        {
            fields: {
                monto_nc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0.01,
                            max: 100,
                            message: 'Monto no válido.'
                        }
                    }
                },
                motivo_nc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto_nc':
                                return '.col-6';

                            case 'motivo_nc':
                                return '.col-12';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea guardar los cambios?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Guardar',
            denyButtonText: `No guardar`,
            cancelButtonText: `Cancelar`
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                let parameters = new FormData(document.getElementById('form_nota_c'));
                parameters.append('action', 'agregar_NC');
                let url = window.location.pathname;
                fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                    .catch(error => console.log(error))
                    .then(function (response) {
                        console.log(response);
                        Swal.fire({
                            position: 'top-end',
                            icon: 'info',
                            title: 'Se aplicó la NC.',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#modal_nota_c').modal('hide');
                        fv_nota_c.resetForm(true);
                        document.getElementById("form_nota_c").reset();
                        lista_cuotas.ajax.reload(val => cargar_saldos_span(val));
                        tabla_pagos.ajax.reload();
                    })
                // Swal.fire('Saved!', '', 'success');
            } else if (result.isDenied) {
                $('#modal_nota_c').modal('hide');
            }
        })
    });
});

function borrar_pagos_todos() {
    let msg = '<b class="text-danger h4">¡Esta acción es irreversible!</b>';
    Swal.fire({
        title: '¿Desea continuar?',
        html: msg,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, borrar todo!',
        cancelButtonText: '¡Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            let parameters = new FormData(document.getElementById('token_form'));
            parameters.append('action', 'borrar_pagos');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(error => console.log(error))
                .then(function (response) {
                    if (!('error' in response)) {
                        if ('ok' in response) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'warning',
                                title: 'Pagos eliminados',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            setTimeout(function (){
                                location.reload();
                            }, 1200);
                        } else if ('advertencia' in response){
                            Swal.fire(response['advertencia']);
                        }
                    }
                })
        }
    })
}