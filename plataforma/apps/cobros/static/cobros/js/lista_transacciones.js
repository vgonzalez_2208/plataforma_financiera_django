var csrftoken = getCookie('csrftoken');

let lista_transacciones = $('#lista_transacciones').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'cargar_tr'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "id_tr"},
        {"data": "banco"},
        {"data": "fecha_tr"},
        {"data": "monto_tr"},
        {"data": "monto_disp"},
        {"data": "tr_source"},
        {"data": "id_tr_db"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            // orderable: false,
            render: function (data, type, row) {
                let buttons;
                if (row.monto_tr === row.monto_disp) {
                    buttons = '&nbsp <button class="btn btn-danger btn-xs" onclick="delete_tr(' + data + ',' + 2 + ')"><i class="fas fa-trash"></i></button>';
                } else {
                    buttons = '<button class="btn btn-info btn-xs" onclick="cargar_info_tr(' + data + ')" data-toggle="modal" data-target="#modal_tr"><i class="fas fa-eye"></i></button>';
                    buttons += '&nbsp <button class="btn btn-danger btn-xs" onclick="delete_tr(' + data + ')"><i class="fas fa-trash"></i></button>';
                }
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                if (data === 'Empresa') {
                    return '<span class="badge badge-pill badge-success">' + data + '</span>'
                } else if (data === 'Cliente') {
                    return '<span class="badge badge-pill badge-warning">' + data + '</span>'
                }
                return data
            },
        },
        {
            targets: [-3],
            class: 'text-center',
            render: function (data, type, row) {
                return f_dollar.format(data);
            },
        },
        {
            targets: [-4],
            class: 'text-center',
            render: function (data, type, row) {
                return f_dollar.format(data);
            },
        },

    ],
    initComplete: function (settings, json) {
    }
});

function delete_tr(id_tr, tipo = 1) {
    document.getElementById('last_tr').value = [id_tr, tipo];
    Swal.fire({
        title: '¿Desea eliminar la transacción?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'No',
        denyButtonText: 'Sí',
    }).then((result) => {
        if (result.isDenied) {
            (async () => {
                let last_tr = document.getElementById('last_tr').value.split(',');
                // console.log(last_tr);
                let adv;
                if (last_tr[1] === '1') {
                    adv = '<b class="text-danger">';
                    adv += '¡Esta acción es irreversible!. Se eliminarán todos los pagos relacionados.' + '</b>';
                    adv += '<span class="text-primary"> Clientes afectados:</span><br><br>';
                    let parameters = new FormData();
                    parameters.append('id_tr', last_tr[0]);
                    let info = await global_fetch('clientes_tr', parameters);
                    adv += '<ul class="text-primary text-left">' + info['ok'] + '</ul>';
                } else {
                    adv = '<b class="text-danger">';
                    adv += '¡Esta acción es irreversible!. La transacción dejará de estar disponible al aplicar pagos.' + '</b>';
                }
                Swal.fire({
                    title: '¿Desea continuar?',
                    html: adv,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '¡Sí, eliminar!',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    if (result.isConfirmed) {
                        (async () => {
                            let id_tr = document.getElementById('last_tr').value.split(',');
                            let parameters = new FormData();
                            parameters.append('id_tr', id_tr[0]);
                            let data = await global_fetch('borrar_tr', parameters);
                            if (!('error' in data)) {
                                if ('ok' in data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'info',
                                        title: data['ok'],
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                    info_transacciones.ajax.reload();
                                    return false;
                                } else if ('advertencia' in data) {
                                    let msj = '<span class="h3">Debe eliminar primero la transacción: ';
                                    msj += '<span class="h3 font-weight-bold">' + data['advertencia'] + '</span></span>';
                                    Swal.fire({html: msj})
                                }
                            }
                        })()
                    }
                })
            })()
        }
    })
}

function cargar_info_tr(id_tr) {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('id_tr', id_tr);
    parameters.append('action', 'cargar_info_tr');
    let url = window.location.pathname;
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                console.log(response);
                document.getElementById('info_1').innerHTML = response[0]['id_tr'];
                document.getElementById('info_2').innerHTML = response[0]['banco'];
                document.getElementById('info_3').innerHTML = response[0]['registro'];
                document.getElementById('info_4').innerHTML = response[0]['tipo'];
                document.getElementById('info_5').innerHTML = response[0]['monto'];
                document.getElementById('info_6').innerHTML = response[0]['fecha'];
                document.getElementById('info_7').innerHTML = response[0]['info'];
                document.getElementById('info_8').innerHTML = response[0]['notas'];
                lista_pagos_relacionados(response[1]);
            }
        });
}

function lista_pagos_relacionados(pagos) {
    let lista = document.getElementById('lista_pg_relacionados');
    lista.innerHTML = '';
    let info_pg;
    pagos.forEach(function (pg) {
        let entry = document.createElement('li');
        if ('cliente_ant' in pg) {
            info_pg = '(Pre-p) ' + pg['cliente_ant'];
        } else {
            info_pg = '(Post-p) ' + pg['cliente_new'];
        }
        info_pg += ' | Pagado: ' + pg['monto_pagado'] + ' | ' + pg['fecha_1']
        entry.appendChild(document.createTextNode(info_pg));
        lista.appendChild(entry);
    })
}

async function clientes_info() {
    let salida = await global_fetch('test');
    console.log(salida);
    return salida
}