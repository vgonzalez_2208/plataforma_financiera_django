var csrftoken = getCookie('csrftoken');

$(function provincia() {
    $('select[name="provincia"]').on('change', function () {
        var id = $(this).val();
        var select_distrito = $('select[name="distrito"]');
        var select_corregimiento = $('select[name="corregimiento"]');
        var options = '<option value="">---------------</option>';
        select_corregimiento.html(options);
        if (id === '') {
            select_distrito.html(options);
            return false;
        }
        // let parameters = new FormData(document.getElementById('token_form'));
        // parameters.append('action', 'select_p')
        $.ajax({
            url: window.location.pathname, //window.location.pathname
            type: 'POST',
            data: {
                'action': 'select_p',
                'id': id
            },
            headers: {'X-CSRFToken': csrftoken},
            dataType: 'json',
        }).done(function (data) {
            console.log(data);
            if (!data.hasOwnProperty('error')) {
                $.each(data, function (key, value) {
                    options += '<option value="' + value.id + '">' + value.name + '</option>'
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {
            select_distrito.html(options);
        });
    })
});

$(function () {
    $('select[name="distrito"]').on('change', function () {
        var id = $(this).val();
        var select_corregimiento = $('select[name="corregimiento"]');
        var options = '<option value="">---------------</option>';
        if (id === '') {
            select_corregimiento.html(options);
            return false;
        }
        $.ajax({
            url: window.location.pathname, //window.location.pathname
            type: 'POST',
            data: {
                'action': 'select_d',
                'id': id
            },
            headers: {'X-CSRFToken': csrftoken},
            dataType: 'json',
        }).done(function (data) {
            console.log(data);
            if (!data.hasOwnProperty('error')) {
                $.each(data, function (key, value) {
                    options += '<option value="' + value.id + '">' + value.name + '</option>'
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {
            select_corregimiento.html(options);
        });
    })
});