function aplicar_prepago() {
    console.log('flag');
}

function saldo_cliente_pr() {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'saldo_cliente');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            console.log(response);
            if (!response.hasOwnProperty('error')) {
                let span_disp = document.getElementById('cred_disp');
                let span_pagado = document.getElementById('cred_pago');
                let span_saldo = document.getElementById('cred_saldo');
                let disp = f_dollar.format(response['balance_cliente']);
                let pagado = f_dollar.format(response['total_pagado']);
                let saldo_cliente = f_dollar.format(response['saldo_cliente']);
                if (response['balance_cliente'] === 0) {
                    span_disp.innerHTML = '<span class="text-warning">' + disp + '</span>'
                } else {
                    span_disp.innerHTML = '<span class="text-success">' + disp + '</span>'
                }
                select_tr_cliente(response);
                if (response['total_pagado'] === '0') {
                    span_pagado.innerHTML = '<span class="text-warning">' + pagado + '</span>'
                } else {
                    span_pagado.innerHTML = '<span class="text-purple">' + pagado + '</span>'
                }
                if (response['saldo_cliente'] === '0') {
                    span_saldo.innerHTML = '<span class="text-warning">' + saldo_cliente + '</span>'
                } else {
                    span_saldo.innerHTML = '<span class="text-purple">' + saldo_cliente + '</span>'
                }
            }
        })
}

function select_tr_cliente(response) {
    if ('tr_cliente' in response) {
        let select_tr = document.getElementById('lista_tr_cliente');
        let select_list = document.querySelectorAll('#lista_tr_cliente option');
        select_list.forEach(option => option.remove())
        select_transacciones(response['tr_cliente'], select_tr, 'cliente');
    } else {
        let select_list = document.querySelectorAll('#lista_tr_cliente option');
        select_list.forEach(option => option.remove())
    }
}

function select_transacciones(info_tr, select_tr, tipo) {
    let texto_op;
    info_tr.forEach(function (tr) {
        texto_op = tr[tipo] + ' /ID trans.: ' + tr['tr_id'] + ' / Disponible: ' + f_dollar.format(tr['disponible']);
        const newOption = document.createElement('option');
        const optionText = document.createTextNode(texto_op);
        newOption.appendChild(optionText);
        newOption.setAttribute('value', tr['model_id']);
        select_tr.appendChild(newOption);
    })
}

let fv; //Agregar transacción

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('form_agregar_tr'),
        {
            fields: {
                monto_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                tipo_pago: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                fecha_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                id_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                info_tr: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ0-9 ]+$/,
                            message: 'Escriba caracteres validos.'
                        }
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto_tr':
                            case 'tipo_pago':
                            case 'id_tr':
                            case 'banco':
                            case 'fecha_tr':
                                return '.col-sm-4';

                            case 'notas':
                                return '.col-sm-12';

                            case 'info_tr':
                                return '.col-sm-8';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea agregar la transferencia?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Agregar',
            denyButtonText: `No agregar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let parameters = new FormData(document.getElementById('form_agregar_tr'));
                parameters.append('registro_tr', '2');
                parameters.append('action', 'agregar_tr');
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó la transacción.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#modal_tr').modal('hide');
                                fv.resetForm(true);
                                fv_2.resetForm(true);
                                document.getElementById("form_agregar_tr").reset();
                                saldo_cliente_pr();
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire(response['advertencia']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="id_fecha_tr"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_tr');
        });
});

let fv_2; //Aplicar Pago anticipado

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_2 = FormValidation.formValidation(
        document.getElementById('form_prepago_manual'),
        {
            fields: {
                fecha_pago: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_pago: {
                            format: 'YYYY-MM-DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ret_int: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ret_int: {
                            format: 'YYYY-MM-DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ret_seg: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ret_seg: {
                            format: 'YYYY-MM-DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                ret_int: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                ret_seg: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                penalidad: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 10000,
                            message: 'Monto superior a 10,000.00 USD'
                        }
                    }
                },
                tr_pagado: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'fecha_pago':
                            case 'fecha_ret_int':
                            case 'fecha_ret_seg':
                            case 'ret_int':
                            case 'ret_seg':
                            case 'penalidad':
                                return '.col-3';

                            case 'tr_pagado':
                                return '.col-6';

                            case 'notas':
                                return '.col-12';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea aplicar el pago anticipado?',
            text: "¡Luego de esta acción no podrá aplicar pagos regulares!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, continuar!'
        }).then((result) => {
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let parameters = new FormData(document.getElementById('form_prepago_manual'));
                parameters.append('action', 'aplicar_anticipado');
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó la transacción.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#modal_tr').modal('hide');
                                fv_2.resetForm(true);
                                document.getElementById("form_agregar_tr").reset();
                                saldo_cliente_pr();
                                setTimeout(function () {
                                    location.href = '/cobros/lista_clientes/';
                                }, 1500);
                                return false;
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire(response['advertencia']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="id_fecha_pago"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('fecha_pago');
        });
    $('[id="id_fecha_ret_int"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('fecha_ret_int');
        });
    $('[id="id_fecha_ret_seg"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '0'
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('fecha_ret_seg');
        });
});
