var csrftoken = getCookie('csrftoken');

let info_transacciones = $('#info_transacciones').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'CargarTabla_1'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "id_tr"},
        {"data": "banco"},
        {"data": "fecha_tr"},
        {"data": "monto_tr"},
        {"data": "monto_disp"},
        {"data": "tr_source"},
        {"data": "id_tr_db"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let buttons;
                buttons = '<button class="btn btn-info btn-xs" onclick="cargar_info_tr(' + data + ')" data-toggle="modal" data-target="#modal_tr"><i class="fas fa-eye"></i></button>';
                buttons += '&nbsp <button class="btn btn-danger btn-xs" onclick="delete_tr(' + data + ')"><i class="fas fa-trash"></i></button>';
                return buttons;
            },
        },
        {
            targets: [-3, -4],
            class: 'text-center',
            render: function (data, type, row) {
                return f_dollar.format(data);
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                if (data === 'Empresa actual') {
                    return '<span class="badge badge-pill badge-success">' + data + '</span>'
                } else if (data === 'Empresa anterior') {
                    return '<span class="badge badge-pill badge-warning">' + data + '</span>'
                } else if (data === 'Cliente') {
                    return '<span class="badge badge-pill badge-primary">' + data + '</span>'
                }
                return data
            },
        },

    ],
    initComplete: function (settings, json) {
    }
});

function delete_tr(id_tr) {
    document.getElementById('last_tr').value = id_tr;
    Swal.fire({
        title: '¿Desea eliminar la transacción?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'No',
        denyButtonText: 'Sí',
    }).then((result) => {
        if (result.isDenied) {
            let adv = '<b class="text-danger">';
            adv += '¡Esta acción es irreversible!. Se eliminarán todos los pagos relacionados.';
            Swal.fire({
                title: '¿Desea continuar?',
                html: adv,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, eliminar!',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    (async () => {
                        let id_tr = document.getElementById('last_tr').value;
                        let parameters = new FormData();
                        parameters.append('id_tr', id_tr);
                        let data = await global_fetch('borrar_tr', parameters);
                        if (!('error' in data)) {
                            if ('ok' in data) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'info',
                                    title: data['ok'],
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                info_transacciones.ajax.reload();
                                return false;
                            } else if ('advertencia' in data){
                                let msj = '<span class="h3">Debe eliminar primero la transacción: ';
                                msj += '<span class="h3 font-weight-bold">' + data['advertencia'] + '</span></span>';
                                Swal.fire({html: msj})
                            }
                        }
                    })()
                }
            })
        }
    })
}

function cargar_info_tr(id_tr) {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('id_tr', id_tr);
    parameters.append('action', 'cargar_info_tr');
    let url = window.location.pathname;
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                console.log(response);
                document.getElementById('info_1').innerHTML = response[0]['id_tr'];
                document.getElementById('info_2').innerHTML = response[0]['banco'];
                document.getElementById('info_3').innerHTML = response[0]['registro'];
                document.getElementById('info_4').innerHTML = response[0]['tipo'];
                document.getElementById('info_5').innerHTML = response[0]['monto'];
                document.getElementById('info_6').innerHTML = response[0]['fecha'];
                document.getElementById('info_7').innerHTML = response[0]['info'];
                document.getElementById('info_8').innerHTML = response[0]['notas'];
                lista_pagos_relacionados(response[1]);
            }
        });
}

function lista_pagos_relacionados(pagos) {
    let lista = document.getElementById('lista_pg_relacionados');
    lista.innerHTML = '';
    let info_pg;
    pagos.forEach(function (pg) {
        let entry = document.createElement('li');
        if ('cliente_ant' in pg) {
            info_pg = '(Pre-p) ' + pg['cliente_ant'];
        } else {
            info_pg = '(Post-p) ' + pg['cliente_new'];
        }
        info_pg += ' | Pagado: ' + pg['monto_pagado'] + ' | ' + pg['fecha_1']
        entry.appendChild(document.createTextNode(info_pg));
        lista.appendChild(entry);
    })
}