// var tabla_ligeras = $('#lista_cobros').DataTable({
var csrftoken = getCookie('csrftoken');

let tabla_clientes = $('#lista_cobros').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'CargarTabla'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    "createdRow": function (row, data, dataIndex) {
        if (data['atraso'] === '0 días') {
            $(row).addClass('table-success');
        } else if (data['atraso'] === '30 días') {
            $(row).addClass('table-info');
            // page_index = 20;
        } else if (data['atraso'] === '60 días') {
            $(row).addClass('table-secondary');
        } else if (data['atraso'] === '90 días') {
            $(row).addClass('table-warning');
        } else if (data['atraso'] === '+90 días') {
            $(row).addClass('table-danger');
        }
    },
    columns: [
        {"data": "contrato"},
        {"data": "nombre"},
        {"data": "apellido"},
        {"data": "atraso"},
        {"data": "id"},
        {"data": "tipo_ingreso"},
        {"data": "monto"},
        {"data": "plazo"},
        {"data": "celular"},
        {"data": "id"},
        // {"data": "atraso"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let info = data.split('/');
                let buttons;
                let tipo = info[1];
                let id_1 = info[0];
                let id_2 = info[2] + '/' + tipo;
                // console.log(tipo, id_1, id_2);
                buttons = '<a href="/cobros/ver_cliente/' + id_2 + '" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>';
                buttons += ' ' + '<a href="/cobros/editar_cliente/' + id_2 + '/" class="btn btn-secondary btn-xs"><i class="far fa-edit"></i></a>';
                buttons += ' ' + '<a href="/cobros/lista_clientes/' + id_1 + '/" class="btn btn-warning btn-xs"><i class="fas fa-hand-holding-usd"></i></a>';
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                return '<a href="https://wa.me/507' + data + '" target="_blank"><i class=" fab fa-whatsapp"></i> ' + data + '</a>';
            },
        },
        {
            targets: [-6],
            class: 'text-center',
            render: function (data, type, row) {
                let tipo = data.split('/')[1];
                let salida;
                if (tipo === 'ant') {
                    salida = '<span class="badge badge-pill badge-secondary">Pre-p.</span>';
                } else {
                    salida = '<span class="badge badge-pill badge-info">Post-p.</span>';
                }
                return salida;
            },
        },
    ],
    initComplete: function (settings, json) {
        this.api().columns([3, 5]).every(function () {
            var column = this;
            // console.log(column['0'][0]);
            let placeholder;
            if (column['0'][0] === 3){
                placeholder = 'Sel...';
            }else if (column['0'][0] === 5){
                placeholder = 'Seleccione...';
            }
            var select = $('<select class="form-control"><option class="text-muted" value="">' + placeholder + '</option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });
            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    }
});

var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('cliente_antiguo'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ'"Üü]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_doc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                f_nacimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                nacionalidad: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 5 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                telefono: {
                    validators: {
                        stringLength: {
                            min: 7,
                            max: 7,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                fecha_ini: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ini: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ini_OD: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ini_OD: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                f_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_contrato: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                compra_saldo: {
                    validators: {
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'Mínimo 6 letras'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ0-9, ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                promotor: {
                    validators: {
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'Mínimo 6 letras'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                ob_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 10000,
                            message: 'Ingrese un monto entre 300 y 10,000'
                        }
                    }
                },
                empresa_2: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9999.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                c_cierre: {
                    validators: {}
                },
                notas: {
                    validators: {}
                },
                num_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^(([0-9]{4})+)-(([0-9]{2})+)-([0-9]{4})$/i,
                            message: 'Formato incorrecto.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'ob_contrato':
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_doc':
                            case 'dni':
                            case 'f_nacimiento':
                            case 'nacionalidad':
                            case 'celular':
                            case 'telefono':
                            case 'monto':
                            case 'plazo':
                            case 'fecha_ini':
                            case 'fecha_ini_OD':
                            case 'f_contrato':
                            case 'cancelaciones':
                            case 'promotor':
                            case 'salario':
                            case 'tipo_ingreso':
                            case 'num_contrato':
                                return '.col-sm-3';

                            case 'email_1':
                            case 'empresa_2':
                                return '.col-sm-6';

                            case 'notas':
                                return '.col-sm-12';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea agregar esta información de cliente?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Agregar',
            denyButtonText: `No agregar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let pre_parameters = new FormData(document.getElementById("cliente_antiguo"));
                let parameters = new FormData;
                pre_parameters.forEach(function (value, key) {
                    if (!(value === "" || value.name === "")) {
                        parameters.append(key, value);
                    }
                })
                parameters.append('action', 'add_cliente');
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó la info del cliente.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#add_cliente_modal').modal('hide');
                                document.getElementById("cliente_antiguo").reset();
                                tabla_clientes.ajax.reload();
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire({
                                title: 'El crédito está mal liquidado.',
                                text: "La obligación exacta del crédito debe ser: $" + response['advertencia'] + ". Desea amortizar en base a la obligación del contrato?",
                                icon: 'warning',
                                showCancelButton: true,
                                showDenyButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '¡Sí, continuar!',
                                denyButtonText: 'No'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    let pre_parameters_2 = new FormData(document.getElementById("cliente_antiguo"));
                                    let parameters_2 = new FormData;
                                    pre_parameters_2.forEach(function (value, key) {
                                        if (!(value === "" || value.name === "")) {
                                            parameters_2.append(key, value);
                                        }
                                    })
                                    parameters_2.append('action', 'force_add');
                                    let url = window.location.pathname;
                                    fetch(url, {
                                        method: 'POST', // or 'PUT'
                                        body: parameters_2, // data can be `string` or {object}!
                                    }).then(res => res.json())
                                        .catch(error => console.error('Error:', error))
                                        .then(function (response) {
                                            if (!response.hasOwnProperty('error')) {
                                                Swal.fire({
                                                    position: 'top-end',
                                                    icon: 'warning',
                                                    title: 'Se agregó la info del cliente.',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                $('#add_cliente_modal').modal('hide');
                                                document.getElementById("cliente_antiguo").reset();
                                                tabla_clientes.ajax.reload();
                                            }
                                            console.log(response);
                                        });
                                } else if (result.isDenied) {
                                    Swal.fire(
                                        '¡Cancelado!',
                                        'Revise los parámetros del crédito/contrato y vuelva a intentarlo.',
                                        'warning'
                                    )
                                }
                            })
                        } else if (response.hasOwnProperty('error')) {
                            alert(response['error']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="id_f_nacimiento"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '-22y'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_nacimiento');
        });
    $('[id="id_fecha_ini"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini');
        });
    $('[id="id_f_contrato"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_contrato');
        });
    $('[id="id_fecha_ini_OD"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini_OD');
        });
});

function resumen_saldos(){
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'resumen_saldos');
    let url = window.location.pathname;
    fetch(url, {method:'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response){
            if (!('error' in response)){
                if ('ok' in response){
                    let info = response['ok'];
                    document.getElementById('cap_1').innerHTML = f_dollar.format(info[0][0]);
                    document.getElementById('cap_2').innerHTML = f_dollar.format(info[1][0]);
                    document.getElementById('cap_3').innerHTML = f_dollar.format(info[2][0]);
                    document.getElementById('s_agr_1').innerHTML = f_dollar.format(info[0][1]);
                    document.getElementById('s_agr_2').innerHTML = f_dollar.format(info[1][1]);
                    document.getElementById('s_agr_3').innerHTML = f_dollar.format(info[2][1]);
                    document.getElementById('rec_1').innerHTML = f_dollar.format(info[0][2]);
                    document.getElementById('rec_2').innerHTML = f_dollar.format(info[1][2]);
                    document.getElementById('rec_3').innerHTML = f_dollar.format(info[2][2]);
                    document.getElementById('pend_1').innerHTML = f_dollar.format(info[0][3]);
                    document.getElementById('pend_2').innerHTML = f_dollar.format(info[1][3]);
                    document.getElementById('pend_3').innerHTML = f_dollar.format(info[2][3]);
                    // Info de Garantías
                    document.getElementById('gr_1').innerHTML = f_dollar.format(info[0][3]);
                    document.getElementById('gr_2').innerHTML = f_dollar.format(info[1][3]/2);
                    document.getElementById('gr_3').innerHTML = f_dollar.format(info[0][3] + info[1][3]/2);
                }
            }
        })
}

