# Generated by Django 3.2.3 on 2021-11-30 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobros', '0013_auto_20211124_1535'),
    ]

    operations = [
        migrations.AddField(
            model_name='bancos',
            name='nombre_cuenta',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='num cuenta'),
        ),
    ]
