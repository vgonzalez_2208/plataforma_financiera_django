# Generated by Django 3.2.3 on 2021-11-06 00:19

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cobros', '0006_alter_clienteantiguo_fecha_ini_od'),
    ]

    operations = [
        migrations.AddField(
            model_name='transacciones',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
