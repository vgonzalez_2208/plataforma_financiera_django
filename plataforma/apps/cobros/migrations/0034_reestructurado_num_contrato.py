# Generated by Django 3.2.3 on 2022-01-03 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cobros', '0033_auto_20220102_2100'),
    ]

    operations = [
        migrations.AddField(
            model_name='reestructurado',
            name='num_contrato',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Num. contrato'),
        ),
    ]
