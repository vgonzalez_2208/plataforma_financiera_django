from django.forms import *
from django import forms

from apps.cobros.choices import *
from apps.cobros.models import ClienteAntiguo, Transacciones, Pagos, Desembolso, PrepagoAnticipado, Reestructurado
from apps.prestamos.models import Aprobado
from apps.ventas.models import Empresa


class FormClienteAntiguo(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control form-control-sm'
            form.field.widget.attrs['autocomplete'] = 'off'

    c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)

    class Meta:
        model = ClienteAntiguo
        exclude = ('user_view', 'estatus',)

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'dni': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'f_nacimiento': DateInput(format='%Y-%m-%d', attrs={
                'placeholder': 'Ingrese fecha...',
            }),
            'nacionalidad': TextInput(
                attrs={
                    'placeholder': 'Panameño',
                }
            ),
            'email_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese email',
                }
            ),
            'celular': TextInput(
                attrs={
                    'placeholder': 'Ingrese celular',
                }
            ),
            'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese teléfono',
                }
            ),
            'monto': NumberInput(
                attrs={
                    'min': '300',
                    'placeholder': '1,000.00'
                }
            ),
            'plazo': NumberInput(
                attrs={
                    'min': '6',
                    'placeholder': '24'
                }
            ),
            'fecha_ini': TextInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'fecha_ini_OD': TextInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'f_contrato': TextInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'notas': Textarea(
                attrs={
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'rows': '2',
                    'cols': '150'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'min': '300',
                    'max': '9999',
                    'placeholder': '300'
                }
            ),
            'compra_saldo': TextInput(
                attrs={
                    'placeholder': 'Rapicash: 200.50, Rapi...',
                }
            ),
            'promotor': TextInput(
                attrs={
                    'placeholder': 'Mar del Sojo...',
                }
            ),
            'ob_contrato': NumberInput(
                attrs={
                    'placeholder': '2,194.50'
                }
            ),
            'num_contrato': TextInput(
                attrs={
                    'placeholder': '2021-12-9999',
                }
            ),
        }


class FormTransacciones(ModelForm):
    class Meta:
        model = Transacciones
        fields = '__all__'

        widgets = {
            'tipo_pago': Select(attrs={
                'class': 'form-control form-control-sm',
            }),
            'fecha_tr': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control form-control-sm',
                'placeholder': 'Ingrese fecha',
                'onchange': 'gen_tr_id(false)'
            }),
            'banco': Select(attrs={
                'class': 'form-control form-control-sm',
            }),
            'monto_tr': NumberInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': '91.54'
            }),
            'info_tr': TextInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': 'ACH P-O RC FINANCIAL GROUP, CORP',
            }),
            'id_tr': TextInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': '3191389-2',
                'onclick': 'gen_tr_id()'
            }),
            'notas': Textarea(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '2',
                    'cols': '150'
                }
            ),
        }


class FormPagosClientes(ModelForm):
    class Meta:
        model = Pagos
        exclude = ('desembolso', 'fecha_pago_2', 'fecha_pago_3', 'monto_2')

        widgets = {
            'fecha_pago_1': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control form-control-sm',
                'placeholder': 'Ingrese fecha',
                'aria-describedby': 'text_help',
            }),
            'tipo_pago': Select(attrs={
                'class': 'form-control form-control-sm',
            }),
            'id_tr': TextInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': '3191389-2',
            }),
            'banco': Select(attrs={
                'class': 'form-control form-control-sm',
            }),
            'fecha_tr': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control form-control-sm',
                'placeholder': 'Ingrese fecha',
                'aria-describedby': 'text_help',
            }),
            'info_tr': TextInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': 'ACH P-O RC FINANCIAL GROUP, CORP',
            }),
            'monto_1': NumberInput(attrs={
                'class': 'form-control form-control-sm',
                'placeholder': '91.45'
            }),
            'notas': Textarea(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '2',
                    'cols': '150'
                }
            ),
        }


class FormDesembolso(ModelForm):
    class Meta:
        model = Desembolso
        fields = '__all__'

        widgets = {
            'comprobante': FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'accept': 'application/pdf,',
                    'onchange': 'label_desembolso()'
                }
            )
        }


class FormClienteAntiguo2(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            if form.name in ('f_nacimiento', 'f_contrato', 'fecha_ini', 'fecha_ini_OD'):
                form.field.widget.attrs['class'] = 'form-control datepicker'
                form.field.widget.format = '%Y-%m-%d'
            elif form.name == 'empresa_2':
                form.field.widget.attrs['class'] = 'form-control select2'
            else:
                form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    c_promotor = forms.ChoiceField(choices=c_promotor_dic, required=False)
    s_descuento = forms.ChoiceField(choices=s_descuento_dic, required=False)
    c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)

    class Meta:
        model = ClienteAntiguo
        exclude_1 = ('user_view', 'estatus', 'orden_descuento', 'contrato', 'pagare', 'conoce_cliente', 'f_1')
        exclude_2 = ('DNI_img', 'carta_trabajo_img', 'talonario_1_img', 'talonario_2_img', 'ficha_img', 'recibo_img')
        exclude_3 = ('carta_saldo_img', 'file_apc', 'check_cumplimiento',)
        exclude = exclude_1 + exclude_2 + exclude_3

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'dni': TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'f_nacimiento': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'nacionalidad': TextInput(
                attrs={
                    'placeholder': 'Panameño',
                }
            ),
            'email_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese email',
                }
            ),
            'celular': TextInput(
                attrs={
                    'placeholder': 'Ingrese celular',
                }
            ),
            'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese teléfono',
                }
            ),
            'monto': NumberInput(
                attrs={
                    'placeholder': '1,000.00'
                }
            ),
            'plazo': NumberInput(
                attrs={
                    'placeholder': '24'
                }
            ),
            'fecha_ini': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'f_contrato': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'fecha_ini_OD': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'notas': Textarea(
                attrs={
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'rows': '2',
                    'cols': '150'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'min': '300',
                    'max': '9999',
                    'placeholder': '300'
                }
            ),
            'empresa_1': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre/razón comercial',
                }
            ),
            'promotor': TextInput(
                attrs={
                    'placeholder': 'Mar del Sojo...',
                }
            ),
            'ob_contrato': NumberInput(
                attrs={
                    'placeholder': '2,194.50'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'placeholder': 'Barriada/ # de casa/ edificio/ apto.',
                }
            ),
            'num_contrato': TextInput(
                attrs={
                    'placeholder': '2021-12-9999',
                }
            ),
        }


class FormClienteNuevo(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            if form.name in ('f_contrato', 'fecha_ini', 'fecha_ini_OD'):
                form.field.widget.attrs['class'] = 'form-control datepicker'
                form.field.widget.format = '%Y-%m-%d'
            else:
                form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    c_promotor = forms.ChoiceField(choices=c_promotor_dic, required=False)
    s_descuento = forms.ChoiceField(choices=s_descuento_dic, required=False)
    c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)

    class Meta:
        model = Aprobado
        exclude = ('creado_el', 'fecha_ingreso', 'cliente', 'aviso_cliente', 'estatus')
        exclude += ('orden_descuento', 'f_1', 'contrato', 'pagare', 'conoce_cliente')

        widgets = {
            'fecha_ini': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'f_contrato': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'fecha_ini_OD': DateInput(
                attrs={
                    'placeholder': 'Ingrese fecha...',
                }
            ),
            'notas_user': Textarea(
                attrs={
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'rows': '4',
                    'cols': '150'
                }
            ),
            'c_cierre': Select(
                attrs={
                    'required': 'false'
                }
            ),
        }


class FormPrepago(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            if form.name in ('fecha_pago', 'fecha_ret_int', 'fecha_ret_seg'):
                form.field.widget.attrs['class'] = 'form-control'
                form.field.widget.format = '%Y-%m-%d'
                form.field.widget.attrs['placeholder'] = 'Ingrese fecha...'
            elif form.name == 'notas':
                form.field.widget.attrs['class'] = 'form-control'
                form.field.widget.attrs['rows'] = '3'
                form.field.widget.attrs['placeholder'] = 'Ingrese sus comentarios...'
            else:
                form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    # c_promotor = forms.ChoiceField(choices=c_promotor_dic, required=False)
    # s_descuento = forms.ChoiceField(choices=s_descuento_dic, required=False)
    # c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False)
    notas = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PrepagoAnticipado
        # exclude_1 = ('user_view', 'estatus', 'orden_descuento', 'contrato', 'pagare', 'conoce_cliente', 'f_1')
        # exclude_2 = ('DNI_img', 'carta_trabajo_img', 'talonario_1_img', 'talonario_2_img', 'ficha_img', 'recibo_img')
        # exclude_3 = ('carta_saldo_img', 'file_apc', 'check_cumplimiento',)
        # exclude = exclude_1 + exclude_2 + exclude_3
        fields = ('fecha_pago', 'fecha_ret_int', 'fecha_ret_seg', 'ret_int', 'ret_seg', 'penalidad', 'notas')


class FormReestructurado2(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            if form.name in ('fecha_ini', 'f_contrato', 'fecha_ini_OD'):
                form.field.widget.attrs['class'] = 'form-control'
                form.field.widget.format = '%Y-%m-%d'
                form.field.widget.attrs['placeholder'] = 'Ingrese fecha...'
            elif form.name == 'notas':
                form.field.widget.attrs['class'] = 'form-control'
                form.field.widget.attrs['rows'] = '3'
                form.field.widget.attrs['placeholder'] = 'Ingrese sus comentarios...'
            else:
                form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    diciembres = forms.ChoiceField(choices=sorted(diciembres_dic), required=True)
    int_global = forms.ChoiceField(choices=sorted(int_global_dic), required=True, label='Interés global')
    s_colectivo = forms.ChoiceField(choices=sorted(s_colectivo_dic), required=True, label='S. colectivo')
    s_desempleo = forms.ChoiceField(choices=sorted(s_desempleo_dic), required=True, label='S. desempleo')
    s_dental = forms.ChoiceField(choices=sorted(s_dental_dic), required=False, label='S. dental')
    c_cierre = forms.ChoiceField(choices=c_cierre_dic, required=False, label='C. cierre')
    notas = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Reestructurado
        exclude_1 = ('created_date', 'created_ref', 'desembolso', 'usuario', 'estatus', 'check_cumplimiento')
        exclude_2 = ('check_gerencia', 'orden_descuento', 'contrato', 'pagare', 'conoce_cliente', 'f_1')
        # exclude_3 = ('carta_saldo_img', 'file_apc', 'check_cumplimiento',)
        exclude = exclude_1 + exclude_2
        # fields = ('int_global', 's_colectivo', 's_desempleo', 's_dental', 'c_cierre', 'monto_r', 'plazo_r')


class FormEmpresaNOD(Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

    empresa_2 = forms.ModelChoiceField(queryset=Empresa.objects.all())

