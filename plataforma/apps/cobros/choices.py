c_cierre_dic = (
    (None, '------'),
    (50, '50%'),
    (45, '45%'),
    (40, '40%'),
    (35, '35%'),
)

c_promotor_dic = (
    (None, '------'),
    (0, '0%'),
    (3, '3%'),
    (4, '4%'),
    (5, '5%'),
    (7, '7%'),
)

s_descuento_dic = (
    (None, '------'),
    (0, '0%'),
    (2, '2%'),
    (3, '3%'),
    (4, '4%'),
)

tipo_pago_dic = (
    ('ach', 'ACH (transferencia electrónica)'),
    ('deposito', 'Depósito en efectivo'),
    ('voluntario', 'Pago Voluntario'),
    ('cheque', 'Cheque'),
)

tipo_pago_dic_dev = (
    (1, 'Devolución prepago'),
    (2, 'Devolución de letra'),
    (3, 'Otro'),
)

modo_pago_dic_dev = (
    ('ach', 'ACH (transferencia electrónica)'),
    ('deposito', 'Depósito en efectivo'),
    ('cheque', 'Cheque'),
    ('efectivo', 'Efectivo'),
)

estatus_dic = {
    ('Sin O.D.', 'Sin O.D.'),
    ('O.D. Recibida y verificada', 'O.D. Recibida y verificada'),
    ('Documentación firmada', 'Documentación firmada'),
    ('Desembolsado', 'Desembolsado')
}

diciembres_dic = {
    ('Si', 'Si'),
    ('No', 'No'),
}

int_global_dic = [
    ('0.5', '0.5 %'),
    ('1', '1.0 %'),
    ('1.5', '1.5 %'),
    ('2', '2.0 %'),
]

s_colectivo_dic = [
    ('3.5', '3.5 %'),
]

s_desempleo_dic = [
    ('2.5', '2.5 %'),
]

s_dental_dic = [
    ('1.0', '1.0 %'),
    ('plazo', 'En función de plazo'),
]
