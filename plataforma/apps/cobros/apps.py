from django.apps import AppConfig


class CobrosConfig(AppConfig):
    name = 'apps.cobros'
