import os

from django.db import models
from django.forms import model_to_dict
from django.utils import timezone as tz
from django.utils.translation import gettext_lazy as _

from apps.formulario.models import Provincia, Distrito, Corregimiento, TipoDocumento, EstadoCivil, Genero, Dependientes, \
    TipoIngreso, IngresosExtra, Escolaridad, PEP, Motivo
from apps.ventas.choices import *


# Create your models here.

class SectorEmpresa(models.Model):
    sector = models.CharField(max_length=25, null=True, blank=True)

    def __str__(self):
        return self.sector


class SubSector(models.Model):
    sector = models.ForeignKey(SectorEmpresa, on_delete=models.CASCADE)
    sub_sector = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.sub_sector


class TipoEmpresa(models.Model):
    tipo = models.CharField(max_length=25, null=True, blank=True)

    def __str__(self):
        return self.tipo


class AlcanceEmpresa(models.Model):
    alcance = models.CharField(max_length=25, null=True, blank=True)

    def __str__(self):
        return self.alcance


class Empresa(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=False, blank=True, default=tz.now)
    fecha_ingreso = models.DateTimeField(auto_now_add=True)
    r_social = models.CharField(_('r social'), max_length=100, null=True, blank=True)
    nombre = models.CharField(_('nombre'), max_length=100)
    ruc = models.CharField(_('ruc'), max_length=50)
    sector = models.ForeignKey(SectorEmpresa, on_delete=models.SET_NULL,
                               null=True, blank=True)  # Si es supermercado, hotelería, etc.
    sub_sector = models.ForeignKey(SubSector, on_delete=models.SET_NULL, null=True, blank=True)
    tipo = models.ForeignKey(TipoEmpresa, on_delete=models.SET_NULL,
                             null=True, blank=True)  # Si es empresa privada, gobierno, independiente
    alcance = models.ForeignKey(AlcanceEmpresa, on_delete=models.SET_NULL,
                                null=True, blank=True)  # Si es nacional, multinacional, ect.
    nombre_contacto = models.CharField(_('nombre contacto'), max_length=30)
    apellido_contacto = models.CharField(_('apellido'), max_length=30)
    telefono = models.IntegerField(_('telefono'), blank=True)
    celular = models.IntegerField(_('celular'), blank=True)
    email = models.EmailField(_('email'), max_length=100)
    cert_pj = models.CharField(_('cert pj'), max_length=40, null=True, blank=True)
    fecha_const = models.DateField(_('fecha const'), null=True, blank=True)
    provincia = models.ForeignKey(Provincia, on_delete=models.SET_NULL, null=True)
    distrito = models.ForeignKey(Distrito, on_delete=models.SET_NULL, null=True)
    corregimiento = models.ForeignKey(Corregimiento, on_delete=models.SET_NULL, null=True)
    direccion = models.CharField(_('dir empresa'), max_length=250)
    verificacion = models.BooleanField(_('verificacion'), null=True, blank=True, default=False)
    notas = models.CharField(_('notas'), max_length=500, null=True, blank=True)
    certificado_pe = models.CharField(_('certificado pe'), max_length=1000, null=True, blank=True)
    certificado_pj = models.CharField(_('certificado pj'), max_length=1000, null=True, blank=True)
    pagina_web = models.CharField(_('pagina web'), max_length=1000, null=True, blank=True)

    def __str__(self):
        return self.nombre + ' | ' + self.r_social

    def nombre_corto(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        return item


class PreAprobado(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=False, blank=True, default=tz.now)  # Fecha de solicitud
    fecha_ingreso = models.DateTimeField(auto_now_add=True)  # Fecha de proceso de la solicitud
    monto = models.CharField(_('monto'), null=False, blank=False, max_length=40)  #
    plazo_meses = models.CharField(_('plazo meses'), null=False, blank=False, max_length=40)  #
    nombre_1 = models.CharField(_('nombre 1'), max_length=40, null=False, blank=False)  #
    nombre_2 = models.CharField(_('nombre 2'), max_length=40, null=True, blank=True)  # Puede estar en blanco  #
    apellido_1 = models.CharField(_('apellido 1'), max_length=40, null=False, blank=False)  #
    apellido_2 = models.CharField(_('apellido 2'), max_length=40, null=True, blank=True)  #
    nacionalidad = models.CharField(_('apellido 2'), max_length=40, null=True, blank=True)  #
    motivo = models.ForeignKey(Motivo, on_delete=models.SET_NULL, null=True, blank=True)  #
    tipo_doc = models.ForeignKey(TipoDocumento, on_delete=models.SET_NULL, null=True)  #
    dni = models.CharField(_('dni'), max_length=20, null=False, blank=False)  #
    f_nacimiento = models.DateField(_('f_nacimiento'), null=False, blank=False)  #
    email_1 = models.EmailField(_('email 1'), max_length=254, null=False, blank=False)  #
    celular = models.IntegerField(_('celular'), null=False, blank=False)  #
    e_civil = models.ForeignKey(EstadoCivil, on_delete=models.SET_NULL, null=True)  #
    genero = models.ForeignKey(Genero, on_delete=models.SET_NULL, null=True)  #
    dependientes = models.ForeignKey(Dependientes, on_delete=models.SET_NULL, null=True)  #
    tipo_ingreso = models.ForeignKey(TipoIngreso, on_delete=models.SET_NULL, null=True)  #
    salario = models.DecimalField(_('salario'), max_digits=6, decimal_places=2, null=False, blank=False)  #
    empresa_1 = models.CharField(_('empresa'), max_length=50)  #
    empresa_2 = models.ForeignKey(Empresa, max_length=50, on_delete=models.SET_NULL, null=True, blank=True)  #
    in_extras = models.ForeignKey(IngresosExtra, on_delete=models.SET_NULL, null=True)  #
    estudios = models.ForeignKey(Escolaridad, on_delete=models.SET_NULL, null=True)  #
    p_expuesto = models.ForeignKey(PEP, on_delete=models.SET_NULL, null=True)  #
    provincia = models.ForeignKey(Provincia, on_delete=models.SET_NULL, null=True)  #
    distrito = models.ForeignKey(Distrito, on_delete=models.SET_NULL, null=True)  #
    corregimiento = models.ForeignKey(Corregimiento, on_delete=models.SET_NULL, null=True)  #
    direccion = models.CharField(_('dir_cliente'), max_length=250, null=True, blank=True, default='')  #
    ref = models.CharField(_('ref'), max_length=100, null=True, blank=True)  # opcional  # Promotor
    terms = models.BooleanField(_('terms'), default=False)  #
    apc = models.BooleanField(_('apc'), default=False)  #
    # Checks
    check_1 = models.BooleanField(_('check 1'), default=False)  # Pre-aprobada por Ventas
    check_2 = models.BooleanField(_('check 2'), default=False)  # Archivar
    check_3 = models.BooleanField(_('check 2'), default=False, null=True, blank=True)  # check cumplimiento
    check_cumplimiento = models.CharField(_('check 2'), default='Pendiente', max_length=25, null=True,
                                          blank=True)  # Aprobada por cumplimiento
    check_gerencia = models.CharField(_('check 2'), default='Pendiente', max_length=25, null=True,
                                      blank=True)  # Aprobada por gerencia
    # Documentos
    DNI_img = models.FileField(_('dni img'), upload_to='DNI/%Y/%m/%d', null=True, blank=True)
    carta_trabajo_img = models.FileField(_('carta trabajo'), upload_to='carta_trabajo/%Y/%m/%d', null=True, blank=True)
    talonario_1_img = models.FileField(_('talonario 1'), upload_to='talonario_1/%Y/%m/%d', null=True, blank=True)
    talonario_2_img = models.FileField(_('talonario 2'), upload_to='talonario_2/%Y/%m/%d', null=True, blank=True)
    ficha_img = models.FileField(_('ficha'), upload_to='ficha_seguro/%Y/%m/%d', null=True, blank=True)
    recibo_img = models.FileField(_('recibo'), upload_to='recibo_publico/%Y/%m/%d', null=True, blank=True)
    carta_saldo_img = models.FileField(_('carta_saldo'), upload_to='carta_saldo/%Y/%m/%d', null=True, blank=True)
    file_apc = models.FileField(_('file apc'), upload_to='APC/%Y/%m/%d', null=True, blank=True)

    user_view = models.CharField(_('user view'), max_length=50, null=True, blank=True)

    #  Notas
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)
    notas_cumplimiento = models.CharField(_('notas cumplimiento'), max_length=1000, null=True, blank=True)
    notas_gerencia = models.CharField(_('notas gerencia'), max_length=1000, null=True, blank=True)

    inicio_pago = models.CharField(_('inicio pago'), null=True, blank=True, max_length=100)

    #  Cotización

    #  Préstamo

    def __str__(self):
        nombre = ''
        if self.nombre_1: nombre += self.nombre_1
        if self.nombre_2: nombre += ' ' + self.nombre_2
        if self.apellido_1: nombre += ' ' + self.apellido_1
        if self.apellido_2: nombre += ' ' + self.apellido_2
        return nombre

    def nombre_corto(self):
        return self.nombre_1 + ' ' + self.apellido_1

    def toJSON(self):
        item = model_to_dict(self)
        return item

    def dni_name(self):
        return os.path.basename(self.DNI_img.name)

    def carta_trabajo_name(self):
        return os.path.basename(self.carta_trabajo_img.name)

    def talonario_1_name(self):
        return os.path.basename(self.talonario_1_img.name)

    def talonario_2_name(self):
        return os.path.basename(self.talonario_2_img.name)

    def ficha_name(self):
        return os.path.basename(self.ficha_img.name)

    def recibo_name(self):
        return os.path.basename(self.recibo_img.name)

    def carta_saldo_name(self):
        return os.path.basename(self.carta_saldo_img.name)

    def file_apc_name(self):
        return os.path.basename(self.file_apc.name)

    # def save(self, *args, **kwargs):
    #     try:
    #         this = PreAprobado.objects.get(id=self.id)
    #         if this.file_apc:
    #             this.file_apc.delete()
    #     except ObjectDoesNotExist:
    #         pass
    #     super(PreAprobado, self).save(*args, **kwargs)


class CotizacionPreAprobado(models.Model):
    cliente = models.OneToOneField(PreAprobado, on_delete=models.CASCADE, null=False, blank=False)
    nombre = models.CharField(_('nombre cliente'), max_length=100, null=True, blank=True)
    monto = models.CharField(_('monto'), max_length=100, null=True, blank=True)
    plazo = models.CharField(_('plazo'), max_length=100, null=True, blank=True)
    fecha_ini = models.CharField(_('fecha ini'), max_length=100, null=True, blank=True)
    compra_saldo = models.CharField(_('compra saldo'), max_length=100, null=True, blank=True)
    c_promotor = models.CharField(_('c promotor'), max_length=100, null=True, blank=True)
    s_descuento = models.CharField(_('s descuento'), max_length=100, null=True, blank=True)
    cambio_c = models.BooleanField(_('cambio c'), default=False, null=True, blank=True)

    def __str__(self):
        return self.nombre


class CotizacionLigera(models.Model):
    creado_el = models.DateTimeField(_('creado el'), null=False, blank=True, default=tz.now)
    fecha_ingreso = models.DateTimeField(auto_now_add=True)
    nombre_1 = models.CharField(_('nombre_1'), max_length=20)
    nombre_2 = models.CharField(_('nombre_2'), max_length=20)
    apellido_1 = models.CharField(_('apellido 1'), max_length=20)
    apellido_2 = models.CharField(_('apellido 2'), max_length=20)
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.SET_NULL, null=True)
    dni = models.CharField(_('dni'), max_length=25, null=False, blank=False)
    tipo_ingreso = models.ForeignKey(TipoIngreso, on_delete=models.SET_NULL, null=True)
    salario = models.DecimalField(_('salario'), max_digits=6, decimal_places=2, null=False, blank=False)
    email_1 = models.EmailField(_('email 1'), max_length=254, null=False, blank=False)
    celular = models.IntegerField(_('celular'), null=False, blank=False)
    user_view = models.CharField(_('user view'), max_length=30, null=False, blank=False)
    monto = models.CharField(_('monto'), null=False, blank=False, max_length=40)
    plazo_meses = models.CharField(_('plazo meses'), null=False, blank=False, max_length=40)
    notas = models.CharField(_('notas'), max_length=1000, null=True, blank=True)
    estatus = models.CharField(_('estatus'), max_length=20, null=True, blank=True, default='Activa')
    cancelaciones = models.CharField(_('cancelaciones'), max_length=200, null=True, blank=True)
    promotor = models.CharField(_('promotor'), null=True, blank=True, max_length=110)
    c_promotor = models.CharField(_('c promotor'), choices=c_promotor_dic, default=0, null=True, blank=True,
                                  max_length=10)
    s_descuento = models.CharField(_('s descuento'), choices=s_descuento_dic, default=0, null=True, blank=True,
                                   max_length=10)

    def __str__(self):
        salida = self.nombre_1 + ' ' + self.apellido_1
        return salida


# Tabla de info. call-center 2

class TablaCallCenter(models.Model):
    last_process = models.CharField(_('last process'), max_length=40, null=True, blank=True)

    def __str__(self):
        return self.last_process
