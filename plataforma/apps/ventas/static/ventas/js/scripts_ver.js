function borrar_archivo(archivo) {
    let tag;
    if (archivo === 'dni') {
        tag = 'DNI';
    } else if (archivo === 'c_trabajo') {
        tag = 'CARTA DE TRABAJO';
    } else if (archivo === 'talonario_1') {
        tag = 'TALONARIO #1';
    } else if (archivo === 'talonario_2') {
        tag = 'TALONARIO #2';
    } else if (archivo === 'ficha') {
        tag = 'FICHA';
    } else if (archivo === 'recibo') {
        tag = 'RECIBO';
    } else if (archivo === 'c_saldo') {
        tag = 'CARTA DE SALDO';
    } else if (archivo === 'apc') {
        tag = 'APC';
    }
    Swal.fire({
        title: 'Desea eliminar el archivo >' + tag,
        toast: true,
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Sí`,
        cancelButtonText: `Cancelar`,
        denyButtonText: `No`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'borrar_archivo',
                    'archivo': archivo
                },
                dataType: 'json',
            }).done(function (data) {
                console.log(data['ok']);
                if (!data.hasOwnProperty('error')) {
                    if (data.hasOwnProperty('ok')) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'info',
                            title: 'Se ha borrado el archivo ' + tag,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        setTimeout(function () {
                            location.href = window.location.pathname;
                        }, 1500);
                        return false;
                    }
                } else {
                    message_error(data);
                }
            }).fail(function (data) {
                alert("error");
            }).always(function (data) {
                // alert("complete")
            });
        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
    })
}