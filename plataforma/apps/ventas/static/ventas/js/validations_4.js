var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('edit_proceso'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_doc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                f_nacimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                genero: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                e_civil: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                dependientes: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9000.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                empresa_1: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ()"&'Üü ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                empresa_2: {
                    validators: {

                    }
                },
                in_extras: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                estudios: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                p_expuesto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                provincia: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                distrito: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                corregimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                motivo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                nacionalidad: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 5 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_doc':
                            case 'dni':
                            case 'f_nacimiento':
                            case 'celular':
                            case 'tipo_ingreso':
                            case 'estudios':
                            case 'corregimiento':
                            case 'apc':
                            case 'terms':
                            case 'check_1':
                            case 'check_2':
                            case 'nacionalidad':
                            case 'empresa_2':
                                return '.col-md-3';

                            case 'DNI_img':
                            case 'carta_trabajo_img':
                            case 'talonario_1_img':
                            case 'talonario_2_img':
                            case 'ficha_img':
                            case 'recibo_img':
                            case 'carta_saldo_img':
                            case 'motivo':
                                return '.col-md-4';

                            case 'e_civil':
                            case 'dependientes':
                            case 'salario':
                            case 'in_extras':
                            case 'monto':
                            case 'provincia':
                            case 'distrito':
                            case 'p_expuesto':
                            case 'plazo_meses':
                            case 'genero':
                                return '.col-md-2';

                            case 'email_1':
                            case 'empresa_1':
                            case 'direccion':
                            case 'sda':
                                return '.col-md-5';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        let temp = new FormData(document.getElementById("edit_proceso"));
        let pre_parameters = new FormData;
        let id_form = document.getElementById('id_form').value;
        temp.forEach(function (value, key){
            if (value !== "" || value.name !== ""){
                pre_parameters.append(key, value);
            }
        })
        let parameters = process_formdata(pre_parameters);
        /*parameters.forEach(function (value, key) {
            console.log(key + ":" + value);
        });*/
        // console.log(parameters);
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataType: 'json',
            processData: false,
            contentType: false,
        }).done(function (data) {
            console.log(data['ok']);
            if (!data.hasOwnProperty('error')) {
                let info_split = data['ok'].split('-');
                if (info_split[0] === '1') {
                    location.href = '/ventas/solicitudes/proceso/ver/' + id_form + '/';
                } else if (info_split[0] === '2') {
                    location.href = '/ventas/solicitudes/preaprobadas/';
                } else if (info_split[0] === '3') {
                    location.href = '/prestamos/aprobados/ver/' + info_split[1] + '/';
                } else if (info_split[0] === '4') {
                    location.href = '/cobros/editar_cliente/' + info_split[1] + '/new/';
                }
            } else {
                message_error(data);
            }
        }).fail(function (data) {
            alert("error");
        }).always(function (data) {
            // alert("complete")
        });
    });

    $('[name="f_nacimiento"]')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            endDate: '-18y'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_nacimiento');
        });
});

function process_formdata(formulario) {
    if (formulario.has('image_1')) {
        formulario.delete('DNI_img');
    }
    if (formulario.has('image_2')) {
        formulario.delete('carta_trabajo_img');
    }
    if (formulario.has('image_3')) {
        formulario.delete('talonario_1_img');
    }
    if (formulario.has('image_4')) {
        formulario.delete('talonario_2_img');
    }
    if (formulario.has('image_5')) {
        formulario.delete('ficha_img');
    }
    if (formulario.has('image_6')) {
        formulario.delete('recibo_img');
    }
    if (formulario.has('image_7')) {
        formulario.delete('carta_saldo_img');
    }
    return formulario;
}







