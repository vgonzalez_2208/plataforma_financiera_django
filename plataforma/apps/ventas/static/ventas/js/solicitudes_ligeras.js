var csrftoken = getCookie('csrftoken');

var tabla_ligeras = $('#tabla_ligeras').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'CargarTabla'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "id"},
        {"data": "nombre"},
        {"data": "apellido"},
        {"data": "tipo_ingreso"},
        {"data": "monto"},
        {"data": "f_solicitud"},
        {"data": "proceso"},
        {"data": "celular"},
        {"data": "opciones"},
        // {"data": "genero"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let buttons = '<button type="button" onclick="ligeras_ver(' + row.id + ')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_ver"><i class="fas fa-eye"></i></button>';
                buttons += ' ' + '<a href="/ventas/solicitudes/ligeras/' + row.id + '/" class="btn btn-warning btn-xs" target="_blank"><i class="fas fa-cogs"></i></a>';
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                return '<a href="https://wa.me/507' + data + '" target="_blank"><i class=" fab fa-whatsapp"></i> ' + data + '</a>';
            },
        },
        {
            targets: [-4],
            class: 'text-center',
            render: function (data, type, row) {
                let hoy_temp = new Date();
                let mes_act;
                if (hoy_temp.getMonth() < 9) {
                    mes_act = hoy_temp.getFullYear().toString() + '-0' + (hoy_temp.getMonth() + 1).toString();
                } else {
                    mes_act = hoy_temp.getFullYear().toString() + '-' + (hoy_temp.getMonth() + 1).toString();
                }
                if (data === mes_act) {
                    return '<span class="badge badge-info">' + data + '</span>';
                } else {
                    return '<span class="badge badge-secondary">' + data + '</span>';
                }
            },
        },
    ],
    order: [0, 'des'],
    initComplete: function (settings, json) {
        this.api().columns([3, 5, 6]).every(function () {
            let column = this;
            // console.log(column['0'][0]);
            let placeholder;
            if (column['0'][0] === 5) {
                placeholder = 'Sel...';
            } else if (column['0'][0] === 6) {
                placeholder = 'Selecc...';
            }else if (column['0'][0] === 3) {
                placeholder = 'Seleccione...';
            }
            let select = $('<select class="form-control"><option class="text-muted" value="">' + placeholder + '</option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    let val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });
            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    }
});

var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('modal_ligera'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_documento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9000.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                promotor: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                c_promotor: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                s_descuento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_documento':
                            case 'dni':
                            case 'email_1':
                            case 'celular':
                            case 'monto':
                            case 'plazo_meses':
                            case 'tipo_ingreso':
                            case 'salario':
                                return '.col-sm-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        let parameters = $('#modal_ligera').serializeArray();
        parameters.push({name: 'action', value: 'guardar_ligera'});
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataType: 'json',
        }).done(function (data) {
            tabla_ligeras.ajax.reload();
            $('#modal_agregar').modal('hide');
            document.getElementById("modal_ligera").reset();
            fv.resetForm(true);
            if (!data.hasOwnProperty('error')) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Se ha creado la solicitud',
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                message_error(data);
            }
        }).fail(function (data) {
            alert("error");
        }).always(function (data) {
            // alert("complete")
        });
    });
});

function ligeras_ver(id_val) {
    document.getElementById('id_solicitud').value = id_val;
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'ver_ligera');
    parameters.append('id', id_val);
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (data) {
            if (!data.hasOwnProperty('error')) {
                document.getElementById('ver_fecha').innerText = data['f_solicitud'];
                document.getElementById('ver_nombre_1').value = data['nombre_1'];
                document.getElementById('ver_nombre_2').value = data['nombre_2'];
                document.getElementById('ver_apellido_1').value = data['apellido_1'];
                document.getElementById('ver_apellido_2').value = data['apellido_2'];
                document.getElementById('ver_tipo_documento').value = data['tipo_documento'];
                document.getElementById('ver_dni').value = data['dni'];
                document.getElementById('ver_email_1').value = data['email_1'];
                document.getElementById('ver_celular').value = data['celular'];
                document.getElementById('ver_monto').value = data['monto'];
                document.getElementById('ver_plazo_meses').value = data['plazo_meses'];
                document.getElementById('ver_tipo_ingreso').value = data['tipo_ingreso'];
                document.getElementById('ver_salario').value = data['salario'];
                document.getElementById('ver_cancelaciones').value = data['cancelaciones'];
                document.getElementById('ver_promotor').value = data['promotor'];
                document.getElementById('ver_c_promotor').value = data['c_promotor'];
                document.getElementById('ver_s_descuento').value = data['s_descuento'];
                document.getElementById('ver_notas').value = data['notas'];
            } else {
                message_error(data);
            }
        })
}

function ampliar_solicitud() {
    let id_solicitud = document.getElementById('id_solicitud').value;
    $('#modal_ver').modal('hide');
    location.href = '/ventas/solicitudes/agregar/' + id_solicitud;
}

var fv_2;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_2 = FormValidation.formValidation(
        document.getElementById('form_ver'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_documento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9000.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                promotor: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                c_promotor: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                s_descuento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_documento':
                            case 'dni':
                            case 'email_1':
                            case 'celular':
                            case 'monto':
                            case 'plazo_meses':
                            case 'tipo_ingreso':
                            case 'salario':
                                return '.col-sm-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        let id_solicitud = document.getElementById('id_solicitud').value;
        let parameters = $('#form_ver').serializeArray();
        parameters.push({name: 'action', value: 'update_ligera'});
        parameters.push({name: 'id', value: id_solicitud});
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataType: 'json',
        }).done(function (data) {
            tabla_ligeras.ajax.reload();
            $('#modal_ver').modal('hide');
            document.getElementById("form_ver").reset();
            fv_2.resetForm(true);
            if (!data.hasOwnProperty('error')) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Se guardaron los cambios',
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                message_error(data);
            }
        }).fail(function (data) {
            alert("error");
        }).always(function (data) {
            // alert("complete")
        });
    });
});


function descartar_ligera() {
    $(document).off('focusin.modal');
    let id_solicitud = document.getElementById('id_solicitud').value;
    Swal.fire({
        title: '¿Desea descartar la solicitud?',
        showDenyButton: true,
        confirmButtonText: 'Sí',
        denyButtonText: `No`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            (async () => {
                const {value: text} = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Comentarios',
                    inputPlaceholder: 'Escriba sus comentarios aquí...',
                    inputAttributes: {
                        'aria-label': 'Type your message here'
                    },
                    showCancelButton: true
                })
                if (text) {
                    let parameters = new FormData(document.getElementById('token_form'));
                    parameters.append('action', 'descartar_ligera');
                    parameters.append('id', id_solicitud);
                    parameters.append('notas', text);
                    let url = window.location.pathname;
                    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                        .catch(error => console.log(error))
                        .then(function (data) {
                            tabla_ligeras.ajax.reload();
                            $('#modal_ver').modal('hide');
                            document.getElementById("form_ver").reset();
                            fv_2.resetForm(true);
                            if (!data.hasOwnProperty('error')) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'info',
                                    title: 'Se descartó la solicitud',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            } else {
                                message_error(data);
                            }
                        })
                } else if (!text) {
                    Swal.fire('Debe ingresar un comentario para continuar.')
                }
            })()
        }
    })
}