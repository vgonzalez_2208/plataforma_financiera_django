$(function () {
    $('button[name="guardar"]').on('click', function () {
        var notas = $('textarea[name="notas"]').val();
        // console.log(notas);
        $.ajax({
            url: window.location.pathname, //window.location.pathname
            type: 'POST',
            data: {
                'action': 'comentar',
                'nota': notas
            },
            dataType: 'json',
        }).done(function (data) {
            // console.log(data);
            $.each(data[0], function (key, data) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: data,
                    showConfirmButton: false,
                    timer: 1200
                })
            })
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {

        });
    })
});

function select() {
    let selector = document.getElementById('selector_1');
    if (selector.value !== '----') {
        if (selector.value === '0') {
            $('#tarjeta').collapse('hide');
            document.getElementById("entrada_1").disabled = true;
            document.getElementById("entrada_2").disabled = true;
            document.getElementById("inicio").disabled = true;
            document.getElementById("calcular").disabled = true;
            document.getElementById("selector_2").disabled = true;
            document.getElementById("selector_3").disabled = true;
            document.getElementById("label_1").innerHTML = '-------';
            document.getElementById("label_2").innerHTML = '-------';
            document.getElementById("text_help_1").innerHTML = '-------';
            document.getElementById("text_help_6").innerHTML = '-------';
        }
        if (selector.value === '1') {
            document.getElementById("entrada_1").disabled = false;
            document.getElementById("entrada_2").disabled = false;
            document.getElementById("inicio").disabled = false;
            document.getElementById("calcular").disabled = false;
            document.getElementById("selector_2").disabled = true;
            document.getElementById("selector_3").disabled = true;
            document.getElementById("label_1").innerHTML = 'Monto:';
            document.getElementById("label_2").innerHTML = 'Cuota:';
            document.getElementById("text_help_1").innerHTML = 'Neto a entregar.';
            document.getElementById("text_help_6").innerHTML = 'Mensual.';
            $('#tarjeta').collapse('show');
        }
        if (selector.value === '2') {
            document.getElementById("entrada_1").disabled = false;
            document.getElementById("entrada_2").disabled = false;
            document.getElementById("inicio").disabled = false;
            document.getElementById("calcular").disabled = false;
            document.getElementById("selector_2").disabled = true;
            document.getElementById("selector_3").disabled = true;
            document.getElementById("label_1").innerHTML = 'Cuota:';
            document.getElementById("label_2").innerHTML = 'Monto:';
            document.getElementById("text_help_1").innerHTML = 'Letra mensual.';
            document.getElementById("text_help_6").innerHTML = 'Neto a entregar.';
            $('#tarjeta').collapse('show');
        }
        if (selector.value === '3') {
            document.getElementById("entrada_1").disabled = false;
            document.getElementById("entrada_2").disabled = false;
            document.getElementById("inicio").disabled = false;
            document.getElementById("calcular").disabled = false;
            document.getElementById("selector_2").disabled = false;
            document.getElementById("selector_3").disabled = false;
            document.getElementById("label_1").innerHTML = 'desembolso:';
            document.getElementById("label_2").innerHTML = 'Monto:';
            document.getElementById("text_help_1").innerHTML = 'Dinero en mano.';
            document.getElementById("text_help_6").innerHTML = 'Neto a entregar.';
            $('#tarjeta').collapse('show');
        }
    }
}

$(document).ready(function () {
    $('#calcular').on('click', function () {
        let selector = document.getElementById('selector_1');
        if (selector.value === '1') {
            let ajuste, mes_extra;
            let fecha_temp = document.getElementById("inicio");
            let monto = parseFloat(document.getElementById("entrada_1").value);
            let plazo = parseInt(document.getElementById("entrada_2").value, 10);
            let temp = Date.parse(fecha_temp.value);
            let fecha = new Date(temp);
            if (fecha.getMonth() === 11) {
                temp = Date.parse((fecha.getFullYear() + 1).toString() + "-1" + "-15");
                fecha = new Date(temp);
            }
            let dia = fecha.getDate();
            if (dia > 15) {
                ajuste = 0.5;
            } else {
                ajuste = 0;
            }
            mes_extra = Math.floor(((fecha.getMonth() + plazo + ajuste) / 12 + plazo + fecha.getMonth() + ajuste) / 12);
            let total_meses = plazo + mes_extra;
            let monto_prestamo = monto + monto * 0.5 + monto * 0.02 * total_meses + monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25 + 12;

            let cuota_q = monto_prestamo / (plazo * 2);
            let cuota_m = cuota_q.toFixed(2) * 2;

            document.getElementById("salida_1").innerHTML = "<strong>" + "$ " + cuota_m.toString() + "</strong>";
            document.getElementById("salida_2").innerHTML = '<strong>' + "$ " + monto_prestamo.toFixed(2).toString() + '</strong>';
        }
        if (selector.value === '2') {
            let ajuste, mes_extra, monto_prestamo;
            let fecha_temp = document.getElementById("inicio").value;

            let cuota = parseFloat(document.getElementById("entrada_1").value);
            let plazo = parseInt(document.getElementById("entrada_2").value, 10);
            let temp = Date.parse(fecha_temp);
            let fecha = new Date(temp);
            if (fecha.getMonth() === 11) {
                temp = Date.parse((fecha.getFullYear() + 1).toString() + "-1" + "-15");
                fecha = new Date(temp);
            }
            let dia = fecha.getDate();
            if (dia > 15) {
                ajuste = 0.5;
            } else {
                ajuste = 0;
            }
            mes_extra = Math.floor(((fecha.getMonth() + plazo + ajuste) / 12 + plazo + fecha.getMonth() + ajuste) / 12);

            monto_prestamo = cuota * plazo;
            let total_meses = plazo + mes_extra;
            let monto = ((monto_prestamo - total_meses * 1.25 - 12) / ((0.06 * total_meses) / 12 + 1 + 0.02 * total_meses + 0.5));

            document.getElementById("salida_1").innerHTML = "<strong>" + "$ " + monto.toFixed(2).toString() + "</strong>";
            document.getElementById("salida_2").innerHTML = '<strong>' + "$ " + monto_prestamo.toString() + '</strong>';
        }
        if (selector.value === '3') {
            let ajuste, mes_extra, comision;
            let fecha_temp = document.getElementById("inicio");
            let desembolso = parseInt(document.getElementById("entrada_1").value, 10);
            let plazo = parseInt(document.getElementById("entrada_2").value, 10);
            let promotor = parseInt(document.getElementById('selector_2').value, 10) / 100;
            let s_descuento = parseInt(document.getElementById('selector_3').value, 10) / 100;
            let temp = Date.parse(fecha_temp.value);
            let fecha = new Date(temp);
            let monto = desembolso;
            let monto_prestamo;
            let desembolso_temp = 0;
            if (fecha.getMonth() === 11) {
                temp = Date.parse((fecha.getFullYear() + 1).toString() + "-1" + "-15");
                fecha = new Date(temp);
            }
            let dia = fecha.getDate();
            if (dia > 15) {
                ajuste = 0.5;
            } else {
                ajuste = 0;
            }
            mes_extra = Math.floor(((fecha.getMonth() + plazo + ajuste) / 12 + plazo + fecha.getMonth() + ajuste) / 12);
            let total_meses = plazo + mes_extra;
            while (desembolso_temp < desembolso) {
                monto += 0.01;
                comision = monto * 0.5;
                monto_prestamo = monto + comision + monto * 0.02 * total_meses + monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25 + 12;
                itbms = (comision - (Math.ceil(monto_prestamo / 100) / 10).toFixed(2) - promotor*monto - s_descuento*monto_prestamo)*0.07;
                desembolso_temp = monto - itbms;
            }
            document.getElementById("salida_1").innerHTML = "<strong>" + "$ " + monto.toFixed(2).toString() + "</strong>";
            document.getElementById("salida_2").innerHTML = '<strong>' + "$ " + monto_prestamo.toFixed(2).toString() + '</strong>';
        }
    });
});

