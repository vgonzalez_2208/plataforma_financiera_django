function info_dashboard() {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'admin_dash');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (!('error' in response)) {
                // console.log(response);
                let num_registro = document.getElementById('num_op_registro');
                let num_revision = document.getElementById('num_op_revision');
                let num_enviadas = document.getElementById('num_op_enviadas');
                if ('num_registro' in response) {
                    box_class_admin('box_op_registro', 2);
                    num_registro.innerHTML = response['num_registro'];
                } else {
                    box_class_admin('box_op_registro', 1);
                    num_registro.innerHTML = '0';
                }
                if ('num_revision' in response) {
                    box_class_admin('box_op_revision', 0);
                    num_revision.innerHTML = response['num_revision'];
                } else {
                    box_class_admin('box_op_revision', 1);
                    num_revision.innerHTML = '0';
                }
                if ('num_enviadas' in response) {
                    box_class_admin('box_op_enviadas', 3);
                    num_enviadas.innerHTML = response['num_enviadas'];
                } else {
                    box_class_admin('box_op_enviadas', 1);
                    num_enviadas.innerHTML = '0';
                }
            }
        })
}

function box_class_admin(id, tipo) {
    let box = document.getElementById(id);
    if (tipo === 1) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-success');
    } else if (tipo === 2) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-success');
        box.classList.add('bg-danger');
    } else if (tipo === 3) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-danger');
        box.classList.remove('bg-success');
        box.classList.add('bg-info');
    } else {
        box.classList.remove('bg-success');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-warning');
    }
}
