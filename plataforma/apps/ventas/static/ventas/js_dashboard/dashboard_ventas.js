function info_dashboard() {
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'info_call-center');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                let reg_emp_pend = response['empresas_faltantes'];
                let num_emp_pend = reg_emp_pend.length;
                document.getElementById('num_emp_pendientes').innerText = num_emp_pend.toString();
                // console.log(reg_emp_pend);
                let lista_2 = document.getElementById('li_emp_pend');
                reg_emp_pend.forEach(function (info) {
                    let li_2 = document.createElement('li');
                    li_2.innerHTML = ' >> ' + '<b>' + info[0] + '</b> ' + '. Ref. ' + '<span class="text-indigo">' + info[1] + '</span>';
                    lista_2.appendChild(li_2);
                })
            }
        })
}