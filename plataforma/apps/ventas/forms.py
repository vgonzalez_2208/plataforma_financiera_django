import datetime

from django.forms import *

from apps.formulario.models import Formulario, Provincia, Distrito, Corregimiento, TipoDocumento
from apps.ventas.models import Empresa, TipoEmpresa, AlcanceEmpresa, PreAprobado, SectorEmpresa, SubSector, \
    CotizacionLigera

dia = datetime.date.today()
year = dia.year - 5

x = datetime.datetime(year, dia.month, dia.day)
x = x.strftime("%d/%m/%Y")


class SolicitudRevisada(forms.Form):
    check = BooleanField(widget=CheckboxInput(attrs={
        'class': 'form-check-input',
        # 'required': 'False'
    }))

    class Meta:
        model = Formulario
        fields = 'check'


class EditSolicitud(ModelForm):
    provincia = ModelChoiceField(queryset=Provincia.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    distrito = ModelChoiceField(queryset=Distrito.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    corregimiento = ModelChoiceField(queryset=Corregimiento.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    class Meta:
        model = Formulario
        exclude = ('user_view', 'inicio_pago', 'notas')
        # fields = 'nombre_1', 'nombre_2', 'apellido_1', 'apellido_2', 'tipo_doc', 'dni', 'f_nacimiento', 'email_1', \
        #          'celular', 'e_civil', 'genero', 'dependientes', 'tipo_ingreso', 'salario', 'empresa', 'ex_ingresos', \
        #          'estudios', 'p_expuesto', 'ciudad', 'sector', 'desc_dir', 'ref', 'terms', 'apc', 'monto', \
        #          'plazo_meses', 'check_1', 'check_2'

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'tipo_doc': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dni': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese ID',
                    'autocomplete': 'off'
                }
            ),
            'f_nacimiento': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control datepicker',
                'id': 'datepicker',
                # 'placeholder': x,
                # 'value': x,
                # 'data-target': '#f_nacimiento'
            }
                                      ),
            'email_1': EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'micorreo@mail.com',
                    'autocomplete': 'on'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '61234567',
                    'autocomplete': 'off'
                }
            ),
            'e_civil': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'genero': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dependientes': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'tipo_ingreso': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '100',
                    'placeholder': '100.00'
                }
            ),
            'empresa_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'in_extras': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'estudios': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'p_expuesto': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Panamá',
                    'autocomplete': 'off'
                }
            ),

            'ref': TextInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off'
                }
            ),
            'terms': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True',
                    # 'disabled': None,
                    'checked': None,
                    # 'required': 'True'
                }
            ),
            'apc': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True',
                    # 'disabled': None,
                    'checked': None,
                    # 'required': 'True'
                }
            ),
            'monto': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '300',
                    'max': '3500',
                    # 'step': '10',
                    'id': 'entrada_1',
                    'oninput': 'myFunction()',
                    'value': '1000'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '6',
                    'max': '30',
                    # 'step': '1',
                    'id': 'entrada_2',
                    'oninput': 'myFunction()',
                    'value': '24'
                }
            ),
            'check_1': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'check_2': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'check_3': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'motivo': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
        }


class CrearSolicitud(ModelForm):
    provincia = ModelChoiceField(queryset=Provincia.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    distrito = ModelChoiceField(queryset=Distrito.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    corregimiento = ModelChoiceField(queryset=Corregimiento.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    class Meta:
        model = Formulario
        fields = '__all__'

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'tipo_doc': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dni': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese ID',
                    'autocomplete': 'off'
                }
            ),
            'f_nacimiento': TextInput(
                attrs={
                    'type': 'text',
                    'class': 'form-control',
                    'placeholder': x,
                    # 'class': 'form-control datepicker',
                    # 'id': 'datepicker',
                    # 'placeholder': x,
                    # 'value': x,
                    # 'data-target': '#f_nacimiento'
                }
            ),
            'email_1': EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'micorreo@mail.com',
                    'autocomplete': 'on'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '61234567',
                    'autocomplete': 'off'
                }
            ),
            'e_civil': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'genero': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dependientes': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'tipo_ingreso': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '100',
                    'placeholder': '100.00'
                }
            ),
            'empresa_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'in_extras': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'estudios': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'p_expuesto': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Panamá',
                    'autocomplete': 'off'
                }
            ),

            'ref': TextInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off'
                }
            ),
            'terms': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True'
                }
            ),
            'apc': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True'
                }
            ),
            'monto': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '300',
                    'max': '3500',
                    # 'step': '10',
                    'id': 'entrada_1',
                    'oninput': 'myFunction()',
                    'value': '1000'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '6',
                    'max': '24',
                    # 'step': '1',
                    'id': 'entrada_2',
                    'oninput': 'myFunction()',
                    'value': '24'
                }
            ),
            'check_1': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'check_2': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'motivo': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
        }


class EmpresasForm(ModelForm):
    provincia = ModelChoiceField(queryset=Provincia.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    distrito = ModelChoiceField(queryset=Distrito.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    corregimiento = ModelChoiceField(queryset=Corregimiento.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    sector = ModelChoiceField(queryset=SectorEmpresa.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    sub_sector = ModelChoiceField(queryset=SubSector.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    tipo = ModelChoiceField(queryset=TipoEmpresa.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    alcance = ModelChoiceField(queryset=AlcanceEmpresa.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    class Meta:
        model = Empresa
        fields = '__all__'

        widgets = {
            'nombre': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese un nombre',
                    'autocomplete': 'off'
                }
            ),
            'r_social': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese razón social',
                    'autocomplete': 'off'
                }
            ),
            'ruc': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese un nombre',
                    'autocomplete': 'off'
                }
            ),
            'nombre_contacto': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese un nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_contacto': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese un nombre',
                    'autocomplete': 'off'
                }
            ),
            'telefono': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '3975152',
                    'autocomplete': 'off'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '63975152',
                    'autocomplete': 'off'
                }
            ),
            'email': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'ventas@rapicash.com',
                    'autocomplete': 'off'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Calle, barriada, residencial, edificio.',
                    'autocomplete': 'off'
                }
            ),
            'verificacion': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '3',
                    'cols': '150'
                }
            ),
            'fecha_const': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control datepicker',
                'id': 'fecha_const',
                'aria-describedby': 'text_help',
                'placeholder': x,
                # 'value': x,
                # 'data-target': '#f_nacimiento'
            }
                                     ),
            'certificado_pe': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese enlace https://www...',
                    'autocomplete': 'off',
                    'aria-label': 'Enlace de certificado de Panamá Emprende',
                    'aria-describedby': 'button_pe'
                }
            ),
            'certificado_pj': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese enlace https://www...',
                    'autocomplete': 'off',
                    'aria-label': 'Enlace de certificado de Persona Jurídica',
                    'aria-describedby': 'button_pj'
                }
            ),
            'pagina_web': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese enlace https://www...',
                    'autocomplete': 'off',
                    'aria-label': 'Página web de la empresa',
                    'aria-describedby': 'button_pw'
                }
            ),
        }


class EditSolicitudProceso(ModelForm):
    provincia = ModelChoiceField(queryset=Provincia.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    distrito = ModelChoiceField(queryset=Distrito.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    corregimiento = ModelChoiceField(queryset=Corregimiento.objects.all(), widget=Select(attrs={
        'class': 'form-select form-control',
    }))

    class Meta:
        model = PreAprobado
        exclude_1 = ('DNI_img', 'carta_trabajo_img', 'talonario_1_img', 'talonario_2_img', 'ficha_img', 'recibo_img')
        exclude_2 = ('carta_saldo_img', 'file_apc', 'check_cumplimiento', 'check_gerencia', 'user_view', 'check_1')
        exclude_3 = ('check_2', 'check_3', 'notas', 'notas_cumplimiento', 'notas_gerencia',)
        exclude = exclude_1 + exclude_2 + exclude_3
        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'tipo_doc': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dni': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese ID',
                    'autocomplete': 'off'
                }
            ),
            'f_nacimiento': DateInput(format='%Y-%m-%d', attrs={
                'class': 'form-control datepicker',
                'id': 'datepicker',
                # 'placeholder': x,
                # 'value': x,
                # 'data-target': '#f_nacimiento'
            }
                                      ),
            'email_1': EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'micorreo@mail.com',
                    'autocomplete': 'on'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '61234567',
                    'autocomplete': 'off'
                }
            ),
            'e_civil': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'genero': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'dependientes': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'tipo_ingreso': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '100',
                    'placeholder': '100.00'
                }
            ),
            'empresa_1': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'empresa_2': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'in_extras': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'estudios': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'p_expuesto': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'direccion': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Panamá',
                    'autocomplete': 'off'
                }
            ),

            'ref': TextInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off'
                }
            ),
            'terms': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True',
                    # 'disabled': None,
                    'checked': None,
                    # 'required': 'True'
                }
            ),
            'apc': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'required': 'True',
                    # 'disabled': None,
                    'checked': None,
                    # 'required': 'True'
                }
            ),
            'monto': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '300',
                    'max': '3500',
                    # 'step': '10',
                    'id': 'entrada_1',
                    'oninput': 'myFunction()',
                    'value': '1000'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    # 'type': 'range',
                    'class': 'form-control',
                    # 'required': 'True',
                    'min': '6',
                    'max': '30',
                    # 'step': '1',
                    'id': 'entrada_2',
                    'oninput': 'myFunction()',
                    'value': '24'
                }
            ),
            'check_1': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'alertas("1")'
                }
            ),
            'check_2': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                }
            ),
            'check_3': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'alertas("2")'
                }
            ),
            'motivo': Select(
                attrs={
                    'class': 'form-select form-control',
                    'autocomplete': 'off'
                }
            ),
            'nacionalidad': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Panameño - Colombiano - ect.',
                    'autocomplete': 'off',
                }
            ),
        }


class VerSolicitudesP(ModelForm):
    tipo_doc = TextInput()

    class Meta:
        model = PreAprobado
        fields = '__all__'
        # fields = ['notas', 'check_1', 'check_2', 'check_3']

        widgets = {
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '5',
                    'cols': '150'
                }
            ),
            'check_1': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'ventas();'
                }
            ),
            'check_2': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'descartar();'
                }
            ),
            'check_3': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'cumplimiento();'
                }
            ),
        }


class VerSolicitudesN(ModelForm):
    tipo_doc = TextInput()

    class Meta:
        model = Formulario
        fields = '__all__'

        widgets = {
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '5',
                    'cols': '150'
                }
            ),
            'check_3': CheckboxInput(
                attrs={
                    'type': 'checkbox',
                    'class': 'custom-control-input',
                    'onchange': 'recuperar();'
                }
            ),
        }


class Cotizador(ModelForm):
    class Meta:
        model = Formulario
        fields = '__all__'

        widgets = {
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '5',
                    'cols': '150'
                }
            ),
            'monto': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '300',
                    'placeholder': '3500.00'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '6',
                    'placeholder': '30'
                }
            ),
            'inicio_pago': DateInput(format='%d-%m-%Y', attrs={
                'class': 'form-control datepicker',
            }
                                     ),
        }


class CotizadorP(ModelForm):
    class Meta:
        model = PreAprobado
        fields = '__all__'

        widgets = {
            'notas': Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '5',
                    'cols': '150'
                }
            ),
            'monto': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '300',
                    'placeholder': '3500.00'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    'class': 'form-control',
                    'autocomplete': 'off',
                    'min': '6',
                    'placeholder': '30'
                }
            )
        }


class FormLigeras(ModelForm):
    class Meta:
        model = CotizacionLigera
        exclude = ('user_view', 'estatus',)

        widgets = {
            'nombre_1': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off',
                }
            ),
            'nombre_2': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese nombre',
                    'autocomplete': 'off'
                }
            ),
            'apellido_1': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'apellido_2': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'tipo_documento': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off'
                }
            ),
            'dni': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese apellido',
                    'autocomplete': 'off'
                }
            ),
            'email_1': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese email',
                    'autocomplete': 'off'
                }
            ),
            'celular': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Ingrese celular',
                    'autocomplete': 'off'
                }
            ),
            'monto': NumberInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off',
                    'min': '300',
                    'placeholder': '3500.00'
                }
            ),
            'plazo_meses': NumberInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off',
                    'min': '6',
                    'placeholder': '30'
                }
            ),
            'notas': Textarea(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Escriba sus comentarios aquí.',
                    'autocomplete': 'off',
                    'rows': '2',
                    'cols': '150'
                }
            ),
            'tipo_ingreso': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off'
                }
            ),
            'salario': NumberInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off',
                    'min': '300',
                    'max': '9999',
                    'placeholder': '300'
                }
            ),
            'cancelaciones': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Rapicash: 200.50, Rapi...',
                    'autocomplete': 'off'
                }
            ),
            'promotor': TextInput(
                attrs={
                    'class': 'form-control form-control-sm',
                    'placeholder': 'Mar del Sojo...',
                    'autocomplete': 'off'
                }
            ),
            'c_promotor': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off'
                }
            ),
            's_descuento': Select(
                attrs={
                    'class': 'form-control form-control-sm',
                    'autocomplete': 'off'
                }
            ),
        }
