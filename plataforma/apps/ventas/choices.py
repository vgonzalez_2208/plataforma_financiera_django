t_expuesto = (
    (None, 'Seleccione...'),
    ('No', 'Ninguna Condición Especial PEP'),
    ('p1', 'Alto Dignatario del Poder Ejecutivo de un Estado'),
    ('p2', 'Alto Dignatario del Poder Judicial de un Estado'),
    ('p3', 'Alto Directivo de una Empresa Pública'),
    ('p4', 'Asociado Cercano o Allegado de Confianza a una PEP'),
    ('p5', 'Dirigente de Partido Político'),
    ('p6', 'Familiar de PEP'),
    ('p7', 'Funcionario Público con Jurisdicción y Mando'),
    ('p8', 'Jefe de Estado o Gobierno'),
    ('p9', 'Jefe de Organismo Supranacional'),
    ('p10', 'Miembro de la Familia Real con Funciones de Gobierno'),
    ('p11', 'Miembro de la Fuerza Pública de un Estado'),
    ('p12', 'Miembro del Parlamento, Congreso o Asamblea Nacional'),
    ('p13', 'Miembro o Directivo de un Banco Central'),
    ('p14', 'Persona Natural Electa y Posesionado en Cargo por Votación Popular'),
    ('p15', 'Persona Públicamente Expuesta Internacional'),
    ('p16', 'Persona Públicamente Expuesta Nacional'),
    ('p17', 'Persona Políticamente Expuesta Internacional'),
    ('p18', 'Persona Políticamente Expuesta Nacional'),
    ('p19', 'Representante Diplomático'),
)

c_promotor_dic = (
    (0, '0%'),
    (2, '2.0%'),
    (2.5, '2.5%'),
    (3, '3.0%'),
    (3.25, '3.25%'),
    (3.5, '3.5%'),
    (3.75, '3.75%'),
    (4, '4.0%'),
    (4.25, '4.25%'),
    (4.5, '4.5%'),
    (4.75, '4.75%'),
    (5, '5.0%'),
    (7, '7.0%'),
)

s_descuento_dic = (
    (0, '0%'),
    (2, '2%'),
    (3, '3%'),
    (4, '4%'),
)

