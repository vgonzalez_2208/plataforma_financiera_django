from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, CreateView, TemplateView

from apps.formulario.models import Distrito, Corregimiento
from apps.ventas.forms import EmpresasForm
from apps.ventas.models import Empresa, SubSector


class Empresas(LoginRequiredMixin, TemplateView):
    template_name = 'ventas/empresas/empresas.html'

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'CargarEmpresas':
                consulta = Empresa.objects.all()
                data = list(map(info_empresa, consulta))
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Lista de Empresas'
        context['card_title_1'] = 'Empresas registradas'
        context['active_empresas'] = 'active'
        context['create_url'] = reverse_lazy('ventas:empresas_agregar')
        # context['active_ventas_nuevas'] = 'active'
        return context


class AgregarEmpresa(LoginRequiredMixin, CreateView):
    model = Empresa
    form_class = EmpresasForm
    template_name = 'ventas/empresas/formulario_empresa.html'
    success_url = reverse_lazy('ventas:empresas_lista')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'add':
                # form = CategoryForm(request.POST)
                form = self.get_form()
                if form.is_valid():
                    form.save()
                elif form.errors:
                    data['error'] = [k for k in form.errors.items()]
                    print(data)
                else:
                    data['error'] = form.errors
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_sector':
                data = []
                print(request.POST)
                for i in SubSector.objects.filter(sector=request.POST['id']):
                    data.append({'id': i.id, 'name': i.sub_sector})
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
            # data = Category.objects.get(pk=request.POST['id']).toJSON()
        except Exception as e:
            data = {}
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Lista de Empresas'
        context['card_title'] = 'Agregar empresa'
        context['active_empresas'] = 'active'
        context['action'] = 'add'
        context['list_url'] = reverse_lazy('ventas:empresas_lista')
        # context['active_ventas_nuevas'] = 'active'
        return context


class EmpresasEdit(UpdateView):
    model = Empresa
    form_class = EmpresasForm
    template_name = 'ventas/empresas/formulario_empresa.html'
    success_url = reverse_lazy('ventas:empresas_lista')

    @method_decorator(login_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'edit':
                # form = CategoryForm(request.POST)
                form = self.get_form()
                # print(form)
                if form.is_valid():
                    if form.has_changed():
                        print(form.changed_data)
                    form.save()
                elif form.errors:
                    data['error'] = [k for k in form.errors.items()]
                    print(data)
                else:
                    data['error'] = form.errors
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_sector':
                data = []
                print(request.POST)
                for i in SubSector.objects.filter(sector=request.POST['id']):
                    data.append({'id': i.id, 'name': i.sub_sector})
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
            # data = Category.objects.get(pk=request.POST['id']).toJSON()
        except Exception as e:
            data['error'] = str(e)

        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Lista de Empresas'
        context['card_title'] = 'Editar empresa'
        context['active_empresas'] = 'active'
        context['action'] = 'edit'
        context['list_url'] = reverse_lazy('ventas:empresas_lista')
        # context['active_ventas_nuevas'] = 'active'
        return context


def info_empresa(i):
    valor = {
        'id': str(i.id),
        'nombre': str(i.nombre),
        'ruc': str(i.ruc),
        'negocio': str(i.sector),
        'tipo': str(i.tipo),
        'telefono': str(i.telefono),
        'estatus': str(i.verificacion),
        'celular': str(i.celular),
    }
    return valor
