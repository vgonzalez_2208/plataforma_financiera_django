import os
from datetime import datetime, date, timedelta
from math import floor

import numpy_financial as npf
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView
from weasyprint import HTML, CSS

from apps.formulario.models import Formulario, Cotizaciones
from apps.ventas.forms import Cotizador, CotizadorP
from apps.ventas.models import PreAprobado, CotizacionPreAprobado


def vista_cotizacion(request):
    data = {
        'name': 'Vicente'
    }
    return render(request, 'ventas/cotizador/cotizacion_pdf.html', data)


class GenerarCotizacion(LoginRequiredMixin, UpdateView):
    model = Formulario
    form_class = Cotizador
    template_name = 'ventas/cotizador/cotizador_2.html'
    success_url = ''

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # nota = request.POST['nota']
            print(action)
            if action == 'cotizar':
                try:
                    if Cotizaciones.objects.get(pk=self.object.id):
                        print('existe')
                        Cotizaciones.objects.get(pk=self.object.id).delete()
                        datos = procesar_modelos(self, request.POST, Cotizaciones())
                        data = [datos]
                except Exception:
                    datos = procesar_modelos(self, request.POST, Cotizaciones())
                    data = [datos]
            elif action == 'comentar':
                data = []
                self.object.notas = request.POST['nota']
                self.object.save()
                data.append({'msg': 'Se guardó el comentario'})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Cotizador'
        context['info'] = 'Cotizacion para: ' + '' + self.object.nombre_1 + ' ' + self.object.apellido_1
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['action'] = 'cotizar'
        context['regresar'] = reverse_lazy('ventas:ver_solicitudes', kwargs={'pk': self.object.id})
        context['editar'] = reverse_lazy('ventas:editar_solicitudes', kwargs={'pk': self.object.id})
        return context


class Cotizacion(LoginRequiredMixin, View):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        modelo = Cotizaciones.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo)
        saludo = ''
        if datetime.now().hour < 12:
            saludo = "Buenos días"
        elif datetime.now().hour < 18:
            saludo = "Buenas tardes"
        else:
            saludo = "Buenas noches"
        try:
            template = get_template('ventas/cotizador/cotizacion_pdf.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'saludo': saludo,
                'nombre': modelo.nombre,
                'monto': f'{detalles_p[0]:,.2f}',
                'comision': f'{detalles_p[2]:,.2f}',
                'plazo': modelo.plazo,
                'fecha_ini': modelo.fecha_ini,
                'compra_saldo': f'{detalles_p[18]:,.2f}',
                'desembolso': f'{detalles_p[14]:,.2f}',
                'letra': f'{detalles_p[15]:,.2f}',
                'itbms': f'{detalles_p[13]:,.2f}',
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('ventas:generar_cotizacion'))


#  Liquidación de préstamo

class CondicionesPrestamo(LoginRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        modelo = CotizacionPreAprobado.objects.get(pk=self.kwargs.get('pk'))
        modelo_solicitud = modelo.cliente
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo)
        total_meses = detalles_p[3]
        letra_u = detalles_p[8] - detalles_p[15] * (int(modelo.plazo) * 2 - 1)
        g_inicial = (-1) * (
                detalles_p[0] + detalles_p[7] + detalles_p[13] + detalles_p[11] + detalles_p[9] + detalles_p[10])
        # print(g_inicial)
        flujos = []
        if int(modelo.plazo) < 12:
            print('Menor que 12')
            f_year = round(detalles_p[15] * int(modelo.plazo) * 2, 2)
            flujos = [
                g_inicial, f_year
            ]
        elif int(modelo.plazo) < 23:
            print('Menor que 23')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * (int(modelo.plazo) - 11) * 2, 2)
            flujos = [
                g_inicial, f_year, s_year
            ]
        elif int(modelo.plazo) < 34:
            print('Menor que 34')
            f_year = round(detalles_p[15] * 22, 2)
            s_year = round(detalles_p[15] * 22, 2)
            t_year = round(detalles_p[8] - f_year - s_year, 2)
            flujos = [
                g_inicial, f_year, s_year, t_year
            ]
        # print(p_val, s_val, t_val)
        #  Monto solicitado, notaría, ITBMS, timbres, c. promotor, s. descuento, otros.
        tir = round(npf.irr(flujos), 4) * 100
        # print(tir)
        seguros_mes = round((detalles_p[4] + detalles_p[5] + detalles_p[6]) / int(modelo.plazo), 2)
        aplica_promotor = ''
        as_descuento = ''
        c_promotor = round(float(modelo.c_promotor), 2)
        if c_promotor > 0:
            aplica_promotor = 'Sí'
        else:
            aplica_promotor = 'No'
        if int(modelo.s_descuento) > 0:
            as_descuento = 'Sí'
        else:
            as_descuento = 'No'
        user = request.user.first_name + ' ' + request.user.last_name
        # print(detalles_p[17])
        c_manejo = '50.00 %'
        if str(modelo.cliente.tipo_ingreso) == 'Empresa privada':
            c_manejo = '50.00 %'
        elif str(modelo.cliente.tipo_ingreso) == 'Empresa pública':
            c_manejo = '40.00 %'

        try:
            template = get_template('ventas/cotizador/detalles_prestamo.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'nombre': modelo.nombre,
                'dni': str(modelo_solicitud.dni),
                'salario': '$ ' + f'{int(modelo_solicitud.salario):,.2f}',
                't_ingreso': str(modelo_solicitud.tipo_ingreso),
                'c_promotor': f'{c_promotor:,.2f}',
                'c_manejo': c_manejo,
                's_descuento': f'{int(modelo.s_descuento):,.2f}',
                's_vida': '3.50',
                's_desempleo': '2.50',
                's_dental': '1.25',
                'ls_vida': '$ ' + f'{detalles_p[4]:,.2f}',
                'ls_desempleo': '$ ' + f'{detalles_p[5]:,.2f}',
                'ls_dental': '$ ' + f'{detalles_p[6]:,.2f}',
                'total_seguros': '$ ' + f'{(detalles_p[4] + detalles_p[5] + detalles_p[6]):,.2f}',
                'letra_q': '$ ' + f'{detalles_p[15]:,.2f}',
                'letra_m': '$ ' + f'{detalles_p[15] * 2:,.2f}',
                'letra_u': '$ ' + f'{letra_u:,.2f}',
                'tir': f'{tir:,.2f}',
                'seguros_mes': '$ ' + f'{seguros_mes:,.2f}',
                'monto': '$ ' + f'{float(modelo.monto):,.2f}',
                'plazo': modelo.plazo,
                'meses_ad': str(total_meses - int(modelo.plazo)),
                'total_meses': str(total_meses),
                'quincenas': str(int(modelo.plazo) * 2),
                'a_promotor': aplica_promotor,
                'as_descuento': as_descuento,
                'fecha_ini': detalles_p[17].strftime('%d/%m/%Y'),
                'fecha_fin': detalles_p[16].strftime('%d/%m/%Y'),
                'f_desembolso': '--',
                'comision_manejo': '$ ' + f'{detalles_p[2]:,.2f}',
                'intereses': '$ ' + f'{detalles_p[1]:,.2f}',
                'promotor': '$ ' + f'{detalles_p[9]:,.2f}',
                'servicio_desc': '$ ' + f'{detalles_p[10]:,.2f}',
                'notaria': '$ ' + f'{12:,.2f}',
                'timbres': '$ ' + f'{detalles_p[11]:,.2f}',
                'otros_gastos': '$ ' + f'{0:,.2f}',
                'total_prestamo': '$ ' + f'{detalles_p[8]:,.2f}',
                'admin': '$ ' + f'{detalles_p[12]:,.2f}',
                'itbms': '$ ' + f'{detalles_p[13]:,.2f}',
                'desembolso': '$ ' + f'{detalles_p[14]:,.2f}',
                'compra_saldo': '$ ' + f'{detalles_p[18]:,.2f}',
                'user_name': user,
            }
            if modelo.compra_saldo.isdigit():
                if float(modelo.compra_saldo) > 0:
                    context['c_saldo_1'] = 'Sin definir:'
                    context['c_sub_1'] = '$ ' + f'{float(modelo.compra_saldo):,.2f}'
                else:
                    context['c_saldo_1'] = '--Ninguna--'
            else:
                c_saldos = modelo.compra_saldo.split(',')
                val_i = len(c_saldos)
                for i in range(val_i):
                    temp = c_saldos[i].split(':')
                    context['c_saldo_' + str(i + 1)] = temp[0] + ':'
                    context['c_sub_' + str(i + 1)] = '$ ' + f'{float(temp[1]):,.2f}'
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
        return HttpResponseRedirect(reverse_lazy('ventas:solicitudes_nuevas'))


class Liquidacion(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = Cotizaciones.objects.get(pk=self.kwargs.get('pk'))
        # print(modelo.fecha_ini)
        # print(self.kwargs.get('pk'))
        date_string = datetime.strptime(modelo.fecha_ini, '%d/%m/%Y')
        fecha_encabezado = fecha_completa()
        f_inicio = fecha_inicio(date_string)
        if date.today().month == 12 and date.today().day < 15:
            dic_extra = 1
        total_meses = dic_extra + int(modelo.plazo) + meses_extra(modelo.plazo, f_inicio)
        # print(date_string)
        comision = int(modelo.monto) * 0.5
        intereses = int(modelo.monto) * total_meses * 0.02
        monto_prestamo = monto_p(modelo.monto, date_string, modelo.plazo, total_meses)
        # print(monto_prestamo)
        c_promotor = int(modelo.c_promotor) * int(modelo.monto) / 100
        s_descuento = int(modelo.s_descuento) * monto_prestamo / 100
        print(c_promotor, s_descuento)
        timbres = round(monto_prestamo / 1000 + 0.05, 1)
        admin = comision - timbres - c_promotor - s_descuento
        itbms = round(admin * 0.07, 2)
        print(itbms)
        desembolso = int(modelo.monto) - itbms - int(modelo.compra_saldo)
        letra_q = round(monto_prestamo / (int(modelo.plazo) * 2) + 0.005, 2)
        n_cuota = 0
        tabla = {
            'encabezado': ['# Mes', '# Letra', 'Fecha', 'Cuota', 'Abono intereses', 'Saldo agregado'],
        }
        rows = []
        fecha_inicial = f_inicio
        print(fecha_inicial)
        mes = 1
        while n_cuota < int(modelo.plazo):
            row = []
            s_agregado = round(monto_prestamo - letra_q * n_cuota * 2, 2)
            a_intereses = abono_intereses(int(modelo.plazo), n_cuota,
                                          intereses)  # abono_intereses(plazo, n_cuotas, t_intereses)
            if n_cuota == 0:
                fecha_inicial = datetime(f_inicio.year, (f_inicio + relativedelta(months=mes)).month, 1) - timedelta(
                    days=1)
                if fecha_inicial.month == 12:
                    row.append(str(mes))
                    row.append(str(n_cuota))  # '$ ' + f'{int(i.salario):,.2f}',
                    row.append('--')
                    row.append('--')
                    row.append('--')
                    row.append(str(s_agregado))
                    rows.append(row)
                    n_cuota -= 1
                else:
                    if f_inicio.day > 15:
                        n_cuota += 0.5
                        fecha = fecha_inicial
                        row.append(str(mes))
                        row.append(str(n_cuota))
                        row.append(str(fecha.date()))
                        row.append('$ ' + f'{letra_q:,.2f}')
                        row.append('$ ' + f'{a_intereses:,.2f}')
                        row.append('$ ' + f'{s_agregado:,.2f}')
                        rows.append(row)
                    else:
                        n_cuota += 1
                        fecha = fecha_inicial
                        row.append(str(mes))
                        row.append(str(n_cuota))
                        row.append(str(fecha.date()))
                        row.append('$ ' + f'{letra_q * 2:,.2f}')
                        row.append('$ ' + f'{a_intereses:,.2f}')
                        row.append('$ ' + f'{s_agregado:,.2f}')
                        rows.append(row)
            else:
                fecha = fecha_inicial + relativedelta(months=(mes - 1))
                if fecha.month == 12:
                    row.append(str(mes))
                    row.append(str(n_cuota))
                    row.append('--')
                    row.append('--')
                    row.append('--')
                    row.append('$ ' + f'{s_agregado:,.2f}')
                    rows.append(row)
                else:
                    if n_cuota >= (int(modelo.plazo) - 1):
                        if f_inicio.day > 15:
                            n_cuota += 0.5
                            row.append(str(mes))
                            row.append(str(n_cuota))
                            row.append(str(fecha.date()))
                            letra_final = round(round(monto_prestamo, 2) - letra_q * (int(modelo.plazo) * 2 - 1), 2)
                            row.append('$ ' + f'{letra_final:,.2f}')
                            row.append('$ ' + f'{a_intereses:,.2f}')
                            row.append('$ ' + f'{s_agregado:,.2f}')
                            rows.append(row)
                        else:
                            n_cuota += 1
                            row.append(str(mes))
                            row.append(str(n_cuota))
                            row.append(str(fecha.date()))
                            letra_final = round(monto_prestamo, 2) - letra_q * (int(modelo.plazo) * 2)
                            row.append('$ ' + f'{letra_final:,.2f}')
                            row.append('$ ' + f'{a_intereses:,.2f}')
                            row.append('$ ' + f'{s_agregado:,.2f}')
                            rows.append(row)
                    else:
                        n_cuota += 1
                        row.append(str(mes))
                        row.append(str(n_cuota))
                        row.append(str(fecha.date()))
                        row.append('$ ' + f'{letra_q * 2:,.2f}')
                        row.append('$ ' + f'{a_intereses:,.2f}')
                        row.append('$ ' + f'{s_agregado:,.2f}')
                        rows.append(row)
            mes += 1
        tabla['datos'] = rows
        # print(tabla)

        try:
            template = get_template('ventas/cotizador/liquidacion_credito.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'nombre': modelo.nombre,
                'monto': f'{int(modelo.monto):,.2f}',
                'plazo': modelo.plazo + "/" + str(total_meses),
                'fecha_ini': modelo.fecha_ini,
                'compra_saldo': f'{int(modelo.compra_saldo):,.2f}',
                'desembolso': f'{desembolso:,.2f}',
                'letra': f'{letra_q:,.2f}',
                'itbms': f'{itbms:,.2f}',
                'tabla': tabla
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('ventas:generar_cotizacion'))


class GenerarCotizacionP(LoginRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = CotizadorP
    template_name = 'ventas/cotizador/cotizador_2.html'
    success_url = ''

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # nota = request.POST['nota']
            print(action)
            if action == 'cotizar_p':
                try:
                    if CotizacionPreAprobado.objects.get(pk=self.object.id):
                        print('existe')
                        CotizacionPreAprobado.objects.get(pk=self.object.id).delete()
                        datos = procesar_modelos(self, request.POST, CotizacionPreAprobado())
                        data = [datos]
                        print(data)
                except Exception:
                    datos = procesar_modelos(self, request.POST, CotizacionPreAprobado())
                    data = [datos]
                    print(data)
            elif action == 'comentar':
                data = []
                self.object.notas = request.POST['nota']
                self.object.save()
                data.append({'msg': 'Se guardó el comentario'})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Cotizador'
        context['info'] = 'Cotizacion para: ' + '' + self.object.nombre_1 + ' ' + self.object.apellido_1
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['action'] = 'cotizar_p'
        if self.kwargs.get('tipo'):
            context['regresar'] = reverse_lazy('ventas:ver_pre-aprobada', kwargs={'pk': self.object.id})
        else:
            context['regresar'] = reverse_lazy('ventas:ver_solicitudes_proceso', kwargs={'pk': self.object.id})
        context['editar'] = reverse_lazy('ventas:editar_solicitudes_proceso', kwargs={'pk': self.object.id})
        return context


def procesar_modelos(modelo_cliente, post, cot_model):
    data = ''
    # file_pdf = CotizacionPreAprobado()
    file_pdf = cot_model
    file_pdf.id = modelo_cliente.object.id
    file_pdf.cliente = modelo_cliente.object  # datos = procesar_modelos(self, request.POST, Cotizaciones())
    file_pdf.nombre = modelo_cliente.object.nombre_1 + ' ' + modelo_cliente.object.apellido_1
    file_pdf.monto = post['monto']
    file_pdf.plazo = post['plazo_meses']
    file_pdf.fecha_ini = post['inicio_pagos']
    file_pdf.compra_saldo = post['compra_saldo']
    file_pdf.c_promotor = post['c_promotor']
    file_pdf.s_descuento = post['s_descuento']
    modelo_cliente.object.monto = file_pdf.monto
    modelo_cliente.object.plazo_meses = file_pdf.plazo
    modelo_cliente.object.inicio_pago = file_pdf.fecha_ini
    file_pdf.save()
    modelo_cliente.object.save()
    if post['detalle_val'] == '0':
        data = 'cotizacion'
    elif post['detalle_val'] == '1':
        data = 'detalles'
    return data


class CotizacionPre(View):
    def get(self, request, *args, **kwargs):
        dic_extra = 0
        modelo = CotizacionPreAprobado.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_credito(modelo)
        saludo = ''
        if datetime.now().hour < 12:
            saludo = "Buenos días"
        elif datetime.now().hour < 18:
            saludo = "Buenas tardes"
        else:
            saludo = "Buenas noches"
        try:
            template = get_template('ventas/cotizador/cotizacion_pdf.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'saludo': saludo,
                'nombre': modelo.nombre,
                'monto': f'{detalles_p[0]:,.2f}',
                'comision': f'{detalles_p[2]:,.2f}',
                'plazo': modelo.plazo,
                'fecha_ini': modelo.fecha_ini,
                'compra_saldo': f'{detalles_p[18]:,.2f}',
                'desembolso': f'{detalles_p[14]:,.2f}',
                'letra': f'{detalles_p[15]:,.2f}',
                'itbms': f'{detalles_p[13]:,.2f}',
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('ventas:generar_cotizacionP'))


# response = HttpResponse(content_type='application/pdf')

# Funciones de cotizador

def monto_p(monto, f_ini, meses, total_meses, comision):
    if f_ini.month == 12:
        f_ini = date(f_ini.year + 1, 1, 15)
    int_mensual = 0.02
    int_total = float(monto) * int_mensual * total_meses
    monto_prestamo = round(float(monto) + comision + int_total + float(monto) * (
            0.035 + 0.025) * total_meses / 12 + total_meses * 1.25 + 12, 2)

    return monto_prestamo


def last_day_of_month(fecha):
    if fecha.month == 12:
        return fecha.replace(day=31)
    return fecha.replace(month=fecha.month + 1, day=1) - timedelta(days=1)


def meses_extra(plazo, f_ini):
    plazo = int(plazo)
    if f_ini.day > 15:
        ajuste = 0.5
    else:
        ajuste = 0
    mes_extra = floor(((f_ini.month + plazo - 1 + ajuste) / 12 + plazo + f_ini.month - 1 + ajuste) / 12)
    return mes_extra


def fecha_inicio(f_ini):
    if f_ini.month == 12:
        f_ini = date(f_ini.year + 1, 1, 15)
    else:
        f_ini = f_ini
    return f_ini


def fecha_completa(flag=0):
    months = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
              "Noviembre", "Diciembre")
    dia_semana = ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo")
    if flag == 0:
        dia_s = dia_semana[datetime.today().weekday()]
        day = datetime.now().day
        month = months[datetime.now().month - 1]
        year = datetime.now().year
        fecha = "{}, {} de {} de {}".format(dia_s, day, month, year)
    else:
        dia_s = dia_semana[flag.weekday()]
        day = flag.day
        month = months[flag.month - 1]
        year = flag.year
        fecha = "{}, {} de {} de {}".format(dia_s, day, month, year)
    return fecha


#  Funciones del liquidador de préstamo

def abono_intereses(plazo, n_cuotas, t_intereses):
    meses_f = plazo - n_cuotas
    ajuste_meses = meses_f + 1
    if n_cuotas > 0:
        a_anterior = (ajuste_meses ** 2 + ajuste_meses) * t_intereses / (plazo ** 2 + plazo)
        a_intereses = (meses_f ** 2 + meses_f) * t_intereses / (plazo ** 2 + plazo)
        a_intereses = a_anterior - a_intereses
    else:
        a_intereses = 0
    print(meses_f, a_intereses)
    resultado = t_intereses - a_intereses
    return a_intereses


#  Función global

#  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
#  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
#  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])

def desglose_credito(modelo, aprobado='no'):
    #  def(monto, intereses, comision, total_meses, s_colectivo, s_desempleo, s_dental, notaria, m_prestamo, c_promotor,
    #  s_descuento, timbres, admin, itbms, desembolso, letra_q, fecha_fin, fecha_ini)
    salida = []
    salida.append(float(modelo.monto))
    dic_extra = 0
    if int(modelo.fecha_ini.split('/')[0]) > 1000:
        date_string = datetime.strptime(modelo.fecha_ini, '%Y/%m/%d')
    else:
        date_string = datetime.strptime(modelo.fecha_ini, '%d/%m/%Y')
    f_inicio = fecha_inicio(date_string)
    if f_inicio.day <= 15:
        f_inicio = f_inicio.replace(day=15)
    elif f_inicio.month == 2:
        f_inicio = datetime(f_inicio.year, f_inicio.month + 1, 1) - timedelta(days=1)
    elif f_inicio.day > 15:
        f_inicio = f_inicio.replace(day=30)
    if aprobado == 'si':
        if f_inicio.month == 12 and f_inicio.day < 15:
            dic_extra = 1
    else:
        if date.today().month == 12 and date.today().day < 15:
            dic_extra = 1
    total_meses = dic_extra + int(modelo.plazo) + meses_extra(modelo.plazo, f_inicio)
    intereses = float(modelo.monto) * total_meses * 0.02
    salida.append(intereses)
    if aprobado == 'si':
        if modelo.c_cierre:
            comision = float(modelo.monto) * float(modelo.c_cierre) / 100
            p_comision = modelo.c_cierre
        else:
            if str(modelo.cliente.tipo_ingreso) == 'Empresa privada':
                comision = float(modelo.monto) * 0.5
                p_comision = '50'
            else:
                comision = float(modelo.monto) * 0.4
                p_comision = '40'
    else:
        if str(modelo.cliente.tipo_ingreso) == 'Empresa privada':
            comision = float(modelo.monto) * 0.5
            p_comision = '50'
        else:
            comision = float(modelo.monto) * 0.4
            p_comision = '40'
    salida.append(comision)
    salida.append(total_meses)
    s_colectivo = float(modelo.monto) * 0.025 * total_meses / 12
    s_desempleo = float(modelo.monto) * 0.035 * total_meses / 12
    s_dental = 1.25 * total_meses
    salida.append(s_colectivo)
    salida.append(s_desempleo)
    salida.append(s_dental)
    notaria = 12
    salida.append(notaria)
    # print(date_string)
    monto_prestamo = monto_p(modelo.monto, date_string, modelo.plazo, total_meses, comision)
    salida.append(monto_prestamo)
    # print(monto_prestamo)
    c_promotor = round(float(modelo.c_promotor) * float(modelo.monto) / 100, 2)
    salida.append(c_promotor)
    s_descuento = round(int(modelo.s_descuento) * monto_prestamo / 100, 2)
    salida.append(s_descuento)
    # print(c_promotor, s_descuento)
    timbres = round(monto_prestamo / 1000 + 0.05, 1)
    salida.append(timbres)
    admin = comision - timbres - c_promotor - s_descuento
    salida.append(admin)
    itbms = round(admin * 0.07, 2)
    salida.append(itbms)
    compra_saldo_total = 0
    if modelo.compra_saldo:
        if modelo.compra_saldo.isdigit():
            compra_saldo_total = float(modelo.compra_saldo)
        else:
            for i in modelo.compra_saldo.split(','):
                compra_saldo_total += float(i.split(':')[1])
    desembolso = float(modelo.monto) - itbms - compra_saldo_total
    salida.append(desembolso)
    letra_q = round(monto_prestamo / (int(modelo.plazo) * 2) + 0.005, 2)
    salida.append(letra_q)
    f_ini = 0
    if f_inicio.day == 15:
        f_ini = f_inicio - timedelta(days=15)
    elif f_inicio.day > 15:
        f_ini = f_inicio.replace(day=15)
    fecha_fin = f_ini + relativedelta(months=total_meses)
    if fecha_fin.day == 31:
        fecha_fin = fecha_fin.replace(day=30)
    salida.append(fecha_fin)
    salida.append(f_inicio)
    salida.append(compra_saldo_total)
    salida.append(p_comision)
    # print(str(len(salida)) + ' helou')
    return salida
