from datetime import datetime, timedelta
from functools import reduce
from time import strftime

from apps.cobros.models import Desembolso
from apps.cobros.views.cobros.functions import to_num
from apps.prestamos.models import Aprobado
from apps.ventas.models import Empresa, PreAprobado


def reg_empresas_pendientes():
    salida = []
    salida_2 = []
    consulta_1 = Aprobado.objects.filter(cliente__empresa_2=None).filter(num_contrato__isnull=False).exclude(
        num_contrato__exact='')
    for obj in consulta_1:
        salida.append([obj.cliente.empresa_1, obj.num_contrato, obj.id, obj.cliente.id])
    consulta_2 = Empresa.objects.all().only('nombre', 'r_social')
    dic_empresas = {}
    for obj in consulta_2:
        dic_empresas[obj.nombre] = obj.r_social
    # print(dic_empresas)
    # print(salida)
    # print(len(salida))
    # for i in range(0, len(salida)):
    #     # print(i)
    #     # print(salida[i][0])
    #     if not dic_empresas.get(salida[i][0]):
    #         salida_2.append(salida[i])
    # print(len(salida_2))
    return salida


def info_inversor(user):
    lista_cifras = ('gerencia.rc', 'guillermo.lemos', 'vicente_rc', 'carlos.gomez')
    if user in lista_cifras:
        hoy = datetime.now().date()
        mes_ant = hoy - timedelta(days=hoy.day)
        info_char = hoy.strftime('%Y-%m')
        info_char_ant = mes_ant.strftime('%Y-%m')
        ap_actual = Aprobado.objects.filter(creado_el__year=hoy.year, creado_el__month=hoy.month).only('monto', 'plazo')
        ap_anterior = Aprobado.objects.filter(creado_el__year=mes_ant.year, creado_el__month=mes_ant.month).only('monto', 'plazo')
        ct_actual = Aprobado.objects.filter(num_contrato__startswith=info_char).exclude(estatus='Anulado').only('monto', 'plazo')
        ct_anterior = Aprobado.objects.filter(num_contrato__startswith=info_char_ant).exclude(estatus='Anulado').only('monto', 'plazo')
        # Datos actuales
        monto_ap, monto_ct = info_resumida(ap_actual, ct_actual)
        # Datos anteriores
        monto_ap_ant, monto_ct_ant = info_resumida(ap_anterior, ct_anterior)
        # Pendientes de Pendientes Gerencia
        pend_gerencia = info_gerencia()
        salida = {
            'aprobaciones_total': len(ap_actual),
            'suma_monto_aprobados': monto_ap[0],
            'monto_pr': monto_ap[1],
            'plazo_pr': monto_ap[2],
            'contratos_total': len(ct_actual),
            'suma_monto_contratos': monto_ct[0],
            'ct_monto_pr': monto_ct[1],
            'ct_plazo_pr': monto_ct[2],
            'aprobaciones_total_ant': len(ap_anterior),
            'suma_monto_aprobados_ant': monto_ap_ant[0],
            'monto_pr_ant': monto_ap_ant[1],
            'plazo_pr_ant': monto_ap_ant[2],
            'contratos_total_ant': len(ct_anterior),
            'suma_monto_contratos_ant': monto_ct_ant[0],
            'ct_monto_pr_ant': monto_ct_ant[1],
            'ct_plazo_pr_ant': monto_ct_ant[2],
            'info_gerencia': pend_gerencia
        }
        return salida


def info_resumida(ap, ct):
    monto_plazo_ap = list(map(lambda x: (to_num(x.monto), to_num(x.plazo)), ap))
    monto_plazo_ct = list(map(lambda x: (to_num(x.monto), to_num(x.plazo)), ct))
    # print(monto_plazo_ct, monto_plazo_ap)
    monto_ap = salida_info_resumida(ap, monto_plazo_ap)
    monto_ct = salida_info_resumida(ct, monto_plazo_ct)
    return monto_ap, monto_ct


def salida_info_resumida(model, lista):
    if lista:
        salida = list(reduce(suma_consulta, lista))
        salida = [salida[0], round(salida[0] / len(model), 2), round(salida[1] / len(model), 1)]
    else:
        salida = [0, round(0, 2), round(0, 1)]
    return salida


def suma_consulta(val_1, val_2):
    salida = [
        val_1[0] + val_2[0],
        val_1[1] + val_2[1]
    ]
    return salida


def total_montos(modelo):
    suma = 0
    monto_pr = 0
    plazo_pr = 0
    if len(modelo) > 0:
        for obj in modelo:
            suma = round(suma + float(obj.monto), 2)
            monto_pr = round(monto_pr + float(obj.monto), 2)
            plazo_pr = round(plazo_pr + float(obj.plazo), 2)
        monto_pr = round(monto_pr / len(modelo), 2)
        plazo_pr = round(plazo_pr / len(modelo), 1)
        return [suma, monto_pr, plazo_pr]
    else:
        return [suma, monto_pr, plazo_pr]


def check_dash_admin(consulta):
    check_list = ('reg_desembolso', 'pend_desembolso', 'env_desembolso')
    if consulta['operaciones'] in check_list:
        return True
    else:
        return False


def info_admin(consulta):
    if consulta['operaciones'] == 'reg_desembolso':
        return ['num_registro', consulta['count']]
    elif consulta['operaciones'] == 'pend_desembolso':
        return ['num_revision', consulta['count']]
    elif consulta['operaciones'] == 'env_desembolso':
        return ['num_enviadas', consulta['count']]


def info_total_od(modelo):
    len_od = len(modelo)
    suma_temp = list(map(lambda x: to_num(x.monto), modelo))
    suma = reduce(lambda x, y: x + y, suma_temp)
    return str(len_od) + ' / ' + to_num(suma, True)


def info_gerencia():
    consulta_1 = Aprobado.objects.filter(estatus='env_desembolso').exclude(num_contrato__isnull=True).count()
    consulta_2 = PreAprobado.objects.filter(check_1=1).filter(check_3=1).filter(check_2=0).exclude(check_cumplimiento='true').exclude(check_cumplimiento='Aprobado').exclude(check_cumplimiento='Rechazado').exclude(check_cumplimiento='Revisado').count()
    estatus = ('Anulado', 'Desembolsado')
    desembolsos = Desembolso.objects.filter(cliente__isnull=False).values_list('cliente_id', flat=True)
    consulta_3 = Aprobado.objects.exclude(estatus__in=estatus).exclude(cliente__in=desembolsos)
    consulta_4 = Empresa.objects.filter(verificacion=False).count()
    salida = {
        'aprobaciones': consulta_1,
        'preaprobaciones': consulta_2,
        'OD': info_total_od(consulta_3),
        'empresas': consulta_4
    }
    return salida
