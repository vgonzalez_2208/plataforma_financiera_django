from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django.http import JsonResponse
from django.views.generic import TemplateView

from apps.cobros.models import ClienteAntiguo
from apps.formulario.models import Formulario
from apps.ventas.models import CotizacionLigera
from apps.ventas.views.dashboard.dashboard_functions import *


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard.html'

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        post = request.POST
        action = post['action']
        if action == 'cargar_resumen':
            data['info'] = 'test'
            info_user = post['info_user']
            if info_user == 'gerencia.rc':
                consulta_1 = Aprobado.objects.filter(estatus='env_desembolso').exclude(num_contrato__isnull=True).count()
                consulta_2 = PreAprobado.objects.filter(check_1=1).filter(check_3=1).filter(check_2=0).exclude(
                    check_cumplimiento='true').exclude(check_cumplimiento='Aprobado').exclude(
                    check_cumplimiento='Rechazado').exclude(check_cumplimiento='Revisado').count()
                data['aprobaciones'] = consulta_1
                data['preaprobaciones'] = consulta_2
            elif info_user in ('guillermo.lemos', 'vicente_rc'):
                consulta_1 = PreAprobado.objects.filter(check_1=1).filter(check_3=1).filter(check_2=0).exclude(
                    check_cumplimiento='true').exclude(check_cumplimiento='Aprobado').exclude(
                    check_cumplimiento='Rechazado').exclude(check_cumplimiento='Revisado').count()
                consulta_2 = PreAprobado.objects.filter(check_1=1).filter(check_3=0).filter(check_2=0).count()
                consulta_5 = ClienteAntiguo.objects.all()
                data['aprobaciones'] = consulta_1
                data['preaprobaciones'] = consulta_2
                # Info de Ordenes de descuento
                data['reg_empresas'] = reg_empresas_ord(consulta_5)
                data['empresas_faltantes'] = reg_empresas_pendientes()
                # Info de operaciones pendientes administración
                consulta_admin = Aprobado.objects.values('operaciones').annotate(count=Count('operaciones'))
                info = list(filter(check_dash_admin, consulta_admin))
                data['info_admin'] = dict(map(info_admin, info))
            estatus = ('Anulado', 'Desembolsado')
            desembolsos = Desembolso.objects.filter(cliente__isnull=False).values_list('cliente_id', flat=True)
            consulta_3 = Aprobado.objects.exclude(estatus__in=estatus).exclude(cliente__in=desembolsos)
            consulta_4 = Empresa.objects.filter(verificacion=False).count()
            data['OD'] = info_total_od(consulta_3)
            data['empresas'] = consulta_4
            data['cifras_rc'] = info_inversor(post['info_user'])
        elif action == 'dashboard_inversor':
            inversor_list = ('carlos.gomez',)
            if request.user.username in inversor_list:
                data = info_inversor(post['info_user'])
        elif action == 'info_call-center':
            # print(post)
            data['empresas_faltantes'] = reg_empresas_pendientes()
        elif action == 'admin_dash':
            if request.user.username == 'jeison.gonzalez':
                consulta = Aprobado.objects.values('operaciones').annotate(count=Count('operaciones'))
                # consulta = Aprobado.objects.all().annotate(Count('operaciones', distinct=True))
                # print(consulta)
                info = list(filter(check_dash_admin, consulta))
                data = dict(map(info_admin, info))
        else:
            data['error'] = 'No ha ingresado a ninguna opción.'
        # print(post)
        print(action)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        solicitudes = conteo_solicitudes()
        aprobaciones = info_aprobaciones()
        mensaje = saludo()
        context['titulo_p'] = mensaje
        context['solicitudes'] = str(solicitudes[0])
        context['solicitudes_nuevas'] = str(solicitudes[1])
        context['solicitudes_vistas'] = str(solicitudes[2])
        context['solicitudes_mes_actual'] = str(solicitudes[3])
        context['solicitudes_mes_anterior'] = str(solicitudes[4])
        context['solicitudes_descartadas'] = str(solicitudes[5])
        context['solicitudes_procesadas_descartadas'] = str(solicitudes[6])
        context['total_descartadas'] = str(solicitudes[7])
        context['suma_actual'] = '$ ' + f'{float(solicitudes[8] + solicitudes[9] + solicitudes[10]):,.2f}'
        context['suma_anterior'] = '$ ' + f'{float(solicitudes[11] + solicitudes[12] + solicitudes[13]):,.2f}'
        # Info de aprobaciones
        context['aprobaciones_mes_anterior'] = aprobaciones[0]
        context['aprobaciones_mes_actual'] = aprobaciones[2]
        context['suma_aprobaciones_anterior'] = '$ ' + f'{aprobaciones[1]:,.2f}'
        context['suma_aprobaciones_actual'] = '$ ' + f'{aprobaciones[3]:,.2f}'
        # Flags varios
        context['lista_resumen'] = ('vicente_rc', 'gerencia.rc', 'guillermo.lemos')
        context['lista_ventas'] = ('catalino.camaño', 'ariel.reina', 'jeimy.diaz')
        return context


def saludo():
    now = datetime.now()
    if now.hour < 6:
        mensaje = '¡El que madruga Dios le ayuda! '
    elif now.hour < 12:
        mensaje = '¡Buenos días! '
    elif now.hour < 18:
        mensaje = '¡Buenas tardes! '
    else:
        mensaje = '¡Buenas noches! '
    # print(now.hour)
    return mensaje


def conteo_solicitudes():
    modelo = Formulario.objects
    modelo_procesado = PreAprobado.objects
    modelo_ligeras = CotizacionLigera.objects
    conteo_ligeras = modelo_ligeras.all().count()
    solicitudes = modelo.all().count() + modelo_procesado.all().count() + conteo_ligeras
    solicitudes_nuevas = modelo.filter(user_view=None).count()
    solicitudes_descartadas = modelo.filter(check_3=True).count()
    pre_aprobadas_descartadas = modelo_procesado.filter(check_2=True).count()
    ligeras_descartadas = modelo_ligeras.filter(estatus='Descartada').count()
    descartadas = solicitudes_descartadas + pre_aprobadas_descartadas + ligeras_descartadas
    mes_actual = datetime.now().month
    mes_anterior = (datetime.now() - timedelta(days=datetime.now().day)).month
    solicitudes_mes_actual = modelo.filter(creado_el__month=mes_actual).count()
    solicitudes_mes_actual += modelo_procesado.filter(creado_el__month=mes_actual).count()
    solicitudes_mes_actual += modelo_ligeras.filter(creado_el__month=mes_actual).count()
    solicitudes_mes_anterior = modelo.filter(creado_el__month=mes_anterior).count()
    solicitudes_mes_anterior += modelo_procesado.filter(creado_el__month=mes_anterior).count()
    solicitudes_mes_anterior += modelo_ligeras.filter(creado_el__month=mes_anterior).count()
    suma_solicitudes_1 = sumar_montos(Formulario, mes_actual)
    suma_procesados_1 = sumar_montos(PreAprobado, mes_actual)
    suma_ligeras_1 = sumar_montos(CotizacionLigera, mes_actual)
    suma_solicitudes_2 = sumar_montos(Formulario, mes_anterior)
    suma_procesados_2 = sumar_montos(PreAprobado, mes_anterior)
    suma_ligeras_2 = sumar_montos(CotizacionLigera, mes_anterior)
    # print(solicitudes_mes_actual, solicitudes_mes_anterior)
    solicitudes_vistas = solicitudes - solicitudes_nuevas
    conteo = [solicitudes, solicitudes_nuevas, solicitudes_vistas, solicitudes_mes_actual, solicitudes_mes_anterior,
              solicitudes_descartadas, pre_aprobadas_descartadas, descartadas, suma_solicitudes_1, suma_procesados_1,
              suma_ligeras_1, suma_solicitudes_2, suma_procesados_2, suma_ligeras_2]
    return conteo
    #  print(solicitudes, solicitudes_nuevas, solicitudes_vistas)


def sumar_montos(mod_1, mes):
    consulta_1 = mod_1.objects.filter(creado_el__month=mes).only('monto')
    suma = 0
    for i in consulta_1:
        suma += float(i.monto)
    return round(suma, 2)


def info_aprobaciones():
    mes_actual = datetime.now().month
    mes_anterior = (datetime.now() - timedelta(days=datetime.now().day)).month
    # print(mes_anterior, mes_actual)
    consulta_1 = Aprobado.objects.filter(creado_el__month=10).only('monto')
    consulta_2 = Aprobado.objects.filter(creado_el__month=mes_actual).only('monto')
    anterior = len(consulta_1)
    actual = len(consulta_2)
    anterior_total = 0
    actual_total = 0
    for i in consulta_1:
        anterior_total += float(i.monto)
    for j in consulta_2:
        actual_total += float(j.monto)
    anterior_total = round(anterior_total, 2)
    actual_total = round(actual_total, 2)
    salida = [anterior, anterior_total, actual, actual_total]
    # print(salida)
    return salida


def reg_empresas_ord(consulta):
    salida = {}
    for obj in consulta:
        if salida.get(str(obj.fecha_ingreso.date())):
            salida[str(obj.fecha_ingreso.date())] += 1
        else:
            salida[str(obj.fecha_ingreso.date())] = 1
    # print(salida)
    return salida
