import threading
from datetime import datetime, timezone, timedelta

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView, CreateView, TemplateView

from apps.formulario.models import Formulario, Distrito, Corregimiento
from apps.ventas.forms import EditSolicitud, CrearSolicitud, VerSolicitudesN
from apps.ventas.models import PreAprobado, CotizacionLigera, TablaCallCenter


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class SolicitudesNuevas(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'ventas/nuevas/1_solicitudes.html'

    def has_permission(self):
        perm = ('Ventas', 'Ventas_2', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, *args, **kwargs):
        # time = datetime.now()
        data = {}
        try:
            action = request.POST['action']
            #  print(action)
            if action == 'CargarTabla_1':
                data = []
                consulta = Formulario.objects.filter(check_1=0).filter(check_2=0).filter(check_3=0)
                if request.user.id == 7:  # ID de Jeimy Díaz o vendedora de turno asignada
                    fecha_hoy = datetime.now(timezone.utc)
                    for i in consulta:
                        dias = (fecha_hoy - i.fecha_ingreso).days
                        if dias >= 2:
                            data = tabla_ventas(i, data)
                else:
                    # for i in consulta:
                    #     data = tabla_ventas(i, data)
                    data = list(map(tabla_ventas_2, consulta))
                # print(data)
            elif action == "CargarTabla_2":
                user_id = request.user.id
                if user_id == 8:
                    lista_call_center = ('Dennis Caballero', 'Ariel Reina')
                elif user_id == 14:
                    lista_call_center = ('Catalino Camaño',)
                else: lista_call_center = ('Jeimy Diaz',)
                # usuario = request.user.first_name + ' ' + request.user.last_name
                consulta = Formulario.objects.filter(user_view__in=lista_call_center).filter(check_3=0)
                data = list(map(ventas_tabla, consulta))
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data = {}
            data['error'] = str(e)
            print(data)
        # print(datetime.now() - time)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['card_title_1'] = 'Nuevas solicitudes'
        context['card_title_2'] = 'Solicitudes revisadas'
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['lista_ventas'] = (7, 8, 14)
        context['create_url'] = reverse_lazy('ventas:solicitudes_agregar')
        return context


class EdicionSolicitud(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Formulario
    form_class = EditSolicitud
    template_name = 'ventas/nuevas/1_solicitudes_rev.html'
    success_url = reverse_lazy('ventas:solicitudes_nuevas')

    def has_permission(self):
        perm = ('Ventas', 'Ventas_2', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        set_user_solicitud(self.object, request.user)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            # print(action)
            if action == 'edit':
                # form = CategoryForm(request.POST)
                form = self.get_form()
                if form.errors:
                    data['error'] = [k for k in form.errors.items()]
                    # print(data)
                if form.is_valid:
                    check_2 = request.POST['check_2']
                    check_3 = request.POST['check_3']
                    # print(self.object.DNI_img, self.object.ficha_img)
                    if check_3 == 'on':  # Se descarta la solicitud
                        data['vista_lista'] = ''
                        form.save()
                    elif check_2 == 'on':
                        # print(self.object.nombre_1)
                        form.save()
                        username = request.user.id
                        if username == 14:
                            consulta = TablaCallCenter.objects.get(pk=1)
                            if consulta.last_process == 'jeimy.diaz':
                                vendedor = 'C.C./ Ariel Reina'
                                consulta.last_process = 'ariel.reina'
                                consulta.save()
                            else:
                                vendedor = 'C.C./ Jeimy Diaz'
                                consulta.last_process = 'jeimy.diaz'
                                consulta.save()
                        else:
                            vendedor = self.object.user_view
                        x = threading.Thread(target=proceso_solicitud, args=(self.object, vendedor,))
                        x.start()
                        data['vista_lista'] = ''
                    else:
                        # print('sin descarte')
                        form.save()
                        data['vista_edit'] = ''
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Editar solicitud'
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['action'] = 'edit'
        context['retorno_1'] = reverse_lazy('ventas:solicitudes_nuevas')
        return context


class SolicitudesAgregar(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Formulario
    form_class = CrearSolicitud
    template_name = 'ventas/nuevas/1_solicitudes_nueva.html'
    success_url = reverse_lazy('ventas:solicitudes_nuevas')

    def has_permission(self):
        perm = ('Ventas', 'Ventas_2', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                # form = CategoryForm(request.POST)
                form = self.get_form()
                if form.is_valid():
                    if self.kwargs.get('class'):
                        consulta = CotizacionLigera.objects.get(pk=self.kwargs['class'])
                        pre_save = form.save(commit=False)
                        if consulta.user_view:
                            pre_save.user_view = consulta.user_view
                        else:
                            if request.user.username in ('jeimy.diaz', 'ariel.reina'):
                                pre_save.user_view = request.user.first_name + ' ' + request.user.last_name
                        if consulta.notas:
                            pre_save.notas = str(consulta.notas) + '\n'
                        else:
                            pre_save.notas = '---\n'
                        if consulta.cancelaciones:
                            pre_save.notas += str(consulta.cancelaciones) + '\n'
                        if consulta.c_promotor:
                            pre_save.notas += str(consulta.c_promotor) + '% promotor.\n'
                        if consulta.s_descuento:
                            pre_save.notas += str(consulta.s_descuento) + '% s. de descuento.\n'
                        consulta.delete()
                        pre_save.save()
                    else:
                        if request.user.username in ('jeimy.diaz', 'ariel.reina'):
                            pre_save = form.save(commit=False)
                            pre_save.user_view = request.user.first_name + ' ' + request.user.last_name
                            pre_save.save()
                        else:
                            form.save()
                else:
                    print(form.errors)
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        if self.kwargs.get('class'):
            context['ampliar_solicitud'] = 'ok'
            consulta = CotizacionLigera.objects.get(pk=self.kwargs['class'])
            context['info_recuperada'] = {
                'nombre_1': str(consulta.nombre_1),
                'nombre_2': str(consulta.nombre_2),
                'apellido_1': str(consulta.apellido_1),
                'apellido_2': str(consulta.apellido_2),
                'tipo_documento': str(consulta.tipo_documento_id),
                'dni': str(consulta.dni),
                'email_1': str(consulta.email_1),
                'celular': str(consulta.celular),
                'monto': str(consulta.monto),
                'plazo_meses': str(consulta.plazo_meses),
                'tipo_ingreso': str(consulta.tipo_ingreso_id),
                'salario': str(consulta.salario),
                'promotor': str(consulta.promotor),
                'notas': str(consulta.notas)
            }
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Nueva solicitud'
        context['active_ventas'] = 'active'
        context['action'] = 'add'
        context['active_ventas_nuevas'] = 'active'
        context['list_url'] = reverse_lazy('ventas:solicitudes_nuevas')
        return context


class VerSolicitudesNuevas(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Formulario
    form_class = VerSolicitudesN
    template_name = 'ventas/nuevas/1_solicitudes_nuevas_ver.html'
    success_url = reverse_lazy('ventas:solicitudes_nuevas')

    def has_permission(self):
        perm = ('Ventas', 'Ventas_2', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        set_user_solicitud(self.object, request.user)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            nota = request.POST['nota']
            print(action, nota)
            if action == 'comentar':
                data = []
                self.object.notas = request.POST['nota']
                self.object.save()
                data.append({'msg': 'Se guardó el comentario'})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Información del cliente'
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['action'] = 'edit'
        context['editar'] = reverse_lazy('ventas:editar_solicitudes', kwargs={'pk': self.object.id})
        context['cotizar'] = reverse_lazy('ventas:generar_cotizacion', kwargs={'pk': self.object.id})
        return context


def proceso_solicitud(self_object, vendedor):
    proceso = PreAprobado()
    proceso.monto = self_object.monto
    proceso.creado_el = self_object.creado_el
    proceso.plazo_meses = self_object.plazo_meses
    proceso.nombre_1 = self_object.nombre_1
    proceso.nombre_2 = self_object.nombre_2
    proceso.apellido_1 = self_object.apellido_1
    proceso.apellido_2 = self_object.apellido_2
    proceso.tipo_doc = self_object.tipo_doc
    proceso.dni = self_object.dni
    proceso.f_nacimiento = self_object.f_nacimiento
    proceso.email_1 = self_object.email_1
    proceso.celular = self_object.celular
    proceso.e_civil = self_object.e_civil
    proceso.genero = self_object.genero
    proceso.dependientes = self_object.dependientes
    proceso.tipo_ingreso = self_object.tipo_ingreso
    proceso.salario = self_object.salario
    proceso.empresa_1 = self_object.empresa_1
    proceso.in_extras = self_object.in_extras
    proceso.estudios = self_object.estudios
    proceso.p_expuesto = self_object.p_expuesto
    proceso.provincia = self_object.provincia
    proceso.distrito = self_object.distrito
    proceso.corregimiento = self_object.corregimiento
    proceso.direccion = self_object.direccion
    proceso.ref = self_object.ref
    proceso.terms = self_object.terms
    proceso.apc = self_object.apc
    proceso.DNI_img = self_object.DNI_img
    proceso.ficha_img = self_object.ficha_img
    proceso.user_view = vendedor
    proceso.motivo = self_object.motivo
    proceso.notas = self_object.notas
    proceso.inicio_pago = self_object.inicio_pago
    proceso.save()
    self_object.delete()
    # print('procesado', self_object.user_view)


def tabla_ventas(i, data):
    if i.user_view:
        visto = 'fue_visto'
    else:
        visto = 'no_visto'
    if i.DNI_img and i.ficha_img:
        documentos = "Sí"
    elif i.DNI_img and not i.ficha_img:
        documentos = "Sólo cédula"
    elif not i.DNI_img and i.ficha_img:
        documentos = "Sólo ficha"
    else:
        documentos = "No"
    fecha_s = i.fecha_ingreso.date()
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        'f_solicitud': fecha_s.strftime("%Y-%m"),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': '$ ' + f'{float(i.salario):,.2f}',
        'celular': str(i.celular),
        'documentos': documentos,
        'genero': str(i.genero),
        'visto': visto,
    }
    # valor['f_solicitud'] = i.fecha_ingreso
    data.append(valor)
    return data


def tabla_ventas_2(i):
    if i.user_view:
        visto = 'fue_visto'
    else:
        visto = 'no_visto'
    if i.DNI_img and i.ficha_img:
        documentos = "Sí"
    elif i.DNI_img and not i.ficha_img:
        documentos = "Sólo cédula"
    elif not i.DNI_img and i.ficha_img:
        documentos = "Sólo ficha"
    else:
        documentos = "No"
    fecha_s = i.fecha_ingreso.date()
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        'f_solicitud': fecha_s.strftime("%Y-%m"),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': '$ ' + f'{float(i.salario):,.2f}',
        'celular': str(i.celular),
        'documentos': documentos,
        'genero': str(i.genero),
        'visto': visto,
    }
    # valor['f_solicitud'] = i.fecha_ingreso
    return valor


def ventas_tabla(i):
    if i.DNI_img and i.ficha_img:
        documentos = "Sí"
    elif i.DNI_img and not i.ficha_img:
        documentos = "Sólo cédula"
    elif not i.DNI_img and i.ficha_img:
        documentos = "Sólo ficha"
    else:
        documentos = "No"
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': '$ ' + f'{int(i.salario):,.2f}',
        'celular': str(i.celular),
        'documentos': documentos,
        'genero': str(i.genero),
    }
    return valor


def set_user_solicitud(modelo, usuario):
    l_ventas = (7, 8, 14)  # IDs de departamento de ventas RC
    if usuario.id in l_ventas:
        if modelo.user_view is None:
            modelo.user_view = usuario.first_name + ' ' + usuario.last_name
            modelo.save()


def check_perms(model, permisos):
    if model.request.user.is_superuser: return True
    roles = model.request.user.groups.values_list('name', flat=True)
    check = list(filter(lambda x: x in permisos, roles))
    if len(check) > 0:
        return True
    else:
        return False
