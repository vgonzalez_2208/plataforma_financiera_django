import os
from datetime import datetime, date

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView
from weasyprint import CSS, HTML

from apps.prestamos.views.aprobaciones.views import fecha_inicio
from apps.ventas.forms import FormLigeras
from apps.ventas.models import CotizacionLigera
from apps.ventas.views.cotizador.views import fecha_completa, meses_extra, monto_p
from apps.ventas.views.solicitudes.views_nuevas import check_perms


class SolicitudLigera(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'ventas/proceso/2_proceso.html'

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            post = request.POST
            action = post['action']
            print(action)
            if action == 'CargarTabla':
                consulta = CotizacionLigera.objects.exclude(estatus='Descartada')
                data = list(map(lista_ligeras, consulta))
            elif action == 'guardar_ligera':
                if request.user.id in (7, 8):
                    user = request.user.first_name + ' ' + request.user.last_name
                else:
                    user = None
                id_nueva = solicitud_ligera(post, user, 1)
                data['test'] = str(id_nueva)
            elif action == 'ver_ligera':
                print(post)
                consulta = CotizacionLigera.objects.get(pk=post['id'])
                data = info_ligeras(consulta, data)
            elif action == 'update_ligera':
                print(post)
                consulta = CotizacionLigera.objects.get(pk=post['id'])
                solicitud_ligera(post, None, consulta)
                data['ok'] = ''
            elif action == 'descartar_ligera':
                print(post)
                consulta = CotizacionLigera.objects.get(pk=post['id'])
                consulta.notas = '\n //' + str(date.today()) + '>> ' + str(post['notas'])
                consulta.estatus = 'Descartada'
                consulta.save()
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data = {}
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['table_name'] = 'tabla_ligeras'
        context['card_title'] = 'Solicitudes rápidas'
        context['active_ventas'] = 'active'
        context['active_ventas_ligeras'] = 'active'
        context['form'] = FormLigeras
        context['create_url'] = reverse_lazy('ventas:solicitudes_agregar')
        return context


class CotizacionLigeraPdf(LoginRequiredMixin, PermissionRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    def get(self, request, *args, **kwargs):
        modelo = CotizacionLigera.objects.get(pk=self.kwargs.get('pk'))
        #  monto[0], intereses[1], comision[2], total_meses[3], s_colectivo[4], s_desempleo[5], s_dental[6], notaria[7],
        #  m_prestamo[8], c_promotor[9], s_descuento[10], timbres[11], admin[12], itbms[13], desembolso[14],
        #  letra_q[15], fecha_fin[16], fecha_ini[17], compra_saldo[18])
        fecha_encabezado = fecha_completa()
        detalles_p = desglose_ligero(modelo, fecha_inicio())
        if datetime.now().hour < 12:
            saludo = "Buenos días"
        elif datetime.now().hour < 18:
            saludo = "Buenas tardes"
        else:
            saludo = "Buenas noches"
        try:
            template = get_template('ventas/cotizador/cotizacion_pdf.html')
            context = {
                'fecha_enc': fecha_encabezado,
                'saludo': saludo,
                'nombre': modelo.nombre_1 + ' ' + modelo.apellido_1,
                'monto': f'{detalles_p[0]:,.2f}',
                'comision': f'{detalles_p[2]:,.2f}',
                'plazo': modelo.plazo_meses + "/" + str(detalles_p[3]),
                'fecha_ini': detalles_p[8],
                'compra_saldo': f'{detalles_p[9]:,.2f}',
                'desembolso': f'{detalles_p[6]:,.2f}',
                'letra': f'{detalles_p[7]:,.2f}',
                'itbms': f'{detalles_p[5]:,.2f}',
            }
            html = template.render(context)
            #  css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-4.6.0-dist/css/bootstrap.min.css')
            css_url = os.path.join(settings.BASE_DIR, 'static/lib/bootstrap-5.0.0-dist/css/bootstrap.min.css')
            pdf = HTML(string=html).write_pdf(stylesheets=[CSS(css_url)])
            return HttpResponse(pdf, content_type='application/pdf')
        except Exception as e:
            print(e)
            pass
        return HttpResponseRedirect(reverse_lazy('ventas:generar_cotizacion'))


def solicitud_ligera(post, user, flag):
    if flag == 1:
        print('nueva')
        modelo = CotizacionLigera()
    else:
        print('editar')
        modelo = flag
    modelo.nombre_1 = post['nombre_1']
    if post.get('nombre_2'):
        modelo.nombre_2 = post['nombre_2']
    modelo.apellido_1 = post['apellido_1']
    if post.get('apellido_2'):
        modelo.nombre_2 = post['apellido_2']
    modelo.tipo_documento_id = post['tipo_documento']
    modelo.dni = post['dni']
    if post.get('email_1'):
        modelo.email_1 = post['email_1']
    if post.get('cancelaciones'):
        modelo.cancelaciones = post['cancelaciones']
    modelo.celular = post['celular']
    modelo.monto = post['monto']
    modelo.plazo_meses = post['plazo_meses']
    modelo.tipo_ingreso_id = post['tipo_ingreso']
    modelo.salario = post['salario']
    if user:
        modelo.user_view = user
    if post.get('notas'):
        modelo.notas = str(post['notas'])
    if post.get('promotor'):
        modelo.promotor = post['promotor']
    modelo.c_promotor = post['c_promotor']
    modelo.s_descuento = post['s_descuento']
    modelo.save()
    return modelo.id


def desglose_ligero(modelo, fecha_i):
    salida = []
    salida.append(float(modelo.monto))
    dic_extra = 0
    date_string = datetime.strptime(fecha_i, '%Y/%m/%d')
    f_inicio = date_string
    if date.today().month == 12 and date.today().day < 15:
        dic_extra = 1
    total_meses = dic_extra + int(modelo.plazo_meses) + meses_extra(modelo.plazo_meses, f_inicio)
    intereses = float(modelo.monto) * total_meses * 0.02
    salida.append(intereses)
    if str(modelo.tipo_ingreso) == 'Empresa privada':
        comision = float(modelo.monto) * 0.5
    else:
        comision = float(modelo.monto) * 0.4
    salida.append(comision)
    salida.append(total_meses)
    notaria = 12
    salida.append(notaria)
    # print(date_string)
    monto_prestamo = monto_p(modelo.monto, date_string, modelo.plazo_meses, total_meses, comision)
    # print(monto_prestamo)
    timbres = round(monto_prestamo / 1000 + 0.05, 1)
    # print(modelo.c_promotor, 'es la comision de promotor')
    if modelo.c_promotor:
        c_promotor = round(float(modelo.monto) * float(modelo.c_promotor) / 100, 2)
    else:
        c_promotor = 0
    # print(c_promotor, ' es el calculo de c')
    if modelo.s_descuento:
        s_descuento = round(monto_prestamo * int(modelo.s_descuento) / 100, 2)
    else:
        s_descuento = 0
    admin = comision - timbres - c_promotor - s_descuento
    itbms = round(admin * 0.07, 2)
    salida.append(itbms)
    # print(modelo.cancelaciones)
    if modelo.cancelaciones:
        lista = modelo.cancelaciones.split(',')
        # print(lista)
        cancelaciones = 0
        for i in lista:
            cancelaciones += float(i.split(':')[1])
    else:
        cancelaciones = 0
    desembolso = round(float(modelo.monto) - itbms - cancelaciones, 2)
    salida.append(desembolso)
    letra_q = round(monto_prestamo / (int(modelo.plazo_meses) * 2) + 0.005, 2)
    salida.append(letra_q)
    salida.append(f_inicio.date())
    salida.append(cancelaciones)
    # print(salida)
    return salida


def info_ligeras(consulta, data):
    data['nombre_1'] = str(consulta.nombre_1)
    data['nombre_2'] = str(consulta.nombre_2)
    data['apellido_1'] = str(consulta.apellido_1)
    data['apellido_2'] = str(consulta.apellido_2)
    data['tipo_documento'] = str(consulta.tipo_documento_id)
    data['dni'] = str(consulta.dni)
    data['email_1'] = str(consulta.email_1)
    data['celular'] = str(consulta.celular)
    data['monto'] = str(consulta.monto)
    data['plazo_meses'] = str(consulta.plazo_meses)
    data['tipo_ingreso'] = str(consulta.tipo_ingreso_id)
    data['salario'] = str(consulta.salario)
    data['f_solicitud'] = str(consulta.creado_el.strftime("%Y-%m-%d, %H:%M:%S"))
    if consulta.notas:
        data['notas'] = str(consulta.notas)
    else:
        data['notas'] = ''
    if consulta.cancelaciones:
        data['cancelaciones'] = str(consulta.cancelaciones)
    else:
        data['cancelaciones'] = ''
    if consulta.promotor:
        data['promotor'] = str(consulta.promotor)
    else:
        data['promotor'] = ''
    data['c_promotor'] = str(consulta.c_promotor)
    data['s_descuento'] = str(consulta.s_descuento)
    return data


def lista_ligeras(i):
    fecha_s = i.creado_el.date()
    valor = {
        'id': i.id,
        'nombre': i.nombre_1,
        'apellido': i.apellido_1,
        'tipo_ingreso': str(i.tipo_ingreso),
        'monto': '$ ' + f'{float(i.monto):,.2f}',
        'f_solicitud': fecha_s.strftime("%Y-%m"),
        'proceso': i.user_view,
        'celular': i.celular,
        'opciones': i.apellido_2,
    }
    return valor
