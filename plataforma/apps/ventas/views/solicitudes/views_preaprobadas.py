import os
import threading

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView, TemplateView

from apps.cobros.models import Desembolso
from apps.formulario.models import Distrito, Corregimiento
from apps.ventas.forms import EditSolicitudProceso, VerSolicitudesP
from apps.ventas.models import PreAprobado
from apps.ventas.views.solicitudes.views_nuevas import check_perms
from apps.ventas.views.solicitudes.views_proceso import check_documentos, process_img, lista_tabla, save_pdf
from plataforma.settings import MEDIA_ROOT


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class SolicitudesPreaprobadas(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'ventas/preaprobadas/3_pre_aprobadas.html'

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            post = request.POST
            action = post['action']
            print(action)
            if action == 'pre-aprobado_1':
                data = []
                # user_ventas = ('dennis.caballero', 'jeimy.diaz', 'orbin.moreno')
                consulta_1 = PreAprobado.objects.filter(check_1=1).filter(check_3=1)
                consulta_2 = Desembolso.objects.exclude(cliente=None).values('cliente_id', 'id')
                dic_desembolsos = dict(map(lambda x: (x['cliente_id'], x['id']), consulta_2))
                data = list(map(lista_tabla_ventas, consulta_1))
                # if request.user.username not in user_ventas:
                data = list(map(lambda x: ajuste_lista(x, dic_desembolsos), data))
                # else:
                # print(data)
                # print(dic_desembolsos)
            elif action == 'pre-aprobado_2':
                data = []
                consulta = PreAprobado.objects.filter(check_1=1).filter(check_3=0).filter(check_2=0)
                data = list(map(lista_tabla, consulta))
            elif action == 'info_rechazado':
                modelo = PreAprobado.objects.filter(pk=request.POST['id']).get()
                print(modelo.notas)
                if modelo.notas_gerencia:
                    data['gerencia'] = modelo.notas_gerencia
                else:
                    data['gerencia'] = '<span class="text-dark">-- No emitió comentarios --</span>'
                data['cumplimiento'] = modelo.notas_cumplimiento
                data['ventas'] = modelo.notas
                data['n_cliente'] = modelo.nombre_1 + ' ' + modelo.apellido_1
            elif action == 'recuperar_cliente':
                print('recuperar_cliente')
                # print(post)
                consulta = PreAprobado.objects.get(id=post['id_cliente'])
                consulta.check_cumplimiento = 'Pendiente'
                consulta.check_gerencia = 'Pendiente'
                # consulta.notas_cumplimiento = post['comentario']
                consulta.save()
                data['ok'] = ''
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        # cat = Formulario.objects.get(pk=request.POST['id'])
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['card_title_1'] = 'Solicitudes Pre-aprobadas'
        context['card_title_2'] = 'Pendientes de revisión'
        context['active_ventas'] = 'active'
        context['active_ventas_prea'] = 'active'
        context['permitidos'] = ('guillermo.lemos', 'vicente_rc', 'gerencia.rc')
        return context


class SolicitudesPreaprobadasEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = EditSolicitudProceso
    template_name = 'ventas/proceso/2_proceso_edit.html'
    success_url = reverse_lazy('ventas:solicitudes_preaprobadas')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        modelo = self.get_object()
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'edit':
                form = self.get_form()
                if action == 'edit':
                    post = request.POST
                    files = request.FILES
                    # form = self.get_form()
                    if form.is_valid:
                        files_n = len(files)
                        if files_n != 0:
                            for f in files.keys():
                                files[f].name = save_pdf(f, modelo)
                        form.save()
                        imagen_1 = process_img(post, self.object, 'image_1', 'DNI_')
                        imagen_2 = process_img(post, self.object, 'image_2', 'c_trabajo_')
                        imagen_3 = process_img(post, self.object, 'image_3', 'talonario_1_')
                        imagen_4 = process_img(post, self.object, 'image_4', 'talonario_2_')
                        imagen_5 = process_img(post, self.object, 'image_5', 'ficha_')
                        imagen_6 = process_img(post, self.object, 'image_6', 'recibo_')
                        imagen_7 = process_img(post, self.object, 'image_7', 'c_saldo_')
                        if imagen_1:
                            if modelo.DNI_img:
                                self.object.DNI_img.delete()
                                modelo.DNI_img = imagen_1
                            else:
                                self.object.DNI_img = imagen_1
                            # print('se guardo cedula')
                        if imagen_2:
                            if modelo.carta_trabajo_img:
                                modelo.carta_trabajo_img.delete()
                                self.object.carta_trabajo_img = imagen_2
                            else:
                                self.object.carta_trabajo_img = imagen_2
                            # print('se guardo carta de trabajo')
                        if imagen_3:
                            if modelo.talonario_1_img:
                                modelo.talonario_1_img.delete()
                                self.object.talonario_1_img = imagen_3
                            else:
                                self.object.talonario_1_img = imagen_3
                            # print('se guardo talonario_1')
                        if imagen_4:
                            if modelo.talonario_2_img:
                                modelo.talonario_2_img.delete()
                                self.object.talonario_2_img = imagen_4
                            else:
                                self.object.talonario_2_img = imagen_4
                            # print('se guardo talonario_2')
                        if imagen_5:
                            if modelo.ficha_img:
                                modelo.ficha_img.delete()
                                self.object.ficha_img = imagen_5
                            else:
                                self.object.ficha_img = imagen_5
                            # print('se guardo ficha')
                        if imagen_6:
                            if modelo.recibo_img:
                                modelo.recibo_img.delete()
                                self.object.recibo_img = imagen_6
                            else:
                                self.object.recibo_img = imagen_6
                            # print('se guardo recibo')
                        if imagen_7:
                            if modelo.carta_saldo_img:
                                modelo.carta_saldo_img.delete()
                                self.object.carta_saldo_img = imagen_7
                            else:
                                self.object.carta_saldo_img = imagen_7
                            # print('se guardo carta de saldo')
                        self.object.save()
                        data['ok'] = '2'
                    else:
                        data['error'] = form.errors
                elif action == 'select_p':
                    data = []
                    for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                        data.append({'id': i.id, 'name': i.nombre})
                elif action == 'select_d':
                    data = []
                    for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                        data.append({'id': i.id, 'name': i.nombre})
                else:
                    data['error'] = 'No ha ingresado ninguna opción'
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Editar solicitud pre-aprobada'
        context['botón_cancelar'] = 'off'
        context['active_ventas'] = 'active'
        context['active_ventas_prea'] = 'active'
        context['action'] = 'edit'
        context['retorno_1'] = reverse_lazy('ventas:solicitudes_preaprobadas')
        return context


class SolicitudesPreAprobadasVer(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = VerSolicitudesP
    template_name = 'ventas/preaprobadas/3_pre_aprobadas_ver.html'
    success_url = reverse_lazy('ventas:solicitudes_preaprobadas')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        modelo = self.object
        try:
            action = request.POST['action']
            print(action)
            if action == 'comentar':
                data = []
                modelo.notas = request.POST['nota']
                print(modelo.nombre_1)
                modelo.save()
                data.append({'msg': 'Se guardó el comentario'})
            elif action == 'cumplimiento':
                if modelo.DNI_img and modelo.carta_trabajo_img and modelo.talonario_1_img and modelo.talonario_2_img and modelo.ficha_img and modelo.recibo_img:
                    data['completo'] = '¡La solicitud ha sido pre-aprobada!'
                else:
                    data['advertencia'] = 'Solicitud pre-aprobada.'
                self.object.check_1 = True
                self.object.check_3 = True
                self.object.save()
            elif action == 'cumplimiento_rt':
                self.object.check_1 = False
                self.object.check_3 = False
                self.object.save()
                data['rt'] = ''
            elif action == 'ventas':
                if modelo.DNI_img and modelo.carta_trabajo_img and modelo.talonario_1_img and modelo.talonario_2_img and modelo.ficha_img and modelo.recibo_img:
                    data['completo'] = '¡La solicitud ha sido pre-aprobada!'
                    self.object.check_1 = True
                    self.object.save()
                else:
                    data['advertencia'] = 'Consulte a cumplimiento.'
            elif action == 'ventas_rt':
                self.object.check_3 = False
                self.object.check_1 = False
                self.object.save()
            elif action == 'descartar':
                self.object.check_2 = True
                self.object.save()
                data['advertencia'] = ''
            elif action == 'descartar_rt':
                self.object.check_2 = False
                self.object.save()
                # data['advertencia'] = 'Consulte a cumplimiento.'
            elif action == 'borrar_archivo':
                post = request.POST
                modelo = self.object
                if post['archivo'] == 'dni':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.DNI_img)))
                    modelo.DNI_img.delete()
                elif post['archivo'] == 'c_trabajo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.carta_trabajo_img)))
                    modelo.carta_trabajo_img.delete()
                elif post['archivo'] == 'talonario_1':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.talonario_1_img)))
                    modelo.talonario_1_img.delete()
                elif post['archivo'] == 'talonario_2':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.talonario_2_img)))
                    modelo.talonario_2_img.delete()
                elif post['archivo'] == 'ficha':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.ficha_img)))
                    modelo.ficha_img.delete()
                elif post['archivo'] == 'recibo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.recibo_img)))
                    modelo.recibo_img.delete()
                elif post['archivo'] == 'c_saldo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.carta_saldo_img)))
                    modelo.carta_saldo_img.delete()
                elif post['archivo'] == 'apc':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.file_apc)))
                    modelo.file_apc.delete()
                modelo.save()
                data['ok'] = ''
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas - Pre-aprobadas'
        context['card_title'] = 'Información del cliente'
        context['active_ventas'] = 'active'
        context['active_ventas_prea'] = 'active'
        context['action'] = 'edit'
        context['editar'] = reverse_lazy('ventas:editar_pre-aprobada', kwargs={'pk': self.object.id})
        context['cotizar'] = reverse_lazy('ventas:gen_cot_pre', kwargs={'pk': self.object.id, 'tipo': 'pre'})
        return context


def lista_tabla_ventas(i):
    documentos = check_documentos(i)
    if i.check_cumplimiento == i.check_gerencia == 'Aprobado':
        estatus = 'Aprobado'
    elif i.check_cumplimiento == 'Revisado' and i.check_gerencia == 'Aprobado':
        estatus = 'Aprobado'
    elif i.check_cumplimiento == 'Rechazado' or i.check_gerencia == 'Rechazado':
        estatus = 'Rechazado'
    else:
        estatus = 'En revisión'
    fecha_s = i.creado_el.date()
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),  #
        'apellido_1': str(i.apellido_1),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': '$ ' + f'{int(i.salario):,.2f}',
        'f_solicitud': fecha_s.strftime("%Y-%m"),
        'usuario': str(i.user_view),
        'documentos': documentos,
        'estatus': estatus,
        'botones': estatus,
    }
    return valor


def ajuste_lista(cliente, dic_clientes):
    val = dic_clientes.get(int(cliente['id']))
    if val:
        cliente['estatus'] = 'Desembolsado'
        cliente['botones'] = cliente['id'] + '/' + str(val)
        return cliente
    else: return cliente
