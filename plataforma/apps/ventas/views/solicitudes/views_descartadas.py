import threading

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView, DeleteView, TemplateView

from apps.formulario.models import Formulario
from apps.ventas.forms import VerSolicitudesN, VerSolicitudesP
from apps.ventas.models import PreAprobado
from apps.ventas.views.solicitudes.views_nuevas import check_perms
from apps.ventas.views.solicitudes.views_proceso import check_documentos


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class SolicitudesDescartadas(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'ventas/descartadas/descartadas.html'

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            post = request.POST
            action = post['action']
            print(action)
            if action == 'descartadas_f':
                data = []
                consulta = Formulario.objects.filter(check_3=True)
                data = list(map(map_descartadas_1, consulta))
            elif action == 'descartadas_p':
                data = []
                consulta = PreAprobado.objects.filter(check_2=True)
                data = list(map(map_descartadas_2, consulta))
            elif action == 'info_descartadas':
                print(post)
                if post['tabla'] == '1':
                    consulta = Formulario.objects.get(pk=post['id'])
                    data['nombre'] = consulta.nombre_1 + ' ' + consulta.apellido_1
                    data['n_ventas'] = consulta.notas
                elif post['tabla'] == '2':
                    consulta = PreAprobado.objects.get(pk=post['id'])
                    data['nombre'] = consulta.nombre_1 + ' ' + consulta.apellido_1
                    data['n_ventas'] = consulta.notas
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['card_title_1'] = 'Solicitudes descartadas'
        context['card_title_2'] = 'Solicitudes procesadas descartadas'
        context['active_ventas'] = 'active'
        context['active_ventas_descartadas'] = 'active'
        return context


# Delete Views 

class BorrarSolicitud(LoginRequiredMixin, DeleteView):
    model = Formulario
    template_name = 'ventas/delete/delete_solicitud.html'
    success_url = reverse_lazy('ventas:solicitudes_nuevas')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.desc = True
            self.object.save()
            print(self.object.desc)
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Nuevas solicitudes'
        context['active_ventas'] = 'active'
        context['active_ventas_nuevas'] = 'active'
        context['solicitudes_url'] = reverse_lazy('ventas:solicitudes_nuevas')
        return context


# UpdateViews para ver solicitudes

class VerSolicitudesDescartadas(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Formulario
    form_class = VerSolicitudesN
    template_name = 'ventas/descartadas/solicitudes_descartadas_ver.html'
    success_url = reverse_lazy('ventas:solicitudes_descartadas')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'recuperar':
                self.object.check_3 = False
                self.object.save()
                data['msg'] = 'Rec'
            elif action == 'recuperar_rt':
                self.object.check_3 = True
                self.object.save()
                data['msg'] = 'Rec_rt'
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Solicitud descartada'
        context['active_ventas'] = 'active'
        context['active_ventas_descartadas'] = 'active'
        return context


class VerProcesadasDescartadas(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = VerSolicitudesP
    template_name = 'ventas/descartadas/procesadas_descartadas_ver.html'
    success_url = reverse_lazy('ventas:solicitudes_descartadas')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        modelo = self.object
        try:
            action = request.POST['action']
            print(action)
            if action == 'recuperar':
                modelo.check_2 = False
                modelo.save()
                data['msg'] = 'Rec'
            elif action == 'recuperar_rt':
                modelo.check_2 = True
                modelo.save()
                data['msg'] = 'Rec_rt'
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Solicitud descartada'
        context['active_ventas'] = 'active'
        context['active_ventas_descartadas'] = 'active'
        return context


def map_descartadas_1(i):
    if i.DNI_img and i.ficha_img:
        documentos = "Sí"
    elif i.DNI_img and not i.ficha_img:
        documentos = "Sólo cédula"
    elif not i.DNI_img and i.ficha_img:
        documentos = "Sólo ficha"
    else:
        documentos = "Sin documentos"
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': f'{int(i.salario):,.2f}',
        'celular': str(i.celular),
        'documentos': documentos,
        'genero': str(i.genero),
    }
    return valor


def map_descartadas_2(i):
    documentos = check_documentos(i)
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),
        'apellido_1': str(i.apellido_1),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': f'{int(i.salario):,.2f}',
        'celular': str(i.celular),
        'documentos': documentos,
        'genero': str(i.genero),
    }
    return valor
