import base64
import os
import threading

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.files.base import ContentFile
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView, TemplateView

from apps.cobros.views.cobros.functions import save_pdf_files
from apps.formulario.models import Distrito, Corregimiento
from apps.ventas.forms import EditSolicitudProceso, VerSolicitudesP
from apps.ventas.models import PreAprobado, CotizacionPreAprobado
from apps.ventas.views.solicitudes.views_nuevas import check_perms
from plataforma.settings import MEDIA_ROOT


def send_async(func, args):
    threading.Thread(target=func, args=args).start()


class SolicitudesProceso(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'ventas/proceso/2_proceso.html'

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @staticmethod
    def post(request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            if action == 'proceso':
                data = []
                lista_per = ('vicente_rc', 'guillermo.lemos', 'gerencia.rc', 'carlos.gomez')
                call_center = ('Dennis Caballero', 'O.M./ Dennis Caballero', 'Ariel Reina', 'C.C./ Ariel Reina')
                ventas = ('Jeimy Diaz', 'O.M./ Jeimy Diaz', 'C.C./ Jeimy Diaz')
                if request.user.username in lista_per:
                    consulta = PreAprobado.objects.filter(check_1=0).filter(check_2=0)
                elif request.user.id == 8:
                    consulta = PreAprobado.objects.filter(check_1=0).filter(check_2=0).filter(user_view__in=call_center)
                elif request.user.username == 'jeimy.diaz':
                    consulta = PreAprobado.objects.filter(check_1=0).filter(check_2=0).filter(user_view__in=ventas)
                else: consulta = ''
                data = list(map(lista_tabla, consulta))
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['section_name'] = 'Ventas'
        context['table_name'] = 'proceso'
        context['card_title'] = 'Solicitudes en proceso'
        context['active_ventas'] = 'active'
        context['active_ventas_proceso'] = 'active'
        return context


class SolicitudesProcesoEdit(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = EditSolicitudProceso
    template_name = 'ventas/proceso/2_proceso_edit.html'
    success_url = reverse_lazy('ventas:solicitudes_proceso')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            print(action)
            # print(action)
            if action == 'edit':
                post = request.POST
                files = request.FILES
                print(post.keys())
                print(files.keys())
                modelo = self.object
                form = self.get_form()
                if form.is_valid():
                    files_n = len(files)
                    if files_n != 0:
                        for f in files.keys():
                            files[f].name = save_pdf(f, self.object)
                    form.save()
                    save_pdf_files(files, self.object)
                    imagen_1 = process_img(post, self.object, 'image_1', 'DNI_')
                    imagen_2 = process_img(post, self.object, 'image_2', 'c_trabajo_')
                    imagen_3 = process_img(post, self.object, 'image_3', 'talonario_1_')
                    imagen_4 = process_img(post, self.object, 'image_4', 'talonario_2_')
                    imagen_5 = process_img(post, self.object, 'image_5', 'ficha_')
                    imagen_6 = process_img(post, self.object, 'image_6', 'recibo_')
                    imagen_7 = process_img(post, self.object, 'image_7', 'c_saldo_')
                    if imagen_1:
                        if modelo.DNI_img: self.object.DNI_img.delete()
                        self.object.DNI_img = imagen_1
                    if imagen_2:
                        if modelo.carta_trabajo_img: modelo.carta_trabajo_img.delete()
                        self.object.carta_trabajo_img = imagen_2
                    if imagen_3:
                        if modelo.talonario_1_img: modelo.talonario_1_img.delete()
                        self.object.talonario_1_img = imagen_3
                    if imagen_4:
                        if modelo.talonario_2_img: modelo.talonario_2_img.delete()
                        self.object.talonario_2_img = imagen_4
                    if imagen_5:
                        if modelo.ficha_img: modelo.ficha_img.delete()
                        self.object.ficha_img = imagen_5
                    if imagen_6:
                        if modelo.recibo_img: modelo.recibo_img.delete()
                        self.object.recibo_img = imagen_6
                    if imagen_7:
                        if modelo.carta_saldo_img: modelo.carta_saldo_img.delete()
                        self.object.carta_saldo_img = imagen_7
                        # print('se guardo carta de saldo')
                    self.object.save()
                    if self.kwargs.get('info'):
                        info = self.kwargs['info'].split('-')
                        if info[0] == 'aprobado_edit':
                            data['ok'] = '3-' + info[1]
                        elif info[0] == 'cobros_new_edit':
                            data['ok'] = '4-' + info[1]
                    else:
                        data['ok'] = '1'
                    if self.kwargs.get('info'):
                        print('info edit cobros')
                else:
                    data['error'] = form.errors
                    print(form.errors)
            elif action == 'select_p':
                data = []
                for i in Distrito.objects.filter(provincia_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            elif action == 'select_d':
                data = []
                for i in Corregimiento.objects.filter(distrito_id=request.POST['id']):
                    data.append({'id': i.id, 'name': i.nombre})
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            data['error'] = str(e)
            print(data)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['action'] = 'edit'
        context['pass_todos'] = '1'
        context['retorno_1'] = reverse_lazy('ventas:solicitudes_proceso')
        # context['form'] = EditSolicitudProceso
        if self.kwargs.get('info'):
            info = self.kwargs['info'].split('-')
            if info[0] == 'aprobado_edit':
                context['card_title'] = 'Editar solicitud aprobada'
                context['section_name'] = 'Préstamos'
                context['active_prestamos'] = 'active'
                context['active_prestamos_lista_aprobados'] = 'active'
            elif info[0] == 'cobros_new_edit':
                context['card_title'] = 'Editar info. cliente'
                context['section_name'] = 'Cobros'
                context['active_cobros'] = 'active'
                context['active_cobros_pagos'] = 'active'
        else:
            context['card_title'] = 'Editar solicitud procesada'
            context['section_name'] = 'Ventas'
            context['active_ventas'] = 'active'
            context['active_ventas_proceso'] = 'active'
        # 'vicente_rc', 'guillermo.lemos', 'jeimy.diaz', 'dennis.caballero' | Subir archivo de APC
        context['file_apc'] = (1, 6, 7, 8)
        return context


class SolicitudesProcesoVer(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = PreAprobado
    form_class = VerSolicitudesP
    template_name = 'ventas/proceso/2_proceso_ver.html'
    success_url = reverse_lazy('ventas:solicitudes_proceso')

    def has_permission(self):
        perm = ('Ventas', 'Gerencia', 'Cumplimiento', 'Inversor')
        return check_perms(self, perm)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        modelo = self.object
        try:
            action = request.POST['action']
            print(action)
            if action == 'comentar':
                data = []
                modelo.notas = request.POST['nota']
                print(modelo.nombre_1)
                modelo.save()
                data.append({'msg': 'Se guardó el comentario'})
            elif action == 'cumplimiento':
                if modelo.DNI_img and modelo.carta_trabajo_img and modelo.talonario_1_img and modelo.talonario_2_img and modelo.ficha_img and modelo.recibo_img:
                    data['completo'] = '¡La solicitud ha sido pre-aprobada!'
                else:
                    data['advertencia'] = 'Solicitud pre-aprobada.'
                self.object.check_1 = True
                self.object.check_3 = True
                self.object.save()
            elif action == 'cumplimiento_rt':
                self.object.check_1 = False
                self.object.check_3 = False
                self.object.save()
                data['rt'] = ''
            elif action == 'ventas':
                if modelo.DNI_img and modelo.carta_trabajo_img and modelo.talonario_1_img and modelo.talonario_2_img and modelo.ficha_img and modelo.recibo_img:
                    if CotizacionPreAprobado.objects.filter(id=modelo.id).first():
                        data['completo'] = '¡La solicitud ha sido pre-aprobada!'
                        self.object.check_1 = True
                        self.object.save()
                    else:
                        data['advertencia'] = 'No se ha generado cotización al cliente.'
                else:
                    data['advertencia'] = 'Documentos incompletos. Consulte a cumplimiento.'
            elif action == 'ventas_rt':
                self.object.check_3 = False
                self.object.check_1 = False
                self.object.save()
            elif action == 'descartar':
                self.object.check_2 = True
                self.object.save()
                data['advertencia'] = ''
            elif action == 'descartar_rt':
                self.object.check_2 = False
                self.object.save()
                # data['advertencia'] = 'Consulte a cumplimiento.'
            elif action == 'borrar_archivo':
                post = request.POST
                modelo = self.object
                if post['archivo'] == 'dni':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.DNI_img)))
                    modelo.DNI_img.delete()
                elif post['archivo'] == 'c_trabajo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.carta_trabajo_img)))
                    modelo.carta_trabajo_img.delete()
                elif post['archivo'] == 'talonario_1':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.talonario_1_img)))
                    modelo.talonario_1_img.delete()
                elif post['archivo'] == 'talonario_2':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.talonario_2_img)))
                    modelo.talonario_2_img.delete()
                elif post['archivo'] == 'ficha':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.ficha_img)))
                    modelo.ficha_img.delete()
                elif post['archivo'] == 'recibo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.recibo_img)))
                    modelo.recibo_img.delete()
                elif post['archivo'] == 'c_saldo':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.carta_saldo_img)))
                    modelo.carta_saldo_img.delete()
                elif post['archivo'] == 'apc':
                    os.remove(os.path.join(MEDIA_ROOT + str(modelo.file_apc)))
                    modelo.file_apc.delete()
                modelo.save()
                data['ok'] = ''
                # os.remove(os.path.join(MEDIA_ROOT+str(self.object.DNI_img)))
            else:
                data['error'] = 'No ha ingresado ninguna opción'
        except Exception as e:
            print(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['form'] = SolicitudRevisada()
        context['id_form'] = self.object.id
        context['section_name'] = 'Ventas'
        context['card_title'] = 'Información del cliente'
        context['active_ventas'] = 'active'
        context['active_ventas_proceso'] = 'active'
        context['action'] = 'edit'
        # ('guillermo.lemos', 'jeimy.diaz', 'vicente_rc', 'dennis.caballero') // ariel.reina
        context['pre_aprobacion'] = (6, 7, 1, 8)
        context['editar'] = reverse_lazy('ventas:editar_solicitudes_proceso', kwargs={'pk': self.object.id})
        context['cotizar'] = reverse_lazy('ventas:generar_cotizacionP', kwargs={'pk': self.object.id})
        return context


def check_documentos(i):
    if i.DNI_img and i.ficha_img and i.talonario_1_img and i.talonario_2_img and i.recibo_img and i.carta_trabajo_img:
        documentos = "Completo"
    elif i.DNI_img or i.ficha_img or i.talonario_1_img or i.talonario_2_img or i.recibo_img or i.carta_trabajo_img or i.carta_saldo_img:
        documentos = "Incompleto"
    else:
        documentos = "Ninguno"
    return documentos


def process_img(post, modelo, nombre, tag):
    imagen = None
    try:
        if post[nombre]:
            # print('xxx')
            img_file = post[nombre]
            formato, img_str = img_file.split(';base64,')
            # print("format", formato)
            ext = formato.split('/')[-1]
            imagen = ContentFile(base64.b64decode(img_str), name=tag + modelo.dni + '.' + ext)
    except:
        imagen = None
    return imagen

    # print('se guardo cedula')


def lista_tabla(i):
    documentos = check_documentos(i)
    valor = {
        'id': str(i.id),
        'nombre_1': str(i.nombre_1),  #
        'apellido_1': str(i.apellido_1),
        'tipo_ingreso': str(i.tipo_ingreso),
        'salario': '$ ' + f'{int(i.salario):,.2f}',
        'usuario': str(i.user_view),
        'documentos': documentos,
        'genero': str(i.genero),
    }
    return valor


def save_pdf(clave, modelo):
    file_name = None
    if clave == 'DNI_img':
        if modelo.DNI_img:
            modelo.DNI_img.delete()
            file_name = 'DNI_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'DNI_' + str(modelo.dni) + '.pdf'
    elif clave == 'carta_trabajo_img':
        if modelo.carta_trabajo_img:
            modelo.carta_trabajo_img.delete()
            file_name = 'c_trabajo_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'c_trabajo_' + str(modelo.dni) + '.pdf'
    elif clave == 'talonario_1_img':
        if modelo.talonario_1_img:
            modelo.talonario_1_img.delete()
            file_name = 'talonario_1_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'talonario_1_' + str(modelo.dni) + '.pdf'
    elif clave == 'talonario_2_img':
        if modelo.talonario_2_img:
            modelo.talonario_2_img.delete()
            file_name = 'talonario_2_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'talonario_2_' + str(modelo.dni) + '.pdf'
    elif clave == 'ficha_img':
        if modelo.ficha_img:
            modelo.ficha_img.delete()
            file_name = 'ficha_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'ficha_' + str(modelo.dni) + '.pdf'
    elif clave == 'recibo_img':
        if modelo.recibo_img:
            modelo.recibo_img.delete()
            file_name = 'recibo_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'recibo_' + str(modelo.dni) + '.pdf'
    elif clave == 'carta_saldo_img':
        if modelo.carta_saldo_img:
            modelo.carta_saldo_img.delete()
            file_name = 'c_saldo_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'c_saldo_' + str(modelo.dni) + '.pdf'
    elif clave == 'file_apc':
        if modelo.file_apc:
            modelo.file_apc.delete()
            file_name = 'APC_' + str(modelo.dni) + '.pdf'
        else:
            file_name = 'APC_' + str(modelo.dni) + '.pdf'
    print(file_name)
    return file_name
