from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, UpdateView, DeleteView, CreateView

from apps.formulario.models import Formulario, Distrito, Corregimiento
from apps.ventas.forms import SolicitudRevisada, EditSolicitud, EmpresasForm, CrearSolicitud
from apps.ventas.models import Empresa


def ViewTest(request):
    data = {
        'title': 'Listado de Categorías',
    }
    return render(request, 'ventas/cotizador/cotizacion_pdf.html', data)
