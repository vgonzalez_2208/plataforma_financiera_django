from django.urls import path, re_path

from apps.ventas.views.cotizador.views import GenerarCotizacion, Cotizacion, vista_cotizacion, GenerarCotizacionP, \
    CotizacionPre, Liquidacion, CondicionesPrestamo
from apps.ventas.views.empresas.views import *
from apps.ventas.views.solicitudes.views_descartadas import *
from apps.ventas.views.solicitudes.views_nuevas import *
from apps.ventas.views.solicitudes.views_preaprobadas import *
from apps.ventas.views.solicitudes.views_proceso import *
from apps.ventas.views.solicitudes.views_rapidas import SolicitudLigera, CotizacionLigeraPdf
from apps.ventas.views.test.views import ViewTest

app_name = 'ventas'

urlpatterns = [
    path('form/', ViewTest),
    # Solicitudes nuevas
    path('solicitudes/nuevas/', SolicitudesNuevas.as_view(), name='solicitudes_nuevas'),
    path('solicitudes/agregar/', SolicitudesAgregar.as_view(), name='solicitudes_agregar'),
    path('solicitudes/edit/<int:pk>/', EdicionSolicitud.as_view(), name='editar_solicitudes'),
    path('solicitudes/delete/<int:pk>/', BorrarSolicitud.as_view(), name='borrar_solicitudes'),
    path('solicitudes/nuevas/ver/<int:pk>/', VerSolicitudesNuevas.as_view(), name='ver_solicitudes'),
    # Solicitudes proceso
    path('solicitudes/proceso/', SolicitudesProceso.as_view(), name='solicitudes_proceso'),
    path('solicitudes/proceso/edit/<int:pk>/', SolicitudesProcesoEdit.as_view(), name='editar_solicitudes_proceso'),
    path('solicitudes/proceso/ver/<int:pk>/', SolicitudesProcesoVer.as_view(), name='ver_solicitudes_proceso'),
    # Solicitudes Pre-aprobadas
    path('solicitudes/preaprobadas/', SolicitudesPreaprobadas.as_view(), name='solicitudes_preaprobadas'),
    path('solicitudes/preaprobadas/edit/<int:pk>/', SolicitudesPreaprobadasEdit.as_view(), name='editar_pre-aprobada'),
    path('solicitudes/preaprobadas/ver/<int:pk>/', SolicitudesPreAprobadasVer.as_view(), name='ver_pre-aprobada'),
    # Empresas
    path('empresas/', Empresas.as_view(), name='empresas_lista'),
    path('empresas/agregar/', AgregarEmpresa.as_view(), name='empresas_agregar'),
    path('empresas/edit/<int:pk>/', EmpresasEdit.as_view(), name='empresas_editar'),
    # Cotizador
    path('solicitudes/nuevas/cotizar/<int:pk>/', GenerarCotizacion.as_view(), name='generar_cotizacion'),
    path('solicitudes/cotizacion/<int:pk>/', Cotizacion.as_view(), name='cotizacion_pdf'),
    path('solicitudes/liquidacion/<int:pk>/', Liquidacion.as_view(), name='liquidacion_pdf'),
    path('cotizacion/', vista_cotizacion),
    # Cotizador Pre-aprobado
    path('solicitudes/proceso/cotizar/<int:pk>/', GenerarCotizacionP.as_view(), name='generar_cotizacionP'),
    path('solicitudes/preaprobadas/cotizar/<int:pk>/<str:tipo>/', GenerarCotizacionP.as_view(), name='gen_cot_pre'),
    path('solicitudes/proceso/cotizacion/<int:pk>/', CotizacionPre.as_view(), name='cotizacionP_pdf'),
    path('solicitudes/proceso/detalles/<int:pk>/', CondicionesPrestamo.as_view(), name='condiciones_prestamo'),
    # Descartadas
    path('solicitudes/descartadas/', SolicitudesDescartadas.as_view(), name='solicitudes_descartadas'),
    path('solicitudes/descartadas/ver/<int:pk>/', VerSolicitudesDescartadas.as_view(), name='solicitudes_descartadas_ver'),
    path('solicitudes/descartadas/procesadas_ver/<int:pk>/', VerProcesadasDescartadas.as_view(), name='procesadas_descartadas_ver'),
    # Cotización rápida
    path('solicitudes/ligeras/', SolicitudLigera.as_view(), name='solicitudes_ligeras'),
    re_path(r'^solicitudes/agregar/(?P<class>\d+)/$', SolicitudesAgregar.as_view(), name='solicitudes_agregar_2'),
    path('solicitudes/ligeras/<int:pk>/', CotizacionLigeraPdf.as_view(), name='ligera_pdf'),
]
