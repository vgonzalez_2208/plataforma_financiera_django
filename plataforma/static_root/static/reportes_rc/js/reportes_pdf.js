function generar_pdf() {
    const doc = new jspdf.jsPDF({orientation: "landscape", format: 'legal', title: 'test'});
    doc.setFontSize(10);
    // let width = doc.internal.pageSize.getWidth();
    // console.log(width);
    doc.autoTable({
        html: '#cabecera',
        useCss: true,
        // styles: {
        //    lineColor: [44, 62, 80],
        //    lineWidth: 1,
        // },
        // columnStyles: function (data) {
        //     console.log(data);
        //     if (data.column.index === 0 && data.cell.section === 'body' && data.row.index === 0) {
        //         console.log('print_1');
        //     }
        //     if (data.column.index === 2 && data.cell.section === 'body' && data.row.index === 3) {
        //         console.log('print_2');
        //     }
        // },
        didDrawCell: function (data) {
            // console.log(data.column.index, 'es el index col');
            // console.log(data.row.index, 'es el index row__');
            if (data.column.index === 0 && data.cell.section === 'body' && data.row.index === 0) {
                // data.cell.styles.width = 0.2 * width;
                let td = data.cell.raw;
                let img = td.getElementsByTagName('img')[0];
                let dim = data.cell.height - data.cell.padding('vertical');
                // console.log(dim, 'es el dim');
                let textPos = data.cell.getTextPos();
                // console.log(textPos);
                doc.addImage(img.src, textPos.x, textPos.y, 40, 22.73);
            }
        }
    });
    let finalY = doc.lastAutoTable.finalY || 10;
    doc.autoTable({startY: finalY, html: '#t_contenido', useCss: true});
    finalY = doc.lastAutoTable.finalY;
    doc.autoTable({startY: finalY, html: '#t_foot', useCss: true});
    // doc.autoTable({html: '#table_test', styles: {cellPadding: 0.5, fontSize: 8}})
    // doc.save("a4.pdf");
    // doc.output('datauri');
    // window.open(doc.output("dataurl"));
    window.open(URL.createObjectURL(doc.output("blob")), '_self');
}