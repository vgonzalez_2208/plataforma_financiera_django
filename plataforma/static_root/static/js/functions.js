function message_error(obj) {
    let html = '<ul style="text-align: left;">';
    $.each(obj, function (key, value) {
        /* console.log(key);
        console.log(value); */
        html += '<li>' + value + '</li>';
    });
    html += '</ul>';
    Swal.fire({
        title: 'Atención!',
        html: html,
        icon: 'warning',
    });
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

var f_dollar = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});

// Procesar imágenes ////////////////
let max_width = 1920;  // Ancho
let max_height = 1080;  // Alto

function getExt(file) {
    let ext = file.split('.').pop();
    if (ext === file) return "";
    return ext;
}

function processfile(file, campo, form) {
    if (!(/image/i).test(file.type)) {
        alert("El archivo " + file.name + " no es válido.");
        return false;
    }
    // read the files
    var reader = new FileReader();
    reader.readAsArrayBuffer(file);

    reader.onload = function (event) {
        // blob stuff
        var blob = new Blob([event.target.result]); // create blob...
        window.URL = window.URL || window.webkitURL;
        var blobURL = window.URL.createObjectURL(blob); // and get it's URL

        // helper Image object
        var image = new Image();
        image.src = blobURL;
        //preview.appendChild(image); // preview commented out, I am using the canvas instead
        image.onload = function () {
            // have to wait till it's loaded
            var resized = resizeMe(image); // send it to canvas
            var newinput = document.createElement("input");
            newinput.type = 'hidden';
            newinput.name = 'image_' + campo;
            // console.log(newinput.name)
            newinput.value = resized; // put result from canvas into new hidden input
            // console.log(resized);
            form.appendChild(newinput);
        }
    };
}

function read_files(files, file_n, file_name, form) {
    let existing_inputs;
    existing_inputs = document.getElementsByName(file_name);
    while (existing_inputs.length > 0) { // it's a live list so removing the first element each time
        // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
        form.removeChild(existing_inputs[0]);
        // preview.removeChild(existingcanvases[0]);
    }
    for (let i = 0; i < files.length; i++) {
        processfile(files[i], file_n, form); // process each file at once
    }
    files.value = ""; //remove the original files from fileinput
}

function validar_img(file_input, str_span, file_n, clean_f, form, pdf_t = 0) {
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        alert('Archivo inválido.');
        return false;
    }
    document.getElementById(str_span).innerHTML = file_input.files[0].name;
    document.getElementById(str_span).classList.remove("text-orange");
    let archivo = getExt(file_input.files[0].name);
    if (!(archivo === 'pdf' || archivo === 'PDF')) {
        if (pdf_t === 0) {
            read_files(file_input.files, file_n, clean_f, form);
        } else if (pdf_t === 1) {
            alert('No es pdf');
            file_input.value = "";
        }
    } else {
        if (pdf_t === 0) {
            clean_files(clean_f, form);
        }
    }
    console.log('its ok');
}

// === RESIZE ====

function resizeMe(img) {
    let canvas = document.createElement('canvas');
    let width = img.width;
    let height = img.height;

    // calculate the width and height, constraining the proportions
    if (width > height) {
        if (width > max_width) {
            //height *= max_width / width;
            height = Math.round(height *= max_width / width);
            width = max_width;
        }
    } else {
        if (height > max_height) {
            //width *= max_height / height;
            width = Math.round(width *= max_height / height);
            height = max_height;
        }
    }

    // resize the canvas and draw the image data into it
    canvas.width = width;
    canvas.height = height;
    let ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    // preview.appendChild(canvas); // do the actual resized preview
    return canvas.toDataURL("image/jpeg", 0.8); // get the data from canvas as 70% JPG (can be also PNG, etc.)
}

function clean_files(existente, form) {
    let val = document.getElementsByName(existente);
    while (val.length > 0) { // it's a live list so removing the first element each time
        // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
        form.removeChild(val[0]);
        // preview.removeChild(existingcanvases[0]);
    }
}

// Popup de cancelaciones

function cancelaciones_cliente(input_val, input_span) {
    let c_saldo = document.getElementById(input_val);
    (async () => {
        const {value: formValues} = await Swal.fire({
            title: 'Ingrese cancelaciones:',
            html:
                '<input id="swal-input1" class="swal2-input">' +
                '<input id="swal-input2" class="swal2-input">' +
                '<input id="swal-input3" class="swal2-input">',
            focusConfirm: false,
            preConfirm: () => {
                let val_1 = document.getElementById('swal-input1').value;
                let val_2 = document.getElementById('swal-input2').value;
                let val_3 = document.getElementById('swal-input3').value;
                let salida = [];
                if (!(val_1 === '')) {
                    salida.push(val_1);
                }
                if (!(val_2 === '')) {
                    salida.push(val_2);
                }
                if (!(val_3 === '')) {
                    salida.push(val_3);
                }
                if (salida.length === 0) {
                    salida = 0;
                }
                return salida;
            }
        })

        if (formValues) {
            let suma = 0;
            for (let i = 0; i < (formValues.length); i++) {
                suma += parseFloat(formValues[i].split(':')[1]);
            }
            console.log(suma);
            if (isNaN(suma)) {
                Swal.fire({
                    icon: 'error',
                    title: 'Atención...',
                    text: 'Hubo un error al ingresar las cancelaciones.',
                })
            } else if (suma.toFixed(2) > 1000) {
                document.getElementById(input_val).value = 0;
                document.getElementById(input_span).innerHTML = '$ 0.00';
                Swal.fire({
                    icon: 'error',
                    title: 'Atención...',
                    text: 'La suma de cancelaciones excede $ 1,000 dólares.',
                })
            } else if (suma.toFixed(2) <= 1000) {
                document.getElementById(input_val).value = formValues;
                document.getElementById(input_span).innerHTML = '$ ' + suma.toFixed(2).toString();
            }
            console.log(formValues);
        }
        if (!formValues) {
            document.getElementById(input_val).value = 0;
            document.getElementById(input_span).innerHTML = '$ 0.00';
        }
    })()
    if (c_saldo.value && c_saldo.value !== 0 && c_saldo.value !== "0"){
        let compras = c_saldo.value.split(',');
        console.log(compras.length);
        console.log(compras[0]);
        if (compras.length > 0){
            document.getElementById('swal-input1').value = compras[0];
        }if (compras.length > 1){
            document.getElementById('swal-input2').value = compras[1];
        }if (compras.length > 2){
            document.getElementById('swal-input3').value = compras[2];
        }
    }else{
        let val_1 = '';
    }
}

async function global_fetch(action, parameters=new FormData){
    if (!('action' in parameters)){
        parameters.append('action', action);
    }
    let url = window.location.pathname;
    let csrftoken = getCookie('csrftoken');
    let response = await fetch(url, {method: 'POST', body:parameters, headers:{"X-CSRFToken": csrftoken}});
    return response.json();
}