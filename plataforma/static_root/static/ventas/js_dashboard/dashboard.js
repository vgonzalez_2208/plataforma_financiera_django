// function global_fetch(url, parameters) {
//     let salida = 'test';
//     fetch(url, {
//         method: 'POST', // or 'PUT'
//         body: parameters, // data can be `string` or {object}!
//     }).then(res => res.json())
//         .catch(error => console.error('Error:', error))
//         .then(function (response) {
//             console.log(response);
//             salida = response;
//             if (!response.hasOwnProperty('error')) {
//                 return response;
//             } else {
//                 alert(response['error']);
//             }
//         });
//     return salida;
// }

async function info_dashboard() {
    let parameters = new FormData(document.getElementById("token_form"));
    parameters.append('action', 'cargar_resumen');
    let url = window.location.pathname;
    let response = await fetch(url, {method: 'POST', body: parameters})
    await response.json().then(function (resumen) {
        // console.log(resumen);
        let num_aprobaciones = document.getElementById('num_aprobaciones');
        let num_preaprobaciones = document.getElementById('num_preaprobaciones');
        let num_enOD = document.getElementById('num_enOD');
        let num_empresas = document.getElementById('num_empresas');
        num_aprobaciones.innerHTML = resumen['aprobaciones'];
        num_preaprobaciones.innerHTML = resumen['preaprobaciones'];
        num_enOD.innerHTML = resumen['OD'];
        num_empresas.innerHTML = resumen['empresas'];
        if (resumen['aprobaciones'] === 0) {
            box_class_success('box_aprobaciones', 1);
        } else {
            box_class_success('box_aprobaciones', 2);
        }
        if (resumen['preaprobaciones'] === 0) {
            box_class_success('box_preaprobaciones', 1);
        } else {
            box_class_success('box_preaprobaciones', 2);
        }
        if (resumen['OD'] === 0) {
            box_class_success('box_OD', 1);
        } else {
            box_class_success('box_OD', 2);
        }
        if (resumen['empresas'] === 0) {
            box_class_success('box_empresas', 1);
        } else {
            box_class_success('box_empresas', 2);
        }
        // let diccionario = resumen['reg_empresas'];
        // // console.log(resumen['reg_empresas']);
        // let lista = document.getElementById('li_registros');
        // Object.keys(diccionario).forEach(function (key) {
        //     let li = document.createElement('li');
        //     li.innerHTML = 'Fecha: ' + key + ' >> ' + '<b>' + diccionario[key] + '</b>' + ' registros';
        //     lista.appendChild(li);
        //     // console.log(key, diccionario[key]);
        // })
        // let today = new Date();
        // let dd = String(today.getDate()).padStart(2, '0');
        // let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        // let yyyy = today.getFullYear();
        // let fecha_hoy = yyyy + '-' + mm + '-' + dd;
        // // console.log(fecha_hoy);
        // if (fecha_hoy in diccionario){
        //     let num_reg_hoy = document.getElementById('num_reg_hoy');
        //     num_reg_hoy.innerHTML = '<b>' + diccionario[fecha_hoy] + '</b>';
        // }else {
        //     let num_reg_hoy = document.getElementById('num_reg_hoy');
        //     num_reg_hoy.innerHTML = '<b>' + '0' + '</b>';
        // }
        if ('empresas_faltantes' in resumen) {
            let reg_emp_pend = resumen['empresas_faltantes'];
            let num_emp_pend = reg_emp_pend.length;
            document.getElementById('num_emp_pendientes').innerText = num_emp_pend.toString();
            // console.log(reg_emp_pend);
            let lista_2 = document.getElementById('li_emp_pend');
            reg_emp_pend.forEach(function (info) {
                let li_2 = document.createElement('li');
                li_2.innerHTML = ' >> ' + '<b>' + info[0] + '</b> ' + '<a href="/prestamos/aprobados/edit/' + info[3] + '/aprobado_edit-' + info[2] + '/">' + info[1] + '</a>';
                lista_2.appendChild(li_2);
            })
        }
        if ('cifras_rc' in resumen) {
            response = resumen['cifras_rc'];
            let box_1 = document.getElementById('num_aprobados');
            let box_2 = document.getElementById('monto_aprobaciones');
            let box_3 = document.getElementById('aprobados_m_pr');
            let box_4 = document.getElementById('aprobados_p_pr');
            let ct_box_1 = document.getElementById('num_contratos');
            let ct_box_2 = document.getElementById('monto_contratos');
            let ct_box_3 = document.getElementById('contratos_m_pr');
            let ct_box_4 = document.getElementById('contratos_p_pr');
            box_1.innerText = response['aprobaciones_total'];
            box_2.innerText = f_dollar.format(response['suma_monto_aprobados']);
            box_3.innerText = f_dollar.format(response['monto_pr']);
            box_4.innerText = response['plazo_pr'];
            ct_box_1.innerText = response['contratos_total'];
            ct_box_2.innerText = f_dollar.format(response['suma_monto_contratos']);
            ct_box_3.innerText = f_dollar.format(response['ct_monto_pr']);
            ct_box_4.innerText = response['ct_plazo_pr'];
            // Info de mes anterior
            let box_1_ant = document.getElementById('num_ap_ant');
            let box_2_ant = document.getElementById('monto_ap_ant');
            let box_3_ant = document.getElementById('ap_m_pr_ant');
            let box_4_ant = document.getElementById('ap_p_pr_ant');
            let ct_box_1_ant = document.getElementById('num_ct_ant');
            let ct_box_2_ant = document.getElementById('monto_ct_ant');
            let ct_box_3_ant = document.getElementById('ct_m_pr_ant');
            let ct_box_4_ant = document.getElementById('ct_p_pr_ant');
            box_1_ant.innerText = response['aprobaciones_total_ant'] + ' mes ant.';
            box_2_ant.innerText = f_dollar.format(response['suma_monto_aprobados_ant']) + ' mes ant.';
            box_3_ant.innerText = f_dollar.format(response['monto_pr_ant']) + ' mes ant.';
            box_4_ant.innerText = response['plazo_pr_ant'] + ' mes ant.';
            ct_box_1_ant.innerText = response['contratos_total_ant'] + ' mes ant.';
            ct_box_2_ant.innerText = f_dollar.format(response['suma_monto_contratos_ant']) + ' mes ant.';
            ct_box_3_ant.innerText = f_dollar.format(response['ct_monto_pr_ant']) + ' mes ant.';
            ct_box_4_ant.innerText = response['ct_plazo_pr_ant'] + ' mes ant.';
        }
        if ('info_admin' in resumen) {
            let num_registro = document.getElementById('num_op_registro');
            let num_revision = document.getElementById('num_op_revision');
            let num_enviadas = document.getElementById('num_op_enviadas');
            if ('num_registro' in resumen['info_admin']) {
                box_class_admin('box_op_registro', 2);
                num_registro.innerHTML = resumen['info_admin']['num_registro'];
            } else {
                box_class_admin('box_op_registro', 1);
                num_registro.innerHTML = '0';
            }
            if ('num_revision' in resumen['info_admin']) {
                box_class_admin('box_op_revision', 0);
                num_revision.innerHTML = resumen['info_admin']['num_revision'];
            } else {
                box_class_admin('box_op_revision', 1);
                num_revision.innerHTML = '0';
            }
            if ('num_enviadas' in resumen['info_admin']) {
                box_class_admin('box_op_enviadas', 3);
                num_enviadas.innerHTML = resumen['info_admin']['num_enviadas'];
            } else {
                box_class_admin('box_op_enviadas', 1);
                num_enviadas.innerHTML = '0';
            }
        }
    });
}

function box_class_success(id, tipo) {
    let box = document.getElementById(id);
    if (tipo === 1) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-success');
    } else {
        box.classList.remove('bg-success');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-warning');
    }
}

function box_class_admin(id, tipo) {
    let box = document.getElementById(id);
    if (tipo === 1) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-success');
    } else if (tipo === 2) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-success');
        box.classList.add('bg-danger');
    } else if (tipo === 3) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-danger');
        box.classList.remove('bg-success');
        box.classList.add('bg-info');
    } else {
        box.classList.remove('bg-success');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-warning');
    }
}

