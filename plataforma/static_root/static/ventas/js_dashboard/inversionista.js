function info_dashboard_carlos() {
    let parameters = new FormData(document.getElementById("token_form"));
    parameters.append('action', 'dashboard_inversor');
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            console.log(response);
            if (!response.hasOwnProperty('error')) {
                let box_1 = document.getElementById('num_aprobaciones');
                let box_2 = document.getElementById('monto_aprobaciones');
                let box_3 = document.getElementById('aprobados_m_pr');
                let box_4 = document.getElementById('aprobados_p_pr');
                let ct_box_1 = document.getElementById('num_contratos');
                let ct_box_2 = document.getElementById('monto_contratos');
                let ct_box_3 = document.getElementById('contratos_m_pr');
                let ct_box_4 = document.getElementById('contratos_p_pr');
                box_1.innerText = response['aprobaciones_total'];
                box_2.innerText = f_dollar.format(response['suma_monto_aprobados']);
                box_3.innerText = f_dollar.format(response['monto_pr']);
                box_4.innerText = response['plazo_pr'];
                ct_box_1.innerText = response['contratos_total'];
                ct_box_2.innerText = f_dollar.format(response['suma_monto_contratos']);
                ct_box_3.innerText = f_dollar.format(response['ct_monto_pr']);
                ct_box_4.innerText = response['ct_plazo_pr'];
                // Info de mes anterior
                let box_1_ant = document.getElementById('num_ap_ant');
                let box_2_ant = document.getElementById('monto_ap_ant');
                let box_3_ant = document.getElementById('ap_m_pr_ant');
                let box_4_ant = document.getElementById('ap_p_pr_ant');
                let ct_box_1_ant = document.getElementById('num_ct_ant');
                let ct_box_2_ant = document.getElementById('monto_ct_ant');
                let ct_box_3_ant = document.getElementById('ct_m_pr_ant');
                let ct_box_4_ant = document.getElementById('ct_p_pr_ant');
                box_1_ant.innerText = response['aprobaciones_total_ant'] + ' mes ant.';
                box_2_ant.innerText = f_dollar.format(response['suma_monto_aprobados_ant']) + ' mes ant.';
                box_3_ant.innerText = f_dollar.format(response['monto_pr_ant']) + ' mes ant.';
                box_4_ant.innerText = response['plazo_pr_ant'] + ' mes ant.';
                ct_box_1_ant.innerText = response['contratos_total_ant'] + ' mes ant.';
                ct_box_2_ant.innerText = f_dollar.format(response['suma_monto_contratos_ant']) + ' mes ant.';
                ct_box_3_ant.innerText = f_dollar.format(response['ct_monto_pr_ant']) + ' mes ant.';
                ct_box_4_ant.innerText = response['ct_plazo_pr_ant'] + ' mes ant.';
                if ('info_gerencia' in response) {
                    let num_desembolsos = document.getElementById('num_desembolsos');
                    let num_preaprobaciones = document.getElementById('num_preaprobaciones');
                    let num_enOD = document.getElementById('num_enOD');
                    let num_empresas = document.getElementById('num_empresas');
                    num_desembolsos.innerHTML = response['info_gerencia']['aprobaciones'];
                    num_preaprobaciones.innerHTML = response['info_gerencia']['preaprobaciones'];
                    num_enOD.innerHTML = response['info_gerencia']['OD'];
                    num_empresas.innerHTML = response['info_gerencia']['empresas'];
                    if (response['info_gerencia']['aprobaciones'] === 0) {
                        box_class_success('box_desembolsos', 1);
                    } else {
                        box_class_success('box_desembolsos', 2);
                    }
                    if (response['info_gerencia']['preaprobaciones'] === 0) {
                        box_class_success('box_preaprobaciones', 1);
                    } else {
                        box_class_success('box_preaprobaciones', 2);
                    }
                    if (response['info_gerencia']['OD'] === 0) {
                        box_class_success('box_OD', 1);
                    } else {
                        box_class_success('box_OD', 2);
                    }
                    if (response['info_gerencia']['empresas'] === 0) {
                        box_class_success('box_empresas', 1);
                    } else {
                        box_class_success('box_empresas', 2);
                    }
                    if ('empresas_faltantes' in response['info_gerencia']) {
                        let reg_emp_pend = response['info_gerencia']['empresas_faltantes'];
                        let num_emp_pend = reg_emp_pend.length;
                        document.getElementById('num_emp_pendientes').innerText = num_emp_pend.toString();
                        // console.log(reg_emp_pend);
                        let lista_2 = document.getElementById('li_emp_pend');
                        reg_emp_pend.forEach(function (info) {
                            let li_2 = document.createElement('li');
                            li_2.innerHTML = ' >> ' + '<b>' + info[0] + '</b> ' + '<a href="/prestamos/aprobados/edit/' + info[3] + '/aprobado_edit-' + info[2] + '/">' + info[1] + '</a>';
                            lista_2.appendChild(li_2);
                        })
                    }
                }
            }
        })
}

function box_class_success(id, tipo) {
    let box = document.getElementById(id);
    if (tipo === 1) {
        box.classList.remove('bg-warning');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-success');
    } else {
        box.classList.remove('bg-success');
        box.classList.remove('bg-info');
        box.classList.remove('bg-danger');
        box.classList.add('bg-warning');
    }
}

