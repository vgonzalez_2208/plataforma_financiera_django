var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('form_cotizador'),
        {
            fields: {
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                inicio_pagos: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'monto':
                            case 'plazo_meses':
                            case 'inicio_pagos':
                                return '.col-md-3';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
                /*recaptcha: new FormValidation.plugins.Recaptcha({
                    element: 'g-captcha',
                    message: 'The captcha is not valid',
                    // Replace with the site key provided by Google
                    siteKey: '6LdB08gaAAAAAMZmG-jcL-3_QFvffJS3Wpac7VLW',
                    theme: 'light',
                }),*/
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        var parameters = $('#form_cotizador').serializeArray();
        var id_modelo = document.getElementById("id_modelo").value;
        console.log(parameters);
        // console.log(parameters);
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataType: 'json',
        }).done(function (data) {
            // alert('guardado');
            if (!data.hasOwnProperty('error')) {
                console.log(data);
                if (data[0] === 'detalles') {
                    window.open('/ventas/solicitudes/proceso/detalles/' + id_modelo + '/', '_blank');
                    return false;
                } else if (data[0] === 'cotizacion'){
                    window.open('/ventas/solicitudes/proceso/cotizacion/' + id_modelo + '/', '_blank');
                    return false;
                }
            }
            message_error(data.error)
        }).fail(function (data) {
            alert("error");
        }).always(function (data) {
            // alert("complete")
        });
    });

    $('[name="inicio_pagos"]')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('inicio_pagos');
        });
});






