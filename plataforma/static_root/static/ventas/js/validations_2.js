var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('form_edit'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_doc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                f_nacimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                genero: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                e_civil: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                dependientes: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9000.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                empresa_1: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ'"Üü ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                in_extras: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                estudios: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                p_expuesto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_meses: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                provincia: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                distrito: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                corregimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ'"Üü ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                apc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                terms: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                motivo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_doc':
                            case 'dni':
                            
                            case 'tipo_ingreso':
                            case 'estudios':
                            case 'p_expuesto':
                            case 'corregimiento':
                            case 'apc':
                            case 'terms':
                            case 'check_1':
                            case 'check_2':
                            case 'plazo_meses':
                                return '.col-md-3';
                            
                            case 'genero':
                            case 'celular':
                            case 'f_nacimiento':    
                            case 'e_civil':
                            case 'dependientes':
                            case 'salario':
                            case 'in_extras':
                            case 'monto':
                            case 'provincia':
                            case 'distrito':
                                return '.col-md-2';
                            
                            case 'motivo':   
                            case 'email_1':
                                return '.col-md-4';

                            case 'empresa_1':
                            case 'direccion':
                            case 'sda':
                                return '.col-md-5';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
                /*recaptcha: new FormValidation.plugins.Recaptcha({
                    element: 'g-captcha',
                    message: 'The captcha is not valid',
                    // Replace with the site key provided by Google
                    siteKey: '6LdB08gaAAAAAMZmG-jcL-3_QFvffJS3Wpac7VLW',
                    theme: 'light',
                }),*/
            },
        }
    ).on('core.form.valid', function (event) {
            var parameters = new FormData(document.getElementById("form_edit"));
            var id_modelo = document.getElementById("id_modelo").value;
            console.log(parameters);
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: parameters,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function (data) {
                console.log(data);
                if (data.hasOwnProperty('vista_edit')){
                    location.href = '/ventas/solicitudes/nuevas/ver/' + id_modelo + '/';
                    return false;
                }else if (data.hasOwnProperty('error')) {
                    message_error(data.error);
                }else if (data.hasOwnProperty('vista_lista')){
                    location.href = '/ventas/solicitudes/nuevas/';
                    return false;
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
            }).always(function (data) {
                console.log(data);
            });
        });

    $('[name="f_nacimiento"]')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            endDate: '-22y'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_nacimiento');
        });
});






