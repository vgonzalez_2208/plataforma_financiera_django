var csrftoken = getCookie('csrftoken');

let preaprobados_1 = $('#tabla_pre_1').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 5,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'pre-aprobado_1'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "id"},
        {"data": "nombre_1"},
        {"data": "apellido_1"},
        {"data": "tipo_ingreso"},
        {"data": "salario"},
        {"data": "f_solicitud"},
        {"data": "usuario"},
        {"data": "documentos"},
        {"data": "estatus"},
        {"data": "botones"},
        // {"data": "genero"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let buttons;
                // console.log(data)
                if (data === 'En revisión') {
                    buttons = '<span class="badge badge-pill badge-secondary">---</span>';
                } else if (data === 'Aprobado') {
                    buttons = '<a href="/prestamos/aprobados/ver/' + row.id + '/" class="btn btn-primary btn-xs "><i class="fas fa-eye"></i></a>';
                } else if (data === 'Rechazado') {
                    buttons = '<button type="button" onclick="rechazado_info(' + row.id + ')" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_rechazados"><i class="fas fa-info-circle"></i></button>';
                } else {
                    console.log(data);
                    let info = data.split('/');
                    if (info.length === 2){
                        buttons = '<a href="/prestamos/aprobados/ver/' + info[0] + '/" class="btn btn-primary btn-xs "><i class="fas fa-eye"></i></a>&nbsp';
                        buttons += '<a href="/prestamos/aprobados/desembolsados/' + info[0] + '/new/" class="btn btn-success btn-xs "><i class="fas fa-user"></i>';
                    }
                    // buttons = '<span class="badge badge-pill badge-secondary">---</span>'; aprobados/desembolsados/
                }
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                let valor;
                if (data === 'En revisión') {
                    valor = '<span class="badge badge-pill badge-warning">En revisión</span>';
                } else if (data === 'Aprobado') {
                    valor = '<span class="badge badge-pill badge-primary">Aprobado</span>';
                } else if (data === 'Rechazado') {
                    valor = '<span class="badge badge-pill badge-danger">Rechazado</span>';
                } else if (data === 'Desembolsado') {
                    valor = '<span class="badge badge-pill badge-success">Desembolsado</span>';
                }
                return valor;
            },
        },
        {
            targets: [-3],
            class: 'text-center',
            render: function (data, type, row) {
                let valor;
                if (data === 'Completo') {
                    valor = '<span class="badge badge-pill badge-success">Completo</span>';
                } else if (data === 'Incompleto') {
                    valor = '<span class="badge badge-pill badge-warning">Incompleto</span>';
                } else {
                    valor = '<span class="badge badge-pill badge-danger">Ninguno</span>';
                }
                return valor;
            },
        },
        {
            targets: [-5],
            class: 'text-center',
            render: function (data, type, row) {
                return '<span class="badge badge-secondary">' + data + '</span>';
            },
        },
        {
            targets: [1, 2],
            render: function (data, type, row) {
                return '<span style="text-transform:capitalize">' + data + '</span>';
            },
        },
    ],
    initComplete: function (settings, json) {
        this.api().columns([3, 5, 8]).every(function () {
            let column = this;
            // console.log(column['0'][0]);
            let placeholder;
            if (column['0'][0] === 3) {
                placeholder = 'Seleccione...';
            } else if (column['0'][0] === 5) {
                placeholder = 'Sel...';
            }else if (column['0'][0] === 8) {
                placeholder = 'Seleccione...';
            }
            let select = $('<select class="form-control"><option class="text-muted" value="">' + placeholder + '</option></select>')
                .appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();
                });
            column.data().unique().sort().each(function (d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    }
});

$(function () {
    $('#tabla_pre_2').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'pre-aprobado_2'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "usuario"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/preaprobadas/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<a href="/ventas/solicitudes/preaprobadas/edit/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fas fa-edit"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Completo') {
                        valor = '<span class="badge badge-pill badge-success">Completo</span>';
                    } else if (data === 'Incompleto') {
                        valor = '<span class="badge badge-pill badge-warning">Incompleto</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">Ninguno</span>';
                    }
                    return valor;
                },
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});

function rechazado_info(id_cliente) {
    document.getElementById('id_last_client').value = id_cliente;
    let parameters = new FormData(document.getElementById('token_form'));
    parameters.append('action', 'info_rechazado');
    parameters.append('id', id_cliente);
    let url = window.location.pathname;
    fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
        .catch(error => console.log(error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                document.getElementById('n_cliente').innerHTML = response['n_cliente'];
                document.getElementById('n_gerencia').innerHTML = response['gerencia'];
                document.getElementById('n_cumplimiento').innerHTML = response['cumplimiento'];
                document.getElementById('n_ventas').innerHTML = response['ventas'];
            } else {
                message_error(response);
            }
        });
}

function recuperar_cliente() {
    $('#modal_rechazados').modal('hide');
    // let id_cliente = document.getElementById('id_last_client').value;
    Swal.fire({
        title: '¿Desea recuperar la solicitud pre-aprobada?',
        text: "La solicitud será devuelta a comité.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, recuperar!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            let parameters = new FormData(document.getElementById('token_form'));
            parameters.append('action', 'recuperar_cliente');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(error => console.log(error))
                .then(function (response) {
                    if (!response.hasOwnProperty('error')) {
                        if ('ok' in response) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se recuperó la solicitud.',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            preaprobados_1.ajax.reload();
                        }
                    }
                })
        }
    })
}