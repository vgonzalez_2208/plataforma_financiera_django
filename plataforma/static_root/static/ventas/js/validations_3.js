var fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('form_empresa'),
        {
            fields: {
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ0-9'"ÜüÁÉÍÓÚ., ]+$/,
                            message: 'Escriba un nombre válido.'
                        }
                    }
                },
                r_social: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑÁÉÍÓÚ0-9., ]+$/,
                            message: 'Escriba un nombre válido.'
                        }
                    }
                },
                ruc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9- ]+$/,
                            message: 'Escriba un RUC válido.'
                        }
                    }
                },
                sector: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                sub_sector: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                tipo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                alcance: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                nombre_contacto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ0-9'"ÜüÁÉÍÓÚ. ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_contacto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ0-9'"ÜüÁÉÍÓÚ. ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 7,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 7,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                certificado_pe: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        uri: {
                            message: 'Dirección web inválida.'
                        }
                    }
                },
                certificado_pj: {
                    validators: {
                        uri: {
                            message: 'Dirección web inválida.'
                        }
                    }
                },
                pagina_web: {
                    validators: {
                        uri: {
                            message: 'Dirección web inválida.'
                        }
                    }
                },
                provincia: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                distrito: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                corregimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 100,
                            message: 'Mínimo 3 letras, máximo 100.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑÁÉÍÓÚ,. ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                notas: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 500,
                            message: 'Mínimo 3 letras, máximo 500.'
                        }
                    }
                },
                fecha_const: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_const: {
                            format: 'DD/MM/YYYY',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre':
                            case 'ruc':
                            case 'negocio':
                            case 'tipo':
                            case 'r_social':
                            case 'nombre_contacto':
                            case 'apellido_contacto':
                            case 'corregimiento':
                            case 'sector':
                            case 'sub_sector':
                                return '.col-md-3';

                            case 'alcance':
                            case 'telefono':
                            case 'celular':
                            case 'provincia':
                            case 'distrito':
                            case 'fecha_const':
                                return '.col-md-2';

                            case 'direccion':
                                return '.col-md-5';

                            case 'email':
                            case 'certificado_pe':
                            case 'certificado_pj':
                            case 'pagina_web':
                                return '.col-md-4';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
                /*recaptcha: new FormValidation.plugins.Recaptcha({
                    element: 'g-captcha',
                    message: 'The captcha is not valid',
                    // Replace with the site key provided by Google
                    siteKey: '6LdB08gaAAAAAMZmG-jcL-3_QFvffJS3Wpac7VLW',
                    theme: 'light',
                }),*/
            },
        }
    ).on('core.form.valid', function () {
        var parameters = $('#form_empresa').serializeArray();
        let val_action = document.getElementById('action');
        console.log(parameters);
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataType: 'json'
        }).done(function (data) {
            console.log(data);
            if (!data.hasOwnProperty('error')) {
                if (val_action.value === 'edit') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Se guardaron los cambios!',
                        showConfirmButton: false
                    });
                } else if (val_action.value === 'add'){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡La empresa ha sido creada!',
                        showConfirmButton: false
                    });
                }
                setTimeout(function () {
                    location.href = '/ventas/empresas/';
                }, 1500);
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {

        });
    });

    $('[name="fecha_const"]')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            endDate: '-5y'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_const');
        });
});






