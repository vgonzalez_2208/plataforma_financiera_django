let fileinput_1 = document.getElementById('id_DNI_img');
let fileinput_2 = document.getElementById('id_carta_trabajo_img');
let fileinput_3 = document.getElementById('id_talonario_1_img');
let fileinput_4 = document.getElementById('id_talonario_2_img');
let fileinput_5 = document.getElementById('id_ficha_img');
let fileinput_6 = document.getElementById('id_recibo_img');
let fileinput_7 = document.getElementById('id_carta_saldo_img');
let fileinput_8 = document.getElementById('id_file_apc');
// var preview = document.getElementById('preview');
let form = document.getElementById('edit_proceso');

// this is where it starts. event triggered when user selects files
fileinput_1.onchange = function () {
    validar_img(fileinput_1, 'dni_span', 1, 'image_1', form)
}

fileinput_2.onchange = function () {
    validar_img(fileinput_2, 'carta_trabajo_span', 2, 'image_2', form)
}

fileinput_3.onchange = function () {
    validar_img(fileinput_3, 'talonario_1_span', 3, 'image_3', form)
}

fileinput_4.onchange = function () {
    validar_img(fileinput_4, 'talonario_2_span', 4, 'image_4', form)
}

fileinput_5.onchange = function () {
    validar_img(fileinput_5, 'ficha_span', 5, 'image_5', form)
}

fileinput_6.onchange = function () {
    validar_img(fileinput_6, 'recibo_span', 6, 'image_6', form)
}

fileinput_7.onchange = function () {
    validar_img(fileinput_7, 'c_saldo_span', 7, 'image_7', form)
}

fileinput_8.onchange = function () {
    validar_img(fileinput_8, 'apc_file_span', 8, 'not_image', form, 1)
}

