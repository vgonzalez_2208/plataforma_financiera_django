// let form = document.getElementById('desembolso');
//
// form.onsubmit = function (event) {
//     event.preventDefault();
//     Swal.fire({
//         title: '¿Desea guardar los cambios?',
//         showDenyButton: true,
//         showCancelButton: true,
//         confirmButtonText: 'Guardar',
//         denyButtonText: `No guardar`,
//     }).then((result) => {
//         /* Read more about isConfirmed, isDenied below */
//         if (result.isConfirmed) {
//             Swal.fire({
//                 title: '¿Desea generar el desembolso?',
//                 showDenyButton: true,
//                 showCancelButton: true,
//                 confirmButtonText: 'Si',
//                 denyButtonText: `No`,
//             }).then((result) => {
//                 if (result.isConfirmed) {
//                     let parameters = new FormData(this);
//                     parameters.append('action', 'desembolso');
//                     parameters.forEach(value => console.log(value));
//                     let url = window.location.pathname;
//                     fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
//                         .catch(error => console.log(error))
//                         .then(function (response) {
//                             if (!response.hasOwnProperty('error')) {
//                                 if ('advertencia' in response) {
//                                     Swal.fire(response['advertencia']);
//                                 }
//                             } else if ('ok' in response) {
//                                 Swal.fire({
//                                     position: 'top-end',
//                                     icon: 'success',
//                                     title: 'Se creó el desembolso.',
//                                     showConfirmButton: false,
//                                     timer: 1500
//                                 })
//                             }
//                         });
//                 } else if (result.isDenied) {
//                     let parameters = new FormData(this);
//                     parameters.append('action', 'guardar_datos');
//                     parameters.forEach(value => console.log(value));
//                     let url = window.location.pathname;
//                     fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
//                         .catch(error => console.log(error))
//                         .then(function (response) {
//                             if (!response.hasOwnProperty('error')) {
//                                 if ('advertencia' in response) {
//                                     Swal.fire(response['advertencia']);
//                                 }
//                             } else if ('ok' in response) {
//                                 Swal.fire({
//                                     position: 'top-end',
//                                     icon: 'success',
//                                     title: 'Se guardaron los datos.',
//                                     showConfirmButton: false,
//                                     timer: 1500
//                                 })
//                             }
//                         });
//                 }
//             })
//         }
//     })
//
// }

document.addEventListener('DOMContentLoaded', function (e) {
    fv = FormValidation.formValidation(
        document.getElementById('desembolso'),
        {
            fields: {
                cliente: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                fecha_ini: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                f_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ini_OD: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                ob_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 10000,
                            message: 'Ingrese un monto entre 300 y 10,000'
                        }
                    }
                },
                c_cierre: {
                    validators: {}
                },
                compra_saldo: {
                    validators: {
                        stringLength: {
                            min: 1,
                            max: 150,
                            message: 'Mínimo 6 letras'
                        },
                        regexp: {
                            regexp: /^[a-zA-Záéíóúñ0-9:Ñ,. ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                c_promotor: {
                    validators: {}
                },
                s_descuento: {
                    validators: {}
                },
                estatus: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                num_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^(([0-9]{4})+)-(([0-9]{2})+)-([0-9]{4})$/i,
                            message: 'Formato incorrecto.'
                        }
                    }
                },
                notas: {
                    validators: {}
                },

            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'ob_contrato'://
                            case 'c_cierre'://
                            case 'compra_saldo'://
                            case 'c_promotor'://
                            case 's_descuento'://
                            case 'estatus'://
                            case 'fecha_ini_OD'://
                            case 'f_contrato': //
                            case 'num_contrato'://
                                return '.col-3';

                            case 'cliente':
                                return '.col-6';

                            case 'monto':
                            case 'plazo':
                            case 'fecha_ini':
                                return '.col-2';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // event.preventDefault();
        Swal.fire({
            title: '¿Desea guardar los cambios?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Guardar',
            denyButtonText: `No guardar`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                Swal.fire({
                    title: '¿Desea generar el desembolso?',
                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Si',
                    denyButtonText: `No`,
                }).then((result) => {
                    if (result.isConfirmed) {
                        let parameters = new FormData(document.getElementById('desembolso'));
                        parameters.append('action', 'desembolso');
                        parameters.forEach(value => console.log(value));
                        let url = window.location.pathname;
                        fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                            .catch(error => console.log(error))
                            .then(function (response) {
                                if (!response.hasOwnProperty('error')) {
                                    if ('advertencia' in response) {
                                        Swal.fire(response['advertencia']);
                                    } else if ('ok' in response) {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Se creó el desembolso.',
                                            showConfirmButton: false,
                                            timer: 1500
                                        })
                                        setTimeout(function () {
                                            location.href = '/prestamos/aprobados/lista_ajuste/';
                                        }, 1500);
                                        return false;
                                    }
                                }
                            });
                    } else if (result.isDenied) {
                        let parameters = new FormData(document.getElementById('desembolso'));
                        parameters.append('action', 'guardar_datos');
                        // parameters.forEach(value => console.log(value));
                        let url = window.location.pathname;
                        fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                            .catch(error => console.log(error))
                            .then(function (response) {
                                console.log(response);
                                if (!response.hasOwnProperty('error')) {
                                    if ('advertencia' in response) {
                                        Swal.fire(response['advertencia']);
                                    } else if ('ok' in response) {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Se guardaron los datos.',
                                            showConfirmButton: false,
                                            timer: 1500
                                        })
                                        setTimeout(function () {
                                            location.href = '/prestamos/aprobados/lista_ajuste/';
                                        }, 1500);
                                        return false;
                                    }
                                }
                            });
                    }
                })
            }
        })
    });
    $('[id="id_fecha_ini"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: 'bottom',
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini');
        });
    $('[id="id_f_contrato"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: 'bottom',
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_contrato');
        });
    $('[id="id_fecha_ini_OD"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: 'bottom',
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini_OD');
        });
});

function registro_empresa() {
    Swal.fire({
        title: '¿Desea guardar los cambios?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Si',
        denyButtonText: `No`,
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            let parameters = new FormData(document.getElementById('reg_empresa_2'));
            parameters.append('action', 'registrar_empresa');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(error => console.log(error))
                .then(function (response) {
                    if (!response.hasOwnProperty('error')) {
                        if ('ok' in response) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se ha registrado la información de empresa.',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function () {
                                location.reload();
                            }, 1250);
                            return false;
                        } else if ('advertencia' in response) {
                            Swal.fire(response['advertencia']);
                        }
                    }
                })
        }
    })
    // alert('hola');
    // let parameters = new FormData(document.getElementById('desembolso'));
}