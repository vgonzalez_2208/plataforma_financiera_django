var contrato = document.getElementById('id_contrato');
var pagare = document.getElementById('id_pagare');

contrato.onchange = function (){
    let etiqueta = document.getElementById('contrato_span');
    doc_upload(contrato, etiqueta);
}

pagare.onchange = function (){
    let etiqueta = document.getElementById('pagare_span');
    doc_upload(pagare, etiqueta);
}

function doc_upload(campo, etiqueta){
    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
        alert('Archivo inválido.');
        return false;
    }
    let archivo = getExt(campo.files[0].name);
    if (!(archivo === 'pdf' || archivo === 'PDF')){
        Swal.fire('Sólo se admite archivo PDF')
        campo.value = "";
        return false;
    }
    etiqueta.innerHTML = campo.files[0].name;
    etiqueta.classList.remove("text-orange");
}