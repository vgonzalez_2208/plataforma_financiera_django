// var csrftoken;

let lista_operaciones = $('#lista_operaciones').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    "searching": false,
    "lengthChange": false,
    "bInfo": false,
    pageLength: 10,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'cargar_tabla'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    "createdRow": function (row, data, dataIndex) {
        if (data['opciones'] === 2) {
            $(row).addClass('table-warning');
        } else if (data['opciones'] === 3) {
            $(row).addClass('table-secondary');
        } else if (data['opciones'] === 1) {
            $(row).addClass('table-success');
        }
    },
    columns: [
        {"data": "contrato"},
        {"data": "nombre"},
        {"data": "apellido"},
        {"data": "tipo_ingreso"},
        {"data": "monto"},
        {"data": "plazo"},
        {"data": "estatus"},
        {"data": "id_cliente"},
        // {"data": "genero"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                return '<a href="/prestamos/aprobados/lista_operaciones/' + data + '/" class="btn btn-info btn-xs"><i class="fas fa-eye"></i></a>';
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                console.log(data, 'es datatable');
                let salida;
                if (data === 'pend_desembolso'){
                    salida = '<span class="badge badge-warning">Pendiente revisión</span>';
                }else if (data === 'env_desembolso'){
                    salida = '<span class="badge badge-primary">Enviado</span>';
                }else if (data === 'reg_desembolso'){
                    salida = '<span class="badge badge-danger">Registrar desembolso</span>';
                }
                return salida;
            },
        },
    ],
    initComplete: function (settings, json) {
    }
});