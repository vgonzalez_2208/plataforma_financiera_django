let fv_banco;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_banco = FormValidation.formValidation(
        document.getElementById('info_bancaria'),
        {
            fields: {
                nombre_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 5 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                numero_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            max: 30,
                            message: 'Mínimo 5 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9- ]+$/,
                            message: 'Escriba caracteres válidos.'
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_cuenta: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                notas: {
                    validators: {}
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_cuenta':
                            case 'numero_cuenta':
                            case 'notas':
                                return '.col-md-12';

                            case 'banco':
                            case 'tipo_cuenta':
                                return '.col-md-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        let parameters = new FormData(document.getElementById("info_bancaria"));
        let action_val = document.getElementById('action_info_banco').value;
        // console.log(parameters);
        let url = window.location.pathname;
        if (action_val === 'update_info_banco') {
            let parameters_check = $('#info_bancaria').serializeArray();
            let info = document.getElementById('info_recuperada').value;
            let val = JSON.parse(info.toString());
            console.log(val);
            console.log(parameters);
            let flag = 1;
            parameters_check.some(function (i) {
                if (i.name !== 'action' && i.name !== 'Si' && i.name !== 'csrfmiddlewaretoken') {
                    if (i.value !== val[i.name]) {
                        console.log(i.value, ' ', val[i.name]);
                        flag = 0;
                        Swal.fire({
                            title: '¿Desea guardar los cambios?',
                            showDenyButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Guardar',
                            denyButtonText: `No guardar`,
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                f_fetch_info(url, parameters);
                            } else if (result.isDenied) {
                                $('#modal_info_bancaria').modal('hide');
                            }
                        })
                    }
                    return i.value !== val[i.name];
                }
            });
            if (flag === 1) {
                $('#modal_parametros').modal('hide');
            }
        } else if (action_val === 'load_info_banco') {
            f_fetch_info(url, parameters);
        }
        // let parameters = new FormData(document.getElementById("info_bancaria"));


    });
});

function info_bancaria_cliente(id_cliente) {
    let parameters = new FormData(document.getElementById("token_form"));
    parameters.append('action', 'info_banco_cliente');
    parameters.append('id_cliente', id_cliente);
    // var id_form = document.getElementById('id_form').value;
    let url = window.location.pathname;
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                if ('Si' in response) {
                    document.getElementById('info_recuperada').value = JSON.stringify(response);
                    document.getElementById('action_info_banco').value = 'update_info_banco';
                    document.getElementById('id_nombre_cuenta').value = response['nombre_cuenta'];
                    document.getElementById('id_numero_cuenta').value = response['numero_cuenta'];
                    document.getElementById('id_banco').value = response['banco'];
                    document.getElementById('id_tipo_cuenta').value = response['tipo_cuenta'];
                    document.getElementById('id_notas').value = response['notas'];
                } else {
                    // alert('No existe');
                    document.getElementById('action_info_banco').value = 'load_info_banco';
                }
            } else {
                message_error(response);
            }
            console.log(response);
        });
}

function f_fetch_info(url, parameters) {
    console.log(parameters);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: parameters, // data can be `string` or {object}!
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(function (response) {
            if (!response.hasOwnProperty('error')) {
                $('#modal_info_bancaria').modal('hide');
                document.getElementById("info_bancaria").reset();
                fv_banco.resetForm(true);
                if ('Ok' in response) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response['Ok'],
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else if ('Ok_2' in response) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response['Ok_2'],
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            } else {
                message_error(response);
            }
            console.log(response);
        });
}

function pr_cargar() {
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'pr_cargar'
        },
        dataType: 'json',
    }).done(function (data) {
        // alert('guardado');
        if (!data.hasOwnProperty('error')) {
            document.getElementById('info_recuperada').value = JSON.stringify(data);
            document.getElementById('pr_monto').value = data['pr_monto'];
            document.getElementById('pr_plazo').value = data['pr_plazo'];
            document.getElementById('pr_inicio').value = data['pr_inicio'];
            document.getElementById('pr_c_cierre').value = data['pr_c_cierre'];
            if (data['pr_compra_saldo'] !== '0') {
                let info = data['pr_compra_saldo'].split(',');
                let suma = 0;
                info.forEach(i => suma += parseFloat(i.split(':')[1]));
                document.getElementById('cancelaciones_span').innerHTML = suma.toString();
                document.getElementById('pr_compra_saldo').value = data['pr_compra_saldo'];
            } else {
                document.getElementById('cancelaciones_span').innerHTML = '0';
            }
            document.getElementById('pr_c_promotor').value = data['pr_c_promotor'];
            document.getElementById('pr_s_descuento').value = data['pr_s_descuento'];
            document.getElementById('pr_f_contrato').value = data['pr_f_contrato'];
            return false;
        }
        message_error(data.error)
    }).fail(function (data) {
        alert("error");
    }).always(function (data) {
        // alert("complete")
    });
}


var fv_2;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv_2 = FormValidation.formValidation(
        document.getElementById('parametros_cambiar'),
        {
            fields: {
                pr_monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 3500.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                pr_plazo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6.00,
                            max: 30.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                pr_inicio: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        pr_inicio: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                pr_c_cierre: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                pr_c_promotor: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                pr_s_descuento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                pr_f_contrato: {
                    validators: {
                        pr_f_contrato: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'pr_monto':
                            case 'pr_plazo':
                            case 'pt_inicio':
                            case 'pr_c_cierre':
                            case 'pr_c_promotor':
                            case 'pr_descuento':
                            case 'pr_f_contrato':
                                return '.col-sm-6';

                            default:
                                return '.form-group';
                        }
                    }
                }),

                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        var parameters = $('#parametros_cambiar').serializeArray();
        let info = document.getElementById('info_recuperada').value;
        let val = JSON.parse(info.toString());
        let flag = 1;
        parameters.some(function (i) {
            if (i.name !== 'action') {
                if (i.value !== val[i.name]) {
                    flag = 0;
                    Swal.fire({
                        title: '¿Desea guardar los cambios?',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Guardar',
                        denyButtonText: `No guardar`,
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            (async () => {
                                const {value: text} = await Swal.fire({
                                    input: 'textarea',
                                    inputLabel: 'Comentarios',
                                    inputPlaceholder: 'Escriba sus comentarios aquí...',
                                    inputAttributes: {
                                        'aria-label': 'Escriba sus comentarios aquí'
                                    },
                                    showCancelButton: true
                                })
                                if (text) {
                                    parameters.push({name: 'comentario', value: text});
                                    $.ajax({
                                        url: window.location.pathname,
                                        type: 'POST',
                                        data: parameters,
                                        dataType: 'json',
                                    }).done(function (data) {
                                        // alert('guardado');
                                        if (!data.hasOwnProperty('error')) {
                                            // console.log(data);
                                            $('#modal_parametros').modal('hide');
                                            Swal.fire({
                                                position: 'top-end',
                                                icon: 'success',
                                                title: 'Parámetros del crédito actualizados.',
                                                showConfirmButton: false,
                                                timer: 1500
                                            });
                                            setTimeout(function () {
                                                location.reload();
                                            }, 1200);
                                            return false;
                                        }
                                        message_error(data.error)
                                    }).fail(function (data) {
                                        alert("error");
                                    }).always(function (data) {
                                        // alert("complete")
                                    });
                                } else if (!text) {
                                    Swal.fire('Debe ingresar sus comentarios para continuar.')
                                }
                            })()
                        } else if (result.isDenied) {
                            $('#modal_parametros').modal('hide');
                        }
                    })
                }
                return i.value !== val[i.name];
            }
        });
        if (flag === 1) {
            $('#modal_parametros').modal('hide');
        }
    });
    $('[name="pr_inicio"]')
        .datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('pr_inicio');
        });
    $('[name="pr_f_contrato"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv_2.revalidateField('pr_f_contrato');
        });
});

function enviar_operaciones() {
    Swal.fire({
        title: '¿Desea enviar el cliente a operaciones?',
        text: "Una vez enviado se diligenciará el desembolso con gerencia.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, enviar!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            let parameters = new FormData(document.getElementById('token_form'));
            parameters.append('action', 'enviar_operaciones');
            let url = window.location.pathname;
            fetch(url, {method: 'POST', body: parameters}).then(res => res.json())
                .catch(error => console.log(error))
                .then(function (response) {
                    console.log(response);
                    if ('ok' in response) {
                        Swal.fire({
                            title: '¡El cliente ha sido enviado a operaciones!',
                            text: "¿Desea volver a la lista de aprobados?",
                            icon: 'success',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, volver',
                            cancelButtonText: 'Cancelar',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = '/prestamos/aprobados/';
                            }
                        })
                    } else if ('advertencia' in response) {
                        Swal.fire({
                            title: 'EL cliente ya fue enviado a operaciones.',
                            text: "¿Desea revertir el envío?",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '¡Si, revertir!',
                            cancelButtonText: 'Cancelar',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                let parameters_2 = new FormData(document.getElementById('token_form'));
                                parameters_2.append('action', 'cancelar_operaciones');
                                fetch(url, {method: 'POST', body: parameters_2}).then(res => res.json())
                                    .catch(error => console.log(error))
                                    .then(function (response) {
                                        if ('ok' in response) {
                                            Swal.fire({
                                                title: '¡Envío revertido!',
                                                text: "¿Desea volver a la lista de aprobados?",
                                                icon: 'info',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Si, volver'
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                    window.location.href = '/prestamos/aprobados/';
                                                }
                                            })
                                        }
                                    })
                            }
                        })
                    } else if ('stop' in response) {
                        let lista = '';
                        response['stop'].forEach(function (obj) {
                            lista += '<li>' + obj + '</li>'
                        });
                        console.log(lista);
                        Swal.fire({
                            icon: 'error',
                            title: 'Hace falta la siguiente información:',
                            html: '<ul>' + lista + '</ul>',
                        })
                    }
                })
        }
    })

}