// var fileinput_1 = document.getElementById('od_f1');
// var max_width = 1920;  // Ancho
// var max_height = 1080;  // Alto
// // var preview = document.getElementById('preview');
// var form = document.getElementById('doc_od_f1');
//
// function getExt(file) {
//     let ext = file.split('.').pop();
//     if (ext === file) return "";
//     return ext;
// }
//
// function processfile(file, campo) {
//     if (!(/image/i).test(file.type)) {
//         alert("El archivo " + file.name + " no es válido.");
//         return false;
//     }
//     // read the files
//     var reader = new FileReader();
//     reader.readAsArrayBuffer(file);
//
//     reader.onload = function (event) {
//         // blob stuff
//         var blob = new Blob([event.target.result]); // create blob...
//         window.URL = window.URL || window.webkitURL;
//         var blobURL = window.URL.createObjectURL(blob); // and get it's URL
//
//         // helper Image object
//         var image = new Image();
//         image.src = blobURL;
//         //preview.appendChild(image); // preview commented out, I am using the canvas instead
//         image.onload = function () {
//             // have to wait till it's loaded
//             var resized = resizeMe(image); // send it to canvas
//             var newinput = document.createElement("input");
//             newinput.type = 'hidden';
//             newinput.name = 'image_' + campo;
//             console.log(newinput.name)
//             newinput.value = resized; // put result from canvas into new hidden input
//             // console.log(resized);
//             form.appendChild(newinput);
//         }
//     };
// }
//
// function readfiles(files) {
//     var existinginputs;
//     // remove the existing canvases and hidden inputs if user re-selects new pics
//     if (fileinput_1.value) {
//         existinginputs = document.getElementsByName('image_1');
//         while (existinginputs.length > 0) { // it's a live list so removing the first element each time
//             // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
//             form.removeChild(existinginputs[0]);
//             // preview.removeChild(existingcanvases[0]);
//         }
//         for (let i = 0; i < files.length; i++) {
//             processfile(files[i], 1); // process each file at once
//         }
//         fileinput_1.value = ""; //remove the original files from fileinput
//     }
//
// }
//
// // this is where it starts. event triggered when user selects files
// fileinput_1.onchange = function () {
//     if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
//         alert('Archivo inválido.');
//         return false;
//     }
//     document.getElementById('od_span').innerHTML = fileinput_1.files[0].name;
//     document.getElementById('od_span').classList.remove("text-orange");
//     let archivo = getExt(fileinput_1.files[0].name);
//     if (!(archivo === 'pdf' || archivo === 'PDF')){
//         readfiles(fileinput_1.files);
//     }
// }
//
// // === RESIZE ====
//
// function resizeMe(img) {
//
//     var canvas = document.createElement('canvas');
//     var width = img.width;
//     var height = img.height;
//
//     // calculate the width and height, constraining the proportions
//     if (width > height) {
//         if (width > max_width) {
//             //height *= max_width / width;
//             height = Math.round(height *= max_width / width);
//             width = max_width;
//         }
//     } else {
//         if (height > max_height) {
//             //width *= max_height / height;
//             width = Math.round(width *= max_height / height);
//             height = max_height;
//         }
//     }
//
//     // resize the canvas and draw the image data into it
//     canvas.width = width;
//     canvas.height = height;
//     var ctx = canvas.getContext("2d");
//     ctx.drawImage(img, 0, 0, width, height);
//
//     // preview.appendChild(canvas); // do the actual resized preview
//     return canvas.toDataURL("image/jpeg", 1); // get the data from canvas as 70% JPG (can be also PNG, etc.)
//
// }

/////////////////////////////////////////////

let fileinput_1 = document.getElementById('od_f1');
let form = document.getElementById('doc_od_f1');

fileinput_1.onchange = function () {
    validar_img(fileinput_1, 'od_span', 1, 'image_1', form)
}