var csrftoken = getCookie('csrftoken');

$(document).ready(function () {
    let table = $('#aprobados').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'aprobados'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "estatus"},
            {"data": "monto"},
            {"data": "plazo"},
            {"data": "num_contrato"},
            {"data": "opciones"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/prestamos/aprobados/crear_desembolso/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    return buttons;
                },
            },
        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {

        }
    });
    $("#update_cumplimiento").on("click", function () {
        console.log('sdsd');
        // table.button('.buttons-reload').trigger();
        table.ajax.reload();
    });
});

