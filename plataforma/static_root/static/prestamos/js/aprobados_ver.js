function c_aceptada() {
    let id_modelo = document.getElementById('id_modelo').value;
    Swal.fire({
        title: '¿Aceptó el cliente la nueva cotización?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2ba646',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, la aceptó!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'cotizacion_cliente'
                },
                dataType: 'json',
            }).done(function (data) {
                // alert('guardado');
                if (!data.hasOwnProperty('error')) {
                    console.log(data);
                    if (data.hasOwnProperty('ok')) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '¡Se marcó la cotización como aceptada por el cliente.!',
                            showConfirmButton: false
                        });
                        setTimeout(function () {
                            location.href = '/prestamos/aprobados/ver/' + id_modelo;
                        }, 1500);
                        return false;
                    }
                    return false;
                }
                message_error(data)
            }).fail(function (data) {
                alert("error");
            }).always(function (data) {
                // alert("complete")
            });
        }
    })
}

function c_descartada() {
    Swal.fire({
        title: '¿Rechazó el cliente la nueva cotización?',
        icon: 'warning',
        text: '',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#2ba646',
        confirmButtonText: '¡Sí!',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                title: '¿Qué desea hacer?',
                icon: 'warning',
                text: '',
                showCancelButton: true,
                showDenyButton: true,
                confirmButtonColor: '#2ba646',
                denyButtonColor: '#d33',
                confirmButtonText: 'Devolver a comité',
                denyButtonText: 'Anular',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                    if (result.isConfirmed) {
                        (async () => {
                            const {value: text} = await Swal.fire({
                                input: 'textarea',
                                inputLabel: 'Comentarios',
                                inputPlaceholder: 'Escriba sus comentarios aquí...',
                                inputAttributes: {
                                    'aria-label': 'Type your message here'
                                },
                                showCancelButton: true
                            })
                            if (text) {
                                $.ajax({
                                    url: window.location.pathname,
                                    type: 'POST',
                                    data: {
                                        'comentario': text,
                                        'action': 'devolver_comité'
                                    },
                                    dataType: 'json',
                                }).done(function (data) {
                                    // alert('guardado');
                                    if (!data.hasOwnProperty('error')) {
                                        console.log(data);
                                        if (data.hasOwnProperty('ok')) {
                                            Swal.fire({
                                                position: 'top-end',
                                                icon: 'success',
                                                title: '¡Se devolvió la solicitud a comité!',
                                                showConfirmButton: false
                                            });
                                            setTimeout(function () {
                                                location.href = '/prestamos/aprobados/';
                                            }, 1500);
                                            return false;
                                        }
                                        return false;
                                    }
                                    message_error(data)
                                }).fail(function (data) {
                                    alert("error");
                                }).always(function (data) {
                                    // alert("complete")
                                });

                            } else if (!text) {
                                Swal.fire('Debe ingresar un comentario para continuar')
                            }
                        })()

                    } else if (result.isDenied) {
                        (async () => {
                            const {value: text} = await Swal.fire({
                                input: 'textarea',
                                inputLabel: 'Comentarios',
                                inputPlaceholder: 'Escriba sus comentarios aquí...',
                                inputAttributes: {
                                    'aria-label': 'Type your message here'
                                },
                                showCancelButton: true
                            })

                            if (text) {
                                $.ajax({
                                    url: window.location.pathname,
                                    type: 'POST',
                                    data: {
                                        'comentario': text,
                                        'action': 'anular'
                                    },
                                    dataType: 'json',
                                }).done(function (data) {
                                    // alert('guardado');
                                    if (!data.hasOwnProperty('error')) {
                                        console.log(data);
                                        if (data.hasOwnProperty('ok')) {
                                            Swal.fire(
                                                '¡Confirmado!',
                                                'Se anuló la aprobación.',
                                                'success'
                                            )
                                            return false;
                                        }
                                        return false;
                                    }
                                    message_error(data)
                                }).fail(function (data) {
                                    alert("error");
                                }).always(function (data) {
                                    // alert("complete")
                                });
                            } else if (!text) {
                                Swal.fire('Debe ingresar un comentario para continuar')
                            }
                        })()
                    }
                }
            )
        }
    })
}


function desglose() {
    // Etiquetas en la tabla
    let solicitado = document.getElementById('d_solicitado');
    let manejo_1 = document.getElementById('d_manejo_1');
    let manejo_2 = document.getElementById('d_manejo_2');
    let interes = document.getElementById('d_interes');
    let promotor = document.getElementById('d_promotor');
    let descuento = document.getElementById('d_descuento');
    let notaria = document.getElementById('d_notaria');
    let timbres = document.getElementById('d_timbres');
    let seguros = document.getElementById('d_seguros');
    let otros = document.getElementById('d_otros');
    let obligacion = document.getElementById('d_obligacion');
    let admin = document.getElementById('d_administracion');
    let m_solicitado = document.getElementById('m_solicitado');
    let span_cuota_q = document.getElementById('cuota_q');
    let span_cuota_u = document.getElementById('cuota_q_u');
    let span_cuota_m = document.getElementById('cuota_m');
    let compra_saldo = document.getElementById('compra_saldo_1');
    let span_itbms = document.getElementById('itbms');
    let desembolso = document.getElementById('desembolso');
    // Parámetros del crédito
    let monto = parseFloat(document.getElementById('id_monto').value);
    let plazo = parseFloat(document.getElementById('id_plazo_meses').innerHTML);
    let f_ini = new Date(Date.parse(document.getElementById('id_inicio').innerHTML));
    let p_promotor = parseFloat(document.getElementById('val_promotor').innerHTML);
    let p_descuento = parseFloat(document.getElementById('val_descuento').innerHTML);
    let p_manejo = parseFloat(document.getElementById('p_manejo').value);
    let c_saldo = parseFloat(document.getElementById('c_saldo').value);
    let temp, ajuste, mes_extra, val_interes, val_manejo, val_promotor, val_descuento, val_seguros, val_timbres,
        val_admin;
    let itbms, val_desembolso, val_ultima_q;
    let ajuste_mes = 0;
    // Cálculos
    if (f_ini.getMonth() === 11) {
        if (f_ini.getDate() <= '15') {
            ajuste_mes = 1;
        }
        temp = Date.parse((f_ini.getFullYear() + 1).toString() + "-1" + "-15");
        f_ini = new Date(temp);
    }
    if (f_ini.getDate() > 15) {
        ajuste = 0.5;
    } else {
        ajuste = 0;
    }
    mes_extra = Math.floor(((f_ini.getMonth() + plazo + ajuste) / 12 + plazo + f_ini.getMonth() + ajuste) / 12);
    let total_meses = plazo + mes_extra + ajuste_mes;
    val_manejo = (monto * p_manejo / 10);
    val_interes = (monto * total_meses * 0.02);
    val_promotor = (monto * p_promotor / 100).toFixed(2);
    val_seguros = (monto * (0.035 + 0.025) * total_meses / 12 + total_meses * 1.25);
    let monto_prestamo = (monto + val_manejo + val_interes + val_seguros + 12).toFixed(2);
    val_descuento = (monto_prestamo * p_descuento / 100).toFixed(2);
    val_timbres = ((Math.ceil(monto_prestamo / 100)) / 10).toFixed(2);
    let cuota_q = (monto_prestamo / (plazo * 2) + 0.005).toFixed(2);
    val_ultima_q = monto_prestamo - cuota_q * (plazo * 2 - 1);
    let cuota_m = cuota_q * 2;
    val_admin = val_manejo - val_promotor - val_descuento - val_timbres;
    itbms = val_admin * 0.07;
    val_desembolso = monto - c_saldo - itbms;
    solicitado.innerHTML = '$ ' + monto.toFixed(2).toString();
    manejo_1.innerHTML = '$ ' + val_manejo.toFixed(2).toString();
    manejo_2.innerHTML = manejo_1.innerHTML;
    interes.innerHTML = '$ ' + val_interes.toFixed(2).toString();
    promotor.innerHTML = '$ ' + val_promotor.toString();
    descuento.innerHTML = '$ ' + val_descuento.toString();
    notaria.innerHTML = '$ ' + ((12).toFixed(2)).toString();
    timbres.innerHTML = '$ ' + val_timbres.toString();
    seguros.innerHTML = '$ ' + val_seguros.toFixed(2).toString();
    otros.innerHTML = '$ ' + ((0).toFixed(2)).toString();
    obligacion.innerHTML = '$ ' + monto_prestamo;
    admin.innerHTML = '$ ' + val_admin.toFixed(2).toString();
    // Segunda tabla de desglose
    m_solicitado.innerHTML = solicitado.innerHTML;
    span_cuota_q.innerHTML = '$ ' + cuota_q.toString();
    span_cuota_u.innerHTML = '$ ' + val_ultima_q.toFixed(2).toString(); //
    span_cuota_m.innerHTML = '$ ' + cuota_m.toString();
    compra_saldo.innerHTML = '$ ' + c_saldo.toFixed(2).toString(); //
    span_itbms.innerHTML = '$ ' + itbms.toFixed(2).toString();
    desembolso.innerHTML = '$ ' + val_desembolso.toFixed(2).toString();
}


function anular() {
    Swal.fire({
        title: '¿Desea anular la aprobación?',
        text: "Una vez anulada sólo cumplimiento/gerencia podrán cambiar el estatus.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, anular!',
        cancelButtonText: 'Cancelar',
    }).then((result) => {
        if (result.isConfirmed) {
            (async () => {
                const {value: text} = await Swal.fire({
                    input: 'textarea',
                    inputLabel: 'Comentarios',
                    inputPlaceholder: 'Escriba sus comentarios aquí...',
                    inputAttributes: {
                        'aria-label': 'Type your message here'
                    },
                    showCancelButton: true
                })
                if (text) {
                    $.ajax({
                        url: window.location.pathname,
                        type: 'POST',
                        data: {
                            'comentario': text,
                            'action': 'anular'
                        },
                        dataType: 'json',
                    }).done(function (data) {
                        // alert('guardado');
                        if (!data.hasOwnProperty('error')) {
                            console.log(data);
                            if (data.hasOwnProperty('ok')) {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: '¡Se anuló la aprobación!',
                                    showConfirmButton: false
                                });
                                setTimeout(function () {
                                    location.href = '/prestamos/aprobados/';
                                }, 1500);
                                return false;
                            }
                            return false;
                        }
                        message_error(data)
                    }).fail(function (data) {
                        alert("error");
                    }).always(function (data) {
                        // alert("complete")
                    });
                } else if (!text) {
                    Swal.fire('Debe ingresar un comentario para continuar')
                }
            })()

        }
    })
}


function cargar_od() {
    let flag = '0';
    let flag_img = false;
    if (document.getElementById('od_f1').value) {
        flag = '1';
    }
    if (document.getElementsByName('image_1')[0]) {
        flag = '1';
        flag_img = true;
    }
    if (flag === '1') {
        let fecha_od = document.getElementById('fecha_od');
        if (fecha_od.value === ''){
            Swal.fire('Ingrese la fecha acordada de O.D.');
            return false;
        }else{
            let valid_date = Date.parse(fecha_od.value);
            if (isNaN(valid_date)){
                Swal.fire('Ingrese una fecha válida.');
                return false;
            }
        }
        Swal.fire({
            title: '¿Desea continuar?',
            text: "Al continuar se cargará el archivo y la solicitud dejará de ser accesible a ventas.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#fec107',
            cancelButtonColor: '#d33',
            confirmButtonText: '!Sí, continuar!',
            cancelButtonText: '!Cancelar!'
        }).then((result) => {
            if (result.isConfirmed) {

                (async () => {

                    const {value: text} = await Swal.fire({
                        input: 'textarea',
                        inputLabel: 'Comentarios',
                        inputPlaceholder: 'Escriba sus comentarios aquí...',
                        inputAttributes: {
                            'aria-label': 'Type your message here'
                        },
                        showCancelButton: true
                    })

                    if (text) {
                        // document.getElementById('action_form').value = 'cargar_od';
                        let parameters = new FormData(document.getElementById("doc_od_f1"));
                        parameters.append("action", "cargar_od");
                        parameters.append("comentario", text);
                        if (flag_img) {
                            parameters.delete('f_1');
                            parameters.delete('orden_descuento');
                        }
                        let id_modelo = document.getElementById('id_modelo').value;
                        $.ajax({
                            url: window.location.pathname,
                            type: 'POST',
                            data: parameters,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                        }).done(function (data) {
                            // alert('guardado');
                            if (!data.hasOwnProperty('error')) {
                                console.log(data);
                                if (data.hasOwnProperty('ok')) {
                                    console.log(data);
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Se guardó la O.D./F1 y se cambió el estatus!',
                                        showConfirmButton: false
                                    });
                                    setTimeout(function () {
                                        location.href = '/prestamos/aprobados/';
                                    }, 1500);
                                    return false;
                                }
                                return false;
                            }
                            message_error(data)
                        }).fail(function (data) {
                            alert("error");
                        }).always(function (data) {
                            // alert("complete")
                        });
                    }
                    if (!text) {
                        Swal.fire('Debe ingresar un comentario para continuar.')
                    }

                })()
            }
        })
    } else {
        location.href = '/prestamos/aprobados/';
    }
}


function cargar_docs() {
    let flag = '0';
    let flag_2 = false;
    if (document.getElementById('od_f1').value) {
        flag = '1';
    }
    if (document.getElementsByName('image_1')[0]) {
        flag = '1';
    }
    if (document.getElementById('id_contrato').value) {
        flag = '1';
    }
    if (document.getElementById('id_pagare').value) {
        flag = '1';
    }
    if (document.getElementById('fecha_od').value) {
        flag = '1';
        let flag_2 = true;
    }

    if (flag === '1') {
        Swal.fire({
            title: '¿Desea guardar los cambios?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Sí`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                (async () => {
                    const {value: text} = await Swal.fire({
                        input: 'textarea',
                        inputLabel: 'Comentario',
                        inputPlaceholder: 'Escriba su comentario aquí...',
                        inputAttributes: {
                            'aria-label': 'Type your message here'
                        },
                        showCancelButton: true
                    })
                    if (text) {
                        let parameters = new FormData(document.getElementById("doc_od_f1"));
                        parameters.append("action", "cargar_docs");
                        parameters.append("comentario", text);
                        console.log(parameters);
                        $.ajax({
                            url: window.location.pathname,
                            type: 'POST',
                            data: parameters,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                        }).done(function (data) {
                            // alert('guardado');
                            if (!data.hasOwnProperty('error')) {
                                // console.log(data);
                                if (data.hasOwnProperty('ok')) {
                                    // console.log(data);
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: '¡Se actualizó la solicitud!',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    setTimeout(function () {
                                        // location.href = '/prestamos/aprobados/';
                                    }, 1500);
                                    return false;
                                } else if (data.hasOwnProperty('advertencia')) {
                                    Swal.fire(data['advertencia']);
                                }
                                return false;
                            }
                            message_error(data)
                        }).fail(function (data) {
                            alert("error");
                        }).always(function (data) {
                            // alert("complete")
                        });
                    } else if (!text) {
                        Swal.fire('Debe ingresar un comentario para continuar')
                    }
                })()
            }
        })
    } else {
        location.href = '/prestamos/aprobados/';
    }
}