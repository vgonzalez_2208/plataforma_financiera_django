var csrftoken = getCookie('csrftoken');

let tabla_clientes = $('#info_transacciones').DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    pageLength: 5,
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'CargarTabla_1'
        },
        headers: {'X-CSRFToken': csrftoken},
        dataSrc: ""
    },
    columns: [
        {"data": "contrato"},
        {"data": "nombre"},
        {"data": "apellido"},
        {"data": "id"},
        {"data": "tipo_ingreso"},
        {"data": "monto"},
        {"data": "plazo"},
        {"data": "celular"},
        {"data": "id"},
    ],
    columnDefs: [
        {
            targets: [-1],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                let info = data.split('/');
                let buttons;
                let tipo = info[1];
                let id_1 = info[0];
                let id_2 = info[2] + '/' + tipo;
                console.log(tipo, id_1, id_2);
                buttons = '<a href="/cobros/ver_cliente/' + id_2 + '" class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></a>';
                buttons += ' ' + '<a href="/cobros/editar_cliente/' + id_2 + '/" class="btn btn-secondary btn-xs"><i class="far fa-edit"></i></a>';
                buttons += ' ' + '<a href="/cobros/lista_clientes/' + id_1 + '/" class="btn btn-warning btn-xs"><i class="fas fa-hand-holding-usd"></i></a>';
                return buttons;
            },
        },
        {
            targets: [-2],
            class: 'text-center',
            render: function (data, type, row) {
                return '<a href="https://wa.me/507' + data + '" target="_blank"><i class=" fab fa-whatsapp"></i> ' + data + '</a>';
            },
        },
        {
            targets: [-6],
            class: 'text-center',
            render: function (data, type, row) {
                let tipo = data.split('/')[1];
                let salida;
                if (tipo === 'ant'){
                    salida = '<span class="badge badge-pill badge-secondary">Pre-p.</span>';
                }else {
                    salida = '<span class="badge badge-pill badge-info">Post-p.</span>';
                }
                return salida;
            },
        },

    ],
    initComplete: function (settings, json) {
    }
});