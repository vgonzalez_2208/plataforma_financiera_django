let fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('edit_antiguo'),
        {
            fields: {
                nombre_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nombre_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                apellido_2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ'"Üü ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                tipo_doc: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9-]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                nacionalidad: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 5 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                f_nacimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                celular: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Ingrese un número válido.'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Ingrese números solamente.'
                        }
                    }
                },
                email_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 5,
                            message: 'Mínimo 5 letras.'
                        },
                        regexp: {
                            regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/i,
                            message: 'Formato inválido.'
                        }
                    }
                },
                genero: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                e_civil: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                dependientes: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                tipo_ingreso: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                salario: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300.00,
                            max: 9000.00,
                            message: 'Ingrese un valor válido'
                        }
                    }
                },
                in_extras: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                estudios: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                empresa_1: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ'"Üü ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                empresa_2: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                    }
                },
                p_expuesto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                promotor: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'Mínimo 3 letras, máximo 30.'
                        },
                        regexp: {
                            regexp: /^[a-zA-ZáéíóúñÑ ]+$/,
                            message: 'Escriba letras solamente.'
                        }
                    }
                },
                motivo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                provincia: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                distrito: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                corregimiento: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'Mínimo 3 letras, máximo 50.'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9áéíóúñÑ ]+$/,
                            message: 'Ingrese un nombre válido.'
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 30,
                            message: 'Ingrese un plazo entre 6 y 30 meses'
                        }
                    }
                },
                fecha_ini: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                ob_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 500,
                            max: 10000,
                            message: 'Ingrese monto válido',
                        }
                    }
                },
                f_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ini_OD: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_nacimiento: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                c_cierre: {
                    validators: {}
                },
                compra_saldo: {
                    validators: {}
                },
                c_promotor: {
                    validators: {}
                },
                s_descuento: {
                    validators: {}
                },
                num_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^(([0-9]{4})+)-(([0-9]{2})+)-([0-9]{4})$/i,
                            message: 'Formato incorrecto.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'nombre_1':
                            case 'nombre_2':
                            case 'apellido_1':
                            case 'apellido_2':
                            case 'tipo_doc':
                            case 'dni':
                            case 'f_nacimiento':
                            case 'celular':
                            case 'tipo_ingreso':
                            case 'estudios':
                            case 'corregimiento':
                            case 'apc':
                            case 'terms':
                            case 'check_1':
                            case 'check_2':
                            case 'nacionalidad':
                            case 'empresa_2':
                            case 'promotor':
                                return '.col-md-3';

                            case 'motivo':
                            case 'empresa_1':
                                return '.col-md-4';

                            case 'e_civil':
                            case 'dependientes':
                            case 'salario':
                            case 'in_extras':
                            case 'monto':
                            case 'provincia':
                            case 'distrito':
                            case 'p_expuesto':
                            case 'plazo':
                            case 'genero':
                            case 'fecha_ini':
                            case 'ob_contrato':
                            case 'f_contrato':
                            case 'fecha_ini_OD':
                            case 'c_cierre':
                            case 'compra_saldo':
                            case 'c_promotor':
                            case 's_descuento':
                            case 'num_contrato':
                                return '.col-md-2';

                            case 'email_1':
                            case 'direccion':
                                return '.col-md-5';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        // var parameters = $('#formulario').serializeArray();
        let pre_parameters = new FormData(document.getElementById("edit_antiguo"));
        console.log(document.getElementById('compra_saldo').value);
        let empty = [];
        pre_parameters.forEach(function (value, key) {
            if (value.name === "" || value === "") {
                empty.push(key);
            }
        })
        console.log(empty);
        empty.forEach(value => pre_parameters.delete(value));
        //let id_form = document.getElementById('id_form').value;
        let parameters = process_formdata(pre_parameters);
        /*parameters.forEach(function (value, key) {
            console.log(key + ":" + value);
        });*/
        // console.log(parameters);
        parameters.forEach(function (value, key) {
            console.log(key, value);
        })
        let url = window.location.pathname;
        fetch(url, {
            method: 'POST', // or 'PUT'
            body: parameters, // data can be `string` or {object}!
        }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(function (response) {
                if (!response.hasOwnProperty('error')) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se guardó la info del cliente.',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    setTimeout(function () {
                        location.href = '/cobros/lista_clientes/';
                    }, 1500);
                    return false;
                }
                console.log(response);
            });
    });
    $('[id="id_f_nacimiento"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '-22y'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_nacimiento');
        });
    $('[id="id_fecha_ini"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '+90d'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini');
        });
    $('[id="id_f_contrato"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '+90d'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_contrato');
        });
    $('[id="id_fecha_ini_OD"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            endDate: '+90d'
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini_OD');
        });
});

function process_formdata(formulario) {
    if (formulario.has('image_1')) {
        formulario.delete('DNI_img');
    }
    if (formulario.has('image_2')) {
        formulario.delete('carta_trabajo_img');
    }
    if (formulario.has('image_3')) {
        formulario.delete('talonario_1_img');
    }
    if (formulario.has('image_4')) {
        formulario.delete('talonario_2_img');
    }
    if (formulario.has('image_5')) {
        formulario.delete('ficha_img');
    }
    if (formulario.has('image_6')) {
        formulario.delete('recibo_img');
    }
    if (formulario.has('image_7')) {
        formulario.delete('carta_saldo_img');
    }
    if (formulario.has('image_9')) {
        if (formulario.has('orden_descuento')) {
            formulario.delete('orden_descuento');
        } else if (formulario.has('f_1')) {
            formulario.delete('f_1');
        }
    }
    if (formulario.has('image_11')) {
        formulario.delete('pagare');
    }
    if (formulario.has('image_12')) {
        formulario.delete('conoce_cliente');
    }
    return formulario;
}

function cargar_c_saldo(info) {
    let saldo_span = document.getElementById('cancelaciones_span');
    if (info !== '') {
        let arreglo = info.split(',');
        let salida = 0;
        arreglo.forEach(val => salida += parseFloat(val.split(':')[1]));
        saldo_span.innerHTML = f_dollar.format(salida);
    } else {
        saldo_span.innerHTML = '$ 0.00';
    }
}




