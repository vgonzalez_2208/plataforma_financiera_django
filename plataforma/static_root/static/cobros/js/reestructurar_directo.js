let fv;

document.addEventListener('DOMContentLoaded', function (e) {
    /*const form = document.getElementById('formulario');
    const submitButton = form.querySelector('[type="submit"]');*/
    fv = FormValidation.formValidation(
        document.getElementById('form_reestructurar'),
        {
            fields: {
                int_global: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                s_colectivo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                s_desempleo: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                s_dental: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                c_cierre: {
                    validators: {}
                },
                monto_r: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 3500,
                            message: 'Ingrese un monto entre 300 y 3500'
                        }
                    }
                },
                plazo_r: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 6,
                            max: 60,
                            message: 'Ingrese un plazo entre 6 y 60 meses'
                        }
                    }
                },
                cargos_extra: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 0,
                            max: 3500,
                            message: 'Ingrese un monto entre 0 y 3500'
                        }
                    }
                },
                ob_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        between: {
                            min: 300,
                            max: 10000,
                            message: 'Ingrese un monto entre 300 y 10,000'
                        }
                    }
                },
                fecha_ini: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ini: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                f_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        f_contrato: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                fecha_ini_OD: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        fecha_ini_OD: {
                            format: 'YYYY/MM/DD',
                            message: 'La fecha no es válida.',
                        }
                    }
                },
                diciembres: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        }
                    }
                },
                notas: {
                    validators: {}
                },
                num_contrato: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido.'
                        },
                        regexp: {
                            regexp: /^(([0-9]{4})+)-(([0-9]{2})+)-([0-9]{4})$/i,
                            message: 'Formato incorrecto.'
                        }
                    }
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger(),
                bootstrap: new FormValidation.plugins.Bootstrap({
                    rowSelector: function (field, ele) {
                        // field is the field name
                        // ele is the field element
                        switch (field) {
                            case 'int_global':
                            case 's_colectivo':
                            case 's_desempleo':
                            case 's_dental':
                            case 'c_cierre':
                            case 'monto_r':
                            case 'plazo_r':
                            case 'cargos_extra':
                            case 'ob_contrato':
                            case 'fecha_ini':
                            case 'f_contrato':
                            case 'fecha_ini_OD':
                            case 'diciembres':
                            case 'num_contrato':
                                return '.col-md-3';

                            case 'notas':
                                return '.col-md-12';

                            default:
                                return '.form-group';
                        }
                    }
                }),
                submitButton: new FormValidation.plugins.SubmitButton(),
                /*defaultSubmit: new FormValidation.plugins.DefaultSubmit(),*/
                icon: new FormValidation.plugins.Icon({
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                }),
            },
        }
    ).on('core.form.valid', function (event) {
        Swal.fire({
            title: '¿Desea aplicar la reestructuración?',
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: 'Aplicar',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // var parameters = $('#formulario').serializeArray();
                let parameters = new FormData(document.getElementById("form_reestructurar"));
                parameters.append('action', 'aplicar_rtt');
                let url = window.location.pathname;
                fetch(url, {
                    method: 'POST', // or 'PUT'
                    body: parameters, // data can be `string` or {object}!
                }).then(res => res.json())
                    .catch(error => console.error('Error:', error))
                    .then(function (response) {
                        if (response.hasOwnProperty('ok')) {
                            if (response['ok'] === '') {
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Se agregó la info del cliente.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                $('#add_cliente_modal').modal('hide');
                                document.getElementById("cliente_antiguo").reset();
                                tabla_clientes.ajax.reload();
                            } else {
                                Swal.fire(response['ok']);
                            }
                        } else if (response.hasOwnProperty('advertencia')) {
                            Swal.fire({
                                title: 'El crédito está mal liquidado.',
                                text: "La obligación exacta del crédito debe ser: $" + response['advertencia'] + ". Desea amortizar en base a la obligación del contrato?",
                                icon: 'warning',
                                showCancelButton: true,
                                showDenyButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '¡Sí, continuar!',
                                denyButtonText: 'No'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    let pre_parameters_2 = new FormData(document.getElementById("cliente_antiguo"));
                                    let parameters_2 = new FormData;
                                    pre_parameters_2.forEach(function (value, key) {
                                        if (!(value === "" || value.name === "")) {
                                            parameters_2.append(key, value);
                                        }
                                    })
                                    parameters_2.append('action', 'force_add');
                                    let url = window.location.pathname;
                                    fetch(url, {
                                        method: 'POST', // or 'PUT'
                                        body: parameters_2, // data can be `string` or {object}!
                                    }).then(res => res.json())
                                        .catch(error => console.error('Error:', error))
                                        .then(function (response) {
                                            if (!response.hasOwnProperty('error')) {
                                                Swal.fire({
                                                    position: 'top-end',
                                                    icon: 'warning',
                                                    title: 'Se agregó la info del cliente.',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                $('#add_cliente_modal').modal('hide');
                                                document.getElementById("cliente_antiguo").reset();
                                                tabla_clientes.ajax.reload();
                                            }
                                            console.log(response);
                                        });
                                } else if (result.isDenied) {
                                    Swal.fire(
                                        '¡Cancelado!',
                                        'Revise los parámetros del crédito/contrato y vuelva a intentarlo.',
                                        'warning'
                                    )
                                }
                            })
                        } else if (response.hasOwnProperty('error')) {
                            alert(response['error']);
                        }
                        console.log(response);
                    });
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });
    $('[id="id_fecha_ini"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini');
        });
    $('[id="id_f_contrato"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('f_contrato');
        });
    $('[id="id_fecha_ini_OD"]')
        .datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        })
        .on('changeDate', function (e) {
            fv.revalidateField('fecha_ini_OD');
        });
});
