function guardar_notas() {
    let flag_notas = document.getElementById('flag_notas').value;
    if (flag_notas === '1') {
        let parameters = new FormData(document.getElementById('token_form'));
        let notas = document.getElementById('notas_textarea').value;
        parameters.append('notas_user', notas);
        parameters.append('action', 'notas');
        let url = window.location.pathname;
        fetch(url, {
            method: 'POST', // or 'PUT'
            body: parameters, // data can be `string` or {object}!
        }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(function (response) {
                if (!response.hasOwnProperty('error')) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Se guardaron las notas.',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    document.getElementById('flag_notas').value = '0';
                    //$('#add_cliente_modal').modal('hide');
                    //document.getElementById("cliente_antiguo").reset();
                    //tabla_clientes.ajax.reload();
                }
                console.log(response);
            });
    } else {
        Swal.fire({
            position: 'top-end',
            icon: 'info',
            title: 'No hay cambios.',
            showConfirmButton: false,
            timer: 1500
        })
    }
}