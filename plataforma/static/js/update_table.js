var csrftoken = getCookie('csrftoken');

$(function () {
    $('#tabla_1').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 10,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'CargarTabla_1'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "visto"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "f_solicitud"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/nuevas/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<a href="/ventas/solicitudes/edit/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fas fa-edit"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Sí') {
                        valor = '<span class="badge badge-pill badge-success">Si</span>';
                    } else if (data === 'Sólo cédula') {
                        valor = '<span class="badge badge-pill badge-primary">' + data + '</span>';
                    } else if (data === 'Sólo ficha') {
                        valor = '<span class="badge badge-pill badge-primary">' + data + '</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">No</span>';
                    }
                    return valor;
                },
            },
            {
                targets: [-9],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'no_visto') {
                        valor = '<span class="badge badge-pill badge-success">Nueva</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-warning">Visto</span>';
                    }
                    return valor;
                },
            },
            {
                targets: [-6],
                class: 'text-center',
                render: function (data, type, row) {
                    let hoy_temp = new Date();
                    let mes_act;
                    if (hoy_temp.getMonth() < 9){
                        mes_act = hoy_temp.getFullYear().toString() + '-0' + (hoy_temp.getMonth() + 1).toString();
                    } else {
                        mes_act = hoy_temp.getFullYear().toString() + '-' + (hoy_temp.getMonth() + 1).toString();
                    }
                    if (data === mes_act){
                        return '<span class="badge badge-info">' + data + '</span>';
                    }else {
                        return '<span class="badge badge-secondary">' + data + '</span>';
                    }
                },
            },

        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {
            this.api().columns([4, 5]).every(function () {
                var column = this;
                // console.log(column['0'][0]);
                let placeholder;
                if (column['0'][0] === 4) {
                    placeholder = 'Sel...';
                } else if (column['0'][0] === 5) {
                    placeholder = 'Seleccione...';
                }
                var select = $('<select class="form-control"><option class="text-muted" value="">' + placeholder + '</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
});

$(function () {
    $('#tabla_1222').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'CargarTabla_1'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        buttons: [
            'print'
        ],
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "documentos"},
            {"data": "desc"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/nuevas/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    buttons += '<a href="/ventas/solicitudes/edit/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fas fa-edit"></i></a>';
                    buttons += '<a href="/ventas/solicitudes/delete/' + row.id + '/" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Sí') {
                        valor = '<span class="badge badge-pill badge-success">Si</span>';
                    } else if (data === 'Sólo cédula') {
                        valor = '<span class="badge badge-pill badge-success">' + data + '</span>';
                    } else if (data === 'Sólo ficha') {
                        valor = '<span class="badge badge-pill badge-success">' + data + '</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">No</span>';
                    }
                    return valor;
                },
            },

        ],
        order: [1, 'asc'],
        initComplete: function (settings, json) {

        }
    });
}); // Backup //


$(function () {
    $('#tabla_2').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'CargarTabla_2'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/nuevas/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<a href="/ventas/solicitudes/edit/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fas fa-edit"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Sí') {
                        valor = '<span class="badge badge-pill badge-success">Si</span>';
                    } else if (data === 'Sólo cédula') {
                        valor = '<span class="badge badge-pill badge-primary">' + data + '</span>';
                    } else if (data === 'Sólo ficha') {
                        valor = '<span class="badge badge-pill badge-primary">' + data + '</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">No</span>';
                    }
                    return valor;
                },
            },

        ],
        initComplete: function (settings, json) {

        }
    });
});

$(function () {
    $('#tabla_3').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'CargarTabla_3'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "desc"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/ventas/solicitudes/edit/' + row.id + '/" class="btn btn-warning btn-xs btnSend"><i class="far fa-eye"></i></a>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});

$(function () {
    $('#empresas').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 10,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'CargarEmpresas'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "ruc"},
            {"data": "negocio"},
            {"data": "tipo"},
            {"data": "telefono"},
            {"data": "estatus"},
            {"data": "celular"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/ventas/empresas/edit/' + row.id + '/" class="btn btn-warning btn-xs btnSend"><i class="fas fa-edit"></i></a>';
                    return buttons;
                }
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let badge;
                    if (data === 'False') {
                        badge = '<span class="badge badge-pill badge-warning"><i class="fas fa-exclamation-triangle"></i> Pendiente</span>';
                    } else if (data === 'True') {
                        badge = '<span class="badge badge-pill badge-success"><i class="fas fa-check-circle"></i> Verificada</span>';
                    }
                    return badge;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});

$(function () {
    $('#proceso').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'proceso'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "usuario"},
            {"data": "documentos"},
            {"data": "genero"},
            // {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/proceso/ver/' + row.id + '/" class="btn btn-info btn-xs "><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<a href="/ventas/solicitudes/proceso/edit/' + row.id + '/" class="btn btn-warning btn-xs"><i class="fas fa-edit"></i></a>';
                    return buttons;
                },
            },
            {
                targets: [-2],
                class: 'text-center',
                render: function (data, type, row) {
                    let valor;
                    if (data === 'Completo') {
                        valor = '<span class="badge badge-pill badge-success">Completo</span>';
                    } else if (data === 'Incompleto') {
                        valor = '<span class="badge badge-pill badge-warning">Incompleto</span>';
                    } else {
                        valor = '<span class="badge badge-pill badge-danger">Ninguno</span>';
                    }
                    return valor;
                },
            },

        ],
        initComplete: function (settings, json) {

        }
    });
});

$(function () {
    $('#descartadas_1').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'descartadas_f'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "documentos"},
            {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/descartadas/ver/' + row.id + '/" class="btn btn-info btn-xs btnSend"><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#exampleModalCenter" onclick="info_cliente(' + row.id + ', 1)"><i class="fas fa-info-circle"></i></button>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});

$(function () {
    $('#descartadas_2').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        pageLength: 5,
        lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'Todos']],
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'descartadas_p'
            },
            headers: {'X-CSRFToken': csrftoken},
            dataSrc: ""
        },
        columns: [
            {"data": "id"},
            {"data": "nombre_1"},
            {"data": "apellido_1"},
            {"data": "tipo_ingreso"},
            {"data": "salario"},
            {"data": "celular"},
            {"data": "documentos"},
            {"data": "genero"},
        ],
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/ventas/solicitudes/descartadas/procesadas_ver/' + row.id + '/" class="btn btn-info btn-xs btnSend"><i class="fas fa-eye"></i></a>';
                    buttons += ' ' + '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#exampleModalCenter" onclick="info_cliente(' + row.id + ', 2)"><i class="fas fa-info-circle"></i></button>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
