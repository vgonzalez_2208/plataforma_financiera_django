"""plataforma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static
from apps.login.views import Login
from apps.ventas.views.dashboard.views import Dashboard
from plataforma.settings import DEBUG

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', include('apps.login.urls')),
    path('inicio/', Dashboard.as_view(),  name='dashboard'),
    path('cobros/', include('apps.cobros.urls')),
    path('formulario/', include('apps.formulario.urls')),
    path('prestamos/', include('apps.prestamos.urls')),
    path('ventas/', include('apps.ventas.urls')),
    path('reportes/', include('apps.reportes.urls')),
]

if DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
