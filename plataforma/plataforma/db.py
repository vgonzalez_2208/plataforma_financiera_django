import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SQLITE = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db/sqlite/db.sqlite3'),
    }
}

POSTGRESQL = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_plataforma',
        'USER': 'vicente_rc',
        'PASSWORD': 'rapicash_2208',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}

POSTGRESQL_2 = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'rapicash_linux_2',
        'USER': 'vicente_rc',
        'PASSWORD': 'rapicash_2208',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}