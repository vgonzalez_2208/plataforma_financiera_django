## Plataforma de control Financiero 

El presente repositorio corresponde al código fuente de un sistema completo de manejo de préstamos que envuelve la venta de productos a clientes, procesos críticos de aprobaciones de préstamos nuevos, desembolsos y manejo completo de cartera.

La información aquí expuesta es meramente ilustrativa y se mantendrá disponible por tiempo limitado para procesos demostrativos. 

## Módulos 

El desarrollo contiene 6 módulos bien definidos y creados con diferente propósito. Estos son:

 **1- Formulario:** Incluye formato dinámico a diligenciar por nuevos prospectos para solicitudes de préstamos y su posterior almacenamiento en base de datos PosgreSQL.

 **2- Ventas:** Incluye interface para el manejo dinámico de solicitudes, generación automática de cotizaciones y liquidaciones detalladas, expediente en línea del cliente con documentación adjunta, proceso de pre-aprobacion y base de datos de empresas con colaboradores clientes de la financiera.

 **3- Préstamos:** Incluye herramientas de manejo de solicitudes pre-aprobadas para rápida visualización del comité de aprobaciones, proceso de aprobación y edición de cotizaciones, generación de órdenes de descuento, generación de documentación legal y manejo administrativo previo y posterior al desembolso de créditos.

 **4- Login:** Módulo que da acceso dinámico a usuarios bajo permisos establecidos en función del rol asignado.

 **5- Cobros:** Incluye herramientas para el manejo dinámico de clientes con préstamos activos e información global de estado de cartera (30-60-90 días de atraso) para uso del departamento de cobros. Proporciona información completa de información tanto personal como laboral de clientes para el seguimiento de las transacciones bancarias de pagos de cuotas mensuales e información de morosidad generada. 

 **6- Reportes:** Incluye generación automática de reportes PDF de estado de cartera, histórico de clientes, detalles de cancelaciones anticipadas, entre otros. Genera además resúmenes en tiempo real de los departamento de ventas y cobros para visualización rápida de gerentes y accionistas de la empresa en pantalla de inicio.

Para ampliación del funcionamiento de los diferentes módulos escribir al correo: vicente.gonzalez.2208@gmail.com.
